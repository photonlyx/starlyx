.POSIX: # Utiliser les fonctionnalités standards normées dans POSIX
.SUFFIXES: # Supprime toutes les règles d'inférences par défault

include config.mk

SRC = sx_main.c\
	sx_monte_carlo.c\
	sx_image.c\
	sx_camera.c\
	parsing.c\
	sampled_data.c\
	sx_source.c\
	sx_volume.c\
	sx_path_source.c\
	sx_sensor.c\
	sx_photon.c\
	sx_scene.c\
	sx_mat_surf.c\
	sx_xml.c\
	sx_surface.c\
	sx_cie.c\
	sx_variation.c\
	sx_mat_vol.c\
	sx_phase_function.c\
	sx_geometry.c\
	cplx.c\
	bhmie.c\
	lists.c\
	float_list_symbolic.c\
	sx_symbolic.c\
	sx_cad.c\
	sx_ssp.c\
	sx_polydispersity.c\

OBJ = $(SRC:.c=.o)

#compilation par défaut:
all: starlyx

starlyx: $(OBJ)
	@# Executer l'édition de lien avec le compilateur
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LDFLAGS)

# Activer les règles d'inférences pour les fichier .c et .o
.SUFFIXES: .c .o

# Règle d'inférence pour compiler un fichier .o ($@) à partir d'un fihcier .c ($<)
.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJ) starlyx
