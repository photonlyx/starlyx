# Starlyx

Monte-Carlo software for solving the Radiative Transfer Equation.  Can be used to render synthetic images and to calculate optical sensors signals in a scene.

Web site: www.starlyx.org


Starlyx is written in C and based on the powerful library **star-engine** (meso-star company, meso-star.com) itself based on **embree** (intel, embree.org). The calculation is CPU parallelized by openMP. Embree allows very fast calculation even for very complex geometries. Star-engine is used in many projects of a scientific community dedicated to advanced Monte-Carlo techniques (www.meso-star.com/projects/misc/about-en.html).

The scene and all the parameters are defined in a unique XML file. The output are images and result tables. 2 approaches:

* backward ray-tracing: the paths start at the sensors.
* direct ray-tracing: the paths start at the sources.

The scene is made with volumes delimited by surfaces (obj files), sources and sensors. The volumes have radiative transfer properties (scattering coefficient KS, absorption coefficient KA, refractive index N). The surfaces have simple surface properties (lambert, dielectric)

The physical models applied and validations will be published end-2023 2024.

# Dependencies:

star-engine (https://gitlab.com/meso-star/star-engine) 

mlxml (https://github.com/michaelrsweet/mxml)

# Installation and usage:

• Install git, cmake and build-essential (if necessary):

		sudo apt update
		sudo apt install git
		sudo apt install cmake
		sudo apt install build-essential

• Install gsl 

		sudo apt install libgsl-dev

• Step by step installation:

Note: MYPATH is the path where you install star-engine and starlyx. Set this path in your .bashrc or .profile. For example:
		export STARLYX_PATH=$HOME/my_starlyx_path

• Install star-engine (https://gitlab.com/meso-star/star-engine):

		We get the sources, compile and install star-engine locally:
		cd $STARLYX_PATH 
		git clone https://gitlab.com/meso-star/star-engine.git 
		mkdir star-engine/build 
		cd star-engine/build 
		cmake ../cmake 
		make

• Install mxml2.12 (https://www.msweet.org/mxml/)

		Here we compile the source of the version 2.12 and do a standard installation in /usr/local/lib :
		Download mxml-2.12.tar.gz:
			wget https://github.com/michaelrsweet/mxml/releases/download/v2.12/mxml-2.12.tar.gz
		Uncompress: 
			tar xvf mxml-2.12.tar.gz
		cd mxml-2.12
		./configure
		make
		sudo make install
		

• Install starlyx:

		We get the sources, compile and install star-engine locally:
		cd $STARLYX_PATH
		git clone https://gitlab.com/photonlyx/starlyx.git 
		cd starlyx
		make clean
		make

• If you modified your .profile don't forget to type (or just restart your session):

		export STAR_ENGINE_PATH=$STARLYX_PATH/star-engine/local
		export LD_LIBRARY_PATH="/usr/local/lib:$STARLYX_PATH/mxml-2.12:$STARLYX_PATH/star-engine/local/lib:$LD_LIBRARY_PATH"
		export PATH="$PATH:$STARLYX_PATH/starlyx"

		Add these 3 last lines to your .bashrc or .profile for permanent config.
		If you modified your .profile then type:
		source /home/USERNAME/.profile



# Usage:

• Open a new terminal.

• Run the minimal cube example:

		cd STARLYX_PATH/examples/minimal_cube
		starlyx scene.xml
		
• Check the image: 
		cam0.png






