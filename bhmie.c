/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "cplx.h"
#include "bhmie.h"

#include <math.h>
#define PI 3.14159265358979323846

/**
 * Adaptation of the FORTRAN code (see https://github.com/hyperion-rt/bhmie)
 * itself derived from the Bohren-Huffman Mie scattering subroutine to calculate
 * scattering and absorption by a homogenous isotropic sphere.
 * output:
 *    gsca assymetry factor
 *    qsca scattering cross section
 *    qext extinction cross section
 *    qback backscattering cross section
 *    cxs1  perpendicular electric field ( size 2*nang). Used indices : from 1 to (2*nang-1) included
 *    cxs2  parallel electric field. Used indices : from 1 to (2*nang-1) included
 * input:
 *    nang  number of angles between 0 and 90 degrees(will calculate 2*NANG-1 directions from 0 to 180 deg.)
 													(if called with NANG<2, will set NANG=2 and will compute
 													scattering for theta=0,90,180.)
 *    x     reduced particle size: x=2*PI*diameter/2/lambda*fluidIndex
 *    cxref complex refraction index of the particle material
 *
 */
void bhmie(
		double* gsca,
		double* qsca,
		double* qext,
		double* qback,
		struct cplx*** cxs1_,
		struct cplx*** cxs2_,
		const unsigned int nang,
		const double x,
		const struct cplx* cxref
		)
{
	struct cplx* CXONE= cplx_create(1.0, 0.0);

	/* .. Array Arguments .. */
	/* .. Local Scalars ..*/
	struct cplx* cxan=cplx_create(0,0);
	struct cplx* cxbn1=cplx_create(0,0);
	//struct cplx* cxxi0;
	struct cplx* cxtemp=cplx_create(0,0);
	struct cplx* cxxi1=cplx_create(0,0);
	struct cplx* cxxi=cplx_create(0,0);
	struct cplx* cxan1=cplx_create(0,0);
	struct cplx* cxbn=cplx_create(0,0);

	double apsi,  apsi1, chi, chi0, chi1, dang, fn, p,
		   pii,rn, t, theta, xstop, ymod;

	double  dn, dx, psi, psi0, psi1;
	unsigned int j,nn,nmx,n,nstop,jj;
 
	/* .. Local Arrays ..*/
	double* amu=calloc(1,sizeof(double)*(nang+1));
	double* pi=calloc(1,sizeof(double)*(nang+1));
	double* pi0=calloc(1,sizeof(double)*(nang+1));
	double* pi1=calloc(1,sizeof(double)*(nang+1));
	double* tau=calloc(1,sizeof(double)*(nang+1));

	struct cplx* cx1=cplx_create(0,0);
	struct cplx* cx2=cplx_create(0,0);
	struct cplx* cx3=cplx_create(0,0);
	struct cplx* cx4=cplx_create(0,0);
	struct cplx* cx5=cplx_create(0,0);

	pii =PI;
	dx = x;
	//cxy = Complex.mul(new Complex(x,0.0),cxref);
	//printf("Refractive index:\n");
	//cplx_print(cxref);
	cplx_affect_(cx1,x,0);
	struct cplx*  cxy=cplx_mul(cx1,cxref);

//	cplx_print(cxy);

	/* Series expansion terminated after NSTOP terms */
	xstop = x + 4.E0*pow(x,0.3333) + 2.0;
	nstop = (unsigned int)xstop;
	ymod = cplx_abs(cxy);
	nmx = (unsigned int)(fmax(xstop,ymod) + 15);

	struct cplx **cxd=calloc(1,sizeof(struct cplx*)*(nmx+1));
	for (unsigned int i=0;i<(nmx+1);i++) cplx_create_(&cxd[i],0,0);
	dang = .5E0*pii/ (double)(nang-1);
	for (j = 1; j<=nang; j++)
	{
		theta = (double)(j-1)*dang;
		amu[j] = cos(theta);
	}



	/* Logarithmic derivative D(J) calculated by downward recurrence
    beginning with initial value (0.,0.) at J=NMX */

	cplx_affect_(cxd[nmx],0,0);
	nn = nmx - 1;
	for (n = 1; n<= nn; n++)
	{
		rn = nmx - n + 1;
		cplx_affect_(cx1,rn,0.0);
		cplx_div_(cx2,cx1,cxy);
		cplx_add_(cxtemp,cxd[nmx-n+1],cx2);
		cplx_div_(cxtemp,CXONE,cxtemp);
		cplx_affect_(cx3,rn,0.0);
		cplx_div_(cx4,cx3,cxy);
		cplx_affect_(cxd[nmx-n],0,0);
		cplx_sub_(cxd[nmx-n],cx4,cxtemp);
	}

	for ( j = 1; j <= nang; j++)
	{
		pi0[j] = 0.E0;
		pi1[j] = 1.E0;
	}
	struct cplx** cxs1;
	struct cplx** cxs2;
	cxs1=calloc(1,sizeof(struct cplx*)*(2*nang));
	cxs2=calloc(1,sizeof(struct cplx*)*(2*nang));
	nn = 2*nang - 1;
	for(j = 1; j<= nn; j++)
	{
		cplx_create_(&cxs1[j],0,0);
		cplx_create_(&cxs2[j],0,0);
	}
	(*cxs1_)=cxs1;
	(*cxs2_)=cxs2;



	/* Riccati-Bessel functions with real argument X
    calculated by upward recurrence */

	psi0 = cos(dx);
	psi1 = sin(dx);
	chi0 = -sin(x);
	chi1 = cos(x);
	//apsi0 = psi0;
	apsi1 = psi1;
	//cxxi0 = cplx_create(apsi0,-chi0);
	cplx_affect_(cxxi1,apsi1,-chi1);
	*qsca = 0.E0;
	*gsca = 0.E0;

	for ( n = 1; n <= nstop; n++)
	{
		dn = n;
		rn = n;
		fn = (2.E0*rn+1.E0)/(rn*(rn+1.E0));
		psi = (2.E0*dn-1.E0)*psi1/dx - psi0;
		apsi = psi;
		chi = (2.E0*rn-1.E0)*chi1/x - chi0;
		cplx_affect_(cxxi,apsi,-chi);
		/* Store previous values of AN and BN for use
    in computation of g=<cos(theta)> */
		if (n>1)
		{
			cplx_affect(cxan1,cxan);
			cplx_affect(cxbn1,cxbn);
		}

		/* Compute AN and BN:*/
		cplx_div_(cxan,cxd[n],cxref);
		cplx_affect_(cx1,rn/x,0.0);
		cplx_add_(cxan,cxan,cx1);
		cplx_affect_(cx1,apsi,0.0);
		cplx_mul_(cxan,cxan,cx1);
		cplx_affect_(cx1,apsi1,0.0);
		cplx_sub_(cxan,cxan,cx1);

		cplx_div_(cx1,cxd[n],cxref);
		cplx_affect_(cx5,rn/x,0.0);
		cplx_add_(cx2,cx1,cx5);
		cplx_mul_(cx3,cx2,cxxi);
		cplx_sub_(cx4,cx3,cxxi1);
		cplx_div_(cxan,cxan,cx4);

		cplx_mul_(cxbn,cxref,cxd[n]);
		cplx_affect_(cx1,rn/x,0.0);
		cplx_add_(cxbn,cxbn,cx1);
		cplx_affect_(cx1,apsi,0.0);
		cplx_mul_(cxbn,cxbn,cx1);
		cplx_affect_(cx1,apsi1,0.0);
		cplx_sub_(cxbn,cxbn,cx1);

		cplx_mul_(cxtemp,cxref,cxd[n]);
		cplx_affect_(cx1,rn/x,0.0);
		cplx_add_(cxtemp,cxtemp,cx1);
		cplx_mul_(cxtemp,cxtemp,cxxi);
		cplx_sub_(cxtemp,cxtemp,cxxi1);
		cplx_div_(cxbn,cxbn,cxtemp);

		/* Augment sums for *qsca and g=<cos(theta)> */
		*qsca = (*qsca) + (2.*rn+1.)*
			(cplx_abs(cxan)*cplx_abs(cxan)+cplx_abs(cxbn)*cplx_abs(cxbn));
		//gsca = gsca + ((2.*rn+1.)/(rn*(rn+1.)))*(cxan.re()*cxbn.re()+cxan.im()*cxbn.im());
		*gsca = (*gsca) + ((2.*rn+1.)/(rn*(rn+1.)))*
			(cplx_re(cxan)*cplx_re(cxbn)+cplx_im(cxan)*cplx_im(cxbn));
		//printf("g %f\n",*gsca);

		if (n>1)
		{
			*gsca = (*gsca) + ((rn-1.)*(rn+1.)/rn)*
				(cplx_re(cxan1)*cplx_re(cxan)+
				 cplx_im(cxan1)*cplx_im(cxan)+cplx_re(cxbn1)*
				 cplx_re(cxbn)+cplx_im(cxbn1)*cplx_im(cxbn));
		}

		for ( j = 1; j<= nang; j++)
		{
			jj = 2*nang - j;
			pi[j] = pi1[j];
			tau[j] = rn*amu[j]*pi[j] - (rn+1.E0)*pi0[j];
			p = pow(-1.0,n-1);

			cplx_affect_(cx1,pi[j],0.0);
			cplx_mul_(cxtemp,cxan,cx1);
			cplx_affect_(cx1,tau[j],0.0);
			cplx_mul_(cx2,cxbn,cx1);
			cplx_add_(cxtemp,cxtemp,cx2);
			cplx_affect_(cx1,fn,0.0);
			cplx_mul_(cxtemp,cx1,cxtemp);
			cplx_add_(cxs1[j],cxs1[j],cxtemp);
			t = pow(-1.0,n);

			cplx_affect_(cx1,tau[j],0.0);
			cplx_mul_(cxtemp,cxan,cx1);
			cplx_affect_(cx1,pi[j],0.0);
			cplx_mul_(cx1,cxbn,cx1);
			cplx_add_(cxtemp,cxtemp,cx1);
			cplx_affect_(cx1,fn,0.0);
			cplx_mul_(cxtemp,cx1,cxtemp);
			cplx_add_(cxs2[j],cxs2[j],cxtemp);

			if (j!=jj)
			{
				cplx_affect_(cx1,pi[j]*p,0.0);
				cplx_mul_(cxtemp,cxan,cx1);
				cplx_affect_(cx1,tau[j]*t,0.0);
				cplx_mul_(cx1,cxbn,cx1);
				cplx_add_(cxtemp,cxtemp,cx1);
				cplx_affect_(cx1,fn,0.0);
				cplx_mul_(cxtemp,cx1,cxtemp);
				cplx_add_(cxs1[jj],cxs1[jj],cxtemp);

				cplx_affect_(cx1,tau[j]*t,0.0);
				cplx_mul_(cxtemp,cxan,cx1);
				cplx_affect_(cx1,pi[j]*p,0.0);
				cplx_mul_(cx1,cxbn,cx1);
				cplx_add_(cxtemp,cxtemp,cx1);
				cplx_affect_(cx1,fn,0.0);
				cplx_mul_(cxtemp,cx1,cxtemp);
				cplx_add_(cxs2[jj],cxs2[jj],cxtemp);
			}
		}

		psi0 = psi1;
		psi1 = psi;
		apsi1 = psi1;
		chi0 = chi1;
		chi1 = chi;
		cplx_affect_(cxxi1,apsi1,-chi1);

		/*  For each angle J, compute pi_n+1
    from PI = pi_n , PI0 = pi_n-1 */

		for ( j = 1; j<= nang; j++)
		{
			pi1[j] = ((2.*rn+1.)*amu[j]*pi[j]-(rn+1.)*pi0[j])/rn;
			pi0[j] = pi[j];
		}
	} /*end of big for */

	/*  Have summed sufficient terms.
     Now compute *qsca,*qext,*qback,and *gsca */
	*gsca = 2.* (*gsca)/ (*qsca);
	*qsca = (2.E0/(x*x))* (*qsca);
	*qext = (4.E0/(x*x))*cplx_re(cxs1[1]);
	*qback = (4.E0/(x*x))*cplx_abs(cxs1[2*nang-1])*cplx_abs(cxs1[2*nang-1]);
	//printf("bhmie, qsca=%f  qext=%f  \n",*qsca,*qext);

	free(amu);
	free(pi);
	free(pi0);
	free(pi1);
	free(tau);
	free(CXONE);
	free(cxy);
	for (unsigned int i=0;i<(nmx+1);i++) free(cxd[i]);
	free(cxd);
	free(cx1);
	free(cx2);
	free(cx3);
	free(cx4);
	free(cx5);
	free(cxtemp);
	free(cxxi1);
	free(cxan);
	free(cxbn1);
	free(cxxi);
	free(cxan1);
	free(cxbn);
}




/**
  * calculate just q and g
  * output:
 *    q asymmetry factor
 *    g scattering cross section
 * input:
 *   d sphere diameter (um)
 *   np_r: real part of sphere refractive index
 *   np_i: imaginary part of sphere refractive index
 *   nf:   fluid refractive index
 *   lambda: wavelength in um
 */
/*
int bhmie_qsca_qext_g(double *qsca,double* qext,double *g,const double d_um,const double np_r,const double np_i,double nf,const double lambda)
{
int nang=2;
double x=2*PI*d_um/2/lambda*nf;
struct cplx* cxref=cplx_create(np_r/nf,np_i/nf);
struct cplx** cxs1;
struct cplx** cxs2;
double qback;
printf(" bhmie_qsca_qext_g , lambda=%f  d=%fum  np=%f+i%f nf=%f\n",lambda,d_um,np_r,np_i,nf);
//use Bohren Huffman Mie calculation:
bhmie(g,qsca,qext,&qback,&cxs1,&cxs2,nang,x,cxref);
free(cxs1);
free(cxs2);
free(cxref);
return 1;
}
*/

/**
Water drop in air example.
output for:

fluidIndex= 1
lambda= 0.650 um
diameter= 1 um
particle index = 1.33+i*0

x=4.83322
Q=3.511149  g=0.846029
Angle	perpendicular	parallel
0	22.5587	22.5587
10	20.5049	20.4966
20	15.1336	15.2278
30	8.45317	9.00168
40	3.05916	4.3604
50	2.6027	2.93266
60	3.20665	2.72286
70	2.22025	2.11427
80	0.727704	1.55227
90	1.20951	1.31375
100	1.67131	1.10859
110	1.29254	0.837753
120	0.708358	0.742934
130	1.02129	0.963075
140	1.32383	1.24703
150	1.18573	1.44999
160	1.11768	1.58208
170	1.4895	1.67612
180	1.71472	1.71472

 */
void bhmie_test(void)
{
	struct cplx** cxs1;
	struct cplx** cxs2;
	unsigned int nang=10;//nb of angles between 0 and 90 degrees
	double fluidIndex=1;
	double lambda=0.650; //um
	double diameter=1; //um
	double x=2*PI*diameter/2/lambda*fluidIndex;
	double n=1.33;//sphere refractive index real part
	struct cplx* cxref=cplx_create(n/fluidIndex,0/fluidIndex);
	double gsca;
	double qsca;
	double qext;
	double qback;
	bhmie(&gsca,&qsca,&qext,&qback,&cxs1,&cxs2,nang,x,cxref);

	printf("x=%g\n",x);
	printf("Q=%f  g=%f\n",qsca,gsca);
	//print the phase function:
	printf("Angle\tperpendicular\tparallel\n");
	for (unsigned int i=1;i<=2*nang-1;i++)
	{
		double  angle=180.0*(i-1)/(2.*nang-2.);
		printf("%g\t%g\t%g\n",angle,cplx_abs(cxs1[i]),cplx_abs(cxs2[i]));
	}

	free(cxref);
	for (unsigned int i=0;i<2*nang;i++)
		{
		free(cxs1[i]);
		free(cxs2[i]);
		}
	free(cxs1);
	free(cxs2);
}


/**
  calc the phase function of Mie for the wavelength lambda
 */
void bhmie_calc_mie_phase_function(
		float** vals, //2*nang-1 values of the pf for 179 angles in rad from 0 to PI.
		double* qsca, //scattering cross section
		double* qext, //extinction cross section
		double* g, //assymetry coef.
		const float d_um, //particle size in um
		const float np_r,//real part of refractive index of the particle
		const float np_i,//imag. part of refractive index of the particle
		const float nf, //fluid index
		float lambda,//wavelength in nm
		unsigned int nang//nb of angles between 0 and 90 degrees
		)
{
	double qback;//backscattering cross section
	struct cplx** cxs1;// perpendicular electric field ( size 2*nang). Used indices : from 1 to (2*nang-1) included
	struct cplx** cxs2;//parallel electric field. Used indices : from 1 to (2*nang-1) included
	double x=2*PI*d_um/2/(lambda/1000.0)*nf;
	//printf(" bhmie_qsca_qext_g , lambda=%f  d=%fum  np=%f+i%f nf=%f\n",
	//		lambda,d_um,np_r,np_i,nf);
	struct cplx* cxref=cplx_create(np_r/nf,np_i/nf);
	bhmie(g,qsca,qext,&qback,&cxs1,&cxs2,nang,x,cxref);
	//create the data of the phase function:
	unsigned int dim=2*nang-1;
	(*vals)=(float*) calloc(1,sizeof(float)*dim);
	for (unsigned int i=1;i<=dim;i++)
	{
		float perp=powf((float)cplx_abs(cxs1[i]),2);
		float para=powf((float)cplx_abs(cxs2[i]),2);
		float unpo=(perp+para)/2;
		(*vals)[i-1]=unpo;
	}
	free(cxref);
	for (unsigned int i=0;i<2*nang;i++)
	{
		free(cxs1[i]);
		free(cxs2[i]);
	}
	free(cxs1);
	free(cxs2);
}

