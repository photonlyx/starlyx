/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef BHMIE_H_
#define BHMIE_H_

#include "cplx.h"


void bhmie(
		double* gsca,
		double* qsca,
		double* qext,
		double* qback,
		struct cplx*** cxs1_,
		struct cplx*** cxs2_,
		const unsigned int nang,
		const double x,
		const struct cplx* cxref
		);

void bhmie_test(void);

void bhmie_calc_mie_phase_function(
		float** vals,
		double* qsca,
		double* qext,
		double* g,
		const float d_um,
		const float np_r,
		const float np_i, 
		const float nf,
		float lambda,
		unsigned int nang
		);



#endif /* BHMIE_H_ */
