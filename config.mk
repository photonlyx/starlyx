VERSION = 0.0

# Répertoire destination dans lequel le projet sera installé
PREFIX = /usr/local

# Type de compilation. Les valeurs possibles sont DEBUG et RELEASE. DEBUG
# signifie que le programme est compilé avec des options facilitant son
# débogage. Là ou RELEASE est utilisé pour compiler le programme de manière
# optimisée
#BUILD_TYPE = DEBUG
BUILD_TYPE = RELEASE

# Chemin vers le Star-Engine
STAR_ENGINE=${HOME}/git/edstar/star-engine/local

################################################################################
# Outils utilisés pour génération automatique du projet
################################################################################
# Compilateur C
CC = cc

################################################################################
# Gestion des dépendences externes
################################################################################

# Numéro de version minimum requis 
RSYS_VERSION = 
# Options de compilation 
RSYS_CFLAGS = -I$(STAR_ENGINE)/include/rsys
# Options de l'éditeur de lien
RSYS_LIBS = -L$(STAR_ENGINE)/lib  -lrsys -lm

# Numéro de version minimum requis pour utiliser Star-Sampling 
SSP_VERSION = 0.12
# Options de compilation pour utiliser Star-Sampling
SSP_CFLAGS = -I$(STAR_ENGINE)/include
# Options de l'éditeur de lien pour utiliser Star-Sampling
SSP_LIBS = -L$(STAR_ENGINE)/lib -lstar-sp 

# Numéro de version minimum requis pour utiliser star-3d 
S3D_VERSION = 0.8
# Options de compilation pour utiliser star-3d 
S3D_CFLAGS = -I$(STAR_ENGINE)/include
# Options de l'éditeur de lien pour utiliser star-3d 
S3D_LIBS = -L$(STAR_ENGINE)/lib -ls3d  -ls3dut

# Numéro de version minimum requis pour utiliser star-3daw 
S3DAW_VERSION = 0.4.1 
# Options de compilation pour utiliser star-3daw 
S3DAW_CFLAGS = -I$(STAR_ENGINE)/include
# Options de l'éditeur de lien pour utiliser star-3d star-3daw et star-3dstl
S3DAW_LIBS = -L$(STAR_ENGINE)/lib  -ls3daw 

# Numéro de version minimum requis pour utiliser  star-3dtl
S3DSTL_VERSION = 0.3.2
# Options de compilation pour utiliser  star-3dstl
S3DSTL_CFLAGS = -I$(STAR_ENGINE)/include
# Options de l'éditeur de lien pour utiliser  star-3dstl
S3DSTL_LIBS = -L$(STAR_ENGINE)/lib -ls3dstl

# Numéro de version minimum requis pour utiliser  smc
SMC_VERSION = 
# Options de compilation pour utiliser  smc
SMC_CFLAGS = -I$(STAR_ENGINE)/include
# Options de l'éditeur de lien pour utiliser smc
SMC_LIBS = -L$(STAR_ENGINE)/lib -lsmc

# Numéro de version minimum requis pour utiliser star-cad
#SCAD_VERSION = 
# Options de compilation pour utiliser  star-cad
SCAD_CFLAGS = -I$(STAR_ENGINE)/include
## Options de l'éditeur de lien pour utiliser star-cad
#SCAD_LIBS = -L$(STAR_ENGINE)/lib -lscad

# Numéro de version minimum requis pour utiliser openmp
SOMP_VERSION = 
# Options de compilation pour utiliser openmp
SOMP_CFLAGS = -fopenmp
# Options de l'éditeur de lien pour utiliser openmp
SOMP_LIBS = -L$(STAR_ENGINE)/lib
 
# Numéro de version minimum requis pour utiliser mxml
MXML_VERSION = 
# Options de compilation pour utiliser mxml
MXML_CFLAGS = 
# Options de l'éditeur de lien pour utiliser mxml
MXML_LIBS =  -lmxml


GSL_LIBS =  -lgsl
 
# Options de compilations de l'ensemble des dépendences externes
DPDC_CFLAGS = $(RSYS_CFLAGS) $(SSP_CFLAGS) $(S3D_CFLAGS) $(S3DAW_CFLAGS) $(S3DSTL_CFLAGS)\
 $(SOMP_CFLAGS)  $(SMC_CFLAGS) $(SCAD_CFLAGS) $(MXML_CFLAGS)

# Options de l'éditeur de lien de l'ensemble des dépendences externes
DPDC_LIBS = $(RSYS_LIBS) $(SSP_LIBS) $(S3D_LIBS) $(S3DAW_LIBS) $(S3DSTL_LIBS)  $(SOMP_LIBS)\
$(SMC_LIBS)  $(MXML_LIBS) $(GSL_LIBS)

################################################################################
# Options de compilation
################################################################################

# Options d'avertissement
WFLAGS1 =\
 -Wall\
 -Wcast-align\
 -Wconversion\
 -Wextra\
 -Wmissing-declarations\
 -Wmissing-prototypes\
 -Wshadow

WFLAGS =\
 -Wall\
 -Wextra\
 -Wmissing-declarations\
 -Wmissing-prototypes\
 -Wshadow

# Amélioration de la sécurité et de la robustesse des programmes générés
CFLAGS_HARDENED =\
 -D_FORTIFY_SOURCES=2\
 -fPIE\
 -fcf-protection=full\
 -fstack-clash-protection\
 -fstack-protector-strong

# Options de compilation standards
CFLAGS_COMMON =\
 -fstrict-aliasing\
 -fvisibility=hidden\
 -pedantic\
 -std=c99\
 $(CFLAGS_HARDENED)\
 $(WFLAGS)\
 $(DPDC_CFLAGS)

# Options de compilation par type de compilation
CFLAGS_DEBUG = -g $(CFLAGS_COMMON)
CFLAGS_RELEASE = -O3 -DNDEBUG $(CFLAGS_COMMON)

# Les options de compilation à utiliser
CFLAGS = $(CFLAGS_$(BUILD_TYPE))

################################################################################
# Options d'édition de liens
################################################################################
# Options de l'éditeur du lien pour rendre l'exécutable plus robuste et plus sûr
LDFLAGS_HARDENED = -Wl,-z,relro,-z,now

LDFLAGS_COMMON =\
 -pie\
 $(LDFLAGS_HARDENED)\
 $(DPDC_LIBS)

# Options de l'éditeur de lien par type de compilation
LDFLAGS_DEBUG = $(LDFLAGS_COMMON)
LDFLAGS_RELEASE = -s $(LDFLAGS_COMMON)

# Les options de l'éditeur de lien à utiliser
LDFLAGS = $(LDFLAGS_$(BUILD_TYPE))
