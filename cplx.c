/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "cplx.h"


void cplx_affect_(struct cplx* c,const double re,const double im)
{
	c->re=re;
	c->im=im;
}

void cplx_affect(struct cplx* c,const struct cplx* c1)
{
	c->re=c1->re;
	c->im=c1->im;
}

void cplx_add_(struct cplx* c,const struct cplx* c1,const struct cplx* c2)
{
	c->re=c1->re+c2->re;
	c->im=c1->im+c2->im;
}

void cplx_sub_(struct cplx* c,const struct cplx* c1,const struct cplx* c2)
{
	c->re=c1->re-c2->re;
	c->im=c1->im-c2->im;
}


void cplx_mul_(struct cplx* r,const struct cplx* c1,const struct cplx* c2)
{
	double c1r=c1->re;
	double c1i=c1->im;
	double c2r=c2->re;
	double c2i=c2->im;
	r->re=c1r*c2r-c1i*c2i;
	r->im=c1r*c2i+c2r*c1i;
//	r->re=c1->re*c2->re-c1->im*c2->im;
//	r->im=c1->re*c2->im+c2->re*c1->im;
}


void cplx_inv_(struct cplx* c1,const struct cplx* c)
{
	double r=c->re*c->re+c->im*c->im;
	c1->re=c->re/r;
	c1->im=-c->im/r;
}


void cplx_div_(struct cplx* r,const struct cplx* c1,const struct cplx* c2)
{
	struct cplx* invc2=cplx_create(0,0);
	cplx_inv_(invc2,c2);
	cplx_mul_(r,c1,invc2);
	free(invc2);
}


void cplx_create_(struct cplx** r,double re,double im)
{
	*r=calloc(1,sizeof(struct cplx));
	(*r)->re=re;
	(*r)->im=im;
}

void cplx_conj_(struct cplx* r,const struct cplx* c)
{
	r->re=c->re;
	r->im=-c->im;
}

double cplx_re(const struct cplx* c)
{
	return (*c).re;
}

double cplx_im(const struct cplx* c)
{
	return (*c).im;
}

double cplx_abs(const struct cplx* c1)
{
	struct cplx c= *c1;
	return sqrt(c.re*c.re+c.im*c.im);
}


struct cplx* cplx_add(const struct cplx* c1,const struct cplx* c2)
{
	struct cplx* c=calloc(1,sizeof(struct cplx));
	cplx_add_(c,c1,c2);
	return c;
}

struct cplx* cplx_sub(const struct cplx* c1,const struct cplx* c2)
{
	struct cplx* c=calloc(1,sizeof(struct cplx));
	cplx_sub_(c,c1,c2);
	return c;
}

struct cplx* cplx_mul(const struct cplx* c1,const struct cplx* c2)
{
	struct cplx* c=calloc(1,sizeof(struct cplx));
	cplx_mul_(c,c1,c2);
	return c;
}

struct cplx* cplx_inv(const struct cplx* c1)
{
	struct cplx* c=calloc(1,sizeof(struct cplx));
	cplx_inv_(c,c1);
	return c;
}
struct cplx* cplx_div(const struct cplx* c1,const struct cplx* c2)
{
	struct cplx* c=calloc(1,sizeof(struct cplx));
	cplx_div_(c,c1,c2);
	return c;
}

struct cplx* cplx_create(double re,double im)
{
	struct cplx* c=calloc(1,sizeof(struct cplx));
	c->re=re;
	c->im=im;
	return c;
}

struct cplx* cplx_conj(const struct cplx* c1)
{
	struct cplx* c=calloc(1,sizeof(struct cplx));
	cplx_conj_(c,c1);
	return c;
}

void cplx_print(const struct cplx* c)
{
	printf("%g %g\n",c->re,c->im);
}

void cplx_test()
{
	struct cplx* c1=cplx_create(1,0);
	struct cplx* c2=cplx_create(1,0);
	struct cplx* r=cplx_add(c1,c2);
	printf(" c1 %g %g\n",c1->re,c1->im);
	printf(" c2 %g %g\n",c2->re,c2->im);
	printf(" r %g %g\n",r->re,r->im);
}

