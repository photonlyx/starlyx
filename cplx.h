/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CPLX_H_
#define CPLX_H_

/**
 * complex number
 */
struct cplx
	{
	double re;
	double im;
	};

void cplx_affect_(struct cplx* c,const double re,const double im);
void cplx_affect(struct cplx* c,const struct cplx* c1);
void cplx_add_(struct cplx* c,const struct cplx* c1,const struct cplx* c2);
void cplx_sub_(struct cplx* c,const struct cplx* c1,const struct cplx* c2);
void cplx_mul_(struct cplx* r,const struct cplx* c1,const struct cplx* c2);
void cplx_inv_(struct cplx* c1,const struct cplx* c);
void cplx_div_(struct cplx* r,const struct cplx* c1,const struct cplx* c2);
void cplx_create_(struct cplx** r,double re,double im);
void cplx_conj_(struct cplx* r,const struct cplx* c);
double cplx_re(const struct cplx* c);
double cplx_im(const struct cplx* c);
double cplx_abs(const struct cplx* c);
struct cplx* cplx_add(const struct cplx* c1,const struct cplx* c2);
struct cplx* cplx_sub(const struct cplx* c1,const struct cplx* c2);
struct cplx* cplx_mul(const struct cplx* c1,const struct cplx* c2);
struct cplx* cplx_inv(const struct cplx* c1);
struct cplx* cplx_div(const struct cplx* c1,const struct cplx* c2);
struct cplx* cplx_create(double re,double im);
struct cplx* cplx_conj(const struct cplx* c1);
void cplx_print(const struct cplx* c);
void cplx_test();
#endif /* CPLX_H_ */
