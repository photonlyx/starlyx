#include<stdio.h>


#include <ImfRgbaFile.h>
#include <ImfStringAttribute.h>
#include <ImfMatrixAttribute.h>
#include <ImfRgba.h>

using namespace Imf;
using namespace Imath;



int main(int argc, char* argv[]) 
{
	if (argc!=2)
	{
		fprintf(stderr, "Usage: %s  image.raw \n", argv[0]);
		return 0;
	}
	char* filename=argv[1];
	FILE* file1;
	file1 = fopen(filename,"rb");
	if (file1==NULL)
	{
		printf("Can't open file %s\n",filename);
		exit(1);
	}
	int n=7;//size of header
	float header[n];//header
	fread(&header,sizeof(float),n,file1);
	// get image parameters:
	int i=0;
	int w=(int)header[i++];//width
	int h=(int)header[i++];//height
	int nbc=(int)header[i++];//nb of channels
	float xmin=header[i++];
	float xmax=header[i++];
	float ymin=header[i++];
	float ymax=header[i++];
	//read the data
	int size=w*h*nbc+nbc;
	float data[size];
	fread(&data,sizeof(float),size,file1);
	fclose(file1);
	//get the max:
	float max=-1e30;
	for (int kk=0;kk<w*h*nbc;kk++)
	{
		float v=data[kk];
		if (v>max) max=v;
	}
	if ((nbc==1)||(nbc==3))
	{
		// Create an openEXR RGBA image:
		Rgba *pixels = new Rgba[w*h];
		// Fill the openEXR image:
		if (nbc==1)
		{
			for (int kk=0;kk<w*h;kk++)
			{
				float v=data[kk]/max;
				pixels[kk] = Rgba(v,v,v,1.0);
			}
		}
		else if (nbc==3)
		{
			for (int kk=0;kk<w*h;kk++)
			{
				float r=data[kk*nbc]/max;
				float g=data[kk*nbc+1]/max;
				float b=data[kk*nbc+2]/max;
				pixels[kk] = Rgba(r,g,b,1.0);
			}
		}
		char oexrfile[300];
		strcpy(oexrfile,filename);
		oexrfile[strlen(oexrfile)-4]='\0';//remove extension .raw
		strcat(oexrfile,".exr");
		// Create an OpenEXR output file
		RgbaOutputFile file(oexrfile,w,h,WRITE_RGBA);
		// Write the pixels to the file
		file.setFrameBuffer(pixels,1,w);
		file.writePixels(h);
		// Cleanup
		delete[] pixels;
		printf("convert raw image %s to %s\n",filename,oexrfile);
	}
	else
	{
		printf("convert only 1 or 3 channels images (here %d channels)\n",nbc);
	}
	return 0;
}

