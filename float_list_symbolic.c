/*
   Copyright (C) 2022 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
   */


#include <stdlib.h>
#include <stdio.h>


#include "float_list_symbolic.h"


/**
 * add a float* to the list
 */
void float_list_symbolic_add(struct float_list_symbolic* list,float* element)
{
	if (list->index>=100000)
	{
		printf("float_list_symbolic MAX nb of positions reached (100000) : %ld for path\n" ,list->index);
		return;
	}
	float* p1=calloc(1,list->dim*sizeof(float));
	for (int i=0;i<list->dim;i++) p1[i]=element[i];
	list->list[list->index]=p1;
	list->index++;
}




void float_list_symbolic_print(struct float_list_symbolic* list)
{
	for (int j=0;j<list->index;j++)
	{
		float_list_symbolic_print_element(list,j);
	}
}


void float_list_symbolic_print_element(struct float_list_symbolic* list,int index)
{
	float* element=NULL;
	if (index<list->size) 
	{
		element=list->list[index];
		float_list_symbolic_print_element2(list,element);
	}
}


void float_list_symbolic_print_element2(struct float_list_symbolic* list,float* element)
{
	printf("(");
	for (int i=0;i<list->dim-1;i++) printf("%g,",element[i]);
	printf("%g)\n",element[list->dim-1]);
}


void float_list_symbolic_create(struct float_list_symbolic** list,int dim,int nb_elements)
{
	float_list_symbolic_create2(list,dim,nb_elements,"                                       ");
}

void float_list_symbolic_create2(struct float_list_symbolic** list,int dim,int nb_elements,char* label)
{
	//get the new pointer in the list and attach a new list to it:
	(*list)=calloc(1,sizeof(struct float_list_symbolic));
	//store the nb of elements
	(*list)->size=nb_elements;
	//set the dimension of the elements:
	(*list)->dim=dim;
	//allocate the list of float*
	(*list)->list=calloc(1,nb_elements*sizeof(float*));
	//reset the index of the list of float*
	(*list)->index=0;
	//set the label
	(*list)->label=label;
}

void float_list_symbolic_release(struct float_list_symbolic* list)
{
	for (int j=0;j<list->index;j++)
	{
		free(list->list[j]);//free the pos : float[dim]
	}
	free(list->label);
	free(list->list);//free the list of pos float*
}


float* float_list_symbolic_get(struct float_list_symbolic* list,int index)
{
	float* element=NULL;
	if (index<list->size) element=list->list[index];
	return element;
}

int float_list_symbolic_save(struct float_list_symbolic* list, char* filename)
{
	int r=0;
	FILE* file = fopen(filename, "w");
	if (file)
	{
		fprintf(file,"%s\n",list->label);
		for (int j=0;j<list->index;j++)
		{
			float * element=list->list[j];
			for (int i=0;i<list->dim-1;i++) fprintf(file,"%g\t",element[i]);
			fprintf(file,"%g\n",element[list->dim-1]);
		}
		fclose(file);
		r=1;
	}
	return r;
}

