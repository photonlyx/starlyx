/*
Copyright (C) 2022 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FLOAT_LIST_SYMBOLIC_H_
#define FLOAT_LIST_SYMBOLIC_H_


#define MAX_NB_ELEMENTS 100000



struct float_list_symbolic 
{
	unsigned long size;
	int dim; //dimension of the elements
	float** list;
	//index of current position:
	long index;
	char* label;//label, comment or name
};

void float_list_symbolic_add(struct float_list_symbolic*,float* element);
void float_list_symbolic_print(struct float_list_symbolic*);
void float_list_symbolic_print_element(struct float_list_symbolic*,int index);
void float_list_symbolic_print_element2(struct float_list_symbolic*,float* element);
void float_list_symbolic_create(struct float_list_symbolic** list,int dim,int nb_elements);
void float_list_symbolic_create2(struct float_list_symbolic** list,int dim,int nb_elements,char *label);
void float_list_symbolic_release(struct float_list_symbolic* list);
float* float_list_symbolic_get(struct float_list_symbolic* list,int index);
int float_list_symbolic_save(struct float_list_symbolic* list, char* filename);



#endif /* FLOAT_LIST_SYMBOLIC_H_ */
