
#include <stdlib.h>
#include "lists.h"

//flexible lists of floats or any objects


int float_list_create(struct float_list** l)
{
	(*l)=(struct float_list*) calloc(1,1*sizeof(struct float_list));
	(*l)->dim=0;
	(*l)->dim_buffer=0;
	(*l)->is_closed=0;//flag to tell if the list is closed (impossible to add something)
	return 1;
}

int float_list_release(struct float_list* l)
{
	free(l->data);
	free(l);
	return 1;
} 

/**
 * add a float to the list. The buffer is expanded by a factor 2 when full.
 */
int float_list_add(struct float_list* l,float r)
{
	if ((l->dim+1)>l->dim_buffer)//buffer too small
	{
		//expand the buffer
		if (l->dim_buffer==0) (l->dim_buffer=1); else l->dim_buffer*=2;
		//allocate memory for the new buffer:
		float* new_data=(float*)calloc(1,(l->dim_buffer)*sizeof(float));
		//copy the data to the new buffer:
		for (unsigned long i=0;i<l->dim;i++)
		{
			new_data[i]=l->data[i];
		}
		//release the old buffer:
		free(l->data);
		//affect the new buffer:
		l->data=new_data;
	}
	l->data[l->dim++]=r;
	return 1;
}

float float_list_get(struct float_list* l,unsigned long i)
{
	if (i<l->dim) return l->data[i];else return 0;
}



//a flexible list of objects


int d_list_create(struct d_list** l)
{
	(*l)=(struct d_list*) calloc(1,1*sizeof(struct d_list));
	(*l)->dim=0;
	(*l)->dim_buffer=0;
	(*l)->is_closed=0;//flag to tell if the list is closed (impossible to add something)
	return 1;
}

int d_list_release(struct d_list* l)
{
	free(l->data);
	free(l);
	return 1;
} 

/**
 * add an object to the list. The buffer is expanded by a factor 2 when full.
 */
int d_list_add(struct d_list* l,void* o)
{
	if ((l->dim+1)>l->dim_buffer)//buffer too small
	{
		//expand the buffer
		if (l->dim_buffer==0) (l->dim_buffer=1); else l->dim_buffer*=2;
		//allocate memory for the new buffer:
		void** new_data=(void**)calloc(1,(l->dim_buffer)*sizeof(void*));
		//copy the data to the new buffer:
		for (unsigned long i=0;i<l->dim;i++)
		{
			new_data[i]=l->data[i];
		}
		//release the old buffer:
		free(l->data);
		//affect the new buffer:
		l->data=new_data;
	}
	l->data[l->dim++]=o;
	return 1;
}

void* d_list_get(struct d_list* l,unsigned long i)
{
	if (i<l->dim) return l->data[i];else return NULL;
}

void* d_list_get_last(struct d_list* l)
{
	return l->data[l->dim-1];
}



