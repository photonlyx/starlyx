/*
Copyright (C) 2022 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FLOAT_LIST_H_
#define FLOAT_LIST_H_

//a flexible list of floats:
struct float_list
{
	unsigned long dim;
	unsigned long dim_buffer;
	float* data;
	int is_closed;//flag to tell if the list is closed (impossible to add something)
};
int float_list_create(struct float_list** l);
int float_list_release(struct float_list* l);
int float_list_add(struct float_list* l,float r);
float float_list_get(struct float_list* l,unsigned long i);



//dynamic list of objects:
struct d_list
{
	unsigned long dim;
	unsigned long dim_buffer;
	void** data;
	int is_closed;//flag to tell if the list is closed (impossible to add something)
};
int d_list_create(struct d_list** l);
int d_list_release(struct d_list* l);
int d_list_add(struct d_list* l,void* o);
void* d_list_get(struct d_list* l,unsigned long i);
void* d_list_get_last(struct d_list* l);





#endif /* FLOAT_LIST_H_ */
