/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#include "parsing.h"



/**
 * replace every char c1 by c2
 * str: string to process
 * c1: char to replace
 * c2: char that put in place of c1
 */
void parsing_replace(char* str,char c1,char c2)
{
	int i=0;//char counter
	for (;;)
	{
		if (str[i]==c1) str[i]=c2;
		i++;
		if (str[i]=='\0')	break;//end of string
	}
}

/**
 * parse words separated by spaces (max 1000)
 */
int parsing_parse_words(const char* str,char*** p_p_words,int* nb_words)
{
	char* words[NB_MAX_VALS];
	int i=0;//char counter
	int c=0;//value counter
	char word[MAX_WORD_LENGTH];
	int cword=0;
	int res=1;
	//printf("parsing_parse_words parse *%s*\n",str);
	for (;;)
	{
		if (i>NB_MAX_VALS*100) break;
		if (str[i]=='\0') break;//end of string
		if (str[i]==' ')//found a space
		{
			//continue while finding spaces:
			while (str[i]==' ')
			{
				//printf("%d: space (%d)\n",i,' ');
				i++;
			}
		}
		if (str[i]=='\0')	break;//end of string

		//now it is not a space, continue up to the next space:
		while (str[i]!=' ')
		{
			if (str[i]=='\0') break;//end of string
			//printf("%d: %c (%d)\n",i,str[i],str[i]);
			word[cword++]=str[i];
			i++;
			if (cword>=MAX_WORD_LENGTH)
			{
				printf("The word: %s  is longer than the maximum (%d).\nThe data is:\n%s\n",word,MAX_WORD_LENGTH,str);
				while (str[i]!=' ') i++;
				res=0;
				break;
			}
		}
		word[cword]='\0';//set end of string
		//printf("%s\n",word);
		words[c]=calloc(1,sizeof(char)*(cword+1));
		strcpy(words[c],word);
		//printf("store word of index %d : %s\n",c,words[c]);
		c++;
		cword=0;//reset word
		//i++;
		if (c>=NB_MAX_VALS)
		{
			printf("The data has more values than the maximum (%d).\nThe data is:\n%s",NB_MAX_VALS,str);
			break;
		}
	}
	//printf("parsing_parse_words parsing : nb words %d\n",c);

	*nb_words=c;
	(*p_p_words)= calloc(1,sizeof(char**)*c);
	for (i=0;i<c;i++)
	{
		(*p_p_words)[i]=(char *)words[i];
	}

	return res;
}


void parsing_test1(char* s)
{
	int i=0;//char counter
	for (;;)
	{
		if (i>1000) break;
		if (s[i]=='\0') break;//end of string
		printf("char %d : *%c*\n",i,s[i]);
		i++;
	}

}


void parsing_test(char* s)
{
	int n;
	char** words;
	//	char* s[100];
	//	sprintf(s," 230 1 100 2 ");
	printf("Test parsing:  *%s*  \n",s);
	parsing_parse_words(s,&words,&n);
	printf("%d words \n",n);
	for (int i=0;i<n;i++) printf("%d %s\n",i,words[i]);

}

void parsing_floats_test(char* s)
{
	printf("Test parsing floats:  *%s*  \n",s);
	float* vals=NULL;
	int nb_vals=0;
	parsing_parse_floats(s,&vals,&nb_vals) ;
	printf("%d floats \n",nb_vals);
	for (int i=0;i<nb_vals;i++) printf("%d %f\n",i,vals[i]);
	free(vals);
}


/**
 * parse words in a float array
 */
int parsing_parse_floats(const char* str,float** p_p_values,int* nb_vals)
{
	int res;
	char** list;
	int nb_words=0;
	//printf("parse float(s): %s\n",str);
	res=parsing_parse_words(str,&list,&nb_words);
	if (!res) return 0;
	(*nb_vals)=(nb_words);
	(*p_p_values)= calloc(1,sizeof(float)*(nb_words));
	//printf("%d float(s): %f\n",(*nb_vals));
	for (int i=0;i<(nb_words);i++)
	{
		float v=0;
		//printf("%d %s\n",i,list[i]);
		sscanf(list[i],"%f",&v);
		//printf("%d in float: %f\n",i,v);
		(*p_p_values)[i]=v;
	}
	free(list);
	return 1;
}



void parsing_print(float* values,int nb_vals)
{
	printf("%d values:\n",nb_vals);
	for (int i=0;i<nb_vals;i++) printf("%f\n",values[i]);
}

int parsing_is_numerical_value(const char* str)
{
	if (!str) return 0;
	int c1=48;//min ascii nb of numerical chars (included)
	int c2=57;//max ascii nb of numerical chars (included)
	int i=0;
	int r=1;
	for (;;)
	{
		if (str[i]=='\0') break;
		if (((int)str[i])!=46) //dot char
		{
			if ((str[i]<c1)||(str[i]>c2))
			{
				r=0;
			}
		}
		i++;
	}
	return r;
}

/**
 * return the index of the first occurence of c. -1 if not found
 */
int parsing_first_index_of(const char*str,const char c)
{
	int i=0;
	for (;;)
	{
		if (str[i]=='\0')
		{
			i=-1;
			break;
		}
		if (str[i]==c)break;
		i++;
	}
	return i;
}

/**
 * return the index of the first occurence of c. -1 if not found
 */
int parsing_last_index_of(const char*str,const char c)
{
	//get the string length:
	int i=0;
	for (;;) if (str[i++]=='\0')break;
	i--;
	for (;;)
	{
		//printf("%d %d \n",i,(int)str[i]);
		if (str[i]==c)break;
		if (i==0)
		{
			i=-1;
			break;
		}
		i--;
	}
	//printf("index: %d\n",i);
	return i;
}

/**
 * Returns a new string that is a substring of this string.
 * The substring begins at the specified beginIndex and extends to the character at index endIndex - 1.
 * Thus the length of the substring is endIndex-beginIndex.
 */
int parsing_substring(char* s2,const char*str,int i1,int i2)
{
	int c1=0;
	int c2=0;
	for (;;)
	{
		if (str[c1]=='\0') break;
		//printf("%d %c \n",c1,str[c1]);
		if (c1>=i1)
		{
			s2[c2]=str[c1];
			c2++;
		}
		if (c1==i2-1) break;
		c1++;
	}
	s2[c2]='\0';
	return 1;
}

/**
 * compare 2 strings .
 * Return 0 if there are equals, -1 if not
 */
int parsing_compare_string(char *first, char *second)
{
	while (*first == *second)
	{
		if (*first == '\0' || *second == '\0')
			break;
		first++;
		second++;
	}
	if (*first == '\0' && *second == '\0')
		return 0;
	else
		return -1;
}

/**
 * compare 2 strings .
 * Return 0 if there are equals, -1 if not
 */
int parsing_compare_strings(char a[], char b[])
{
   int c = 0;
   while (a[c] == b[c]) {
      if (a[c] == '\0' || b[c] == '\0')
         break;
      c++;
   }
   if (a[c] == '\0' && b[c] == '\0')
      return 0;
   else
      return -1;
}

