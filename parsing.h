/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PARSING_H_
#define PARSING_H_

#define NB_MAX_VALS 10000
#define MAX_WORD_LENGTH 50


void parsing_replace(char* str,char c1,char c2);
int parsing_parse_words(const char* str,char*** p_p_words,int* nb_words);
int parsing_parse_floats(const char* str,float** p_p_values,int* nb_vals);
void parsing_print(float* values,int nb_vals);
int parsing_is_numerical_value(const char* str);
int parsing_last_index_of(const char*str,const char c);
int parsing_substring(char* s2,const char*str,int i1,int i2);
int parsing_compare_string(char *first, char *second);
int parsing_compare_strings(char a[], char b[]);

void parsing_floats_test(char* s);


#endif /* PARSING_H_ */
