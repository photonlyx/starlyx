/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sampled_data.h"


/*
float XY data
*/

struct sampled_data* sampled_data_get_example(void)
{
	struct sampled_data* spd=calloc(1, sizeof(struct sampled_data));
	unsigned int n=3;
	strcpy(spd->name,"Example");
	spd->nb_vals=n;
	spd->x=calloc(1,sizeof(float)*n);
	spd->y=calloc(1,sizeof(float)*n);
	spd->x[0]=400;
	spd->x[1]=500;
	spd->x[2]=600;
	spd->y[0]=1;
	spd->y[1]=2;
	spd->y[2]=8;
	spd->x_regular=0;
	//printf("spectrum: %d points\n",(spd->nbVals));
	return spd;
}


void sampled_data_print(
		struct sampled_data * sd,
		FILE* stream
		)
{
	fprintf(stream,"Sampled data:\n");
	fprintf(stream,"%d point(s)\n",sd->nb_vals);
	fprintf(stream,"Name: *%s*\n",sd->name);
	for (unsigned int i=0;i<sd->nb_vals;i++) 
		fprintf(stream,"%f %f\n",sd->x[i],sd->y[i]);

}



void sampled_data_release(struct sampled_data * sd)
{
	free(sd->x);
	free(sd->y);
	free (sd);
}



void sampled_data_create2(
		struct sampled_data** sp,
		const unsigned int n,
		const char* name
		)
{
	(*sp)=calloc(1,sizeof(struct sampled_data));
	(*sp)->nb_vals=n;
	(*sp)->x=calloc(1,sizeof(float)*n);
	(*sp)->y=calloc(1,sizeof(float)*n);
	strcpy((*sp)->name,name);
	(*sp)->x_regular=0;
	for (unsigned int i=0;i<n;i++)
	{
		(*sp)->x[i]=(float)0;
		(*sp)->y[i]=(float)0;
	}
}

void sampled_data_create3(
		struct sampled_data** sp,
		const unsigned int n
		)
{
	sampled_data_create2(sp,n,"");
}

/**
 * create a sampled_data from an xml definition like:
 *
 * <spectrum NAME="filtreR" DATA="450 2.1  530 2.1 650  1.05  ">  </spectrum>
 *
 */
int sampled_data_create_from_xml(
		struct sampled_data ** p_p_sp,
		mxml_node_t *node
		)
{
	char name[MAX_NAME_LENGTH];
	const char* data;
	if (!read_string_attribute2( node,"NAME",name)) return 0;
	if (!read_string_attribute3( node,"DATA",&data)) return 0;

	//copy the string in a non const char* :
	char* data1=calloc(1,sizeof(char)*(strlen(data)+1));
	strcpy(data1,data);

	//replace tabs new lines and carriage returns by spaces:
	parsing_replace(data1,'\t',' ');
	parsing_replace(data1,'\n',' ');
	parsing_replace(data1,'\r',' ');

	//printf("name =%s\n",name);
	//printf("data1=%s\n",data1);

	//build the sampled_data:
	struct sampled_data* sp=calloc(1,sizeof(struct sampled_data));
	strcpy(sp->name,name);

	float* vals=NULL;
	int nb_vals=0;
	if (parsing_parse_floats(data1,&vals,&nb_vals)==0) return 0;
	//printf("%d values\n",nb_vals);
	if (nb_vals%2!=0)
	{
		printf("The sampled data %s has not an even number of values.\nThe\
                   data is:\n%s\n",name,data);
		//nb_vals=nb_vals-1;
		return 0;
	}

	unsigned int n=(unsigned int)nb_vals/2;
	sp->nb_vals=n;
	sp->x=calloc(1,sizeof(float)*n);
	sp->y=calloc(1,sizeof(float)*n);
	for (unsigned int i=0;i<n;i++)
	{
		sp->x[i]=vals[2*i];
		sp->y[i]=vals[2*i+1];
	}
	sp->x_regular=0;

	*p_p_sp=sp;
	free(data1);

	//sampled_data_print(sp,stdout);

	//printf("sampled_data_create_from_xml end\n");
	return 1;

}

/**
 * fill the data with string like "x0 y0 x1 y1 x2 y2 ....... "
 */
void sampled_data_create_from_string(
struct sampled_data ** sp,
		char *s,
		const char* name
		)
{
	//replace tabs new lines and carriage returns by spaces:
	parsing_replace(s,'\t',' ');
	parsing_replace(s,'\n',' ');
	parsing_replace(s,'\r',' ');
	//build the sampled_data:
	(*sp)=calloc(1,sizeof(struct sampled_data));
	strcpy((*sp)->name,name);
	float* vals=NULL;
	int nn=0;
	if (parsing_parse_floats(s,&vals,&nn)==0) return ;
	if (nn%2!=0)
	{
		printf("The data has not an even number of values\
				(remove the last).\nThe data is:\n%s",s);
		nn=nn-1;
	}
	unsigned int n=(unsigned int)nn/2;
	(*sp)->nb_vals=n;
	(*sp)->x=calloc(1,sizeof(float)*n);
	(*sp)->y=calloc(1,sizeof(float)*n);
	for (unsigned int i=0;i<n;i++)
	{
		(*sp)->x[i]=vals[2*i];
		(*sp)->y[i]=vals[2*i+1];
	}
	(*sp)->x_regular=0;
}

void sampled_data_create_inverse_cumulated_integral(
		struct sampled_data ** p_p_cumulated_integral,
		struct sampled_data * sp
		)
{
	unsigned int n=sp->nb_vals;
	sampled_data_create3(p_p_cumulated_integral,n);
	//calc integral and invert:
	for (unsigned int i=1;i<n;i++)
	{
		(*p_p_cumulated_integral)->y[i]=sp->x[i];
		(*p_p_cumulated_integral)->x[i]=(*p_p_cumulated_integral)->x[i-1]+sp->y[i];
	}
	//normalise such that last value is 1:
	for (unsigned int i=0;i<n;i++) 
		(*p_p_cumulated_integral)->x[i]/=(*p_p_cumulated_integral)->x[n-1];
}


/**
 * sort x value (equal probability for all x values)
 */
float sampled_data_sort_random_x(
		struct sampled_data * sp,
		struct ssp_rng* rng)
{
	unsigned int n=sp->nb_vals;
	int i=(int)ssp_rng_uniform_uint64(rng,0,(uint64_t)(n-1));
	return sp->x[i];
}



/**
 * sort the values in "a" from index "from" to index "to".
 * get the new indices in "indices"
 * indices : the sorted indices (result)
 * a the : array of values to sort
 * from first index to sort
 * to last index to sort
 */
void sort(
		int* indices,
		float* a, 
		int n, 
		int from, 
		int to
		)
{
	int i = from, j = to;
	double center = a[ (from + to) / 2 ];
	do
	{
		while ( i < to && ((center-a[i]) > 0) ) i++;
		while ( j > from && ((center-a[j]) < 0) ) j--;
		if (i < j)
		{
			//swap the indices
			int temp = indices[i]; indices[i] = indices[j]; indices[j] = temp;
			//swap the doubles
			float tempd = a[i]; a[i] = a[j]; a[j] = tempd;
		}
		if (i <= j) { i++; j--; }
	}
	while(i <= j);
	if (from < j) sort(indices,a, n,from, j);
	if (i < to) sort(indices,a, n, i, to);
}

/**
  Sort the array with increasing x values .
 */
void sampled_data_sort_x(struct sampled_data *sp)
{
	if (sp->nb_vals==1) return;
	int* indices=calloc(1,sp->nb_vals*sizeof(int));
	for (unsigned int i=0;i<sp->nb_vals;i++) indices[i]=(int)i;
	sort(indices,sp->x,(int)sp->nb_vals,0,(int)(sp->nb_vals-1));
	//copy the y array:
	float* y2=calloc(1,sp->nb_vals*sizeof(float));
	for (unsigned int i=0;i<sp->nb_vals;i++) y2[i]=sp->y[i];
	//replace the  y array:
	for (unsigned int i=0;i<sp->nb_vals;i++) sp->y[i]=y2[indices[i]];
	free(indices);
	free(y2);
}

/**
 *  linear  interpolation of sampled data
 */
float sampled_data_interpolate_linear(
		struct sampled_data* data,
		float x
		)
{
	float x1=0,x2=1,y1=0,y2=0,r=0;
	float sx;
	unsigned int i;
	unsigned int n=data->nb_vals;
//printf("x=%f   (xmin=%f xmax=%f)\n",x,data->x[0],data->x[n-1]);
	if (x<data->x[0]) r=0;
	else if (x==data->x[0]) r=data->y[0];
	else if (x>data->x[n-1]) r=0;
	else if (x==data->x[n-1]) r=data->y[n-1];
	else 
	{
		if (data->x_regular)
		{
			//the x values are equally spread, can go faster:
			sx=(data->x[n-1]-data->x[0])/(float)(n-1);
			int ii=(int)floor(x/sx);
			if (ii>0) i=(unsigned int)ii;else i=0;
			if (i>(n-1))
			{	
			printf("sampled_data_interpolate_linear, name=%s OUT OF RANGE\
					x=%f  sx=%f  i=%d  \n" ,data->name,x,sx,i);
			return 0;
			}
			else
			{
			x1=data->x[i];
			x2=data->x[i+1];
			}
		}
		else for (i=0;i<data->nb_vals-1;i++)
		{
			//scan all x values to find the interval
			x1=data->x[i];
			x2=data->x[i+1];
			//printf("i=%d x1= %f x2=%f\n",i,x1,x2);
			if ((x>x1)&&(x<=x2)) break;
		}
		y1=data->y[i];
		y2=data->y[i+1];
		r=y1+(y2-y1)/(x2-x1)*(x-x1);
//printf("i= %d x=%f x1=%f x2=%f y1=%f  y2=%f  ---> r=%f  \n",i,x,x1,x2,y1,y2,r);
	}
	return r;
}

/**
 *  return value of y only for existing x values, unless return 0
 */
float sampled_data_discrete(
		struct sampled_data* data,
		float x
		)
{
	if (data->nb_vals==1) return data->y[0];
	float r=0;
	for (unsigned int i=0;i<data->nb_vals;i++)
	{
		if (data->x[i]==x) r=data->y[i];
	}
	return r;
}

/**
 *  return the unique value y of the data for all  lambda
 */
float sampled_data_constant(
		struct sampled_data* data,
		float x 
		)
{
	float v=data->y[0];
	return v;
}

/**
 * return the index of the x value. -1 if not found.
 */
int sampled_data_index_of(
		struct sampled_data* data,
		float x
		)
{
	for (unsigned int i=0;i<data->nb_vals;i++)
	{
		if (data->x[i]==x) return (int)i;
	}
	return -1;
}



/**
 * merge the 2 signals. When x values do not match, create new ones.
 */
int sampled_data_add_merge_create(
		struct sampled_data** merged,
		struct sampled_data* data1,
		struct sampled_data* data2
		)
{
	//create a temporary sampled_data with enough values:
	struct sampled_data* merged_; 
	sampled_data_create3(&merged_,data1->nb_vals+data2->nb_vals);
	//scan data1 and collect all the x values
	for (unsigned int i=0;i<data1->nb_vals;i++)
	{
		merged_->x[i]=data1->x[i];
		merged_->y[i]=data1->y[i];
	}
	//sampled_data_print(data1,stdout);
	//sampled_data_print(data2,sdtdout);
	//scan data2 and add its x values to merged:
	unsigned int index=data1->nb_vals;
	for (unsigned int i=0;i<data2->nb_vals;i++)
	{
		float x=data2->x[i];
		//look for this x value in merged_:
		int index1=sampled_data_index_of(merged_,x);
		if (index1==-1)
		{
			//add this new x value:
			merged_->x[index]=x;
			merged_->y[index]=data2->y[i];
			index++;
		}
		else
		{
			//add y values:
			merged_->y[index1]+=data2->y[i];
		}
	}
	//sampled_data_print(merged_,stdout);
	//create a clean merge:
	sampled_data_create3(merged,index);
	for (unsigned int i=0;i<index;i++)
	{
		(*merged)->x[i]=merged_->x[i];
		(*merged)->y[i]=merged_->y[i];
	}
	//sort the x values:
	sampled_data_sort_x(*merged);
	//free the temporary sample:
	sampled_data_release(merged_);
	//sampled_data_print(*merged,stdout);
	//printf("\n\n");
	return 1;
}


/**
 * affect all data of data_in to data_out (even if different sizes)
 */
int sampled_data_set(
		struct sampled_data* data_out,
		struct sampled_data* data_in
		)
{
	//resize the sample data_out:
	free(data_out->x);
	free(data_out->y);
	data_out->nb_vals=data_in->nb_vals;
	data_out->x=calloc(1,sizeof(float)*data_in->nb_vals);
	data_out->y=calloc(1,sizeof(float)*data_in->nb_vals);
	for (unsigned int i=0;i<data_in->nb_vals;i++)
	{
		data_out->x[i]=data_in->x[i];
		data_out->y[i]=data_in->y[i];
	}
	return 1;
}


/**
 * add data to to_merge. Create new x values in to_merge if they don't exit.
 */
int sampled_data_add_merge(
struct sampled_data* to_merge,
		struct sampled_data* data
		)
{
	struct sampled_data* m;
	sampled_data_add_merge_create(&m,to_merge,data);
	sampled_data_set(to_merge,m);
	sampled_data_release(m);
	return 1;

}


void test_add_merge(void)
{
	struct sampled_data * sp;
	sampled_data_create_from_string(&sp,"450 1 550 1","toto");
	struct sampled_data * sp2;
	sampled_data_create_from_string(&sp2,"650 1","titi");
	sampled_data_print(sp,stdout);
	sampled_data_print(sp2,stdout);
	struct sampled_data * merge;
	sampled_data_create_from_string(&merge,"","merge");
	sampled_data_print(merge,stdout);
	sampled_data_add_merge(merge,sp);
	sampled_data_print(merge,stdout);
}



double sampled_data_integral(
		struct sampled_data* sp
		)
{
	double d=0;
	for (unsigned int i=0;i<sp->nb_vals;i++) d+=sp->y[i];
	return d;
}

float sampled_data_sampling(
		struct sampled_data* sp
		)
{
if (!sp->x_regular) printf("Error asked for sampling() for a non regular\
		sampled_data\n");
return (sp->x[sp->nb_vals-1]-sp->x[0])/(float)(sp->nb_vals-1);
}


void sampled_data_mul(
		struct sampled_data* sp,
		float f
		)
{
	for (unsigned int i=0;i<sp->nb_vals;i++) 
	{
	sp->y[i]*=f;
	}
}



void sampled_data_save(
		struct sampled_data* sd,
		char* filename
		)
{
	FILE* file1 = fopen(filename, "w");
	fprintf(file1,"%s\n",sd->name);
	for (unsigned int i=0;i<sd->nb_vals;i++)
	{
		fprintf(file1,"%f\t%f\n",sd->x[i],sd->y[i]);
	}
	fclose(file1);
}
