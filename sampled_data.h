/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SAMPLED_DATA_H_
#define SAMPLED_DATA_H_


#include "stdio.h"
#include "stdlib.h"
#include<stddef.h>

#include <star/ssp.h>

#include <mxml.h>

#include "string.h"
#include "parsing.h"
#include "sx_xml.h"

struct sampled_data
{
	char name[MAX_NAME_LENGTH];
	unsigned int nb_vals;
	float* x;
	float* y;
	int x_regular;//if 1 x values are equally spreaded
};

/**
* return an example of sampled data
*/
struct sampled_data* sampled_data_get_example(void);

/**
	* print the data for debug
	*/
void sampled_data_print(
		struct sampled_data * sd,
		FILE* stream
		);

/**
	* uniform function
	*/
float uniform(float lambda);

/**
	*linear  interpolation of sampled data
	*/
float sampled_data_interpolate_linear(
		struct sampled_data* data,
		float lambda
		);

/**
	*  return value of y only for existing x values, unless return 0
	*/
float sampled_data_discrete(
		struct sampled_data* data,
		float x
		);
/**
	* return a unique value for all lambdas:
	*/
float sampled_data_constant(
		struct sampled_data* data,
		float lambda
		);

void sampled_data_create2( 
		struct sampled_data ** p_p_sp,
		const unsigned int nbvals,
		const char* name
		);

void sampled_data_create3(
		struct sampled_data** spd,
		const unsigned int n
		);

int sampled_data_create_from_xml( 
		struct sampled_data ** p_p_sp,
		mxml_node_t *node
		);

void sampled_data_create_from_string(
		struct sampled_data ** sp,
		char *s,
		const char* name
		);

void sampled_data_create_inverse_cumulated_integral(
		struct sampled_data ** p_p_cumulated_integral,
		struct sampled_data * sp
		);

float sampled_data_sort_random_x(
		struct sampled_data * sp,
		struct ssp_rng* rng
		);

void sampled_data_release(
		struct sampled_data * sd
		);

int sampled_data_index_of(
		struct sampled_data* data,
		float x
		);

void sort(
		int* indices,
		float* a, 
		int n, 
		int from, 
		int to
		);

void sampled_data_sort_x(
		struct sampled_data *sp
		);

int sampled_data_add_merge_create(
		struct sampled_data** merged,
		struct sampled_data* data1,
		struct sampled_data* data2
		);

int sampled_data_set(
		struct sampled_data* data_out,
		struct sampled_data* data_in
		);

int sampled_data_add_merge(
		struct sampled_data* to_merge,
		struct sampled_data* data
		);

void test_add_merge(void);

double sampled_data_integral(
		struct sampled_data * sp
		);

float sampled_data_sampling(
		struct sampled_data* sp
		);

void sampled_data_mul(
		struct sampled_data* sp,
		float f
		);

void sampled_data_save(
		struct sampled_data* sd,
		char* filename
		);

#endif /* SAMPLED_DATA_H_ */
