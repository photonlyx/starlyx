
#include <mxml.h>
//#include <star/scad.h>

#include "sx_scene.h"
#include "sx_surface.h"
#include "sx_cad.h"
#include "sx_geometry.h"

#define  MAX_OPENSCAD_CODE_LENGTH 10000



/**
	save a disk shape in format OBJ  with normal in direction of axis (for blender):
    the name of the OBJ file is the camera name + ".obj"
	d: diameter
	n: nb of segments
	centre: centre of the disk (float[3])
	axis: axis of the disk (float[3])
	path: path of the file
	file: name of the file 
 */
int sx_cad_save_disk_obj(float d,int dim,float* centre,float* axis,char* path,char* file)
{
	//calc the points of the circle:
	float dAlpha;
	float frameX[]={1,0,0};// X vector of the local frame of the disk
	float frameY[]={0,1,0};// Y vector of the local frame of the disk
	float frameZ[]={0,0,1};// Z vector of the local frame of the disk
	float** pLocal=calloc(1,dim*sizeof(float*));//local coordinates, in the camera frame
	float** pGlobal=calloc(1,dim*sizeof(float*));//global coordinates, in the global frame
	//printf("sx_cad_save_disk_obj d=%f, dim=%d,centre=(%f,%f,%f), axis=(%f,%f,%f)\n",d,dim,
	//		centre[0],centre[1],centre[2],axis[0],axis[1],axis[2]);
	f3_normalize(axis,axis);
	f3_set(frameX,axis);
	sx_geometry_create_frame_from_X(frameX,frameY,frameZ);
	for (int i=0;i<dim;i++) 
	{
		pLocal[i]=calloc(1,3*sizeof(float));
		pGlobal[i]=calloc(1,3*sizeof(float));
		dAlpha=(float)PI*2/(float)dim;
		pLocal[i][0]=0;
		pLocal[i][1]=d/2*(float)cos((float)i*dAlpha);
		pLocal[i][2]=d/2*(float)sin((float)i*dAlpha);
		//printf("local %d  %f %f %f\n",i,pLocal[i][0],pLocal[i][1],pLocal[i][2]);
		//calc the coordinates in the global frame:
		sx_geometry_changeFrame(pGlobal[i],centre,frameX,frameY,frameZ,pLocal[i]);
	}
	//save to OBJ:
	char full_file_name[3*MAX_NAME_LENGTH];
	strcpy(full_file_name,path);
	//strcat(full_file_name,"/");
	strcat(full_file_name,file);
	if (!strstr(file,".obj"))	strcat(full_file_name,".obj");
#ifdef PRINT_ALL_STEPS
 printf("sx_cad_save_disk_obj: save %s\n",full_file_name);
#endif
	FILE* file1 = fopen(full_file_name, "w");
	//write the vertices:
	int i2=0;
	float x1,y1,z1,x2,y2,z2,x3,y3,z3;
	for (int i=0;i<dim;i++) 
	{
		i2=(i+1)%dim;
		x1=centre[0];
		y1=centre[1];
		z1=centre[2];
		x2=pGlobal[i][0];
		y2=pGlobal[i][1];
		z2=pGlobal[i][2];
		x3=pGlobal[i2][0];
		y3=pGlobal[i2][1];
		z3=pGlobal[i2][2];
		fprintf(file1,"v %f %f %f\n",x1, y1, z1);
		fprintf(file1,"v %f %f %f\n",x2, y2, z2);
		fprintf(file1,"v %f %f %f\n",x3, y3, z3);
	}
	//write the faces:
	int c=1;
	for (int i=0;i<dim;i++) 
	{
		fprintf(file1,"f %d %d %d\n",c,(c+1),(c+2));
		c+=3;
	}
	fclose(file1);
	//free memory
	for (int i=0;i<dim;i++) 
	{
		free(pLocal[i]);
		free(pGlobal[i]);
	}
	free(pLocal);
	free(pGlobal);
	return 1;
}



int sx_cad_launch_openscad(struct sx_scene* scene,mxml_node_t *node)
{
	//get the name attribute:
	char name[MAX_NAME_LENGTH];
	if (!read_string_attribute2(node,"NAME",name))return 0;

	//check if openscad is available:
	if (system("which openscad > /dev/null 2>&1")) 
	{
		printf("node %s, openscad is not available!\n",name);
		return 0;
	}

	char file[MAX_NAME_LENGTH];
	char code[MAX_OPENSCAD_CODE_LENGTH];
	char full_openscad_file_name[2*MAX_NAME_LENGTH];//openscad file name
	char full_stl_file_name[2*MAX_NAME_LENGTH];//stl file name
	//get the file attribute:
	if (!read_string_attribute23(node,"FILE",file,0,0,MAX_NAME_LENGTH))
	{
		//the attribute FILE is not available
		//get the code attribute:
		if (!read_string_attribute2(node,"CODE",code))return 0;
		//replace cotes ' by ":
		int i=0;
		for (;;)
			{
				if (code[i]=='\0') break;
				if (code[i]==39) code[i]=34;//repalce single cote by double cote
				i++;
			}	
		//save the .scad file:
		strcpy(full_openscad_file_name,scene->file_path);
		strcat(full_openscad_file_name,name);
		strcat(full_openscad_file_name,".scad");
		if (scene->verbose) printf("Save %s\n",full_openscad_file_name);
		//printf("CODE for openscad:\n %s\n",code);
		FILE* file1 = fopen(full_openscad_file_name, "w");
		fprintf(file1,"%s\n",code);
		fclose(file1);
	}
	else
	{
		printf("The CODE for openscad is taken from the file: %s\n",file);
		//the attribute FILE is available
		//get the openscad code from the file:
		strcpy(full_openscad_file_name,scene->file_path);
		strcat(full_openscad_file_name,file);
	}
	//create the stl filename:
	strcpy(full_stl_file_name,scene->file_path);
	strcat(full_stl_file_name,name);
	strcat(full_stl_file_name,".stl");
	//execute openscad to create the stl file:
	char command[200];
	strcpy(command,"openscad --export-format asciistl -q -o ");
	strcat(command,full_stl_file_name);
	strcat(command," ");
	strcat(command,full_openscad_file_name);
	if (scene->verbose) printf("Execute command: %s\n",command);
	int ret=system(command);
	if (ret) 
	{
		printf("Error executing openscad\n");
		return 0;
	}
	//create a surface and add it to the scene: 
	struct sx_surface* surface;
	sx_surface_create(&surface);
	strcpy(surface->name,name);
	strcpy(surface->file,full_stl_file_name);
	//find material_surface names:
	if (!read_string_attribute23(node,"MATERIALS",surface->mat_names,1,0
				,MAX_NAME_LENGTH))return 0;
	//add the surface to the scene:
	d_list_add(scene->surfaces,surface);
	surface->id=scene->surfaces->dim-1;
	return 1;
}

/**
 * create a box
 */
/*
int sx_cad_create_box_from_xml(struct sx_scene* scene,mxml_node_t *node)
{
	char name[MAX_NAME_LENGTH];
	char buffer[MAX_NAME_LENGTH];
	double pos[]={0,0,0};
	double d[]={1,1,1};
	if (!read_string_attribute2(node,"NAME",name))return 0;
	if (!read_string_attribute2(node,"X",buffer))return 0;
	sscanf(buffer,"%lf",&pos[0]);
	if (!read_string_attribute2(node,"Y",buffer))return 0;
	sscanf(buffer,"%lf",&pos[1]);
	if (!read_string_attribute2(node,"Z",buffer))return 0;
	sscanf(buffer,"%lf",&pos[2]);
	if (!read_string_attribute2(node,"DX",buffer))return 0;
	sscanf(buffer,"%lf",&d[0]);
	if (!read_string_attribute2(node,"DY",buffer))return 0;
	sscanf(buffer,"%lf",&d[1]);
	if (!read_string_attribute2(node,"DZ",buffer))return 0;
	sscanf(buffer,"%lf",&d[2]);
	printf("Create a box\n");
	struct scad_geometry* box=NULL;
	scad_initialize(NULL,NULL,3);
	scad_add_box(NULL,pos,d,&box);
	scad_scene_mesh();
	scad_stl_export(box,name,0,0);
	//create a surface and add it to the scene: 
	struct sx_surface* surface;
	sx_surface_create(&surface);
	strcpy(surface->name,name);
	//create the stl filename:
	char full_stl_file_name[2*MAX_NAME_LENGTH];//stl file name
	strcpy(full_stl_file_name,scene->file_path);
	strcat(full_stl_file_name,name);
	strcat(full_stl_file_name,".stl");
	strcpy(surface->file,full_stl_file_name);
	//find material_surface names:
	if (!read_string_attribute23(node,"MATERIALS",surface->mat_names,1,0,
				MAX_NAME_LENGTH))return 0;
	//add the surface to the scene:
	surface->id=scene->nb_surfaces;
	scene->surfaces[scene->nb_surfaces++]=surface;
	return 1;
}
*/

/**
 * create a cylinder
 */
/*
int sx_cad_create_cyl_from_xml(struct sx_scene* scene,mxml_node_t *node)
{
	char name[MAX_NAME_LENGTH];
	char buffer[MAX_NAME_LENGTH];
	double xyz[]={0,0,0};
	double axis[]={0,0,1};//TODO
	double radius;//radius
	double h;//height
	double angle=0;
	if (!read_string_attribute2(node,"NAME",name))return 0;
	if (!read_string_attribute2(node,"R",buffer))return 0;
	sscanf(buffer,"%lf",&radius);
	if (!read_string_attribute2(node,"H",buffer))return 0;
	sscanf(buffer,"%lf",&h);
	if (!read_string_attribute2(node,"ANGLE",buffer))return 0;
	sscanf(buffer,"%lf",&angle);
	printf("Create a cylinder\n");
	struct scad_geometry* geo=NULL;
	scad_initialize(NULL,NULL,3);
	scad_add_cylinder(NULL,xyz,axis,radius,angle,&geo);
	scad_scene_mesh();
	scad_stl_export(geo,name,0,0);
	//create a surface and add it to the scene: 
	struct sx_surface* surface;
	sx_surface_create(&surface);
	strcpy(surface->name,name);
	//create the stl filename:
	char full_stl_file_name[2*MAX_NAME_LENGTH];//stl file name
	strcpy(full_stl_file_name,scene->file_path);
	strcat(full_stl_file_name,name);
	strcat(full_stl_file_name,".stl");
	strcpy(surface->file,full_stl_file_name);
	//find material_surface names:
	if (!read_string_attribute23(node,"MATERIALS",surface->mat_names,1,0,
				MAX_NAME_LENGTH))return 0;
	//add the surface to the scene:
	surface->id=scene->nb_surfaces;
	scene->surfaces[scene->nb_surfaces++]=surface;
	return 1;
}
*/

/**
 * create a disk
 */
/*
int sx_cad_create_disk_from_xml(struct sx_scene* scene,mxml_node_t *node)
{
	char name[MAX_NAME_LENGTH];
	char buffer[MAX_NAME_LENGTH];
	double xyz[]={0,0,0};
	//double axis[]={0,0,1};//TODO
	double radius;//radius
	if (!read_string_attribute2(node,"NAME",name))return 0;
	if (!read_string_attribute2(node,"R",buffer))return 0;
	sscanf(buffer,"%lf",&radius);
	printf("Create a disk\n");
	struct scad_geometry* geo=NULL;
	scad_initialize(NULL,NULL,3);
	scad_add_disk(NULL,xyz,radius,&geo);
	scad_scene_mesh();
	scad_stl_export(geo,name,0,0);
	//create a surface and add it to the scene: 
	struct sx_surface* surface;
	sx_surface_create(&surface);
	strcpy(surface->name,name);
	//create the stl filename:
	char full_stl_file_name[2*MAX_NAME_LENGTH];//stl file name
	strcpy(full_stl_file_name,scene->file_path);
	strcat(full_stl_file_name,name);
	strcat(full_stl_file_name,".stl");
	strcpy(surface->file,full_stl_file_name);
	//find material_surface names:
	if (!read_string_attribute23(node,"MATERIALS",surface->mat_names,1,0,
				MAX_NAME_LENGTH))return 0;
	//add the surface to the scene:
	surface->id=scene->nb_surfaces;
	scene->surfaces[scene->nb_surfaces++]=surface;
	return 1;
}
*/


