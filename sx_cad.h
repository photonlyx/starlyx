
/*
Copyright (C) 2023 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SX_CAD_H_
#define SX_CAD_H_


#include "sx_xml.h"

int sx_cad_save_disk_obj(float d,int dim,float* centre,float* axis,char* path,char* file);
int sx_cad_launch_openscad(struct sx_scene* scene,mxml_node_t *node);
//int sx_cad_create_box_from_xml(struct sx_scene* scene,mxml_node_t *node);
//int sx_cad_create_cyl_from_xml(struct sx_scene* scene,mxml_node_t *node);
//int sx_cad_create_disk_from_xml(struct sx_scene* scene,mxml_node_t *node);



#endif /* SX_CAD_H_ */
