/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <rsys/float3.h>

#include "sx_camera.h"
#include "sx_image.h"
#include "sx_path_source.h"
#include "sampled_data.h"
#include "sx_xml.h"
#include "sx_scene.h"
#include "sx_geometry.h"
#include "sx_volume.h"
#include "sx_surface.h"
#include "sx_cad.h"



void sx_camera_create(struct sx_camera** camera)
{
	(*camera)=calloc(1,sizeof(struct sx_camera));
	(*camera)->cameraAngularFieldDegrees=0;
	(*camera)->gamma=1;
	(*camera)->max=-1;
	(*camera)->nbPixelsX=1;
	(*camera)->nbPixelsY=1;
	(*camera)->nb_filters=0;
	(*camera)->index_in_path_sources=0;
	(*camera)->focal=1;
	(*camera)->na=0;
	(*camera)->d=0;
	(*camera)->x=0;
	(*camera)->xp=0;
	(*camera)->sensor_center[0]=0;
	(*camera)->sensor_center[1]=0;
	(*camera)->sensor_center[2]=0;
	(*camera)->sensor_sizeX=1;
	(*camera)->sensor_sizeY=1;
	(*camera)->sx=0;
	(*camera)->sy=0;
}


/**
 *  create from xml:
 */
int sx_camera_create_from_xml(struct sx_camera** camera_,mxml_node_t *node)
{
	struct sx_camera* camera;
	sx_camera_create(&camera);
	char buffer[MAX_NAME_LENGTH];
	//get the camera name
	if (!read_string_attribute2(node,"NAME",buffer))return 0;
	strcpy(camera->name,buffer);
	//get the volume where is the camera ("WORLD" by default)
	if (read_string_attribute2(node,"VOLUME",buffer))
	{
		strcpy(camera->volume_name,buffer);
	}
	else
	{
		strcpy(camera->volume_name,"World");
		printf("Set to default (%s)\n",camera->volume_name);
	}

	//get the filters list:
	if (!read_string_attribute2(node,"FILTERS",buffer))return 0;
	strcpy(camera->filters_list,buffer);

	if (!read_string_attribute2(node,"NBPIXELSX",buffer))return 0;
	sscanf(buffer,"%d",&camera->nbPixelsX);
	if (!read_string_attribute2(node,"NBPIXELSY",buffer))return 0;
	sscanf(buffer,"%d",&camera->nbPixelsY);
	if (!read_string_attribute2(node,"FIELD",buffer))return 0;
	sscanf(buffer,"%f",&camera->cameraAngularFieldDegrees);
	//camera->gamma=1;//default value if attribute not found
	if (read_string_attribute2(node,"GAMMA",buffer)) sscanf(buffer,"%f",&camera->gamma);
	else  printf("GAMMA set to default (%g)\n",camera->gamma);
	if (read_string_attribute2(node,"MAX_RADIANCE",buffer)) sscanf(buffer,"%f",&camera->max);
	else  printf("MAX_RADIANCE set to default (%g)\n",camera->max);
	if (read_string_attribute2(node,"FOCAL",buffer)) sscanf(buffer,"%f",&camera->focal);
	else  printf("FOCAL set to default (%g mm)\n",camera->focal);
	if (read_string_attribute2(node,"NA",buffer)) sscanf(buffer,"%f",&camera->na);
	else  printf("Numerical aperture NA set to default (%g)\n",camera->na);

	//find lens
	mxml_node_t* lens=mxmlFindElement(node,node,"lens",NULL,NULL,MXML_DESCEND);
	if (lens)
	{
		float x,y,z;
		if (!read_string_attribute2(lens,"X",buffer))return 0;
		sscanf(buffer,"%f",&x);
		if (!read_string_attribute2(lens,"Y",buffer))return 0;
		sscanf(buffer,"%f",&y);
		if (!read_string_attribute2(lens,"Z",buffer))return 0;
		sscanf(buffer,"%f",&z);
		camera->lens[0]=x;
		camera->lens[1]=y;
		camera->lens[2]=z;
	}
	else
	{
		printf("camera %s has no son of type lens\n",camera->name);
		return 0;
	}
	//find viewPoint
	mxml_node_t* vp=mxmlFindElement(node,node,"viewPoint",NULL,NULL,MXML_DESCEND);
	if (vp)
	{
		float x,y,z;
		if (!read_string_attribute2(vp,"X",buffer))return 0;
		sscanf(buffer,"%f",&x);
		if (!read_string_attribute2(vp,"Y",buffer))return 0;
		sscanf(buffer,"%f",&y);
		if (!read_string_attribute2(vp,"Z",buffer))return 0;
		sscanf(buffer,"%f",&z);
		camera->viewPoint[0]=x;
		camera->viewPoint[1]=y;
		camera->viewPoint[2]=z;
		//check if lens and view point are not superimposed:
		float lens_view_point[3];
		f3_sub(lens_view_point,camera->viewPoint,camera->lens);
		if (f3_len(lens_view_point)<0.0001)
		{
			printf("camera %s: lens and viewPoint are superimposed \n",camera->name);
			return 0;
		}
	}
	else
	{
		printf("camera %s has no son of type viewPoint\n",camera->name);
		return 0;
	}
	(*camera_)=camera;
	return 1;
}




void sx_camera_release(struct sx_camera* camera)
{
/*
	for (int k=0;k<camera->nb_filters;k++)
	{
		sampled_data_release(camera->filters[k]);
	}
*/
	free(camera);
}


void sx_camera_print(
		struct sx_camera* camera,
		FILE* stream
		)
{
	fprintf(stream,"Camera %s:\n",camera->name);
	fprintf(stream,"\tcameraAngularFieldDegrees=%f\n",camera->cameraAngularFieldDegrees);
	fprintf(stream,"\tnbPixelsX=%d\n",camera->nbPixelsX);
	fprintf(stream,"\tnbPixelsY=%d\n",camera->nbPixelsY);
	fprintf(stream,"\tlens=(%f,%f,%f)\n",camera->lens[0],camera->lens[1],camera->lens[2]);
	fprintf(stream,"\tviewPointX=(%f,%f,%f)\n",camera->viewPoint[0],camera->viewPoint[1],
			camera->viewPoint[2]);
	fprintf(stream,"\tfilters list=%s\n",camera->filters_list);
	for (int i=0;i<camera->nb_filters;i++)sampled_data_print(camera->filters[i],stream);
	fprintf(stream,"\t\tsensor_sizeX=%fmm sensor_sizeY=%fmm focal=%fmm na=%f d=%fmm x=%fmm , xp=%fmm  \n",
			camera->sensor_sizeX,camera->sensor_sizeY,camera->focal,
			camera->na,camera->d,camera->x,camera->xp);
	fprintf(stream,"\t\tlens=(%g,%g,%g)\n",camera->lens[0],camera->lens[1],camera->lens[2]);
	fprintf(stream,"\t\tcamX=(%g,%g,%g)\n",camera->frameX[0],camera->frameX[1],camera->frameX[2]);
	fprintf(stream,"\t\tcamY=(%g,%g,%g)\n",camera->frameY[0],camera->frameY[1],camera->frameY[2]);
	fprintf(stream,"\t\tcamZ=(%g,%g,%g)\n",camera->frameZ[0],camera->frameZ[1],camera->frameZ[2]);
	fprintf(stream,"\t\tfocal point=(%g,%g,%g)\n",camera->focal_point[0],
			camera->focal_point[1],camera->focal_point[2]);
}


/**
  calculate the internal geometry of the camera
 */
int sx_camera_configure(struct sx_camera* camera)
{
	//sensor size along X axis:
	float halfCamSize=camera->focal*tanf((float)PI/180.0f*camera->cameraAngularFieldDegrees/2.0f);
	camera->sensor_sizeX=halfCamSize*2;
	//sensor size along Y axis:
	camera->sensor_sizeY=(float)camera->nbPixelsY/(float)camera->nbPixelsX*camera->sensor_sizeX;
	//if just one pixel, the sensor size is zero:
	if (camera->nbPixelsX==1) camera->sensor_sizeX=0;
	if (camera->nbPixelsY==1) camera->sensor_sizeY=0;
	//calc the size of the pixels:
	if (camera->nbPixelsX>1) camera->sx=camera->sensor_sizeX/(float)(camera->nbPixelsX-1);
	if (camera->nbPixelsY>1) camera->sy=camera->sensor_sizeY/(float)(camera->nbPixelsY-1);
	//vector from lens to view point:
	float lens_viewPoint[3];
	//distance from lens to view point:
	f3_sub(lens_viewPoint,camera->viewPoint,camera->lens);
	camera->x=f3_len(lens_viewPoint);
	//use conjugate formula to calculate the image distance:
	camera->xp=powf(camera->focal,2)/(camera->x-camera->focal)+camera->focal;
	//calc the diameter of the aperture:
	camera->d=tanf(asinf(camera->na))*2.0f*camera->focal;
	//set the local frame of the camera:
	f3_sub(camera->frameX,camera->viewPoint,camera->lens); //put X local frame parallel to (lens,view point)
	f3_normalize(camera->frameX,camera->frameX);
	sx_geometry_create_frame_from_X_with_Z_vertical(camera->frameX,camera->frameY,camera->frameZ);
	//calc the position of the sensor center (it is transparent and in front of the lens !)
	float dir_focal[3];
	f3_mulf(dir_focal,camera->frameX,-camera->xp);//the optical axis is X
	f3_add(camera->sensor_center,camera->lens,dir_focal);
	//calc the position of the image focal point:
	float v_focal[3];
	f3_mulf(v_focal,camera->frameX,-camera->focal);
	f3_add(camera->focal_point,camera->lens,v_focal);
	//sx_camera_print(camera);
	return 1;
}

/**
  create a path_source for each pixel
 */
int sx_camera_create_sx_path_sources(struct sx_camera* camera,struct sx_scene* scene)
{
	struct sx_path_source** sx_path_sources_;// array of pointers to sx_path_sources
	sx_camera_configure(camera);
	// number of sx_path_sources
	int nbChannels=camera->nb_filters;
	int nb_sx_path_sources=camera->nbPixelsX*camera->nbPixelsY*nbChannels;
	// allocate memory for the sx_path_sources
	sx_path_sources_=calloc(1,nb_sx_path_sources*sizeof(struct sx_path_source*));

	if (scene->verbose)
	{
		unsigned long int sx_path_source_mem_size_mb=(sizeof(struct sx_path_source)
				+2*sizeof(double))/(1024*1024);
		if (sx_path_source_mem_size_mb>100)
			printf("Allocate camera paths sources (mem size: %ld Mb) ! \n",
					(nb_sx_path_sources*sx_path_source_mem_size_mb));
	}
	//allocate the path sources:
	for (int i=0;i<nb_sx_path_sources;i++)
	{
		struct sx_path_source* sx_path_source=calloc(1,sizeof(struct sx_path_source));
		sx_path_source->weight=calloc(1,1*sizeof(double));//one dimensional weight
		sx_path_source->sigma=calloc(1,1*sizeof(double));//same as weight
		sx_path_sources_[i]=sx_path_source;
	}
	//create the wavelength sorters:
	struct ssp_ranst_discrete* wavelength_sorter_[nbChannels];
	for (int k=0;k<nbChannels;k++)
	{
		sx_path_source_create_wavelength_sorter(&wavelength_sorter_[k],camera->filters[k]);
	}

	if (scene->verbose) printf("Camera %s: starting volume name :%s\n",
			camera->name,camera->volume_name);
	struct sx_volume* starting_volume=sx_scene_find_volume(scene,camera->volume_name);
	if (starting_volume==NULL)
	{
		return 0;
	}
	int index=0;
	for (int i=0;i<camera->nbPixelsX;i++)
	{
		for (int j=0;j<camera->nbPixelsY;j++)
		{
			for (int k=0;k<nbChannels;k++)
			{
				index=(i+camera->nbPixelsX*j)*nbChannels+k;
				struct sx_path_source* ps=sx_path_sources_[index];
				ps->spectrum=camera->filters[k];
				ps->wavelength_sorter=wavelength_sorter_[k];
				ps->x=i;
				ps->y=j;
				//set the starting volume:
				ps->starting_volume=starting_volume;
				ps->type=PATH_SOURCE_TYPE_CAMERA;
				ps->camera=camera;
				ps->weight_dim=1;
				strcpy(ps->name,"path source of ");
				strcat(ps->name,camera->name);
			}
		}
	}
	//add the sx_path_sources of this camera to the scene list:
	camera->index_in_path_sources=scene->sx_path_source_index;
	for (int i=0;i<nb_sx_path_sources;i++)
	{
		//printf("Allocate sx_path_source %d\n",i);
		d_list_add(scene->path_sources,sx_path_sources_[i]);
	}
	free(sx_path_sources_);
	return 1;
}





/**
 * calc the multispectral float image for the case of reverse algorithm. Format:
 *  RGB     RGB      RGB .........................   RGB
 *  RGB     RGB      RGB .........................   RGB
 *  RGB     RGB      RGB .........................   RGB
 *  RGB     RGB      RGB .........................   RGB
 */
int sx_camera_calc_image_algo_reverse(struct sx_image** image,
		//struct sx_path_source** path_sources,
		struct d_list* path_sources,
		struct sx_camera* camera,
		int save_sigma)
{
	sx_image_create(image,camera->nbPixelsX,camera->nbPixelsY,camera->nb_filters);
	//printf("-Multispectral image: (%dx%d) %d channels\n",(*image)->w,(*image)->h,(*image)->nbc);
	unsigned int w=(*image)->w;
	unsigned int nbc=(*image)->nbc;
	unsigned int i,j,k;
	unsigned int index=0;
	float val=0;
	for (i=0;i<(*image)->w;i++)
	{
		for (j=0;j<(*image)->h;j++)
		{
			for (k=0;k<(*image)->nbc;k++) //scan the wavelengths
			{
				index=(i+j*w)*nbc+k;
				struct sx_path_source* ps=path_sources->data[camera->index_in_path_sources+index];
				if (save_sigma) val=(float)ps->sigma[0]; else val=(float)ps->weight[0];
				(*image)->data[index]=val;
				//printf("i=%d j=%d k=%d %g\n",i,j,k,val);
			}
		}
	}
	//fill the wavelengths:
	for (k=0;k<(*image)->nbc;k++) //scan the wavelengths
	{
		//take the camera filter of index k:
		struct sampled_data* filter=camera->filters[k];
		//take the wavelength of this filter TODO: why just the first wavelength ??
		(*image)->lambdas[k]=filter->x[0];
	}
	return 1;
}





/**
 * calc the multispectral float image for the case of direct algorithm. Format:
 *  RGB     RGB      RGB .........................   RGB
 *  RGB     RGB      RGB .........................   RGB
 *  RGB     RGB      RGB .........................   RGB
 *  RGB     RGB      RGB .........................   RGB
 */
int sx_camera_calc_image_algo_direct(
		struct sx_image** image,
		struct sx_path_source* path_source,
		int index_in_weight,
		struct sx_camera* camera,
		int save_sigma
		)
{
	sx_image_create(image,camera->nbPixelsX,camera->nbPixelsY,camera->nb_filters);
	//printf("-Multispectral image: (%dx%d) %d channels\n",(*image)->w,(*image)->h,(*image)->nbc);
	unsigned int w=(*image)->w;
	unsigned int nbc=(*image)->nbc;
	unsigned int i,j,k;
	unsigned int index=0;
	float val=0;
	for (i=0;i<(*image)->w;i++)
	{
		for (j=0;j<(*image)->h;j++)
		{
			for (k=0;k<(*image)->nbc;k++) //scan the wavelengths
			{
				index=k+(i+j*w)*nbc;
				if (save_sigma) val=(float)path_source->sigma[index_in_weight+index];
				else val=(float)path_source->weight[index_in_weight+index];
				(*image)->data[index]=val;
			}
		}
	}
	//fill the wavelengths:
	for (k=0;k<(*image)->nbc;k++) //scan the wavelengths
	{
		//take the camera filter of index k:
		struct sampled_data* filter=camera->filters[k];
		//take the wavelength of this filter TODO: why just taking lambda[0] of the channel's filter ?
		(*image)->lambdas[k]=filter->x[0];
	}
	return 1;
}

/**
 * find the spectra of each camera channel. For numerical values, create monochromatic spectra.
 */
int sx_camera_connect_spectra(struct sx_camera* cam,struct sx_scene* scene)
{
	char** list;
	int nb_words=0;
	//replace the end of line by spaces:
	parsing_replace(cam->filters_list,'\n',' ');
	parsing_parse_words(cam->filters_list,&list,&nb_words);
	if (nb_words>=1)
	{
		cam->nb_filters=nb_words;
		cam->filters=calloc(1,sizeof (struct sampled_data*)*nb_words);
		for (int i=0;i<nb_words;i++)
		{
			struct sampled_data* spectrum;
			//if it is a number, create a spectrum with one point with wavelength this number (weight 1):
			int is_numerical=parsing_is_numerical_value(list[i]);
			if (is_numerical)
			{
				float value;
				sscanf(list[i],"%f",&value);
				//create a spectrum with a unique value
				sampled_data_create2( &spectrum,1,"monochromatic");
				spectrum->x[0]=value;
				spectrum->y[0]=1;
			}
			else
			{
				//find the spectrum_name:
				spectrum=sx_scene_find_spectrum(scene,list[i]);
				if (spectrum==NULL)
				{
					printf("camera	: %s : can't find filter spectrum of name %s\n", cam->name ,list[i]);
					return 0;
				}
			}
			cam->filters[i]=spectrum;
		}
	}
	else //no filter for camera
	{
		printf("Please indicate at least one filter for the camera in an attribute called FILTERS (%s) \n",cam->filters_list);
		return 0;
	}
	free(list);
	return 1;
}




//CAMERA_DIRECT:
/**
 * calc the pixel hitted for a given photon entering in the aperture
 * pixel_index :  index of the pixel hitted i+w*j
 * hit_global : position of the photon hit into the sensor in global frame
 * hit_local : position of the photon hit into the sensor in the local frame of the camera (frameX,frameY,frameZ)
 * photon: the photon entering in the camera
 */
int sx_camera_calc_pixel_hit(struct sx_camera* camera, int* pixel_index,float hit_global[3],float hit_local[2],struct sx_photon* photon)
{
	float pf[3];//hit point in the focal point in global coordinates
				//get the hit of a parallel ray passing through the lens center with the camera's focal plane:
	sx_geometry_intersection_plane_line(pf,camera->focal_point,camera->frameX,camera->lens,photon->dir);
	//calc the direction of the image ray (pf-photon->pos)
	float dir1[3];//direction of the image ray
	f3_sub(dir1,pf,photon->x);
	//calc the hit of the image ray with the sensor plane:
	sx_geometry_intersection_plane_line(hit_global,camera->sensor_center,camera->frameX,photon->x,dir1);
	//calc the local coordinates of the hit:
	float p[3];//vector sensor_center to hit point in sensor (hit-sensor_center)
	f3_sub(p,hit_global,camera->sensor_center);
	hit_local[0]=f3_dot(camera->frameY,p);
	hit_local[1]=f3_dot(camera->frameZ,p);
	/*
	   printf("lens   %f %f %f\n",camera->lens[0],camera->lens[1],camera->lens[2]);
	   printf("camera->sensor_center  %f %f %f\n",camera->sensor_center[0],camera->sensor_center[1],camera->sensor_center[2]);
	   printf("camera->sensor_size  %f %f\n",camera->sensor_sizeX,camera->sensor_sizeY);
	   printf("camera pixel size :  %f %f\n",camera->sx,camera->sy);
	   printf("hit global  %f %f %f\n",hit_global[0],hit_global[1],hit_global[2]);
	   printf("coord local  %f %f\n",hit_local[0],hit_local[1]);
	 */
	if ( (fabs(hit_local[0])<=(camera->sensor_sizeX/2.0)) &&
			(fabs(hit_local[1])<=(camera->sensor_sizeY/2.0)) )//check if the ray hits the sensor
	{
		//calc the pixel index (i,j):
		int i=0,j=0;
		if (camera->sx>0) i=(int)((hit_local[0]+camera->sensor_sizeX/2)/camera->sx);
		if (camera->sy>0) j=(int)((hit_local[1]+camera->sensor_sizeY/2)/camera->sy);
		//calc the pixel index:
		if ((i<0)||(i>=camera->nbPixelsX)||(j<0)||
				(j>=camera->nbPixelsY))(*pixel_index)=-1; 
		else (*pixel_index)=i+j*camera->nbPixelsX;
		//printf("i=%d  j=%d (index=%d) \n",i,j,*pixel_index);
	}
	else
	{
		(*pixel_index)=-1;
		//printf("(index=%d) \n",*pixel_index);
	}
	return 1;
}


/**
 *   create a surface object corresponding to the camera aperture and save the OBJ file
 */
int sx_camera_create_aperture_surface(
		struct sx_camera* camera,
		struct sx_scene* scene,
		struct sx_surface** aperture_surface_
		)
{
	//save an obj file corresponding to the lens aperture:
	//name of the aperture surface:
	char name[MAX_NAME_LENGTH];
	strcpy(name,camera->name);
	strcat(name,"_aperture");
	//name of the obj file:
	char file[MAX_NAME_LENGTH];
	strcpy(file,name);
	strcat(file,".obj");
	sx_camera_configure(camera);
	sx_cad_save_disk_obj(camera->d,50,camera->lens,camera->frameX,scene->file_path,file);
	//create the surface object:
	//add this surface to the scene:
	struct sx_surface* aperture_surface;
	sx_surface_create(&aperture_surface);
	strcpy(aperture_surface->name,name);
	//affect the OBJ file name:
	strcpy(aperture_surface->file,file);
	aperture_surface->volume1_id=0;
	aperture_surface->volume2_id=0;
	strcpy(aperture_surface->mat_names,"");
	aperture_surface->mat=NULL;
	aperture_surface->camera=camera;
	(*aperture_surface_)=aperture_surface;
	return 1;
}


/**
  create a new photon (reverse) starting from the camera aperture
  pixel_x,int pixel_y : the camera pixel
 */
void sx_camera_create_reverse_photon(
		struct sx_camera* camera,
		struct sx_photon** photon_,
		int pixel_x,int pixel_y,
		struct ssp_rng* rng)
{
	float pixelLocal[3]; //local coordinates of the pixel in the camera frame
	float pixelGlobal[3];//global coord of the pixel
	float dir[3];//direction from pixel to lens 
	int i=pixel_x;
	int j=pixel_y;

	//calc the local coord of the pixel
	//optical axis is X:
	pixelLocal[0]=0;
	//horizontal axis is Y:
	pixelLocal[1]=(-camera->sensor_sizeX/2.0f+(float)i*camera->sx);
	//vertical axis is Z:
	pixelLocal[2]=(-camera->sensor_sizeY/2.0f+(float)j*camera->sy);
	//calc the global coord of the pixel:
	sx_geometry_changeFrame(pixelGlobal,camera->sensor_center,camera->frameX,
			camera->frameY,camera->frameZ,pixelLocal);
	//calc the direction pixel to lens
	f3_sub(dir,pixelGlobal,camera->lens);
	f3_normalize(dir,dir);

	float p_aperture[3],p1[3],pPlaneObject[3],p[3],vObject[3];
	struct sx_photon* photon;
	sx_photon_create(&photon);
	//sort a position in the aperture of centre diametre d:
	if (camera->d>0)
	{
		ssp_ran_uniform_disk_float(rng,camera->d/2,camera->frameX,p_aperture,NULL);
		f3_add(p_aperture,camera->lens,p_aperture);
	}
	else
	{
		f3_set(p_aperture,camera->lens);
	}
	//printf("***sx_path_camera.  frameX=(%g,%g,%g)\n", camera->frameX[0],camera->frameX[1],camera->frameX[2]);
	//printf("***sx_path_camera.  p_aperture=(%g,%g,%g)\n", p_aperture[0],p_aperture[1],p_aperture[2]);
	//calc the vector lens->object plane centre:
	f3_mulf(p1,camera->frameX,camera->x);
	//calc the object plane center:
	f3_add(pPlaneObject,camera->lens,p1);
	//get the hit of the ray with the camera's object plane:
	sx_geometry_intersection_plane_line(p,pPlaneObject,camera->frameX,camera->lens,dir);
	//printf("***sx_camera.  point at object plane=(%g,%g,%g)\n", p[0],p[1],p[2]);
	//calc the vector from aperture to object plane:
	f3_sub(vObject,p,p_aperture);
	//normalise it and affect the photon direction:
	f3_normalize(photon->dir,vObject);
	//add direction dispersion to the path to simulate pixel size
	//sort a point in a sphere cap of half angle: delta->dir
	//approximation of pixel size
	float delta_dir=(camera->sx+camera->sy)/2.0f/2.0f/camera->focal;
	//sort a direction around photon->dir in a cone of half angle delta_dir:
	float aperture=cosf(delta_dir);
	ssp_ran_sphere_cap_uniform_float(rng,photon->dir,aperture,photon->dir,NULL);
	//printf("***sx_camera.  photon->dir=(%g,%g,%g)\n", photon->dir[0],photon->dir[1],photon->dir[2]);
	//affect the photon position:
	f3_set(photon->x,p_aperture);
	//it is a reverse photon set the radiance to 0:
	photon->w=0;
	//printf("***sx_camera.  photon->x=(%g,%g,%g)\n", photon->x[0],photon->x[1],photon->x[2]);
	(*photon_)=photon;
}
