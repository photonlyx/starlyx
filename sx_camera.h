/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SX_CAMERA_H_
#define SX_CAMERA_H_


#include "sx_xml.h"
#include "sx_scene.h"


struct mxml_node_t;
struct sx_image;
struct sx_path_source;
struct sx_photon;

struct sx_camera
{
	char name[MAX_NAME_LENGTH];
	char volume_name[MAX_NAME_LENGTH];
	char t[MAX_NAME_LENGTH];
	float cameraAngularFieldDegrees;
	float gamma;//gamma non linear correction
	float max;//radiance in W/sr/m² corresponding to grey value of 255 for PNG export
	int nbPixelsX;
	int nbPixelsY;
	float lens[3];
	float viewPoint[3];
	//char cameraImageFileName[MAX_NAME_LENGTH];//HDR image file name
	char filters_list[MAX_NAME_LENGTH];
	int nb_filters;
	struct sampled_data **filters;
	long unsigned int index_in_path_sources;
	float focal;//focal length in mm
	float na; //numerical aperture: sin(half angle of diaphragm seen from sensor centre)
	float d;//aperture diameter in mm
	float x;//distance from view point to lens centre
	float xp;//distance from lens centre to sensor centre
			 // the local frame of the camera such that it looks at a view point:
	float frameX[3];// the optical axis of the camera is the local X vector
	float frameY[3];// the pixels horizontal axis is the Y local vector
	float frameZ[3];// the pixels vertical axis is the Z local vector
	float sensor_center[3];
	float focal_point[3];//absolute position of the image focal point
	float sensor_sizeX;
	float sensor_sizeY;
	float sx;//horizontal size of the pixel 
	float sy;//vertical size of the pixel 
};


void sx_camera_create(
		struct sx_camera** camera);

int sx_camera_create_from_xml(
		struct sx_camera** p_p_camera,
		mxml_node_t *node);

void sx_camera_release(
		struct sx_camera* sx_camera
		);

int sx_camera_create_sx_path_sources(
		struct sx_camera* camera,
		struct sx_scene* scene
		);

int sx_camera_calc_image_algo_reverse(
		struct sx_image** image,
		struct d_list* probes,
		struct sx_camera* camera,
		int save_sigma
		);

int sx_camera_calc_image_algo_direct(
		struct sx_image** image,
		struct sx_path_source* path_source,
		int index_in_weight,
		struct sx_camera* camera,
		int save_sigma
		);

void sx_camera_print(
		struct sx_camera* camera,
		FILE* stream
		);

void changeFrame(
		float* res,
		float* O,
		float* X,
		float* Y,
		float *Z,
		float* vloc
		);

int sx_camera_connect_spectra(
		struct sx_camera* cam,
		struct sx_scene* scene
		);

int sx_camera_configure(
		struct sx_camera* camera
		);

int sx_camera_calc_pixel_hit(
		struct sx_camera* camera, 
		int* pixel_index,
		float hit[3],
		float hit_local[2],
		struct sx_photon* photon
		);

int sx_camera_create_aperture_surface(
		struct sx_camera* camera,
		struct sx_scene* scene,
		struct sx_surface** as_
		);

void sx_camera_create_reverse_photon(
		struct sx_camera* camera,
		struct sx_photon** photon,
		int pixel_x,int pixel_y,
		struct ssp_rng* rng
);

#endif /* SX_CAMERA_H_ */
