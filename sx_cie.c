/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sx_cie.h"
#include "sampled_data.h"



// CIE 1931 color matching function for high wavelengths (red)
void sx_cie_create_xbar(struct sampled_data ** xbar)
{
	char* s=" 380.0 0.001368 385.0 0.002236 390.0 0.004243 395.0 0.00765 400.0 0.01431 405.0 0.02319 410.0 0.04351 415.0 0.07763 420.0 0.13438 425.0 0.21477 430.0 0.2839 435.0 0.3285 440.0 0.34828 445.0 0.34806 450.0 0.3362 455.0 0.3187 460.0 0.2908 465.0 0.2511 470.0 0.19536 475.0 0.1421 480.0 0.09564 485.0 0.05795 490.0 0.03201 495.0 0.0147 500.0 0.0049 505.0 0.0024 510.0 0.0093 515.0 0.0291 520.0 0.06327 525.0 0.1096 530.0 0.1655 535.0 0.22575 540.0 0.2904 545.0 0.3597 550.0 0.43345 555.0 0.51205 560.0 0.5945 565.0 0.6784 570.0 0.7621 575.0 0.8425 580.0 0.9163 585.0 0.9786 590.0 1.0263 595.0 1.0567 600.0 1.0622 605.0 1.0456 610.0 1.0026 615.0 0.9384 620.0 0.85445 625.0 0.7514 630.0 0.6424 635.0 0.5419 640.0 0.4479 645.0 0.3608 650.0 0.2835 655.0 0.2187 660.0 0.1649 665.0 0.1212 670.0 0.0874 675.0 0.0636 680.0 0.04677 685.0 0.0329 690.0 0.0227 695.0 0.01584 700.0 0.011359 705.0 0.008111 710.0 0.00579 715.0 0.004109 720.0 0.002899 725.0 0.002049 730.0 0.00144 735.0 0.001 740 0.000690 745 0.000476 750 0.000332 755 0.000235 760 0.000166 765 0.000117 770 0.000083 775 0.000059 780 0.000042";
	sampled_data_create_from_string( xbar,s,"xbar");
}

//  CIE 1931 color matching function for medium wavelengths (green)
void sx_cie_create_ybar(struct sampled_data ** ybar)
{
	char* s="380 0.000039 385 0.000064 390 0.000120 395 0.000217 400 0.000396 405 0.000640 410 0.001210 415 0.002180 420 0.004000 425 0.007300 430 0.011600 435 0.016840 440 0.023000 445 0.029800 450 0.038000 455 0.048000 460 0.060000 465 0.073900 470 0.090980 475 0.112600 480 0.139020 485 0.169300 490 0.208020 495 0.258600 500 0.323000 505 0.407300 510 0.503000 515 0.608200 520 0.710000 525 0.793200 530 0.862000 535 0.914850 540 0.954000 545 0.980300 550 0.994950 555 1.000000 560 0.995000 565 0.978600 570 0.952000 575 0.915400 580 0.870000 585 0.816300 590 0.757000 595 0.694900 600 0.631000 605 0.566800 610 0.503000 615 0.441200 620 0.381000 625 0.321000 630 0.265000 635 0.217000 640 0.175000 645 0.138200 650 0.107000 655 0.081600 660 0.061000 665 0.044580 670 0.032000 675 0.023200 680 0.017000 685 0.011920 690 0.008210 695 0.005723 700 0.004102 705 0.002929 710 0.002091 715 0.001484 720 0.001047 725 0.000740 730 0.000520 735 0.000361 740 0.000249 745 0.000172 750 0.000120 755 0.000085 760 0.000060 765 0.000042 770 0.000030 775 0.000021 780 0.000015";
	sampled_data_create_from_string( ybar,s,"ybar");
}

// CIE 1931 color matching function for short wavelengths (blue)
void sx_cie_create_zbar(struct sampled_data ** zbar)
{
	char* s="380 0.006450 385 0.010550 390 0.020050 395 0.036210 400 0.067850 405 0.110200 410 0.207400 415 0.371300 420 0.645600 425 1.039050 430 1.385600 435 1.622960 440 1.747060 445 1.782600 450 1.772110 455 1.744100 460 1.669200 465 1.528100 470 1.287640 475 1.041900 480 0.812950 485 0.616200 490 0.465180 495 0.353300 500 0.272000 505 0.212300 510 0.158200 515 0.111700 520 0.078250 525 0.057250 530 0.042160 535 0.029840 540 0.020300 545 0.013400 550 0.008750 555 0.005750 560 0.003900 565 0.002750 570 0.002100 575 0.001800 580 0.001650 585 0.001400 590 0.001100 595 0.001000 600 0.000800 605 0.000600 610 0.000340 615 0.000240 620 0.000190 625 0.000100 630 0.000050 635 0.000030 640 0.000020 645 0.000010 650 0.000000 655 0.000000 660 0.000000 665 0.000000 670 0.000000 675 0.000000 680 0.000000 685 0.000000 690 0.000000 695 0.000000 700 0.000000 705 0.000000 710 0.000000 715 0.000000 720 0.000000 725 0.000000 730 0.000000 735 0.000000 740 0.000000 745 0.000000 750 0.000000 755 0.000000 760 0.000000 765 0.000000 770 0.000000 775 0.000000 780 0.000000";
	sampled_data_create_from_string( zbar,s,"zbar");
}

void sx_cie_create_d65(struct sampled_data ** d65)
{
	char* s="300.0 0.03410 305.0 1.66430 310.0 3.29450 315.0 11.76520 320.0 20.23600 325.0 28.64470 330.0 37.05350 335.0 38.50110 340.0 39.94880 345.0 42.43020 350.0 44.91170 355.0 45.77500 360.0 46.63830 365.0 49.36370 370.0 52.08910 375.0 51.03230 380.0 49.97550 385.0 52.31180 390.0 54.64820 395.0 68.70150 400.0 82.75490 405.0 87.12040 410.0 91.48600 415.0 92.45890 420.0 93.43180 425.0 90.05700 430.0 86.68230 435.0 95.77360 440.0 104.86500 445.0 110.93600 450.0 117.00800 455.0 117.41000 460.0 117.81200 465.0 116.33600 470.0 114.86100 475.0 115.39200 480.0 115.92300 485.0 112.36700 490.0 108.81100 495.0 109.08200 500.0 109.35400 505.0 108.57800 510.0 107.80200 515.0 106.29600 520.0 104.79000 525.0 106.23900 530.0 107.68900 535.0 106.04700 540.0 104.40500 545.0 104.22500 550.0 104.04600 555.0 102.02300 560.0 100.0 565.0 98.16710 570.0 96.33420 575.0 96.06110 580.0 95.78800 585.0 92.23680 590.0 88.68560 595.0 89.34590 600.0 90.00620 605.0 89.80260 610.0 89.59910 615.0 88.64890 620.0 87.69870 625.0 85.49360 630.0 83.28860 635.0 83.49390 640.0 83.69920 645.0 81.86300 650.0 80.02680 655.0 80.12070 660.0 80.21460 665.0 81.24620 670.0 82.27780 675.0 80.28100 680.0 78.28420 685.0 74.00270 690.0 69.72130 695.0 70.66520 700.0 71.60910 705.0 72.97900 710.0 74.34900 715.0 67.97650 720.0 61.60400 725.0 65.74480 730.0 69.88560 735.0 72.48630 740.0 75.08700 745.0 69.33980 750.0 63.59270 755.0 55.00540 760.0 46.41820 765.0 56.61180 770.0 66.80540 775.0 65.09410 780.0 63.38280 785.0 63.84340 790.0 64.30400 795.0 61.87790 800.0 59.45190 805.0 55.70540 810.0 51.95900 815.0 54.69980 820.0 57.44060 825.0 58.87650 830.0 60.31250";
	sampled_data_create_from_string( d65,s,"d65");
}




/**
 *
 * http://easyrgb.com/index.php?X=MATH&H=01#text1

	xyz → Standard-RGB (0 to 255)
	x from 0 to  0.95047      (Observer = 2°, Illuminant = D65)
	y from 0 to 1
	z from 0 to 1.08883
 */
void sx_cie_xyz2rgb(
		float rgb[],
		const float xyz[]
		)
{
	float x = xyz[0] / 100 ;       //X from 0 to  95.047      (Observer = 2°, Illuminant = D65)
	float y = xyz[1] / 100  ;      //Y from 0 to 100.000
	float z = xyz[2] / 100  ;      //Z from 0 to 108.883
	float r = x *  3.2406f + y * -1.5372f + z * -0.4986f;
	float g = x * -0.9689f + y *  1.8758f + z *  0.0415f;
	float b = x *  0.0557f + y * -0.2040f + z *  1.0570f;

	if ( r > 0.0031308f ) r = 1.055f * powf( r , ( 1.0f / 2.4f ) ) - 0.055f;	else   r = 12.92f * r;
	if ( g > 0.0031308f ) g = 1.055f *  powf( g , ( 1.0f / 2.4f ) ) - 0.055f;	else   g = 12.92f * g;
	if ( b > 0.0031308f ) b = 1.055f *  powf( b , ( 1.0f / 2.4f ) ) - 0.055f;	else    b = 12.92f * b;

	rgb[0]=(int)(r*255);
	rgb[1]=(int)(g*255);
	rgb[2]=(int)(b*255);
}


void sx_cie_calc_xyz(
		float xyz[3],
		float *power,
		struct sampled_data * spectrum,
		struct sampled_data * xbar,
		struct sampled_data * ybar,
		struct sampled_data * zbar
		)
{

	float intx=0,inty=0,intz=0;
	*power=0;
	for (int i=0;i<spectrum->nb_vals-1;i++)
	{
		float y1=spectrum->y[i];
		float lambd=spectrum->x[i];
		float vx=y1*sampled_data_interpolate_linear(xbar, lambd);
		float vy=y1*sampled_data_interpolate_linear(ybar, lambd);
		float vz=y1*sampled_data_interpolate_linear(zbar, lambd);
		intx+=vx;
		inty+=vy;
		intz+=vz;
		*power+=y1;
	}
	if (*power==0)
	{
		xyz[0] = 0;
		xyz[2] = 0;
		xyz[1] = 0;
	}
	else
	{
		//normalise:
		xyz[0]=intx/inty*100.0f;
		xyz[2]=intz/inty*100.0f;
		xyz[1]=100;
		*power/=(float)spectrum->nb_vals;
	}
}

void sx_cie_calc_xyz_2(
		float xyz[3],
		float *power,
		struct sampled_data * spectrum,
		struct sampled_data * xbar,
		struct sampled_data * ybar,
		struct sampled_data * zbar
		)
{
	float intx=0,inty=0,intz=0;
	float sy=0;
	*power=0;
	for (int i=0;i<spectrum->nb_vals-1;i++)
	{
		float y1=spectrum->y[i];
		float y2=spectrum->y[i+1];
		float lambd=(spectrum->x[i+1]+spectrum->x[i])/2.0f;
		float dlambd=spectrum->x[i+1]-spectrum->x[i];
		float yy=(y1+y2)/2;
		float vx=yy*sampled_data_interpolate_linear(xbar,lambd)*dlambd;
		float vy=yy*sampled_data_interpolate_linear(ybar,lambd)*dlambd;
		float vz=yy*sampled_data_interpolate_linear(zbar,lambd)*dlambd;
		intx+=vx;
		inty+=vy;
		intz+=vz;
		sy+=sampled_data_interpolate_linear(ybar,lambd)*dlambd;
		*power+=yy*dlambd;
	}
	xyz[0]=intx/sy*100.0f;
	xyz[1]=inty/sy*100.0f;
	xyz[2]=intz/sy*100.0f;

}


/**
 * must find:
 *  xyz = 30.0 23.8 12.6
 *  rgb = 194 112 89
 */
void sx_cie_test(void)
{
	char* s="650 45.2 640 44.2 630 44.3 620 41.8 610 41.3 600 37.7 590 33.7 580 27.2 570 21.6 560 20.5 550 16.8 540 16.0 530 14.8 520 14.7 510 14.5 500 13.3 490 13.0 48 10.8 470 10.3 460 7.4 450 6.8";
	printf("%s\n",s);
	struct sampled_data * sp;
	sampled_data_create_from_string(&sp,s,"spectrum");
	sampled_data_print(sp,stdout);
	float xyz[3];
	float power;
	struct sampled_data * xbar;
	struct sampled_data * ybar;
	struct sampled_data * zbar;
	sx_cie_create_xbar(&xbar);
	sx_cie_create_ybar(&ybar);
	sx_cie_create_zbar(&zbar);
	sx_cie_calc_xyz(xyz,&power,sp,xbar,ybar,zbar);
	printf("x=%f y=%f z=%f\n",xyz[0],xyz[1],xyz[2]);
	float rgb[3];
	sx_cie_xyz2rgb(rgb,xyz);
	printf("r=%f g=%f b=%f power=%f\n",rgb[0],rgb[1],rgb[2],power);
	//free spectrum and CIE references:
	sampled_data_release(sp);
	sampled_data_release(xbar);
	sampled_data_release(ybar);
	sampled_data_release(zbar);
}


struct sampled_data * sx_cie_create_uniform_spectrum(
		const int n,
		const char* name,
		const float lambdaMin, 
		const float lambdaMax, 
		const float value
		)
{
	struct sampled_data * sp;
	sampled_data_create3(&sp,n);
	//	printf("n=%d\n",n);
	//	printf("lambdaMin=%f\n",lambdaMin);
	//	printf("lambdaMax=%f\n",lambdaMax);
	//	printf("value=%f\n",value);
	//	printf("name=%s\n",name);
	strcpy(sp->name,name);
	float samplingx=(lambdaMax-lambdaMin)/(float)(n-1);
	for (int i=0;i<n;i++)
	{
		(sp)->x[i]=(float)(lambdaMin+samplingx*(float)i);
		(sp)->y[i]=value;
	}
	return sp;

}

