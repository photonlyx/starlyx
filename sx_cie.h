/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SX_CIE_H_
#define SX_CIE_H_

#include "sampled_data.h"

void sx_cie_create_xbar(struct sampled_data ** xbar);
void sx_cie_create_ybar(struct sampled_data ** ybar);
void sx_cie_create_zbar(struct sampled_data ** zbar);
void sx_cie_create_d65(struct sampled_data ** d65);

void  sx_cie_xyz2rgb(
		float rgb[],
		const float xyz[]
		);

/**
 * calculate the xyz color coordinates for a spectrum
 * spectrum: pointer to sampled_data
 * xyz: array of 3 floats
 * power: integral of the spectrum
 */
void sx_cie_calc_xyz(
		float xyz[3],
		float *power,
		struct sampled_data * spectrum,
		struct sampled_data * xbar,
		struct sampled_data * ybar,
		struct sampled_data * zbar);

void sx_cie_calc_xyz_2(
		float xyz[3],
		float *power,
		struct sampled_data * spectrum,
		struct sampled_data * xbar,
		struct sampled_data * ybar,
		struct sampled_data * zbar)
	;

/**
 * create an uniform spectrum
 * sp: pointer to spectrum pointer
 * nbvals: nb of lambdas
 * name: name of the spectrum
 * lambdaMin: min wavelength in nm
 * lambdaMax max wavelength in nm
 * value: const value for all wavelength
 */
//void sx_cie_create_uniform_spectrum(struct sampled_data ** sp,const int nbvals,const char* name,
//		const float lambdaMin,const float lambdaMax,const float value);

struct sampled_data * sx_cie_create_uniform_spectrum(
		const int n,
		const char* name, 
		float lambdaMin, 
		float lambdaMax, 
		float value
		);

void sx_cie_test(void);


#endif /* SX_CIE_H_ */
