
/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <rsys/float3.h>
#include "sx_geometry.h"


/**
 * calc the global coord
 * res coord of the point in the global frame
 * O origin of the frame
 * X X vector of the frame
 * Y Y vector of the frame
 * Z Z vector of the frame
 * vloc coord of the point in the frame
 */
void sx_geometry_changeFrame(float* res,float* O,float* X,float* Y,float *Z,float* vloc)
{
	float v[3];
	f3_mulf(v,X,vloc[0]);
	f3_add(res,O,v);
	f3_mulf(v,Y,vloc[1]);
	f3_add(res,res,v);
	f3_mulf(v,Z,vloc[2]);
	f3_add(res,res,v);
}


/**
 * calc frameY and frameZ from a known frameX to get a orthonormalized frame
 *
 */
void sx_geometry_create_frame_from_X(const float frameX[3],float frameY[3],float frameZ[3])
{
	//find X axis of the source frame:
	if (frameX[2]!=0)
	{
		//frameX is not horizontal
		//take frameZ in plane (x,z) such that it is perpendicular to frameX (solve frameX.frameZ=0 )
		frameZ[0]=1;
		frameZ[1]=0;
		frameZ[2]=-frameX[0]/frameX[2];
		//normalise frameZ:
		f3_normalize(frameZ,frameZ);
	}
	else
	{
		//frameX is horizontal, take frameZ as (0,0,1)
		frameZ[0]=0;
		frameZ[1]=0;
		frameZ[2]=1;
	}
	//calc frameY with a vectorial product:
	f3_cross(frameY,frameZ,frameX);

	//printf("X=(%g,%g,%g)\n",frameX[0],frameX[1],frameX[2]);
	//printf("Y=(%g,%g,%g)\n",frameY[0],frameY[1],frameY[2]);
	//printf("Z=(%g,%g,%g)\n",frameZ[0],frameZ[1],frameZ[2]);
}



/**
 * calc frameX and frame Y from frameZ
 *
 */
void sx_geometry_create_frame_from_Z(float frameX[3],float frameY[3],const float frameZ[3])
{
	//find X axis of the source frame:
	if (frameZ[2]!=0)
	{
		//frameZ is not horizontal
		//take frameZ in plane (x,z) such that it is perpendicular to frameX (solve frameX.frameZ=0 )
		frameX[0]=1;
		frameX[1]=0;
		frameX[2]=-frameZ[0]/frameZ[2];
		//normalise frameX:
		f3_normalize(frameX,frameX);
	}
	else
	{
		//frameZ is horizontal, take frameX as (0,0,1)
		frameX[0]=0;
		frameX[1]=0;
		frameX[2]=1;
	}
	//calc frameY with a vectorial product:
	f3_cross(frameY,frameZ,frameX);

	//printf("X=(%g,%g,%g)\n",frameX[0],frameX[1],frameX[2]);
	//printf("Y=(%g,%g,%g)\n",frameY[0],frameY[1],frameY[2]);
	//printf("Z=(%g,%g,%g)\n",frameZ[0],frameZ[1],frameZ[2]);
}


/**
 * calc frameY and frameZ from frameX with Z in a vertical plane
 *
 */
void sx_geometry_create_frame_from_X_with_Z_vertical(const float frameX[3],float frameY[3],float frameZ[3])
{
	float z[3];
	z[0]=0;
	z[1]=0;
	z[2]=1;
	f3_cross(frameY,z,frameX);//calc the local  vector Y perpendicular to the plane ( Z scene, camX)
	f3_normalize(frameY,frameY);
	f3_cross(frameZ,frameX,frameY);//finally calculate the local vector Z
}




/**
  calculate the intersection between a plan and a line
  pPlane point of the plane
  nPlane normal to the plane
  pLine point of the line
  nLine direction of the line (normalised)
 */
int sx_geometry_intersection_plane_line(float p[3],const float pPlane[3],float nPlane[3],float pLine[3],float nLine[3])
{
	float tdroite;
	float PdPp[3];
	float u[3];
	float r;
	//printf("pPlane =[%f,%f,%f] \n",pPlane[0],pPlane[1],pPlane[2]);
	//printf("nPlane =[%f,%f,%f] \n",nPlane[0],nPlane[1],nPlane[2]);
	//printf("pLine =[%f,%f,%f] \n",pLine[0],pLine[1],pLine[2]);
	//printf("nLine =[%f,%f,%f] \n",nLine[0],nLine[1],nLine[2]);
	f3_sub(PdPp,pPlane,pLine);
	r=f3_dot(nLine,nPlane);
	if (r!=0) tdroite=f3_dot(PdPp,nPlane)/r; else return 0;
	f3_mulf(u,nLine,tdroite);
	f3_add(p,u,pLine);
	//printf("p =[%f,%f,%f] \n",p[0],p[1],p[2]);
	return 1;
}
