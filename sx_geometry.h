
/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SX_GEOMETRY_H_
#define SX_GEOMETRY_H_



void sx_geometry_changeFrame(float* res,float* O,float* X,float* Y,float *Z,
								float* vloc);

void sx_geometry_create_frame_from_Z(float frameX[3],float frameY[3],
								const float frameZ[3]);

void sx_geometry_create_frame_from_X(const float frameX[3],float frameY[3],
										float frameZ[3]);

void sx_geometry_create_frame_from_X_with_Z_vertical(const float frameX[3],
											float frameY[3],float frameZ[3]);

int sx_geometry_intersection_plane_line(float p[3],const float pPlane[3],
								float nPlane[3],float pLine[3],float nLine[3]);

#endif /* SX_GEOMETRY_H_ */
