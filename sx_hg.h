/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
   */

#ifndef SX_HG_H_
#define SX_HG_H_

#include "sx_xml.h"
#include "sx_scene.h"


struct sampled_data;

enum
{
	TYPE_K_KA_G, //material volume defined by k  ka and g
	TYPE_LSTAR_LA_G, //material volume defined by l*(=1/k/(1-g))  la(=1/ka) and g
	TYPE_MIE //material volume which is a suspension of spheres, defined by volume fraction, spheres diameter, and refractive index
};

/**
 * Henyey-Greenstein scattering
 */
struct sx_hg
{
	int type;//TYPE_K_KA_G or TYPE_LSTAR_LA_G
	char name[MAX_NAME_LENGTH];
	char spectrum_A_name[MAX_NAME_LENGTH];//the spectrum name for K , LSTAR or PHI
	char spectrum_B_name[MAX_NAME_LENGTH];//the spectrum name for KA or LA (absorption)
	char spectrum_C_name[MAX_NAME_LENGTH];//the spectrum name for G or D
	char spectrum_D_name[MAX_NAME_LENGTH];//the spectrum name for N (mie)
	struct sampled_data* spectrum_A;//(k , lstar or phi)
	struct sampled_data* spectrum_B;//(ka , la )
	struct sampled_data* spectrum_C;//(g ,d )
	struct sampled_data* spectrum_D;//(n mie)
	float (*k)(struct sx_hg* hg,float lambda);   //function that gives k(lambda), lambda in nm
	float (*ka)(struct sx_hg* hg,float lambda); //function that gives ka(lambda), lambda in nm
	float (*g)(struct sx_hg* hg,float lambda);   //function that gives g(lambda), lambda in nm
	float kmie[1000]; //table for accelerating mie case
	float gmie[1000]; //table for accelerating mie case
	float free_path; //sorted distance to next scattering
	float abs_path;//sorted distance to next absorption
};

int sx_hg_create_from_xml( struct sx_hg ** p_p_mv,mxml_node_t *node);

void sx_hg_release( struct sx_hg * hg);

void sx_hg_print(struct sx_hg* hg);

float sx_hg_k_discrete(struct sx_hg* hg,float lambda);

float sx_hg_ka_discrete(struct sx_hg* hg,float lambda);

float sx_hg_g_discrete(struct sx_hg* hg,float lambda);

float sx_hg_k_constant(struct sx_hg* hg,float lambda);

float sx_hg_ka_constant(struct sx_hg* hg,float lambda);

float sx_hg_g_constant(struct sx_hg* hg,float lambda);

int sx_hg_connect_spectra(struct sx_hg* hg,struct sx_scene* scene);

int sx_hg_create_with_constant_spectra( struct sx_hg ** p_hg,float k,float ka,float g);

int sx_hg_has_attribute(struct sx_hg * hg,const char* name);

struct sampled_data* sx_hg_get_spectrum_of_attribute(struct sx_hg * hg,const char* name,const int verbose);

void sx_hg_init_acceleration_tables(struct sx_hg * hg);

#endif /* SX_HG_H_ */
