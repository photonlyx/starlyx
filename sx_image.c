/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <stdint.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include "sx_image.h"
#include "sampled_data.h"

#include "sx_cie.h"

int sx_image_create(
		struct sx_image** image,
		unsigned int w,
		unsigned int h,
		unsigned int nb_channels
		)
{
	(*image)=calloc(1,sizeof(struct sx_image));
	(*image)->w=w;
	(*image)->h=h;
	(*image)->nbc=nb_channels;
	(*image)->lambdas=calloc(1,sizeof(float)*nb_channels);
	(*image)->data=calloc(1,sizeof(float)*w*h*nb_channels);
	return 1;
}

//TEXTURE:
int sx_image_create_from_png(
		struct sx_image** image,
		const char* filename
		)
{
	//load the png
	//printf("Load png file %s\n",filename);
	int w=0;
	int h=0;
	int channels=0;
	unsigned char *image_png = stbi_load(filename, &w, &h, &channels, 0);
	if (image_png!=NULL)
	{
		//printf("sx_image_create_from_png file:%s  w=%d h=%d %d channels\n",filename,w,h,channels);
		sx_image_create(image,(unsigned int)w,(unsigned int)h,(unsigned int)channels);
		for (int k=0;k<w*h*channels;k++) (*image)->data[k]=image_png[k];
		free(image_png);
		//sx_image_print_ascii(*image,0);
		//sx_image_print_ascii(*image,1);
		return 0;
	}
	else 
	{
		printf("Error loading %s\n",filename);
		return 1;
	}
}

/**
 * set max at 1 (not 255)
 */
void sx_image_normalise(struct sx_image* image)
{
	float max=255;
	for (unsigned int k=0;k<image->w*image->h*image->nbc;k++)
	{
		image->data[k]/=max;
	}
}

void sx_image_print_ascii(
		struct sx_image* image,
		unsigned int channel
		)
{
	unsigned int w=image->w;
	unsigned int h=image->h;
	unsigned int c=image->nbc;
	if (channel>=c)
	{
		printf("sx_image_print_ascii wrong channel: %d\n",channel);
	}
	//print the pixels as a test:
	for (unsigned int j=0;j<h;j++)
	{
		for (unsigned int i=0;i<w;i++)
		{
			float val=image->data[c*(i+w*j)+channel];
			//if (val<0.5
			printf ("%d ",(int)val);
		}
		printf("\n");
	}
}

void sx_image_release(struct sx_image* image)
{
	free(image->lambdas);
	free(image->data);
}


/**
 * return the max value (for all channels)
 */
float sx_image_max(const struct sx_image* image)
{
	float r=-FLT_MAX;
	float v;
	int index=0;
	for (unsigned int i=0;i<image->w;i++)
		for (unsigned int j=0;j<image->h;j++)
			for (unsigned int k=0;k<image->nbc;k++)
			{
				v=image->data[index++];
				if (v>r) r=v;
			}
	return r;
}

/**
 * return the mean value of a channel
 */
float sx_image_mean(
	const struct sx_image* image
	)
{
	float v;
	double r;
	int index=0;
	float w=(float)image->w;
	float h=(float)image->h;
	float nbc=(float)image->nbc;
	for (unsigned int i=0;i<w;i++)
		for (unsigned int j=0;j<h;j++)
			for (unsigned int k=0;k<nbc;k++)
			{
				index=(i+j*w)*nbc+k;
				v=image->data[index];
				r+=v;
			}
	return (float)(r/(w*h));
}



/**
 * remove the bright points
 */
void sx_image_filter_bright_point(
	const struct sx_image* image,
	float max,
	float mean,
	float threshold  //the threshold needed to consider a point bright
	)
{
	float v;
	double r;
	int index=0;
	float w=(float)image->w;
	float h=(float)image->h;
	float nbc=(float)image->nbc;
	for (unsigned int i=0;i<w;i++)
		for (unsigned int j=0;j<h;j++)
			for (unsigned int k=0;k<nbc;k++)
			{
				index=(i+j*w)*nbc+k;
				v=image->data[index];
				if (v>threshold*mean) 
				{
					image->data[index]=0;
				}
			}
}

int sx_image_save2(const struct sx_image* image,const char* filepath,const char* filename)
{
	char full_filename[MAX_NAME_LENGTH*3];
	sprintf(full_filename,"%s%s.raw",filepath,filename);
	return sx_image_save(image,full_filename);
}


/**
 * save spectral image in a binary file with this format
 *  all in 32bits floating point number (float) littleEndian
 *
 *  width height nb_channels xmin xmax ymin ymax
 *  RGB     RGB      RGB .........................   RGB
 *  RGB     RGB      RGB .........................   RGB
 *  RGB     RGB      RGB .........................   RGB
 *  RGB     RGB      RGB .........................   RGB
 *
 *
 */
int sx_image_save (const struct sx_image* image,const char* filename)
{
	//save the multi-channel  image in a binary file
	//printf("Save multispectral image %s (%dx%d %d channels)\n",filename,image->w,image->h,image->nbc);
	FILE* image_file;
	image_file = fopen(filename, "wb");
	if (!image_file) return 0;
	float w_=(float)image->w;
	float h_=(float)image->h;
	float nbc_=(float)image->nbc;
	float xmin=(float)0;
	float xmax=(float)1;
	float ymin=(float)0;
	float ymax=(float)1;
	unsigned int sizef=sizeof(w_);

	//write the header:
	fwrite(&w_, sizef, 1, image_file);
	fwrite(&h_, sizef, 1, image_file);
	fwrite(&nbc_, sizef, 1, image_file);
	fwrite(&xmin, sizef, 1, image_file);
	fwrite(&xmax, sizef, 1, image_file);
	fwrite(&ymin, sizef, 1, image_file);
	fwrite(&ymax, sizef, 1, image_file);

	//write the data:
	unsigned int w=image->w;
	unsigned int h=image->h;
	unsigned int nbc=image->nbc;
	float v=0;
	for (unsigned int kk=0;kk<w*h*nbc;kk++)
	{
		v=image->data[kk];
		fwrite(&v,sizef,1, image_file);
	}
	//write the wavelengths
	for (unsigned int i=0;i<nbc;i++)
	{
		v=image->lambdas[i];
		fwrite(&v, sizef, 1, image_file);
		//printf("save lambda %f\n",v);
	}

	fclose(image_file);
	//printf("Saved multispectral image %s (%dx%d %d channels)\n",filename,w,h,nbc);
	//	printf("See it with: imagej -p 1 %s  (options: 32bits real, %dx%d %d channels, 28 bytes offset, little endian)\n",
	//			filename,(int)w,(int)h,(int)nbc);
	//	printf("Or see it with: lyxchart %s -littleEndian \n",filename);

	return 1;
}

/**
		read an image form a .raw file
	*/
int sx_image_read (struct sx_image** image_,const char* filename)
{
	FILE* file1;
	file1 = fopen(filename,"rb");
	if (file1==NULL)
	{
		printf("Can't open file %s\n",filename);
		return 1;
	}
	long unsigned int n=7;//size of header
	float header[n];//header
	fread(&header,sizeof(float),n,file1);
	// get image parameters from the header:
	unsigned int i=0;
	unsigned int kk=0;
	unsigned int w=(unsigned int)header[i++];//width
	unsigned int h=(unsigned int)header[i++];//height
	unsigned int nbc=(unsigned int)header[i++];//nb of channels
	//read the data:
	long unsigned int size=w*h*nbc+nbc;
	float data[size];
	fread(&data,sizeof(float),size,file1);
	//create the image
	struct sx_image* image;
	sx_image_create(&image,w,h,nbc);
	//set the pixels values
	for (kk=0;kk<w*h*nbc;kk++)
	{
		image->data[kk]=data[kk];
	}
	//set the wavelengths:
	for (i=0;i<nbc;i++)
	{
		image->lambdas[i]=data[w*h*nbc+i];
	}
	(*image_)=image;
	fclose(file1);
	return 0;
}




/**
 * save the image using png format
 */
int sx_image_save_ppm (const struct sx_image* image,float gamma,const char* filepath,const char* filename)
{
	//printf("sx_image_save_ppm file:%s %d channels\n",filename,image->nbc);
	char ppm_filename[MAX_NAME_LENGTH*3];
	int res;
	if (image->nbc>3)
	{
		sprintf(ppm_filename,"%s%s.ppm",filepath,filename);
		res=sx_image_save_ppm_n_channels(image,gamma,ppm_filename);
	}
	else if (image->nbc==3)
	{
		sprintf(ppm_filename,"%s%s.ppm",filepath,filename);
		res=sx_image_save_ppm_3_channels(image,gamma,ppm_filename);
	}
	else if (image->nbc==1)
	{
		sprintf(ppm_filename,"%s%s.pgm",filepath,filename);
		res=sx_image_save_pbm_1_channel(image,gamma,ppm_filename);
	}
	else res=0;
	if(!res) fprintf(stderr, "Couldn't save image %s\n",ppm_filename);
	return res;
}






/**
 * save a 1 channel sx_image using ppm format
 * gamma gamma non linear correction
 */
int sx_image_save_pbm_1_channel (const struct sx_image* image,float gamma,const char* filename)
{
	unsigned int w = image->w;
	unsigned int h = image->h;

	if (image->nbc!=1) return 0;
	FILE* file = fopen(filename, "w");
	fprintf(file,"P2\n");
	fprintf(file,"%d %d\n",w,h);
	fprintf(file,"255\n");

	//printf("Save monochrome float image as pgm image: %s (%d x%d)\n",filename,w,h);
	float max=sx_image_max(image);
	float a=0;//coef of gamma correction Iin = a Iin^gamma
	if (max==0) max=1;//avoid div by zero
	else a=(255.0f/powf(max, gamma));
	unsigned int index=0;
	unsigned char pixel;
	for (unsigned int j=0;j<h;j++)
	{
		for (unsigned int i=0;i<w;i++)
		{
			index=i+j*w;
			pixel=(unsigned char)(a*pow(image->data[index],gamma));
			fprintf(file,"%d ",pixel);
		}
		fprintf(file,"\n");
	}
	fclose(file);
	return 1;
}

/**
 * save a 3 channel (R G B) sx_image using png format
 * gamma gamma non linear correction
 */
int sx_image_save_ppm_3_channels (const struct sx_image* image,float gamma,const char* filename)
{
	//printf("sx_image_save_ppm_3_channels file:%s \n",filename);
	unsigned int w = image->w;
	unsigned int h = image->h;

	if (image->nbc!=3) return 0;
	FILE* file = fopen(filename, "w");
	fprintf(file,"P3\n");
	fprintf(file,"%d %d\n",w,h);
	fprintf(file,"255\n");

	float max=sx_image_max(image);
	float a=0;//coef of gamma correction Iin = a Iin^gamma
	if (max==0) max=1.0f;//avoid div by zero
	else a=(255.0f/powf(max, gamma));
	unsigned int index2=0;
	for (unsigned int j=0;j<h;j++)
		for (unsigned int i=0;i<w;i++)
		{
			index2=(i+j*w)*3;
			//			fprintf(file,"%d ",(int)(image->data[index2]*255.0/max));
			//			fprintf(file,"%d ",(int)(image->data[index2+1]*255.0/max));
			//			fprintf(file,"%d ",(int)(image->data[index2+2]*255.0/max));
			fprintf(file,"%d ",(int)(a*pow(image->data[index2],gamma)));
			fprintf(file,"%d ",(int)(a*pow(image->data[index2+1],gamma)));
			fprintf(file,"%d ",(int)(a*pow(image->data[index2+2],gamma)));
		}
	//printf("Saved 3 channels float image as png image: %s (%d x%d)\n",filename,w,h);
	fclose(file);
	return 1;
}



/**
 * save a spectral  sx_image (n channels) using ppm format
 * gamma gamma non linear correction
 */
int sx_image_save_ppm_n_channels (const struct sx_image* image,float gamma,const char* filename)
{
	unsigned int w = image->w;
	unsigned int h = image->h;
	//create spectrum structure in memory:
	struct sampled_data * spectrum;
	sampled_data_create3(&spectrum,image->nbc);
	//printf("Save %d channels float image as ppm image: %s (%dx%d)\n",image->nbc,filename,w,h);
	int* pixels = calloc(1,sizeof(int)*w * h * 3);//raw data of rgb image
	float* pixels_not_normalized = calloc(1,sizeof(float)*w*h*3);//raw data of rgb image, before normalization
	unsigned int index_pix;//index of pixel
	unsigned int index_hsp;//index in hyperspectral image
	unsigned int index_rgb;//index in rgb image
	float rgb[3],xyz[3];
	float power;
	float max=0;
	struct sampled_data * xbar;
	struct sampled_data * ybar;
	struct sampled_data * zbar;
	sx_cie_create_xbar(&xbar);
	sx_cie_create_ybar(&ybar);
	sx_cie_create_zbar(&zbar);
	for (unsigned int j=0;j<h;j++)
		for (unsigned int i=0;i<w;i++)
		{
			index_pix=i+j*w;//index of pixel
			index_hsp=index_pix*image->nbc; //index of first data of pixel spectrum in raw hyperspectral data
			for (unsigned int k_hsp=0;k_hsp<image->nbc;k_hsp++)
			{
				spectrum->x[k_hsp]=image->lambdas[k_hsp];
				spectrum->y[k_hsp]=image->data[index_hsp+k_hsp];
			}
			//sampled_data_print(spectrum,stdout);
			//convert spectrum to rgb:
			sx_cie_calc_xyz_2(xyz,&power,spectrum,xbar,ybar,zbar);
			//printf("x=%f y=%f z=%f power=%f\n",xyz[0],xyz[1],xyz[2],power);
			sx_cie_xyz2rgb(rgb,xyz);
			//printf("r=%f g=%f b=%f power=%f\n",rgb[0],rgb[1],rgb[2],power);
			for (unsigned int k=0;k<3;k++) if (rgb[k]<0) rgb[k]=0;
			for (unsigned int k_rgb=0;k_rgb<3;k_rgb++)
			{
				index_rgb=index_pix*3;////index of first data of pixel rgb values in raw hyperspectral data
				pixels_not_normalized[index_rgb+k_rgb]=rgb[k_rgb];
				if (rgb[k_rgb]>max) max=rgb[k_rgb];
			}
		}
	//printf("max=%f\n",max);
	//normalize
	float a=0;//coef of gamma correction Iin = a Iin^gamma
	if (max==0) max=1;//avoid div by zero
	else a=(255.0f/powf(max, gamma));
	for (unsigned int kk=0;kk<w*h*3;kk++) pixels[kk]=(int)(a*pow(pixels_not_normalized[kk],gamma));

	//write the file:
	FILE* file = fopen(filename, "w");
	fprintf(file,"P3\n");
	fprintf(file,"%d %d\n",w,h);
	fprintf(file,"255\n");
	for (unsigned int j=0;j<h;j++)
		for (unsigned int i=0;i<w;i++)
		{
			index_pix=i+j*w;//index of pixel
			index_rgb=index_pix*3;////index of first data of pixel rgb values in raw hyperspectral data
								  //printf("i=%d j=%d index2=%d %d %d %d\n",i,j,index_rgb,pixels[index_rgb],pixels[index_rgb+1],pixels[index_rgb+2]);
			fprintf(file,"%d ",(int)(pixels[index_rgb]));
			fprintf(file,"%d ",(int)(pixels[index_rgb+1]));
			fprintf(file,"%d ",(int)(pixels[index_rgb+2]));
		}
	//printf("Saved %d channels float image as ppm image: %s (%d x%d)\n",image->nbc,filename,w,h);
	fclose(file);

	//free spectrum and CIE references:
	sampled_data_release(spectrum);
	sampled_data_release(xbar);
	sampled_data_release(ybar);
	sampled_data_release(zbar);
	//free temporary images:
	free(pixels);
	free(pixels_not_normalized);
	return 1;
}





/**
 * save the image using png format
 */
int sx_image_save_png (
		const struct sx_image* image,
		float gamma,
		float max, //radiance value corresponding to grey value of 255, -1 for auto value
		const char* filepath,
		const char* filename
		)
{
	//printf("sx_image_save_ppm file:%s %d channels\n",filename,image->nbc);
	char filename1[MAX_NAME_LENGTH*3];
	int res;
	sprintf(filename1,"%s%s.png",filepath,filename);
	if (image->nbc>3)
	{
		res=sx_image_save_png_n_channels(image,gamma,filename1);
	}
	else if (image->nbc==3)
	{
		res=sx_image_save_png_3_channels(image,gamma,filename1);
	}
	else if (image->nbc==2)
	{
		res=sx_image_save_png_2_channels(image,gamma,filename1);
	}
	else if (image->nbc==1)
	{
		res=sx_image_save_png_1_channel(image,gamma,max,filename1);
	}
	else res=0;
	if(!res) fprintf(stderr, "Couldn't save image %s\n",filename1);
	return res;
}



/**
 * save a 1 channel sx_image using png format
 */
int sx_image_save_png_1_channel (
		const struct sx_image* image,
		float gamma,
		float max, //radiance value corresponding to grey value of 255, -1 for auto value
		const char* filename
		)
{
	typedef uint8_t u8;
	typedef uint32_t u32;

	u32 w = image->w;
	u32 h = image->h;
	u32 nbc = image->nbc;//here nbc=1

	if (image->nbc!=1) return 0;
	if(max==-1)
	{
		//get the max value:
		max=sx_image_max(image);
		float mean=sx_image_mean(image);
		if (max>1000*mean)
		{
			printf("Warning. Detected bright point (max=%f >1000 mean=%f). Clean the image...\n",max,mean);
			sx_image_filter_bright_point(image,max,mean,1000);
			//recalc the max:
			max=sx_image_max(image);
		}
	}
	//factor for normalisation:
	float factor=0;//coef of gamma correction Iin = a Iin^gamma
	if (max==0) max=1;//avoid div by zero
	else factor=(255.0f/powf(max,gamma));


	//printf("Save monochrome float image as png image: %s (%d x%d)\n",filename,w,h);
	u8* pixels=malloc(sizeof(u8)*w*h*nbc);
	unsigned int index=0;
	float val=0;
	for (u32 i=0;i<w;i++)
		for (u32 j=0;j<h;j++)
		{
			index=i+w*j;
			val=(factor*pow(image->data[index],gamma));
			if (val<=255.0f) pixels[index]=(unsigned char)val; else pixels[index]=255.0f;
		}
	int ret= stbi_write_png(filename, w, h, nbc, pixels, w*nbc);
	free(pixels);
	return ret;
}

/**
 * save a 2 channel  sx_image using png format
 */
int sx_image_save_png_2_channels (const struct sx_image* image,float gamma,const char* filename)
{
	typedef uint8_t u8;
	typedef uint32_t u32;

	u32 w = image->w;
	u32 h = image->h;
	u32 nbc = image->nbc;//nbc=2

	if (image->nbc!=2) return 0;

	//get the max value:
	float max=sx_image_max(image);
	float mean=sx_image_mean(image);
	if (max>1000*mean)
	{
		printf("Warning. Detected bright point (max=%f >1000 mean=%f). Clean the image...\n",max,mean);
		sx_image_filter_bright_point(image,max,mean,1000);
		//recalc the max:
		max=sx_image_max(image);
	}
	//factor for normalisation:
	float factor=0;//coef of gamma correction Iin = a Iin^gamma
	if (max==0) max=1;//avoid div by zero
	else factor=(255.0f/powf(max,gamma));

	//printf("max value=%f\n",max);
	u8* pixels = malloc(sizeof(u8)*w * h * 3);
	unsigned int index=0;
	unsigned int index_png=0;
	float val1=0;
	float val2=0;
	unsigned char average=0;
	for (u32 i=0;i<w;i++)
		for (u32 j=0;j<h;j++)
		{
			index=(i+j*w)*nbc;//nbc=2 here
			val1=image->data[index+0];
			val2=image->data[index+1];
			//average of the 2 channels:
			average=(unsigned char)((val1+val2)/2*255.0/max);
			average=(unsigned char)(factor*pow(average,gamma));
			//fill the png image channels:
			index_png=(i+j*h)*3;
			pixels[index_png+0]=average;
			pixels[index_png+1]=average;
			pixels[index_png+2]=average;
			//printf("%f ",image->data[index]);
			index++;
		}
	//printf("Saved 3 channels float image as png image: %s (%d x%d)\n",filename,w,h);
	int ret=stbi_write_png(filename, w, h, 3, pixels, w*3);
	free(pixels);
	return ret;
}
/**
 * save a 3 channel (R G B) sx_image using png format
 */
int sx_image_save_png_3_channels (const struct sx_image* image,float gamma,const char* filename)
{
	typedef uint8_t u8;
	typedef uint32_t u32;

	u32 w = image->w;
	u32 h = image->h;
	u32 nbc = image->nbc;

	if (image->nbc!=3) return 0;
	//get the max value:
	float max=sx_image_max(image);
	float mean=sx_image_mean(image);
	if (max>1000*mean)
	{
		printf("Warning. Detected bright point (max=%f >1000 mean=%f). Clean the image...\n",max,mean);
		sx_image_filter_bright_point(image,max,mean,1000);
		//recalc the max:
		max=sx_image_max(image);
	}
	//factor for normalisation:
	float factor=0;//coef of gamma correction Iin = a Iin^gamma
	if (max==0) max=1;//avoid div by zero
	else factor=(255.0f/powf(max,gamma));
	//printf("max value=%f\n",max);
	u8* pixels = malloc(sizeof(u8)*w*h*nbc);
	unsigned int index=0;
	//int index2=0;
	for (u32 i=0;i<w;i++)
		for (u32 j=0;j<h;j++)
			for (u32 k=0;k<nbc;k++)
			{
				index=(i+j*w)*nbc+k;
				//index2=(j+i*h)*nbc+k;
				pixels[index]=(unsigned char)(factor*pow(image->data[index],gamma));
				//printf("%f ",image->data[index]);
			}
	//printf("Saved 3 channels float image as png image: %s (%d x%d)\n",filename,w,h);
	int ret=stbi_write_png(filename, w, h, nbc, pixels, w*nbc);
	free(pixels);
	return ret;
}


/**
 * save a spectral  sx_image (n channels) using png format
 */
int sx_image_save_png_n_channels (const struct sx_image* image,float gamma,const char* filename)
{
	typedef uint8_t u8;
	typedef uint32_t u32;
	u32 w = image->w;
	u32 h = image->h;
	unsigned int nbc=image->nbc;
	u32 nbc_png = 3;

	//create spectrum structure in memory:
	struct sampled_data* spectrum;
	sampled_data_create3(&spectrum,image->nbc);
	printf("Save %d channels float image as png image: %s (%d x%d)\n",image->nbc,filename,w,h);
	u8* pixels = malloc(sizeof(u8)*w * h * nbc_png);
	float* pixels_not_normalized = malloc(sizeof(float)*w * h * nbc_png);
	unsigned int index=0;
	unsigned int indexpng=0;
	float power;//,x, y, z,r,g,b;
	float rgb[3],xyz[3];
	float max=0;
	max=sx_image_max(image);
	struct sampled_data * xbar;
	struct sampled_data * ybar;
	struct sampled_data * zbar;
	sx_cie_create_xbar(&xbar);
	sx_cie_create_ybar(&ybar);
	sx_cie_create_zbar(&zbar);
	unsigned int i,j,k,kk;
	float v=0;

	for (i=0;i<w;i++)
		for (j=0;j<h;j++)
		{
			//	float test=image->data[(i+w*j)*nbc]/max*255.0;
			for (k=0;k<image->nbc;k++)
			{
				index=(i+w*j)*nbc+k;
				spectrum->x[k]=image->lambdas[k];
				spectrum->y[k]=image->data[index];
			}
			//convert spectrum to rgb:
			sx_cie_calc_xyz_2(xyz,&power,spectrum,xbar,ybar,zbar);
			//calc rgb
			sx_cie_xyz2rgb(rgb,xyz);
			//if (power!=0) printf("i=%d j=%d k=%d r=%f g=%f b=%f power=%f\n",i,j,k,rgb[0],rgb[1],rgb[2],power);

			// get the max:
			for (k=0;k<nbc_png;k++)
			{
				indexpng=(i+w*j)*nbc_png+k;
				v=rgb[k];
				pixels_not_normalized[indexpng]=v;
				if (v>max) max=v;
			}

		}

	//factor for normalisation:
	float factor=0;//coef of gamma correction Iin = a Iin^gamma
	if (max==0) max=1;//avoid div by zero
	else factor=(255.0f/powf(max,gamma));

	//normalize:
	for (kk=0;kk<w*h*nbc_png;kk++)
	{
		float val=pixels_not_normalized[kk];
		float val1=(int)(factor*powf(val,gamma));
		if (val1>255) val1=255;
		if (val1<0) val1=0;
		pixels[kk]=(unsigned char)val1;
	}

	sampled_data_release(spectrum);
	int ret= stbi_write_png(filename, w, h, nbc_png, pixels, w*nbc_png);
	free(pixels);
	free(pixels_not_normalized);
	return ret;
}


