/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IMAGE_H
#define IMAGE_H


struct sx_scene;

struct sx_image
{
	unsigned int w;
	unsigned int h;
	unsigned int nbc;//nb of color channels
	float* lambdas;//list of wavelengths
	float* data;
};


int sx_image_create(
		struct sx_image** image,
		unsigned int w,
		unsigned int h,
		unsigned int nb_channels
		);

//TEXTURE:
int sx_image_create_from_png(struct sx_image** image,const char* filename);
void sx_image_print_ascii(struct sx_image* image,unsigned int channel);
void sx_image_normalise(struct sx_image* image);
void sx_image_release(struct sx_image* sx_image);
int sx_image_save (const struct sx_image* image,const char* filename);
int sx_image_save2(const struct sx_image* image,const char* filepath,const char* filename);
float sx_image_max(const struct sx_image* image);
float sx_image_mean(
		const struct sx_image* image
		);
void sx_image_filter_bright_point(
	const struct sx_image* image,
	float max,
	float mean,
	float threshold  //the threshold needed to consider a point bright
	);
void sx_image_test_write_png();
int sx_image_read (struct sx_image** image_,const char* filename);
int sx_image_save_ppm (const struct sx_image* image,float gamma,const char* filepath,const char* filename);
int sx_image_save_pbm_1_channel (const struct sx_image* image,float gamma,const char* filename);
int sx_image_save_ppm_3_channels (const struct sx_image* image,float gamma,const char* filename);
int sx_image_save_ppm_n_channels (const struct sx_image* image,float gamma,const char* filename);

int sx_image_save_png (
		const struct sx_image* image,
		float gamma,
		float max, //radiance value corresponding to grey value of 255, -1 for auto value
		const char* filepath,
		const char* filename
		);

int sx_image_save_png_3_channels (const struct sx_image* image,float gamma,const char* filename); 
int sx_image_save_png_2_channels (const struct sx_image* image,float gamma,const char* filename); 

int sx_image_save_png_1_channel (
		const struct sx_image* image,
		float gamma,
		float max, //radiance value corresponding to grey value of 255, -1 for auto value
		const char* filename
		);

int sx_image_save_png_n_channels (const struct sx_image* image,float gamma,const char* filename); 


#endif
