/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <rsys/clock_time.h>
#include <rsys/cstr.h>
#include <rsys/mem_allocator.h>

#include "parsing.h"

#include "sx_monte_carlo.h"
#include "sx_scene.h"
#include "sx_variation.h"
#include "sampled_data.h"



#include "bhmie.h"
#include "sx_volume.h"
#include "sx_mat_vol.h"

#include "lists.h"


void open_doc(void)
{
		//execute openscad to create the stl file:
		char command[200];
		strcpy(command,"open http://starlyx.org/starlyx_doc.pdf");
		printf("Execute command: %s\n",command);
		int ret=system(command);
		if (ret) 
		{
			printf("Error executing openscad\n");
		}
	}



int main(int argc, char* argv[])
{
	struct sx_scene* scene;
	sx_scene_create(&scene);
	int res = 0;

	if (argc!=2)
	{
		fprintf(stderr, "Usage: %s  SCENE_XML_FILE \n", argv[0]);
		fprintf(stderr, "%s -h : open online doc \n", argv[0]);
	return 0;
	}

	if (strcmp(argv[1],"-h")==0)
	{
		open_doc();
  return 1;
	}

	//get the project path:
	int i1=parsing_last_index_of(argv[1],'/');
	if (i1!=-1)parsing_substring(scene->file_path,argv[1],0,i1+1);
	else strcpy(scene->file_path,"");

#ifdef PRINT_ALL_STEPS
	printf("scene file path: %s\n",scene->file_path);
#endif

	//import xml and geometry:
	res = sx_scene_import_data(scene,argv[1]);
	if(!res)
	{
		fprintf(stderr, "Couldn't load starlyx scene data\n");
		return 0;
	}
	if (scene->verbose) printf("StarLyx, GPL3 license, gitlab.com/photonlyx/starlyx\n");
	//if (scene->verbose) printf("Scene path: %s\n",scene->file_path);

#ifdef PRINT_ALL_STEPS
	sx_scene_print(scene,stdout);
#endif

	//save the scene for debugging
	char full_file_name[MAX_NAME_LENGTH];
	strcpy(full_file_name,scene->file_path);
	strcat(full_file_name,"scene_check.txt");
	FILE* file=fopen(full_file_name,"w");
	sx_scene_print(scene,file);
	fclose(file);


	//read project if any:
	mxml_node_t *tree1;
	sx_xml_read_xml(&tree1,argv[1]);
	struct sx_variation* variation;
	res=sx_variation_create(&variation,tree1);
	mxmlRelease(tree1);

	if (res)//a Variation xml element has been found
	{
		//study for inversion:
		sx_variation_study(variation,scene);
	}
	else
	{
		//compute single Monte-Carlo:
		struct time t0, t1;
		char dump[64];
		time_current(&t0);
		res = sx_montecarlo_compute(scene);
		if(res==0)
		{
			fprintf(stderr, "Couldn't run Monte-Carlo\n");
			return 0;
		}
		time_sub(&t0, time_current(&t1), &t0);
		time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
		if (scene->verbose) printf("Computation in %s.\n", dump);
		time_current(&t0);
		sx_scene_export_data(scene);
		time_sub(&t0, time_current(&t1), &t0);
		time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
		//if (scene->verbose) printf("Data exported in %s.\n", dump);
	}




	sx_scene_release(scene);
	sx_variation_release(variation);
	/*
	if(mem_allocated_size() != 0)
	{
		fprintf(stderr, "Memory leaks: %lu Bytes\n",(unsigned long)mem_allocated_size());
	}
	*/
	return 1;
}



