/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <star/ssp.h>
#include <mxml.h>
#include <math.h>

#include "sx_mat_surf.h"
#include "sx_configuration.h"
#include "sx_volume.h"
#include "sampled_data.h"
#include "sx_monte_carlo.h"
#include "sx_surface.h"
#include "sx_source.h"
#include "sx_scene.h"
#include "sx_photon.h"
#include "sx_mat_vol.h"
#include "sx_image.h"



int sx_mat_surf_create(struct sx_mat_surf ** pms)
{
	(*pms)=calloc(1, sizeof(struct sx_mat_surf));
	//SURFACE_TYPE_EMISSION:
	(*pms)->angle=90;
	//SURFACE_TYPE_EMISSION_NADA:
	(*pms)->eta0=0;
	(*pms)->j=1;
	//SURFACE_TYPE_GRATING:
	(*pms)->step_nm=0;
	(*pms)->order=1;
	strcpy((*pms)->name,"");
	strcpy((*pms)->spectrum_name,"1");
	//TEXTURE:
	(*pms)->normal_texture[0]=0;//vector perpendicular to the texture image
	(*pms)->normal_texture[1]=0;//vector perpendicular to the texture image
	(*pms)->normal_texture[2]=0;//vector perpendicular to the texture image
	(*pms)->texture_width=0;
	return 1;
}

/**
create a material of surface of type lambert
*/
int sx_mat_surf_create_lambert(
		struct sx_mat_surf ** ms_,
		char* name,
		float albedo
		)
{
	//create a lambert material surface with albedo=0:
	struct sx_mat_surf* ms;
	sx_mat_surf_create(&ms);
	strcpy(ms->name,name);
	ms->type=SURFACE_TYPE_LAMBERT;
	ms->apply=&surface_lambert;
	//set the albedo spectrum:
	sprintf(ms->spectrum_name,"%f",albedo);
	//associate the pointer to function spectrum(lambda) to
	//a discrete function:
	ms->spectrum=&sampled_data_discrete;
	(*ms_)=ms;
	return 1;
}



int sx_mat_surf_create_from_xml(struct sx_mat_surf ** p_p_ms,mxml_node_t *node)
{
	struct sx_mat_surf* ms;
	sx_mat_surf_create(&ms);
	char name[MAX_NAME_LENGTH];
	char buffer[MAX_NAME_LENGTH];
	if (!read_string_attribute2(node,"NAME",name))return 0;
	strcpy(ms->name,name);
	if (strcmp(node->value.element.name,"lambert")==0)
	{
		ms->type=SURFACE_TYPE_LAMBERT;
		ms->apply=&surface_lambert;

		//TEXTURE:
		if (read_string_attribute22(node,"TEXTURE_IMAGE",buffer,0))//check if there is a texture image
		{
			//set the filename of the texture:
			strcpy(ms->texture_filename,buffer);
			//load the texture image:
			if (sx_image_create_from_png(&ms->texture,ms->texture_filename)==1) return 0;
			//printf("Load texture. file:%s  w=%d h=%d %d channels\n",
			//		ms->texture_filename,ms->texture->w,ms->texture->h,ms->texture->nbc);
			//normalise:
			sx_image_normalise(ms->texture);
			//sx_image_print_ascii(ms->texture,0);

			//get the normal of the texture
			mxml_node_t* node2=mxmlFindElement(node,node,"normal_texture",NULL,NULL,MXML_DESCEND);
			if (node2)
			{
				if (!read_string_attribute2(node2,"X",buffer))return 0;
				sscanf(buffer,"%f",&ms->normal_texture[0]);
				if (!read_string_attribute2(node2,"Y",buffer))return 0;
				sscanf(buffer,"%f",&ms->normal_texture[1]);
				if (!read_string_attribute2(node2,"Z",buffer))return 0;
				sscanf(buffer,"%f",&ms->normal_texture[2]);
				//normalise 
				f3_normalize(ms->normal_texture,ms->normal_texture);
			}
			else
			{
				printf("lambert %s texture has no son of type normal_texture\n",ms->name);
				return 0;
			}

			//get the horizontal axis of the texture
			mxml_node_t* node3=mxmlFindElement(node,node,"horizontal_texture",NULL,NULL,MXML_DESCEND);
			if (node3)
			{
				if (!read_string_attribute2(node3,"X",buffer))return 0;
				sscanf(buffer,"%f",&ms->frameX[0]);
				if (!read_string_attribute2(node3,"Y",buffer))return 0;
				sscanf(buffer,"%f",&ms->frameX[1]);
				if (!read_string_attribute2(node3,"Z",buffer))return 0;
				sscanf(buffer,"%f",&ms->frameX[2]);
				//normalise 
				f3_normalize(ms->frameX,ms->frameX);
			}
			else
			{
				printf("lambert %s texture has no son of type horizontal_texture\n",ms->name);
				return 0;
			}

			//create the internal frame
			//sx_geometry_create_frame_from_Z(ms->frameX,ms->frameY,ms->normal_texture);
			if (f3_dot(ms->frameX,ms->normal_texture)!=0)
			{
				printf("lambert %s normal_texture and horizontal_texture are not perpendicular\n",ms->name);
				return 0;
			}
			//calc the Y axis of the texture:
			f3_cross(ms->frameY,ms->normal_texture,ms->frameX);	

			//get the centre of the texture
			node2=mxmlFindElement(node,node,"centre_texture",NULL,NULL,MXML_DESCEND);
			if (node2)
			{
				if (!read_string_attribute2(node2,"X",buffer))return 0;
				sscanf(buffer,"%f",&ms->texture_centre[0]);
				if (!read_string_attribute2(node2,"Y",buffer))return 0;
				sscanf(buffer,"%f",&ms->texture_centre[1]);
				if (!read_string_attribute2(node2,"Z",buffer))return 0;
				sscanf(buffer,"%f",&ms->texture_centre[2]);
			}
			else
			{
				printf("lambert %s texture has no son of type center_texture\n",ms->name);
				return 0;
			}

#ifdef PRINT_ALL_STEPS
			printf("Texture: centre:(%f,%f,%f)   normal:(%f,%f,%f) )  X (%f,%f,%f)  Y (%f,%f,%f)   \n",
					ms->texture_centre[0],ms->texture_centre[1],ms->texture_centre[2],
					ms->normal_texture[0],ms->normal_texture[1],ms->normal_texture[2],
					ms->frameX[0],ms->frameX[1],ms->frameX[2],
					ms->frameY[0],ms->frameY[1],ms->frameY[2]);

#endif
			//get the width of the texture
			if (read_string_attribute22(node,"TEXTURE_WIDTH",buffer,0)) sscanf(buffer,"%f",&ms->texture_width);
			else
			{
				printf("lambert %s texture has no TEXTURE_WIDTH\n",ms->name);
				return 0;
			}

		} 
		else  //uniform albedo
		{
			//read the albedo spectrum:
			char spectrum_name[MAX_NAME_LENGTH];
			if (!read_string_attribute21(node,"ALBEDO",name,spectrum_name))return 0;
			strcpy(ms->spectrum_name,spectrum_name);
			//associate the pointer to function albedo(lambda) to a discrete function
			ms->spectrum=&sampled_data_discrete;
		}
	}
	else if (strcmp(node->value.element.name,"dielectric")==0)
	{
		ms->type=SURFACE_TYPE_DIELECTRIC;
		ms->apply=&surface_dielectric;
		strcpy(ms->spectrum_name,"");
	}
	else if (strcmp(node->value.element.name,"mirror")==0)
	{
		ms->type=SURFACE_TYPE_MIRROR;
		ms->apply=&surface_mirror;
		//read the albedo spectrum:
		if (!read_string_attribute23(node,"R",buffer,0,1,MAX_NAME_LENGTH))
		{
		strcpy(ms->spectrum_name,"1");
			printf("Reflectivity R of %s set to default (%s W/m²/sr)\n",
					ms->name,ms->spectrum_name);
		}
		else
		{
		strcpy(ms->spectrum_name,buffer);
		}




		//associate the pointer to function albedo(lambda) to a discrete function
		ms->spectrum=&sampled_data_discrete;
	}
	else if (strcmp(node->value.element.name,"grating")==0)
	{
		ms->type=SURFACE_TYPE_GRATING;
		ms->apply=&surface_grating;
		int res=read_string_attribute2(node,"A",buffer);
		float a=0;
		//put the default step in nm corresponding to  600 periods/mm
		if (!res) ms->step_nm=1/600*1000000;
		else res=sscanf(buffer,"%f",&a);
		//printf("Grating mat surf: a=%f\n",a);
		//if (res==0) return 0;
		ms->step_nm=1/a*(float)1000000;
		//printf("step=%f\n",ms->step_nm);
		res=read_string_attribute2(node,"ORDER",buffer);
		if (!res) ms->order=1;//put the default
		else res=sscanf(buffer,"%d",&ms->order);
		//printf("order %f\n",a);
		//ms->order=a;
		//if (res==0) return 0;
		//printf("step_nm=%f   order=%d  \n",ms->step_nm,ms->order);
	}
	else if (strcmp(node->value.element.name,"emission")==0)
	{
		ms->type=SURFACE_TYPE_EMISSION;
		ms->apply=&surface_emission;
		//read the emission spectrum:
		char spectrum_name[MAX_NAME_LENGTH];
		if (!read_string_attribute21(node,"SPECTRUM",name,spectrum_name))return 0;
		strcpy(ms->spectrum_name,spectrum_name);
		//associate the pointer to function albedo(lambda) to a discrete function
		ms->spectrum=&sampled_data_discrete;
		//read the max angle of emission:
		if (!read_string_attribute23(node,"ANGLE",buffer,1,1,MAX_NAME_LENGTH)) return 0;
		sscanf(buffer,"%f",&ms->angle);

	}
	else if (strcmp(node->value.element.name,"emission_nada")==0)
	{
		ms->type=SURFACE_TYPE_EMISSION_NADA;
		ms->apply=&surface_emission_nada;
		int res=read_string_attribute2(node,"ETA0",buffer);
		if (!res) ms->eta0=0;//put the default
		else sscanf(buffer,"%f",&ms->eta0);
		res=read_string_attribute2(node,"J",buffer);
		if (!res) ms->j=1;//put the default
		else sscanf(buffer,"%f",&ms->j);
	}
	(* p_p_ms)=ms;
	return 1;
}


void sx_mat_surf_release(struct sx_mat_surf* sx_mat_surf)
{
	free(sx_mat_surf);
}


void sx_mat_surf_print( 
		struct sx_mat_surf * ms,
		FILE* stream
		)
{
	fprintf(stream,"Material surface:\n");
	fprintf(stream,"\tname=%s\n",ms->name);
	fprintf(stream,"\tsurface=%s\n",ms->surface->name);
	switch (ms->type)
	{
		case SURFACE_TYPE_LAMBERT:
			fprintf(stream,"\ttype: lambert\n");
			fprintf(stream,"\t\talbedo: %s\n",ms->spectrum_name);
			break;
		case SURFACE_TYPE_DIELECTRIC:
			fprintf(stream,"\ttype: dielectric\n");
			break;
		case SURFACE_TYPE_MIRROR:
			fprintf(stream,"\ttype: mirror\n");
			break;
		case SURFACE_TYPE_EMISSION:
			fprintf(stream,"\ttype: emission\n");
			break;
		case SURFACE_TYPE_EMISSION_NADA:
			fprintf(stream,"\ttype: emission_nada\n");
			break;
		case SURFACE_TYPE_GRATING:
			fprintf(stream,"\ttype: grating\n");
			break;
		default:
			fprintf(stream,"\ttype: no_surface\n");
	}
	if (ms->texture)
	{
		fprintf(stream,"  This material surface has a texture\n");
		fprintf(stream,"  texture file: %s\n",ms->texture_filename);
		fprintf(stream,"  texture centre=(%f,%f,%f)\n",ms->texture_centre[0],ms->texture_centre[1],ms->texture_centre[2]);
		fprintf(stream,"  texture frameX=(%f,%f,%f)\n",ms->frameX[0],ms->frameX[1],ms->frameX[2]);
		fprintf(stream,"  texture frameY=(%f,%f,%f)\n",ms->frameY[0],ms->frameY[1],ms->frameY[2]);
		float* frameZ=ms->normal_texture;
		fprintf(stream,"  texture frameZ=(%f,%f,%f)\n",frameZ[0],frameZ[1],frameZ[2]);
	}
}




int sx_mat_surf_connect_spectra(struct sx_mat_surf* ms,struct sx_scene* scene)
{
	int is_numerical=-1;
	struct sampled_data* spectrum=sx_scene_find_spectrum2(scene,ms->spectrum_name,&is_numerical);
	if (spectrum==NULL)
	{
		printf("material_surface : %s : can't find spectrum of name %s\n",
				ms->name ,ms->spectrum_name);
		return 0;
	}
	//affect the spectrum:
	ms->spectrum_data=spectrum;
	//affect the function albedo(lambda):
	// just one numerical value is given: put uniform albedo for all wavelengths
	if (is_numerical) ms->spectrum=&sampled_data_constant;
	else ms->spectrum=&sampled_data_discrete;//put discrete function
	return 1;
}

/**
  return 1 if surface is opaque, 0 if not
 */
int sx_mat_surf_is_opaque(struct sx_mat_surf* ms)
{
	switch(ms->type)
	{
		case SURFACE_TYPE_NO_SURFACE:
			return 0;
		case SURFACE_TYPE_LAMBERT:
			return 1;
		case SURFACE_TYPE_DIELECTRIC:
			return 0;
		case SURFACE_TYPE_EMISSION:
			return 1;
		case SURFACE_TYPE_EMISSION_NADA:
			return 1;
		default:
			return 0;
	}
}

/**
  apply photon (path) modification by a lambertian surface.
  The photon might be absorbed (albedo uniform or textured)
  or reflected uniformly with a random angle
 */
int surface_lambert(
		struct sx_mat_surf* ms,
		struct sx_volume* volume1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		)
{
	struct sx_surface* surface=photon->surface;
#ifdef PRINT_ALL_STEPS
	printf("###surface lambert ## Surface name: %s Material name: %s\n",surface->name,ms->name);
	printf("\t photon pos:(%g,%g,%g)\n",photon->x[0],photon->x[1],photon->x[2]);
	printf("\t photon dir:(%g,%g,%g)\n",photon->dir[0],photon->dir[1],photon->dir[2]);
	printf("\t photon comes from %s\n",photon->volume->name);
	printf("\t photon goes to %s\n",volume1->name);
	printf("\t normal (%g,%g,%g)\n",photon->normal[0],photon->normal[1],photon->normal[2]);
#endif
	int ret;
	//apply albedo:
	float albedo=1.0f;
	//TEXTURE:
	if (ms->texture)
	{
		int w=ms->texture->w;
		int h=ms->texture->h;
		int nbc=ms->texture->nbc;
		float size=ms->texture_width;
		float ratio=(float)h/(float)w;
		float xmin=-size/2;
		float xmax=size/2;
		float ymin=-size*ratio/2;
		float ymax=size*ratio/2;
		float sx=(xmax-xmin)/(float)w;
		float sy=(ymax-ymin)/(float)h;
		// coordinates using the normal of the texture
		float v[3];
		f3_sub(v,ms->texture_centre,photon->x);
		float xx=f3_dot(ms->frameX,v);
		float yy=f3_dot(ms->frameY,v);
		int i=(int)((xx-xmin)/sx);
		int j=(int)((yy-ymin)/sy);

		//int i=(int)((-photon->x[1]-xmin)/sx);
		//int j=(int)((photon->x[2]-ymin)/sy);
		//j=h-1-j;//flip vertically
		i=w-1-i;//fli horizontally
		if ((i>=0)&&(i<w)&&(j>=0)&&(j<h)) albedo=ms->texture->data[nbc*(i+w*j)];
		else albedo=0;
		float x=photon->x[1]+xmin;
		float y=photon->x[2]+ymin;
		//printf("\t size=%f w=%d h=%d sx=%f sy=%f photon pos:(%g,%g,%g) (x,y)=(%f,%f) pix=(%d,%d) albedo=%f \n",size,w,h,sx,sy,photon->x[0],photon->x[1],photon->x[2],x,y,i,j,albedo);
		//printf("\t  photon pos:(%f,%f,%f) (x,y)=(%f,%f) pix=(%d,%d) albedo=%f \n",photon->x[0],photon->x[1],photon->x[2],x,y,i,j,albedo);
	}
	else
	{
		albedo=ms->spectrum(ms->spectrum_data,photon->lambda);
	}

	//apply albedo:
	if (albedo<1)
	{
		//sort a float between 0 and 1
		float r=ssp_rng_canonical_float(rng);
		if (r>albedo)
		{
#ifdef PRINT_ALL_STEPS
			printf("surface scat lambert ooo albedo=%g r=%g absorption by surface %s (id %d)\n",albedo,r,surface->name,surface->id);
#endif
			photon->index_end_surface=surface->id;
			photon->is_propagating = 0;
			if (surface->sensor!=NULL)
				 photon->interaction_type=RAY_ENTERED_A_SENSOR;
			else photon->interaction_type=SURFACE_ABSORBED;
			return SURFACE_ABSORBED;
		}
	}

//calculate a new photon direction:
float normal_photon[3];
if (f3_dot(photon->dir,photon->normal)<0) f3_set(normal_photon,photon->normal);
else f3_mulf(normal_photon,photon->normal,-1);
lambertReflexion(rng,photon,normal_photon);
//set the flag:
ret=SURFACE_REFLEXION;
photon->interaction_type=SURFACE_REFLEXION;
//(stay in the same volume)
#ifdef PRINT_ALL_STEPS
printf("     SURFACE_REFLEXION , stay in same volume\n");
#endif
return ret;
}


/**
 * calc the brdf of a lambertian surface
 * dir : direction of light hitting the surface
 * normal: vector perpendicular to the surface at the same side of light source
 **/
float brdf_lambert(float* dir,float* normal)
{
	//brdf of a lambertian surface:
	float brdf=-f3_dot(dir,normal)/(float)PI;
	if (brdf<0) brdf=0;
	return brdf;
}


/**
 * sort the new photon direction after hitting a lambertian surface
 * rng: random number generator
 * photon: the reflected photon
 * normal: vector perpendicular to the surface at the same side than the light dir
 */
void lambertReflexion(struct ssp_rng* rng,struct sx_photon* photon,float* normal)
{
	//sort a cos weighted vector in the hemisphere centered wrt normal that is at the side of the ray:
	float sample[3];
	//we sample a vector (sample) in cos around outside oriented normal:
	ssp_ran_hemisphere_cos_float(rng,normal,sample,NULL);
	f3_set(photon->dir,sample);
	#ifdef PRINT_ALL_STEPS
	printf("###lambert## New direction (%g,%g,%g)-----\n",
			photon->dir[0], photon->dir[1], photon->dir[2]);
	#endif
}




/**
 * apply photon (path) modification by a dielectric surface.
 * check if occurs Fresnel reflexion, turn the photon if yes unless do the refraction
 * volume1: the volume at the other side of the surface
 * return SURFACE_REFLEXION or SURFACE_REFRACTION
 **/
int surface_dielectric(
		struct sx_mat_surf* ms,
		struct sx_volume* volume1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		)
{
	struct sx_surface* surface=ms->surface;
#ifdef PRINT_ALL_STEPS
	printf("###surface dielectric## Surface name: %s\n",surface->name);
	printf("\t dir:(%g,%g,%g)\n",photon->dir[0],photon->dir[1],photon->dir[2]);
	printf("\t photon comes from %s\n",photon->volume->name);
	printf("\t photon goes to %s\n",volume1->name);
	printf("\t normal (%g,%g,%g)\n",photon->normal[0],photon->normal[1],photon->normal[2]);
#endif
	int ret=dielectric(volume1,photon,rng);
	//		printf("Dielectric Photon weight %g\n",photon->w);
	return ret;
}


/**
 * apply photon (path) modification by a mirror surface.
 * return INTERFACE_REFLEXION
 **/
int surface_mirror(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		)
{
	struct sx_surface* surface=ms->surface;
#ifdef PRINT_ALL_STEPS
	printf("###surface mirror## Surface name: %s\n",surface->name);
	printf("\t dir:(%g,%g,%g)\n",photon->dir[0],photon->dir[1],photon->dir[2]);
	printf("\t photon comes from %s\n",photon->volume->name);
	printf("\t normal (%g,%g,%g)\n",photon->normal[0],photon->normal[1],photon->normal[2]);
#endif

	//apply albedo:
	float albedo=1.0f;
	albedo=ms->spectrum(ms->spectrum_data,photon->lambda);
#ifdef PRINT_ALL_STEPS
	printf("\t albedo=%g \n",albedo);
#endif
	if (albedo<1)
	{
		//sort a float between 0 and 1
		float r=ssp_rng_canonical_float(rng);
#ifdef PRINT_ALL_STEPS
		printf("\t r=%g \n",r);
#endif
		if (r>albedo)
		{
#ifdef PRINT_ALL_STEPS
			printf("\t surface_mirror r=%g absorption by surface %s\n",r,surface->name);
#endif
			photon->index_end_surface=surface->id;
			photon->is_propagating = 0;
			photon->interaction_type=SURFACE_ABSORBED;
			return SURFACE_ABSORBED;
		}
	}
	int ret=mirror(photon);
	//(stay in the same volume)
	return ret;
}


/**
 * apply photon (path) modification by a grating.
 * return INTERFACE_REFLEXION
 **/
int surface_grating(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		)
{
	struct sx_surface* surface=ms->surface;
#ifdef PRINT_ALL_STEPS
	printf("###surface grating## Surface name: %s\n",surface->name);
	printf("\t dir:(%g,%g,%g)\n",photon->dir[0],photon->dir[1],photon->dir[2]);
	printf("\t photon comes from %s\n",photon->volume->name);
	printf("\t normal (%g,%g,%g)\n",photon->normal[0],photon->normal[1],photon->normal[2]);
#endif
	printf("###surface grating## Surface name: %s\n",surface->name);
	printf("\t dir:(%g,%g,%g)\n",photon->dir[0],photon->dir[1],photon->dir[2]);
	printf("\t photon comes from %s\n",photon->volume->name);
	printf("\t normal (%g,%g,%g)\n",photon->normal[0],photon->normal[1],photon->normal[2]);
	int ret=grating(surface,photon);
	//(stay in the same volume)
	return ret;
}

/**
 * no surface: the ray just continue in the same direction
 **/
int surface_no_surface(
		struct sx_mat_surf* ms,
		struct sx_volume* volume1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		)
{
#ifdef PRINT_ALL_STEPS
	printf("###surface: No surface### surface name: %s \n",surface->name);
#endif
	//the ray just continue in the same direction
	// continue inside the other volume
	photon->volume=volume1;
	photon->interaction_type=SURFACE_NO_INTERACTION;
	return SURFACE_NO_INTERACTION;
}



/**
 * apply photon (path) modification by a light emitting surface .
 * return SURFACE_EMISSION
 **/
int surface_emission(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		)
{
	struct sx_surface* surface=ms->surface;
#ifdef PRINT_ALL_STEPS
	printf("###surface emission## Surface name: %s\n",surface->name);
	printf("\t photon dir:(%g,%g,%g)\n",photon->dir[0],photon->dir[1],photon->dir[2]);
	printf("\t photon comes from %s\n",photon->volume->name);
	printf("\t normal (%g,%g,%g)\n",photon->normal[0],photon->normal[1],photon->normal[2]);
#endif
	//get the radiance of the source:
	double radiance=ms->spectrum(ms->spectrum_data,photon->lambda);
//chech if the ray is inside the emission cone of the source:
	if (surface->source)
	{
		struct sx_source* source=surface->source;
		float cos_theta=f3_dot(photon->dir,photon->normal);
	    //printf("###surface emission## Surface name: %s radiance : %g cos_theta=%f\n",surface->name,radiance,cos_theta);
		if (cos_theta>cos(source->angle/2*PI/180.0)) 
			{
 			//printf("###surface emission## add radiance %g\n", source->radiance);
			radiance*=source->radiance;
			}
	}
	//radiance*=fabs(f3_dot(photon->dir,photon->normal));
	//printf("###surface emission## Surface name: %s radiance : %g\n",surface->name,radiance);
	//we have decided that the normal direction is the one given by blender
	//the normal given by s3d is in the opposite same direction as the one given by blender
	//the light direction emitted from the source is then opposite as the normal given by s3d
	//then there is no need to invert both normal and photon dir
	//if (f3_dot(photon->dir,photon->normal)>cos(ms->angle/2*PI/180.0)) 	
	photon->w+=radiance;
	photon->is_propagating = 0;
	int ret=SURFACE_EMISSION;
	photon->interaction_type=SURFACE_EMISSION;
	return ret;
}


/**
 * apply photon (path) modification by a light emitting surface .
 * the surface emits light following the Chandrasekhar infinite space solution of
 * Radiative Transfer Equation 1D along x called "NADA"
 * return INTERFACE_EMISSION_NADA
 **/
int surface_emission_nada(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		)
{
	struct sx_surface* surface=ms->surface;
#ifdef PRINT_ALL_STEPS
	printf("###surface emission## Surface name: %s\n",surface->name);
	printf("\t dir:(%g,%g,%g)\n",photon->dir[0],photon->dir[1],photon->dir[2]);
	printf("\t photon comes from %s\n",photon->volume->name);
	printf("\t normal (%g,%g,%g)\n",photon->normal[0],photon->normal[1],photon->normal[2]);
#endif
	if (photon->volume->nb_mv != 1) 
		printf("surface_emission_nada, the volume %s must have one and only\
				one sx_mat_vol material\n",photon->volume->name);
	struct sx_mat_vol* matvol=photon->volume->mv[0];
	double g=matvol->g(matvol,photon->lambda);
	double ks=matvol->ks(matvol,photon->lambda);
	double w=ms->eta0/(4*PI)+3/(4*PI)*ms->j*((g-1)*ks*photon->x[0]-photon->dir[0]);
	//	printf("surface_emission_nada (volume: %s) eta0=%f j=%f w=%g\n",photon->volume->name,ms->eta0,ms->j,w);
	photon->w+=w;
	photon->is_propagating = 0;
	int ret=SURFACE_EMISSION_NADA;
	photon->interaction_type=SURFACE_EMISSION_NADA;
	return ret;
}




/**
 * calc the normal which direction is the opposite of the path direction
 */
void calcNormalOppositeToPath(struct sx_photon* photon,float* normal2)
{
	//turn eventually  the normal in the opposite direction of the ray:
	double cosi1=f3_dot( photon->dir,photon->normal);
	if (cosi1>0)
	{
		f3_mulf(normal2,photon->normal,-1);
#ifdef PRINT_ALL_STEPS
		printf("###lambert## cosi1>0, invert normal\n");
		printf("###lambert## normal (%g,%g,%g)\n",photon->normal[0],photon->normal[1],photon->normal[2]);
#endif
	}
	else f3_set(normal2,photon->normal);
}




/**
 * check if occurs fresnel reflexion, turn the photon if yes unless do the refraction
 * illumination from source is zero
 * return POBJECT_REFLEXION or POBJECT_REFRACTION
 **/
int dielectric(
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		)
{
	int result=0;
	float r;
	float sini1,cosi1;
	struct sx_volume* vol=photon->volume;

	//turn eventually  the normal in the opposite direction of the ray:
	float normal3[3];
	cosi1=f3_dot(photon->dir,photon->normal);
	if (cosi1>0)
	{
		f3_mulf(normal3,photon->normal,-1);
#ifdef PRINT_ALL_STEPS
		printf("\t cosi1>0, invert normal\n");
#endif
	}
	else
	{
		cosi1*=-1;
		f3_set(normal3,photon->normal);
	}
#ifdef PRINT_ALL_STEPS
	printf("\t normal in direction of ray: (%g,%g,%g)\n",normal3[0],normal3[1],normal3[2]);
#endif

	float n=1,n1=1;
	if (vol!=NULL) n=vol->n(vol->spectrum_n,photon->lambda);
	if (vol1!=NULL) n1=vol1->n(vol1->spectrum_n,photon->lambda);
#ifdef PRINT_ALL_STEPS
	printf("\t fresnel: n1=%g  n2=%g \n",n,n1);
#endif
	sini1=sqrtf(1-cosi1*cosi1);
	if (sini1>n1/n) //case of angle > brewster angle
	{
#ifdef PRINT_ALL_STEPS
		printf("\t TOTAL REFLEXION (sini1=%g)\n",sini1);
#endif
		reflection(photon->dir,cosi1, normal3);
		//(stay in the same volume)
		result=SURFACE_REFLEXION;
		photon->interaction_type=SURFACE_REFLEXION;
	}
	else
	{
		double fresnelCoef=calcFresnelCoef(n,n1,cosi1,sini1);
#ifdef PRINT_ALL_STEPS
		printf("\t cosi1=%g sini1=%g\n",cosi1,sini1);
		printf("\t fresnelCoef %g\n",fresnelCoef);
#endif
		// je tire au sort pour savoir si je vais subir une réflexion:
		r=ssp_rng_canonical_float(rng);
		if (r>(1-fresnelCoef))
		{
#ifdef PRINT_ALL_STEPS
			printf("\t  rand=%g REFLEXION\n",r);
#endif
			//f3_mulf(normal3,normal3,-1);
			reflection(photon->dir,cosi1, normal3);
			//(stay in the same volume)
			//set the flag:
			result=SURFACE_REFLEXION;
			photon->interaction_type=SURFACE_REFLEXION;
		}
		else
		{
#ifdef PRINT_ALL_STEPS
			printf("REFRACTION\n");
			printf("\t dir (%g,%g,%g)-----\n", photon->dir[0], photon->dir[1], photon->dir[2]);
#endif
			refraction(photon->dir,n,n1,sini1,cosi1,normal3);
#ifdef PRINT_ALL_STEPS
			printf("\t new dir= (%g,%g,%g)-----\n", photon->dir[0], photon->dir[1], photon->dir[2]);
#endif
			// continue inside the other volume:
			photon->volume=vol1;
			result=SURFACE_REFRACTION;
			photon->interaction_type=SURFACE_REFRACTION;
		}
	}
	return result;


}




/**
 * mirror reflection on the surface
 * return POBJECT_REFLEXION
 **/
int mirror(struct sx_photon* photon)
{
	int result=0;
	float cosi1;

	//turn eventually  the normal in the opposite direction of the ray:
	float normal3[3];
	cosi1=f3_dot( photon->dir,photon->normal);
	if (cosi1>0)
	{
		f3_mulf(normal3,photon->normal,-1);
#ifdef PRINT_ALL_STEPS
		printf("\t cosi1>0, invert normal\n");
#endif
	}
	else
	{
		cosi1*=-1;
		f3_set(normal3,photon->normal);
	}
#ifdef PRINT_ALL_STEPS
	printf("\t normal in direction of ray: (%g,%g,%g)\n",normal3[0],normal3[1],normal3[2]);
	printf("\t MIRROR\n");
#endif
	reflection(photon->dir,cosi1,normal3);
	//(stay in the same volume)
	result=SURFACE_REFLEXION;
	photon->interaction_type=SURFACE_REFLEXION;
	return result;
}

/**
  deviates the photon following the grating law
step_nm: step of the grating in nm
order : order of the diffraction (usually one)
 */
int grating(
		struct sx_mat_surf* ms,
		struct sx_photon* photon
		)
{
	float cosi1;
	//turn eventually  the normal in the opposite direction of the ray:
	float normal3[3];
	cosi1=f3_dot( photon->dir,photon->normal);
	if (cosi1>0)
	{
		f3_mulf(normal3,photon->normal,-1);
#ifdef PRINT_ALL_STEPS
		printf("\t cosi1>0, invert normal\n");
#endif
		printf("\t cosi1>0, invert normal\n");
	}
	else
	{
		cosi1*=-1;
		f3_set(normal3,photon->normal);
	}
	printf("\t normal3= (%g,%g,%g)\n",normal3[0],normal3[1],normal3[2]);

	printf("step_nm=%f   order=%d  \n",ms->step_nm,ms->order);
	//angleSortie=ASIN(lambda/pas*ordre-SIN(angleEntrée))
	float sini1=sqrtf(1-cosi1*cosi1);
	float sini2=photon->lambda/ms->step_nm*(float)ms->order+sini1;
	printf("sin i2=%f \n",sini2);
	if (sini2<1)
	{
		//printf("sini1=%f\n",sini1);
		//printf("sini2=%f\n",sini2);
		float cosi2=sqrtf(1-sini2*sini2);
		float n1[3],n2[3];// (n,n1,n2) is an orthonormal frame
		f3_cross(n2,normal3,photon->dir);
		f3_normalize(n2,n2);
		f3_cross(n1,n2,normal3);
		//printf("\t n1= (%g,%g,%g)\n",n1[0],n1[1],n1[2]);
		//printf("\t n2= (%g,%g,%g)\n",n2[0],n2[1],n2[2]);

		float np[3],n1p[3];
		f3_mulf(np,normal3,cosi2);
		f3_mulf(n1p,n1,sini2);
		f3_add(photon->dir,np,n1p);
		printf("\t grating out= (%g,%g,%g)\n",photon->dir[0],photon->dir[1],photon->dir[2]);
	}
	else
	{
		photon->index_end_surface=ms->surface->id;
		photon->is_propagating = 0;
		photon->interaction_type=SURFACE_GRATING_OUT_OF_RANGE;
		printf("\t SURFACE_GRATING_OUT_OF_RANGE\n");
		return SURFACE_GRATING_OUT_OF_RANGE;
	}
	return 1;
}


/**
 * return the Fresnel coefficient (specular light /refracted light  fraction)
 * see : https://en.wikipedia.org/wiki/Fresnel_equations
 n1 refractive index of medium 1
 n2 refractive index of medium 2
 cosi1 precalculated cos of input ray wrt normal angle
 sini1 precalculated cos of input ray wrt normal angle
 * @return
 */
double calcFresnelCoef(double n1,double n2, double cosi1,double sini1)
{
	if (n1==n2) return 0;
	double r=n1/n2*sini1;
	double f=sqrt(1-r*r);
	//s polarized coef (perpendicular):
	r=(n1*cosi1-n2*f)/(n1*cosi1+n2*f);
	double rs=r*r;
	//p polarized coef (parallel):
	r=(-n2*cosi1+n1*f)/(n2*cosi1+n1*f);
	double rp=r*r;
	//unpolarized light:
	r=(rs+rp)/2;
	return r;
}


/**
 *
 * @param dir the photon direction
 * @param cosi1 cos of incident angle
 * @param n vector normal to the surface in the opposite direction of the ray
 */
void reflection(
		float* dir,
		float cosi1,
		float* normal
		)
{
	float r1[3];
	f3_mulf(r1,normal,2.0f*cosi1);
	f3_add(dir,dir,r1);
}


/**
 *
 * @param dir photon direction
 * @param m
 * @param mnext
 * @param sini1 sinus of the incident angle between normal and photon
 * @param cosi1 cosinus of the incident angle between normal and photon
 * @param n   vector normal to the surface in the in the opposite direction of the ray
 */
void refraction(
		float* dir,
		float index1,
		float index2,
		float sini1,
		float cosi1,
		float* normal
		)
{
	//apply Snell-Descartes law:
	float sini2=index1/index2*sini1;
	float cosi2=sqrtf(1.0f-sini2*sini2);
	float r[3],n1[3];
	//creation of a  vector perpendicular to n of norm 1
	// n1 = dir + cosi1 * normal
	f3_mulf(r,normal,cosi1);
	f3_add(n1,dir,r);
	// normalize n1
	f3_normalize(n1,n1);
	float r2[3];
	// dir = sini2*n1 - cosi2*n
	f3_mulf(r,n1,sini2);
	f3_mulf(r2,normal,cosi2);
	f3_sub(dir,r,r2);
	// normalize dir
	f3_normalize(dir,dir);
}




/**
  get the proba for the photon to be scattered at the exit direction 
inputs:
	ms: the surface material
	rng: the random number generator
	photon: the photon (path) hitting the surface
	light_dir: source ray direction
 */
float sx_mat_surf_scatter_pdf(
		struct sx_mat_surf* ms,
		struct sx_photon* photon,
		float* light_dir
		)
{
	float normal[3];
	float brdf=0;
	switch(ms->type)
	{
		case SURFACE_TYPE_NO_SURFACE:
			brdf=0;
			break;
		case SURFACE_TYPE_DIELECTRIC:
			brdf=0;
			break;
		case SURFACE_TYPE_MIRROR:
			brdf=0;
			break;
		case SURFACE_TYPE_EMISSION:
			brdf=0;
			break;
		case SURFACE_TYPE_EMISSION_NADA:
			brdf=0;
			break;
		case SURFACE_TYPE_GRATING:
			brdf=0;
			break;
		case SURFACE_TYPE_LAMBERT:
			//calc lambert BRDF:
			//set the normal at the same side of the sources:
			if (f3_dot(light_dir,photon->normal)<0) f3_set(normal,photon->normal);
			else f3_mulf(normal,photon->normal,-1);
			//brdf of a lambertian surface:
			brdf=-f3_dot(light_dir,normal)/(float)PI;
			if (brdf<0) brdf=0;
			float albedo=ms->spectrum(ms->spectrum_data,photon->lambda);
			brdf*=albedo;
		break;
	}
	return brdf;
}
