/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SX_MAT_SURF_H_
#define SX_MAT_SURF_H_

enum {
	SURFACE_TYPE_NO_SURFACE,
	SURFACE_TYPE_LAMBERT,
	SURFACE_TYPE_DIELECTRIC,
	SURFACE_TYPE_MIRROR,
	SURFACE_TYPE_EMISSION,
	SURFACE_TYPE_EMISSION_NADA,
	SURFACE_TYPE_GRATING};

#include "sx_xml.h"

struct sx_scene;
struct sx_surface;
struct sx_volume;
struct filter_context;
struct sx_photon;

struct sx_mat_surf
{
	char name[MAX_NAME_LENGTH];//name of the surface material
	struct sx_surface* surface; //the surface where is this surface material

	//FRONT SURFACE:
	int type;// SURFACE_TYPE_NO_SURFACE, SURFACE_TYPE_LAMBERT, etc...
		 //transform the path:
	int (*apply)(struct sx_mat_surf* ms,
			struct sx_volume* vol1,
			struct sx_photon* photon,
			struct ssp_rng* rng);

	//the  name of the  albedo spectrum (or source spectrum for emission)
	//the  albedo (for lambertian) or emission (for emission mat surf) spectrum
	char spectrum_name[MAX_NAME_LENGTH];
	struct sampled_data* spectrum_data;
	//function that gives albedo(lambda) for lambert or spectrum for emission
	//lambda in nm (for type SURFACE_TYPE_LAMBERT)
	float (*spectrum)(struct sampled_data* spectrum,float lambda); 

	//for the case of SURFACE_TYPE_EMISSION:
	float angle;

	//for the case of SURFACE_TYPE_EMISSION_NADA:
	float eta0;
	float j;

	//for the case of SURFACE_TYPE_GRATING:
	float step_nm;// step of the eventual grating in nm
	int order;//order of diffraction of the eventual grating

	//for the case of TEXTURE:
	char texture_filename[MAX_NAME_LENGTH];
	float texture_centre[3];
	float normal_texture[3];//vector perpendicular to the texture image
	float frameX[3];
	float frameY[3];
	struct sx_image* texture;//float image of the texture
	float texture_width;//width of the texture image in mm

};


int sx_mat_surf_create( 
		struct sx_mat_surf ** ms
		);

int sx_mat_surf_create_lambert(
		struct sx_mat_surf ** ms_,
		char* name,
		float albedo
		);

int sx_mat_surf_create_from_xml( 
		struct sx_mat_surf ** p_p_mv,
		mxml_node_t *node
		);

void sx_mat_surf_print( 
		struct sx_mat_surf * ms,
		FILE* stryyeam
);

int sx_mat_surf_is_opaque(
		struct sx_mat_surf* ms
		);

void sx_mat_surf_release(
		struct sx_mat_surf* sx_mat_surf
		);

int sx_mat_surf_connect_spectra(
		struct sx_mat_surf* ms,
		struct sx_scene* scene);


int surface_no_surface(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		);

int surface_lambert(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		);

int surface_dielectric(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		);

int surface_mirror(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		);

int surface_emission(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
);

int surface_emission_nada(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
);

int surface_grating(
		struct sx_mat_surf* ms,
		struct sx_volume* vol1,
		struct sx_photon* photon,
		struct ssp_rng* rng
		);

void calcNormalOppositeToPath(
		struct sx_photon* photon,
		float* normal2);

float brdf_lambert(
		float* dir,
		float* normal);

void lambertReflexion(
		struct ssp_rng* rng,
		struct sx_photon* photon,
		float* normal2);

int dielectric(
		struct sx_volume* object1,
		struct sx_photon* photon,
		struct ssp_rng* rng);

double calcFresnelCoef(
		double n1,
		double n2, 
		double cosi1,
		double sini1
		);

void reflection(
		float* dir,
		float cosi1,
		float* normal
		);

void refraction(
		float* dir,
		float index1,
		float index2,
		float sini1,
		float cosi1,
		float* normal);

int mirror(
		struct sx_photon* photon
		);

int grating(
		struct sx_mat_surf* ms,
		struct sx_photon* photon
		);


float sx_mat_surf_scatter_pdf(	
		struct sx_mat_surf* ms,
		struct sx_photon* photon,
		float* light_dir
		);


#endif /* SX_MAT_SURF_H_ */
