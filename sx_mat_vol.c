/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sx_xml.h"
#include "sampled_data.h"
#include "sx_mat_vol.h"
#include "sx_scene.h"
#include "bhmie.h"
#include "sx_volume.h"
#include "sx_photon.h"
#include "sx_phase_function.h"
#include "cplx.h"
#include "sx_image.h" 
#include "sx_geometry.h" 
#include "sx_polydispersity.h"



/**
material volume property that defines photon scattering
*/


void sx_mat_vol_create( 
    struct sx_mat_vol** mv_,
    const char* name_
    )
{
	struct sx_mat_vol* mv;
	mv=calloc(1,sizeof(struct sx_mat_vol));
	strcpy(mv->name,name_);
	//associate the pointer to function ks(lambda) to a discrete function:
	mv->ks=&sx_mat_vol_ks_discrete;
	//associate the pointer to function ka(lambda) to a discrete function:
	mv->ka=&sx_mat_vol_ka_discrete;
	//associate the pointer to function g(lambda) to a discrete function:
	mv->g=&sx_mat_vol_g_discrete;
	mv->volume=NULL;
	strcpy(mv->phase_function_name,"");
	strcpy(mv->spectrum_A_name,"");
	strcpy(mv->spectrum_B_name,"0");
	strcpy(mv->spectrum_C_name,"");
	strcpy(mv->spectrum_D_name,"");
 mv->nang=90;
	//init the tables for acceleration:
	for (int i=0;i<1000;i++)
	{
		mv->ksmie[i]=-1;
		mv->kamie[i]=-1;
		mv->gmie[i]=-1;
	}
	(*mv_)=mv;
}


void sx_mat_vol_release( 
    struct sx_mat_vol * mv
   )
{
	//	sampled_data are released by the scene
}

int sx_mat_vol_create_from_xml( 
    struct sx_mat_vol ** p_p_mv,
    mxml_node_t *node
)
{
	//get the name of the material_volume:
	char name[MAX_NAME_LENGTH];

	if (!read_string_attribute2(node,"NAME",name))return 0;
	struct sx_mat_vol* mv;
	sx_mat_vol_create(&mv,name);

	//determine the type of parametrisation:
	char word[MAX_NAME_LENGTH];
	int ik=read_string_attribute22(node,"K",word,0);
	int iks=read_string_attribute22(node,"KS",word,0);
	ik=(iks||ik);
	int i2=read_string_attribute22(node,"LSTAR",word,0);
	int i3=read_string_attribute22(node,"D_UM",word,0);
	int i4=read_string_attribute22(node,"PHASE_FUNCTION",word,0);
	int i41=read_string_attribute22(node,"PHASE_FUNCTIONS",word,0);
	if ((ik==0)&&(i2==0)&&(i3==0))
	{
		printf("mat_vol %s must have KS, LSTAR or D attributes\n",name);
		return 0;
	}
	if (ik)	
	{
		if (i4||i41) mv->type=TYPE_K_KA_PHASE;
		else mv->type=TYPE_K_KA_G;
	}
	else if (i2) mv->type=TYPE_LSTAR_LA_G;
	else if (i3) mv->type=TYPE_HG_MIE;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
			//this is Henyey-Greenstein phase function using ks g and ka parameters:
			if (!read_string_attribute2(node,"KS",mv->spectrum_A_name))
			{
				if (!read_string_attribute2(node,"K",mv->spectrum_A_name))return 0;
			}
			if (!read_string_attribute2(node,"KA",mv->spectrum_B_name))return 0;
			if (!read_string_attribute2(node,"G",mv->spectrum_C_name))return 0;
			break;
		case TYPE_LSTAR_LA_G:
			//this is Henyey-Greenstein phase function using l* g and la parameters:
			if (!read_string_attribute2(node,"LSTAR",mv->spectrum_A_name))return 0;
			if (!read_string_attribute2(node,"LA",mv->spectrum_B_name))return 0;
			if (!read_string_attribute2(node,"G",mv->spectrum_C_name))return 0;
			break;
		case TYPE_HG_MIE:
			sx_mat_vol_read_hg_mie_from_xml(mv,node);
			break;
		case TYPE_K_KA_PHASE:
			//if (!read_string_attribute2(node,"K",mv->spectrum_A_name))return 0;
			if (!read_string_attribute2(node,"KS",mv->spectrum_A_name))
			{
				if (!read_string_attribute2(node,"K",mv->spectrum_A_name))return 0;
			}
			if (!read_string_attribute2(node,"KA",mv->spectrum_B_name))return 0;
			{
				//get the list of the phase function for each wavelength:
				if (!read_string_attribute2(node,"PHASE_FUNCTIONS",mv->phase_function_names))return 0;
				//get the wavelengths:
				char** words;
				int nb_words;
				parsing_parse_words(mv->phase_function_names,&words,&nb_words);
				/*
				   if (nb_words==1)
				   {
				//just one phase function for all wavelenghts
				printf("****** just one phase function: %s\n",words[0]);
				sx_phase_function_create(&(mv->pfs[0]),mv->phase_function_name,0);
				}
				else
				 */
				if (nb_words%2!=0) 
				{
					printf("mat_vol %s: PHASE_FUNCTIONS attribute must have \
							an even nb of words: %s\n",name,mv->phase_function_names);
					return 0;
				}
				else
				{
					//get the names of the phase functions for each wavelength:
					int nb_lambdas=nb_words/2;
					for (int i=0;i<nb_lambdas;i++)
					{
						char* word1=words[i*2];
						int lambda=0;
						sscanf(word1,"%d",&lambda);
						char* pf_name=words[i*2+1];
						//printf("****** lambda=%d pf name=%s\n",lambda,pf_name);
						sx_phase_function_create(&(mv->pfs[lambda]),pf_name,lambda);
					}
					//for (int i=0;i<1000;i++) 
					//if (mv->pfs[i]!=NULL) 
					//printf("******i=%d  lambda=%d pf name=%s\n",i,mv->pfs[i]->lambda,mv->pfs[i]->name);
				}	
			}

			break;
	}
	(* p_p_mv)=mv;
	return 1;
}


int sx_mat_vol_read_hg_mie_from_xml(
		struct sx_mat_vol *mv,
		mxml_node_t *node
)
{
	char buffer[MAX_NAME_LENGTH];
	//this is Henyey-Greenstein phase function with ks and g calculated by Mie:
	if (!read_string_attribute2(node,"PHI",mv->spectrum_A_name))return 0;
	if (read_string_attribute23(node,"NI",buffer,0,1,MAX_NAME_LENGTH))
		strcpy(mv->spectrum_B_name,buffer);
	else  
	{
		printf("Node %s of type %s has no NI attribute. ",mv->name,node->value.element.name);
		strcpy(mv->spectrum_B_name,"0");
		printf("NI set to default (%s)\n",mv->spectrum_B_name);
	}
	if (!read_string_attribute2(node,"D_UM",mv->spectrum_C_name))return 0;
	if (!read_string_attribute22(node,"N",buffer,0))
	{
		if (!read_string_attribute2(node,"NR",mv->spectrum_D_name))return 0;
	}
	//else  strcpy(mv->spectrum_D_name,buffer);
	else 
	{
		strcpy(mv->spectrum_D_name,buffer);
		printf("mat_vol %s please use NR attribute instead of N\n",mv->name);
	}
	return 1;
}


int sx_mat_vol_read_Mie_from_xml( struct sx_mat_vol* mv,mxml_node_t *node)
{
	//get the name of the material_volume:
	char name[MAX_NAME_LENGTH];
	char buffer[200];
	if (!read_string_attribute2(node,"NAME",name))return 0;
	strcpy(mv->name,name);
	//check the attributes:
	char word[MAX_NAME_LENGTH];
	//get the attribute NR:
	if (!read_string_attribute22(node,"N",buffer,0))
	{
		if (!read_string_attribute2(node,"NR",mv->spectrum_D_name))return 0;
	}
	else 
	{
		strcpy(mv->spectrum_D_name,buffer);
		printf("mat_vol %s please use NR attribute instead of N\n",name);
	}
	//get the attribute PHI:
	int iphi=read_string_attribute23(node,"PHI",word,1,1,MAX_NAME_LENGTH);
	if (iphi==0)
	{
		printf("mat_vol %s of type %s: bad syntax.\n",name,node->value.element.name);
		return 0;
	}
	else
	{
		strcpy(mv->spectrum_A_name,word);
	}
	//get the attribute NI:
	int ini=read_string_attribute23(node,"NI",word,1,1,MAX_NAME_LENGTH);
	if (ini==0)
	{
		printf("mat_vol %s of type %s: bad syntax.\n",name,node->value.element.name);
		strcpy(mv->spectrum_B_name,"0");
		printf("NI set to default (%s)\n",mv->spectrum_B_name);
	}
	else strcpy(mv->spectrum_B_name,word);
	//get the attribute D_UM:
	int id=read_string_attribute22(node,"D_UM",word,0);
	if (id==0)
	{
		printf("mat_vol %s must have D_UM attribute\n",name);
		return 0;
	}
	if (!read_string_attribute2(node,"D_UM",mv->spectrum_C_name))return 0;
	//get the attribute NANG:
	ini=read_string_attribute23(node,"NANG",word,1,1,MAX_NAME_LENGTH);
	if (ini==0)
	{
		mv->nang=90;
		printf("NANG set to default (%d)\n",mv->nang);
	}
	else	sscanf(word,"%u",&mv->nang);
	//set the type of mat_vol:
	mv->type=TYPE_MIE;

	return 1;
}









void sx_mat_vol_print(
		struct sx_mat_vol* mv,
		FILE* stream,
		char* indentation
		)
{
	fprintf(stream,"%sMaterial of volume:\n",indentation);
	fprintf(stream,"%s\tname=%s\n",indentation,mv->name);
	if (mv->volume!=NULL) fprintf(stream,"%s\tvolume name=%s\n",indentation,mv->volume->name);
	switch(mv->type)
	{
		case TYPE_K_KA_G:
			fprintf(stream,"%s\ttype K_KA_G\n",indentation);
			fprintf(stream,"%s\tks: %s\n",indentation,mv->spectrum_A_name);
			fprintf(stream,"%s\tka: %s\n",indentation,mv->spectrum_B_name);
			fprintf(stream,"%s\tg: %s\n",indentation,mv->spectrum_C_name);
			break;
		case TYPE_LSTAR_LA_G:
			fprintf(stream,"%s\ttype LSTAR_LA_G\n",indentation);
		 fprintf(stream,"%s\tl*: %s\n",indentation,mv->spectrum_A_name);
			fprintf(stream,"%s\tla: %s\n",indentation,mv->spectrum_B_name);
			fprintf(stream,"%s\tg: %s\n",indentation,mv->spectrum_C_name);
			break;
		case TYPE_HG_MIE:
			fprintf(stream,"%s\ttype HG_MIE\n",indentation);
			fprintf(stream,"%s\tphi: %s\n",indentation,mv->spectrum_A_name);
			fprintf(stream,"%s\td: %s\n",indentation,mv->spectrum_C_name);
			fprintf(stream,"%s\tnr: %s\n",indentation,mv->spectrum_D_name);
			fprintf(stream,"%s\tni: %s\n",indentation,mv->spectrum_B_name);
			break;
		case TYPE_MIE:
			fprintf(stream,"%s\ttype MIE\n",indentation);
			fprintf(stream,"%s\tphi: %s\n",indentation,mv->spectrum_A_name);
			fprintf(stream,"%s\tni: %s\n",indentation,mv->spectrum_B_name);
			fprintf(stream,"%s\td: %s\n",indentation,mv->spectrum_C_name);
			fprintf(stream,"%s\tnr: %s\n",indentation,mv->spectrum_D_name);
			break;
	}
}


/**
 * get ks(lambda) using a discrete spectrum
 */
float sx_mat_vol_ks_discrete(struct sx_mat_vol* mv,float lambda)
{
	float ks=0;//scattering coefficient in mm-1
	float lstar=0;//transport length in mm
	double g=0;
	int lambda_int=(int)lambda;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
		case TYPE_K_KA_PHASE:
			ks=sampled_data_discrete(mv->spectrum_A,lambda);
			break;
		case TYPE_LSTAR_LA_G:
			//k=1/l*/(1-g)
			lstar=sampled_data_discrete(mv->spectrum_A,lambda);
			g=sampled_data_discrete(mv->spectrum_C,lambda);
			ks=1.0f/lstar/(1.0f-(float)g);
			break;
		case TYPE_HG_MIE:
		case TYPE_MIE:
			if (mv->ksmie[lambda_int]==-1)//value not yet calculated
			{
				calc_mie_ks_ka_and_phase_function(mv,lambda);
			}
			ks=mv->ksmie[lambda_int];
			break;
	}
	return ks;
}


void calc_mie_ks_ka_and_phase_function(struct sx_mat_vol* mv,float lambda)
{
	float* vals;//values of the phase function
	double qsca;//scattering cross section
	double qext;//extinction cross section
	double g;//assymetry coef
	float phi=mv->spectrum_A->y[0];//volume fraction (not in %, independent of lambda)
	float d_um=mv->spectrum_C->y[0];//particle diameter, independent of lambda
	float np_r=sampled_data_discrete(mv->spectrum_D,lambda);//particle ref index real, can depends on lambda 
	float np_i=sampled_data_discrete(mv->spectrum_B,lambda);//imaginary part of the particle refractive index 
	float nf=1.33f;//real part of the particle refractive index 
	if (mv->volume!=NULL) 
		//index of the continuous phase may depend on lambda
		nf=mv->volume->n(mv->volume->spectrum_n,lambda);
	bhmie_calc_mie_phase_function(&vals,&qsca,&qext,&g,d_um,np_r,np_i,nf,lambda,mv->nang);
	//see paper Bru, P., Brunel, L., Buron, H., Cayré, I., Ducarre, X., Fraux, A., … Snabre, P. (2004).
	//Particle Size and Rapid Stability Analyses of Concentrated Dispersions:
	// Use of Multiple Light Scattering Technique. Particle Sizing and Characterization, 45–60.
	//DOI: 10.1021/bk-2004-0881.ch003
	int lambda_int=(int)lambda;
	float ks=3.0f*phi*(float)qsca/(2.0f*d_um/1000.0f);
	float kext=3.0f*phi*(float)qext/(2.0f*d_um/1000.0f);
	float ka=kext-ks;
	mv->ksmie[lambda_int]=ks;
	mv->kamie[lambda_int]=ka;
	mv->gmie[lambda_int]=(float)g;
	//float l=1/ks;
	//float la=1/ka;
	//printf("calc_mie_ks_ka_and_phase_function, lambda=%d: ks=%fmm-1 l=%fmm g=%f  ka=%fmm-1 la=%fmm\n",lambda_int,ks,l,g,ka,la);
	//create the phase function for this lambda:
	struct sx_phase_function* pf1;
	sx_phase_function_create(&pf1,"name",lambda_int);
	//create the data of the phase function:
	int dim=(int)mv->nang*2-1;
	struct sampled_data* sd;
	char name[MAX_NAME_LENGTH];
	sprintf(name,"#d=%fum nf=%f np_r=%f np_i=%f lambda=%dnm",d_um,nf,np_r,np_i,lambda_int);
	sampled_data_create2(&sd,dim,name);
	float sx=(float)PI/(float)(dim-1);
	for (int i=1;i<=dim;i++)
	{
		float angle_rad=(float)(i-1)*sx;
		sd->x[i-1]=angle_rad;
		sd->y[i-1]=vals[i-1];
	}
	free(vals);
	//save the phase function for this wavelength:
	//char filename[MAX_NAME_LENGTH];
	//sprintf(filename,"%s_%dnm.txt",mv->name,lambda_int);
	//sampled_data_save(sd,filename);
	//configure the phase function with this angular data:
	sx_phase_function_configure_from_sampled_data(pf1,sd);
	mv->pfs[lambda_int]=pf1;
}




/**
 * get ka(lambda) using a discrete spectrum
 */
float sx_mat_vol_ka_discrete(struct sx_mat_vol* mv,float lambda)
{
	float ka=0,la=0;
	int lambda_int;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
		case TYPE_K_KA_PHASE:
			ka=sampled_data_discrete(mv->spectrum_B,lambda);
			break;
		case TYPE_LSTAR_LA_G:
			//ka=1/la
			la=sampled_data_discrete(mv->spectrum_B,lambda);
			ka=1/la;
			break;
		case TYPE_HG_MIE:
		case TYPE_MIE:
			//ka=sampled_data_discrete(mv->spectrum_B,lambda);
			lambda_int=(int)lambda;
			if (mv->kamie[lambda_int]==-1)//value not yet calculated
			{
				calc_mie_ks_ka_and_phase_function(mv,lambda);
			}
			ka=mv->kamie[lambda_int];
			break;
	}
	return ka;
}

/**
 * get g(lambda) using a discrete spectrum
 */
float sx_mat_vol_g_discrete(struct sx_mat_vol* mv,float lambda)
{
	double g=0;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
			g=sampled_data_discrete(mv->spectrum_C,lambda);
			break;
		case TYPE_LSTAR_LA_G:
			g=sampled_data_discrete(mv->spectrum_C,lambda);
			break;
		case TYPE_HG_MIE:
		case TYPE_MIE:
			if (mv->gmie[(int)lambda]==-1)//value not yet calculated
			{
				calc_mie_ks_ka_and_phase_function(mv,lambda);
			}
			g=mv->gmie[(int)lambda];
			break;
	}
	return (float)g;
}


/**
 * get k(lambda) using a uniform spectrum
 */
float sx_mat_vol_ks_constant(struct sx_mat_vol* mv,float lambda)
{
	float ks=0,lstar=0,g=0;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
		case TYPE_K_KA_PHASE:
			ks=sampled_data_constant(mv->spectrum_A,lambda);
			break;
		case TYPE_LSTAR_LA_G:
			//k=1/l*/(1-g)
			lstar=sampled_data_constant(mv->spectrum_A,lambda);
			g=sampled_data_constant(mv->spectrum_C,lambda);
			ks=1/lstar/(1-g);
			break;
		case TYPE_HG_MIE:
		case TYPE_MIE:
			ks=0;//Not applicable here, ks is never constant
			break;
	}
	return ks;
}

/**
 * get ka(lambda) using a uniform spectrum
 */
float sx_mat_vol_ka_constant(struct sx_mat_vol* mv,float lambda)
{
	float ka=0,la=0;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
		case TYPE_K_KA_PHASE:
			ka=sampled_data_constant(mv->spectrum_B,lambda);
			break;
		case TYPE_LSTAR_LA_G:
			//ka=1/la
			la=sampled_data_constant(mv->spectrum_B,lambda);
			ka=1/la;
			break;
		case TYPE_HG_MIE:
		case TYPE_MIE:
			ka=0;//Not applicable here, ka is never constant
			break;
	}
	return ka;
}
/**
 * get ka(lambda) using a uniform spectrum
 */
float sx_mat_vol_g_constant(struct sx_mat_vol* mv,float lambda)
{
	float g=0;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
		case TYPE_LSTAR_LA_G:
			g=sampled_data_constant(mv->spectrum_C,lambda);
			break;
		case TYPE_HG_MIE:
		case TYPE_MIE:
			g=0;//Not applicable here, g is never constant
			break;
	}
	return g;
}

/**
 * find the spectra used by the material volume in the scene and connect them
 */
int sx_mat_vol_connect_spectra(
		struct sx_mat_vol* mv,
		struct sx_scene* scene
		)
{
	char* name;
	struct sampled_data* sp;
	int is_numerical=0;
	//find or create spectrum A:
	name=mv->spectrum_A_name;
	sp=sx_scene_find_spectrum2(scene,name,&is_numerical);
	if (sp==NULL)return 0;
	mv->spectrum_A=sp;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
		case TYPE_K_KA_PHASE:
		case TYPE_LSTAR_LA_G:
			if (is_numerical) mv->ks=&sx_mat_vol_ks_constant;
   else mv->ks=&sx_mat_vol_ks_discrete;
			break;
		case TYPE_HG_MIE:
		case TYPE_MIE:
			break;
	}

	//find or create spectrum B:
	name=mv->spectrum_B_name;
	sp=sx_scene_find_spectrum2(scene,name,&is_numerical);
	if (sp==NULL)return 0;
	mv->spectrum_B=sp;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
		case TYPE_K_KA_PHASE:
		case TYPE_LSTAR_LA_G:
		case TYPE_MIE:
		case TYPE_HG_MIE:
			//if (is_numerical) mv->ka=&sx_mat_vol_ka_constant;else mv->ka=&sx_mat_vol_ka_discrete;
			break;
	}

	//find or create spectrum C:
	if (mv->type!=TYPE_K_KA_PHASE)
	{
		name=mv->spectrum_C_name;
		sp=sx_scene_find_spectrum2(scene,name,&is_numerical);
		if (sp==NULL)return 0;
		mv->spectrum_C=sp;
		switch(mv->type)
		{
			case TYPE_K_KA_G:
			case TYPE_K_KA_PHASE:
			case TYPE_LSTAR_LA_G:
				if (is_numerical) mv->g=&sx_mat_vol_g_constant;else mv->g=&sx_mat_vol_g_discrete;
				break;
			case TYPE_HG_MIE:
			case TYPE_MIE:
				break;
		}
	}

	//find or create spectrum D:
	name=mv->spectrum_D_name;
	sp=sx_scene_find_spectrum2(scene,name,&is_numerical);
	if (sp==NULL)return 0;
	mv->spectrum_D=sp;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
		case TYPE_K_KA_PHASE:
		case TYPE_LSTAR_LA_G:
			break;
		case TYPE_HG_MIE:
			mv->ks=&sx_mat_vol_ks_discrete;
			mv->ka=&sx_mat_vol_ka_discrete;
			mv->g=&sx_mat_vol_g_discrete;
			break;
	}

	if (mv->type==TYPE_K_KA_PHASE)
	{
		//configure phase functions for each wavelength having one:
		for (int i=0;i<1000;i++) if (mv->pfs[i]!=NULL) sx_phase_function_configure_from_scene(mv->pfs[i],scene);
	}

	//save the phase function (debug)
	//sx_phase_function_save_pf(mv,1000000);
	//save the image of the phase function on a screen (debug)
	//sx_phase_function_save_pf_on_image(mv,500,200,500,500,1000000,0.5);

	return 1;
}




/**
 * create a scatterer of type TYPE_K_KA_G with uniform spectra
 */
int sx_mat_vol_create_with_constant_spectra(
		struct sx_mat_vol ** p_mv,
		float ks,
		float ka,
		float g
		)
{
	*p_mv=calloc(1,sizeof(struct sx_mat_vol));
	struct sx_mat_vol *mv=*p_mv;

	strcpy(mv->name,"uniform");
	mv->type=TYPE_K_KA_G;
	strcpy(mv->spectrum_A_name,"uniform ks");
	strcpy(mv->spectrum_B_name,"uniform ka");
	strcpy(mv->spectrum_C_name,"uniform g");

	struct sampled_data*spk;
	sampled_data_create2(&spk,1,"uniform k");
	spk->x[0]=0;
	spk->y[0]=ks;
	mv->ks=&sx_mat_vol_ks_discrete;
	mv->spectrum_A=spk;

	struct sampled_data*spka;
	sampled_data_create2(&spka,1,"uniform ka");
	spka->x[0]=0;
	spka->y[0]=ka;
	mv->ka=&sx_mat_vol_ka_discrete;
	mv->spectrum_B=spka;

	struct sampled_data*spg;
	sampled_data_create2(&spg,1,"uniform g");
	spg->x[0]=0;
	spg->y[0]=g;
	mv->g=&sx_mat_vol_g_discrete;
	mv->spectrum_C=spg;

	return 1;
}

/**
 * check if it has an attribute of name name
 */
int sx_mat_vol_has_attribute(struct sx_mat_vol * mv,const char* name)
{
	switch(mv->type)
	{
		case TYPE_K_KA_G:
			if (strcmp(name,"K") == 0) return 1;
			if (strcmp(name,"KA") == 0) return 1;
			if (strcmp(name,"G") == 0) return 1;
			break;
		case TYPE_LSTAR_LA_G:
			if (strcmp(name,"LSTAR") == 0) return 1;
			if (strcmp(name,"LA") == 0) return 1;
			if (strcmp(name,"G") == 0) return 1;
			break;
		case TYPE_HG_MIE:
		case TYPE_MIE:
			if (strcmp(name,"PHI") == 0) return 1;
			if (strcmp(name,"D") == 0) return 1;
			if (strcmp(name,"NR") == 0) return 1;
			if (strcmp(name,"NI") == 0) return 1;
	}
	return 0;
}

/**
 * get the spectrum of attribute "attribute"
 * if verbose is 1, print error message if not found
 */
struct sampled_data* sx_mat_vol_get_spectrum_of_attribute(
		struct sx_mat_vol * mv,
		const char* attribute,
		const int verbose
		)
{
	struct sampled_data* sd=NULL;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
			if (strcmp(attribute,"K") == 0) sd=mv->spectrum_A;
			if (strcmp(attribute,"KA") == 0) sd=mv->spectrum_B;
			if (strcmp(attribute,"G") == 0) sd=mv->spectrum_C;
			break;
		case TYPE_LSTAR_LA_G:
			if (strcmp(attribute,"LSTAR") == 0) sd=mv->spectrum_A;
			if (strcmp(attribute,"LA") == 0) sd=mv->spectrum_B;
			if (strcmp(attribute,"G") == 0) sd=mv->spectrum_C;
			break;
		case TYPE_HG_MIE:
		case TYPE_MIE:
			if (strcmp(attribute,"PHI") == 0) sd=mv->spectrum_A;
			if (strcmp(attribute,"D") == 0) sd=mv->spectrum_C;
			if (strcmp(attribute,"NR") == 0) sd=mv->spectrum_D;
			if (strcmp(attribute,"NI") == 0) sd=mv->spectrum_B;
	}
	if (verbose) if (sd==NULL) fprintf(stderr, "Object %s has no attribute of name %s\n",mv->name,attribute);
	return sd;
}



/**
  save the phase function in a txt data file
 */
void sx_phase_function_save_pf(
		struct sx_mat_vol *mv, //the material in volume
		int n //nb of samples
)
{
	//test:
	struct ssp_rng* rng;
	ssp_rng_create(NULL,SSP_RNG_THREEFRY,&rng);
	int dim=200;
	float xmax=(float)PI*0.95f;
	struct sampled_data* pdf;
	sampled_data_create3(&pdf,dim);
	strcpy(pdf->name,"#angle(rad) ");
	strcat(pdf->name,mv->name);
	strcat(pdf->name,"_phase_function");
	//sprintf(pdf->name,"angle(rad) %s_phase_function",mv->name);
	struct sx_photon* photon;
	sx_photon_create(&photon);
	float sx=xmax/(float)(dim-1);
	for (int i=0;i<dim;i++) pdf->x[i]=(float)i*sx;
	for (int k=0;k<n;k++)
	{
		photon->dir[0]=1;
		photon->dir[1]=0;
		photon->dir[2]=0;
		sx_mat_vol_scatter(mv,rng,photon,NULL);
		float theta=acosf(photon->dir[0]);
		//printf("theta=%f\n",theta);
		if ((theta>0)&&(theta<xmax))
		{
			int cell=(int)(theta/sx);
			pdf->y[cell]+=1.0f/sinf(theta)/2.0f/(float)PI;
		}
	}
	sampled_data_mul(pdf,30.0f/(float)n);
	char fname[200];
	strcpy(fname,mv->name);
	strcat(fname,".txt");
	//sprintf(fname,"%s.txt",mv->name);
	sampled_data_save(pdf,fname);
}


/**
  create an image with the diffraction pattern of this material
 */
void sx_phase_function_save_pf_on_image(
		struct sx_mat_vol *mv,
		float d, //distance of the screen
		float screen_size, //size of the (squared) screen
		int w, //image width
		int h, //image height
		int n, //nb of photons sorted
  float gamma //non linearity of the pixel's response
		)
{
	struct ssp_rng* rng;
	ssp_rng_create(NULL,SSP_RNG_THREEFRY,&rng);
	struct sx_image* image;
	float x,y;
	int i,j;
	int index=0;
	float p[3];
	float pPlane[3]={d,0,0};
	float nPlane[3]={1,0,0};
	float pLine[3]={0,0,0};
	float* nLine;
	float sx=screen_size/(float)(w-1);
	float sy=screen_size/(float)(h-1);
	sx_image_create(&image,w,h,1);
	struct sx_photon* photon;
	sx_photon_create(&photon);
	for (int k=0;k<n;k++)
	{
		photon->dir[0]=1;
		photon->dir[1]=0;
		photon->dir[2]=0;
		sx_mat_vol_scatter(mv,rng,photon,NULL);
		//printf("dir =[%f,%f,%f] newdir=[%f,%f,%f] \n",dir[0],dir[1],dir[2],newdir[0],newdir[1],newdir[2]);
		nLine=photon->dir;
		sx_geometry_intersection_plane_line(p,pPlane,nPlane,pLine,nLine);
		x=p[1];
		y=p[2];
		i=(int)((x+screen_size/2)/sx);
		j=(int)((y+screen_size/2)/sy);
		if ((i<0)||(i>=w)) continue;
		if ((j<0)||(j>=h)) continue;
		index=i+w*j;
		//printf("x=%f y=%f i=%d j=%d index=%d\n",x,y,i,j,index);
		//if ((index>=0)&&(index<(w*h))) image->data[index]+=1;
		image->data[index]+=1;
	}
	char fname[200];
	strcpy(fname,mv->name);
	sx_image_save_png(image,gamma,-1,"",fname);
}

/**
  The volume material scatters the photon.
mv: the volume material
rng: used random generator
photon: the photon to scatter
pdf: the probability given by the phase function 
 */
void sx_mat_vol_scatter(
		struct sx_mat_vol* mv,
		struct ssp_rng* rng,
		struct sx_photon* photon,
		float* pdf
		)
{
	float g=0;
	struct sx_phase_function* pf;
	//printf("<---sx_mat_vol_scatter---> photon:  pos=(%g,%g,%g) previous dir=(%g,%g,%g)\n", photon->x[0], photon->x[1], photon->x[2],photon->dir[0], photon->dir[1], photon->dir[2]);
	switch(mv->type)
	{
		case TYPE_K_KA_G:
		case TYPE_LSTAR_LA_G:
		case TYPE_HG_MIE:
			g=mv->g(mv,photon->lambda);
			ssp_ran_sphere_hg_float(rng,photon->dir,g,photon->dir,pdf);
			break;
		case TYPE_K_KA_PHASE:
			//get the phase function corresponding to the wavelength lambda:
			pf=mv->pfs[(int)photon->lambda];
			//if not existing get the phase function of index 0:
			if (pf==NULL) pf=mv->pfs[0];
			//if pf of index 0 is not configured, error message
			if (pf==NULL)
			{
				printf("sx_mat_vol_scatter, TYPE_K_KA_PHASE: phase function unknown for lambda= %d nm \n",(int)photon->lambda);
				return;
			}
			sx_phase_function_sort(pf,rng,photon->dir,photon->dir,pdf);
			break;
		case TYPE_MIE:
			//get the phase function corresponding to the wavelength lambda:
			if (mv->pfs[(int)photon->lambda]==NULL) 
				//phase function not yet calculated
				calc_mie_ks_ka_and_phase_function(mv,photon->lambda);
			pf=mv->pfs[(int)photon->lambda];
			//if  not configured, error message
			if (pf==NULL)
			{
				printf("sx_mat_vol_scatter, TYPE_MIE: phase function unknown for lambda= %d nm \n",(int)photon->lambda);
				return;
			}
			sx_phase_function_sort(pf,rng,photon->dir,photon->dir,pdf);
			break;
		default:
			break;
	}
	//printf("<---sx_mat_vol_scatter---> photon:  pos=(%g,%g,%g) new dir=(%g,%g,%g)\n", photon->x[0], photon->x[1], photon->x[2],photon->dir[0], photon->dir[1], photon->dir[2]);
}

/**
  get the proba for the photon to be scattered at direction newdir
 */
float sx_mat_vol_scatter_pdf(
		struct sx_mat_vol* mv,
		struct sx_photon* photon,
		float* newdir
		)
{
	float pdf=0,g=0;
	struct sx_phase_function* pf;
	switch(mv->type)
	{
		case TYPE_K_KA_G:
		case TYPE_LSTAR_LA_G:
		case TYPE_HG_MIE:
			g=mv->g(mv,photon->lambda);
			pdf=ssp_ran_sphere_hg_float_pdf(photon->dir,g,newdir);
			break;
		case TYPE_K_KA_PHASE:
		case TYPE_MIE:
			if (mv->pfs[(int)photon->lambda]==NULL) calc_mie_ks_ka_and_phase_function(mv,photon->lambda);//phase function not yet calculated
			pf=mv->pfs[(int)photon->lambda];
			pdf=sx_phase_function_pdf(pf,newdir,photon->dir);
			break;
		default:
			break;
	}
	return pdf;
}



