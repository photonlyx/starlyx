/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SX_MAT_VOL_H_
#define SX_MAT_VOL_H_

#include "sx_xml.h"
#include "sx_scene.h"
#include "sx_photon.h"


struct sampled_data;

enum
{
//material volume defined by k  ka and g.
// The phase function is Henyey-Greenstein.
TYPE_K_KA_G, 
//material volume defined by l*(=1/k/(1-g))
//  la(=1/ka) and g The phase function is Henyey-Greenstein.
TYPE_LSTAR_LA_G,
//material volume which is a suspension of spheres, 
//defined by volume fraction, spheres diameter, 
//and refractive index. The phase function is Henyey-Greenstein of same g.
TYPE_HG_MIE, 
 //material volume is defined by k ka and a phase function
TYPE_K_KA_PHASE,
 //material volume which is a suspension of spheres, 
//defined by volume fraction, spheres diameter,
// and refractive index. The phase function is from Mie theory.
TYPE_MIE
};

/**
 * Henyey-Greenstein scattering
 */
struct sx_mat_vol
{
	int type;//TYPE_K_KA_G  TYPE_LSTAR_LA_G   TYPE_HG_MIE  TYPE_K_KA_PHASE  TYPE_MIE
	char name[MAX_NAME_LENGTH];
	struct sx_volume* volume;//volume having this volume material
	char spectrum_A_name[MAX_NAME_LENGTH];//the spectrum name for K , LSTAR or PHI
	char spectrum_B_name[MAX_NAME_LENGTH];//the spectrum name for KA or LA (absorption)
	char spectrum_C_name[MAX_NAME_LENGTH];//the spectrum name for G or D
	char spectrum_D_name[MAX_NAME_LENGTH];//the spectrum name for N (mie)
	struct sampled_data* spectrum_A;//(k , lstar or phi)
	struct sampled_data* spectrum_B;//(ka , la , ni of mie)
	struct sampled_data* spectrum_C;//(g ,d )
	struct sampled_data* spectrum_D;//(nr mie)
	float (*ks)(struct sx_mat_vol* mv,float lambda);   //function that gives k(lambda), lambda in nm
	float (*ka)(struct sx_mat_vol* mv,float lambda); //function that gives ka(lambda), lambda in nm
	float (*g)(struct sx_mat_vol* mv,float lambda);   //function that gives g(lambda), lambda in nm
	float ksmie[1000]; //table for accelerating Mie case
	float kamie[1000]; //table for accelerating Mie case
	float gmie[1000]; //table for accelerating Mie case
	float free_path; //sorted distance to next scattering
	float abs_path;//sorted distance to next absorption
	char phase_function_name[MAX_NAME_LENGTH];//the name of the phase function
	char phase_function_names[MAX_NAME_LENGTH*3];//the nameof the phase functions for each wavelength
	struct sx_phase_function* pfs[1000];
 unsigned int nang;//nb of angles between 0 and 90º (used for TYPE_MIE)
};


int sx_mat_vol_collection_print(
    struct sx_mat_vol_collection* col,
    FILE* stream
    );


void sx_mat_vol_create( 
    struct sx_mat_vol ** mv,
    const char* name_
    );

int sx_mat_vol_create_from_xml(
    struct sx_mat_vol ** p_p_mv,
    mxml_node_t *node
    );

int sx_mat_vol_read_hg_mie_from_xml(
		struct sx_mat_vol * mv,
		mxml_node_t *node
  );

int sx_mat_vol_read_Mie_from_xml( 
    struct sx_mat_vol * mv,
    mxml_node_t *node
    );


void sx_mat_vol_release( 
    struct sx_mat_vol * mv
    );

void sx_mat_vol_print(
		struct sx_mat_vol* mv,
		FILE* stream,
		char* indentation
		);

void calc_mie_ks_ka_and_phase_function(
    struct sx_mat_vol* mv,
    float lambda
    );

float sx_mat_vol_ks_discrete(
    struct sx_mat_vol* mv,
    float lambda
    );

float sx_mat_vol_ka_discrete(
    struct sx_mat_vol* mv,
    float lambda
    );

float sx_mat_vol_g_discrete(
    struct sx_mat_vol* mv,
    float lambda
    );

float sx_mat_vol_ks_constant(
    struct sx_mat_vol* mv,
    float lambda
    );

float sx_mat_vol_ka_constant(
    struct sx_mat_vol* mv,
    float lambda
    );

float sx_mat_vol_g_constant(
    struct sx_mat_vol* mv,
    float lambda
    );


int sx_mat_vol_connect_spectra(
    struct sx_mat_vol* mv,
    struct sx_scene* scene
    );

int sx_mat_vol_create_with_constant_spectra(
		struct sx_mat_vol ** p_mv,
		float k,
		float ka,
		float g
		);

int sx_mat_vol_has_attribute(
		struct sx_mat_vol * mv,
		const char* name
		);

struct sampled_data* sx_mat_vol_get_spectrum_of_attribute(
		struct sx_mat_vol * mv,
		const char* name,
		const int verbose
		);


void sx_mat_vol_scatter(
    struct sx_mat_vol* mv,
    struct ssp_rng* rng,
    struct sx_photon* photon,
    float* pdf
    );

float sx_mat_vol_scatter_pdf(
    struct sx_mat_vol* mv,
    struct sx_photon* photon,
    float* newdir
    );

void sx_phase_function_save_pf(
    struct sx_mat_vol *mv,
    int n
    );

void sx_phase_function_save_pf_on_image(
    struct sx_mat_vol *mv,
    float d,
    float screen_size,
    int w,
    int h,
    int n,
    float gamma 
    );

#endif /* SX_MAT_VOL_H_ */
