/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <time.h>

#include <rsys/float3.h>
#include <star/s3d.h>
#include <omp.h>
#include <star/ssp.h>

#include <rsys/logger.h>

#include "sx_monte_carlo.h"
#include "sx_path_source.h"
#include "sampled_data.h"
#include "sx_surface.h"
#include "sx_xml.h"
#include "sx_configuration.h"
#include "sx_scene.h"
#include "sx_photon.h"
#include "sx_symbolic.h"
#include "sx_camera.h"
#include "sx_sensor.h"
#include "lists.h"
#include "float_list_symbolic.h"




/**
 * launch the MC for this sx_scene
 */
int sx_montecarlo_compute(struct sx_scene* scene)
{
	int image_show_period=10;//seconds

	//nb of threads finally used:
	size_t nt=1;
	int si=check_storing_impacts(scene);
	int st=((scene->store_trajectories==1)||(scene->store_error_trajectories==1));
	if ((!st)&&(si==0))
	{
		if (scene->nb_threads==-1) nt=(size_t)omp_get_num_procs(); 
		else nt=(unsigned int)scene->nb_threads;
		//printf("No path file and don't store trajectories: all threads\n");
	}
	else
	{
		nt=1;
		if (scene->verbose) if (st) printf("Trajectories stored: one thread\n");
		if (scene->verbose) if (si==1) printf("Impacts stored: one thread\n");
	}
	// 
#ifdef PRINT_ALL_STEPS
	//use one thread for debugging:
	nt=1;
#endif
	if (scene->verbose) printf("Finally %ld thread(s)\n",nt);
	//create the scene spectrum(for the case of direct path):
	if (!sx_scene_create_scene_spectrum(scene))
	{
		return 0;
	}




	time_t time0=time(NULL);
	time_t time1,time_used;
	float percent,timeEstimation;
	//printf("Time: %d",time0);
	int last_saving_index=0;



	if (!sx_scene_create_sx_path_sources(scene))
	{
		fprintf(stderr,"Error creating sx_path_sources\n");
		return 0;
	}

	if (!sx_scene_check_spectra(scene))
	{
		return 0;
	}

#ifdef PRINT_ALL_STEPS
	printf("Launch MC for %ld sx_path_sources\n",scene->path_sources->dim);
#endif

	


/* Déclaration du générateur de nombres aléatoires (réalisation d'une variable
				aléatoire uniforme sur [0,1]). Toutes les autres réalisations, pour d'autres 
				variables aléatoires, s'appuieront sur ce générateur. */
	struct ssp_rng_proxy* rng_proxy = NULL; 
	// tableau de generateurs (un par thread):
	struct ssp_rng** rngs = NULL;
	/* Création du générateur mandataire. Dans cet exemple on choisit 
				RNG_MT19937_64   (Mersenne Twister). Voir la liste des générateurs disponibles
				dans ssp.h */  
	int res=ssp_rng_proxy_create(NULL,SSP_RNG_MT19937_64,nt,&rng_proxy);
	if(res!=RES_OK) 
	{
		printf("could not create the proxy RNG\n");
		return 0;
	}
	/* Allocation des generateurs aleatoire pour chaque processus */
	rngs = mem_calloc(nt,sizeof(*rngs));
	if(!rngs) 
	{
		printf("could not allocate the array of per thread RNG\n");
		res = RES_MEM_ERR;
		goto error;
	}
	/* Le generateur mandataire attribue un generateur par sequence aleatoire 
				unique que nous stockons dans le tableau rngs (un par processus) */
	for(unsigned int i=0;i<nt;++i) 
	{
		res = ssp_rng_proxy_create_rng(rng_proxy, i, &rngs[i]);
		if(res != RES_OK) 
		{
			printf("could not create the RNG of the thread %d\n",i);
			goto error;
		}
	}
	// nombre de processus
	omp_set_num_threads((int)nt); 




	unsigned long nbFailure=0;
	for (unsigned long int i=0;i<scene->path_sources->dim;i++)
	{
//printf("Run MC for path source %d\n",i);
		struct sx_path_source* sx_path_source=scene->path_sources->data[i];
		scene->sx_path_source_index=i;
		time1=time(NULL);
		percent=(float)((float)(i+1)/((float)scene->path_sources->dim)*100.f);
		time_used=time1-time0;
		timeEstimation=(float)(time1-time0)*100.f/percent;
		struct sx_path_source* path_source=scene->path_sources->data[i];
		if (scene->verbose)
			printf("pix (%d,%d) %.1f%% %ds/%ds   \r",sx_path_source->x,
					sx_path_source->y,percent,(int)time_used,(int)timeEstimation);

  //launch the Monte-Carlo :
		long nb_errors=0;  //nb of realisations with errors
		mc_loop(scene,path_source,rngs,nt,&nb_errors);

		//nbFailure+=(unsigned long)estimator_status.NF;
		nbFailure+=(unsigned long)nb_errors;

//collect the results of MC:
		if(scene->algo==ALGO_DIRECT)
		{
			//standard Monte-Carlo
			{
			}
			//<SYMBOLIC
			if (scene->symbolic) //symbolic Monte-Carlo
			{
				struct float_list_symbolic** polynoms=NULL;
				int nb_polynoms;
				sx_symbolic_calc_polynom(scene,&polynoms,&nb_polynoms);
				for (int j=0;j<nb_polynoms;j++)
					if (polynoms[j]->index>0)
					{
						char full_filename[2*MAX_NAME_LENGTH];
						//	strcpy(full_filename,scene->file_path);
						//	strcat(full_filename,paths_file);
						sprintf(full_filename,"%spolynom_%d_%s.txt",
								scene->file_path,j,polynoms[j]->label);
						if (scene->verbose) printf("Saving polynom %s\n",full_filename);
						float_list_symbolic_save(polynoms[j],full_filename);
					}
			}
			//SYMBOLIC>

		}
		else //reverse path, just one scalar for the weight
		{
			/*printf("pix (%d,%d)  weight=%g sigma=%g\n",
							sx_path_source->x,sx_path_source->y,
							sx_path_source->weight[0],sx_path_source->sigma[0]);*/
		}

		//release:

		//save partial  image regularly for viewing:
		int saving_index=(int)time_used/image_show_period;
		if (saving_index!=last_saving_index)
		{
			time_t time2=time(NULL);
			//printf ("Saving index %d",saving_index);
			//if (scene->verbose) printf("Save preview image ...");
			sx_scene_save_images(scene,1);
			last_saving_index=saving_index;
			//if (scene->verbose) printf("done\n");
			time_t dt=time(NULL)-time2;
			if (dt>(image_show_period*0.1))
			{
				image_show_period=(int)dt*11;
				if (scene->verbose) 
					printf("Image saving takes too long (%ld s). \
							Set image saving period to %d s\n",dt,image_show_period);
			}
		}


	}



	if (scene->verbose)
	{
		printf("\n");
		printf("%lu FAILURES / %d (%.4f%%)\n",nbFailure,
				(int)(scene->path_sources->dim*scene->nb_photons),
				(float)nbFailure/(float)(scene->path_sources->dim*scene->nb_photons)*100);
	}
		/* Liberation de l'ensemble des generateurs */
		if(rng_proxy) ssp_rng_proxy_ref_put(rng_proxy);
		if(rngs) 
		{
			for(unsigned int i=0; i<nt; ++i) 
			{
				if(rngs[i]) ssp_rng_ref_put(rngs[i]);
			}
			mem_rm(rngs);
		}
	return 1;

error:
	printf("Error in render\n");
	return 0;
}




/**
make the loop of MC realisations to calculate the estimation and variance
*/
int mc_loop(
		struct sx_scene* scene,
		struct sx_path_source* ps,
		struct ssp_rng** rngs,//array of random nb generator, one for each thread
		size_t nt,   //nb of threads
  long* nb_errors  //nb of realisations with errors
		)
{
	// we create the arrays to sum the weights and the squares of weights
	// each thread must have its own place to sum the weights
	// the dimension is weight dimension * nb of threads
 // the array is arrange by concatenating the sums for each thread
	double* sum_thread; //array of weights sums 
	double* sum2_thread; // array of squares of weights sums
	sum_thread = mem_calloc(nt*ps->weight_dim,sizeof(*sum_thread));
	sum2_thread = mem_calloc(nt*ps->weight_dim,sizeof(*sum2_thread));
	long unsigned int n=scene->nb_photons;
	//reset the stats to zero:
	for (unsigned int i=0;i<nt*ps->weight_dim;++i) 
	{
		sum_thread[i]=0;
		sum2_thread[i]=0;
	}
	//share the realisations between the threads:
#pragma omp parallel for schedule(static)
	for (long unsigned int k=0;k<n;k++)
	{
		// indice du processus courant:
		const int ithread=omp_get_thread_num(); 
#ifdef DEBUG
		printf("Realisation %ld\n",k);
#endif

		// do the MC realisation:
		int res=sx_monte_carlo_realisation(scene,ps,sum_thread,sum2_thread,rngs[ithread],ithread);

		if (res!=RES_OK) 
		{
			//error in realisation
			/*
			if (scene->verbose) 
				printf("Error in realisation %ld of pixel (%d,%d)\n",k,ps->x,ps->y);
			*/
			(*nb_errors)++;
			continue;
		}
	}

	/* Calcul de la somme des poids et des poids au carré */
	double* sum = mem_calloc(ps->weight_dim,sizeof(double));
	double* sum2 = mem_calloc(ps->weight_dim,sizeof(double));
	for(unsigned int i=0;i<nt;i++) 
	{
		for(unsigned int j=0;j<ps->weight_dim;j++) 
		{
			sum[j]+=sum_thread[ps->weight_dim*i+j];
			sum2[j]+=sum2_thread[ps->weight_dim*i+j];
		}
	}
	for(unsigned int j=0;j<ps->weight_dim;j++) 
	{	
		double mean=sum[j]/(double)n;
		ps->weight[j]=mean;
		ps->sigma[j]=sqrt((sum2[j]/(double)n-mean*mean)/(double)(n-1));
	}	

	return 1;
}



/**
 *  Execute one realisation of Monte-Carlo random variable
 */
int sx_monte_carlo_realisation (
		struct sx_scene* scene,   //the scene
		struct sx_path_source* path_source,//the path source of this MC calculation
		double *s, //array oh the weights sums dim=nbThreads*weight_dim
		double *s2, //array oh the weights squares sums
		struct ssp_rng* rng, //random number generator
		int ithread //num of thread
		)
{
	// initialise the photon path using the sx_path_source position and direction:
	//struct sx_path_source* path_source=scene->path_sources->data[scene->sx_path_source_index];

	//sort a photon (path) form the sx_path_source:
	struct sx_photon* photon; //pointer to photon sorted in thsi realisation
	sx_path_source_sort_photon(path_source,rng,&photon);

	photon->ray_range[0]=0.f;
	photon->ray_range[1]=FLT_MAX;
	photon->index_end_surface=-1;
	photon->index_end_volume=-1;
	photon->nb_events=0;
	photon->nb_scats=0;
#ifdef PRINT_ALL_STEPS
	printf("***realisation***  photon %d: pos=(%g,%g,%g) dir=(%g,%g,%g) lambda=%f rad=%f\n",
			scene->sx_path_source_index,photon->x[0],photon->x[1],photon->x[2],
			photon->dir[0],photon->dir[1],photon->dir[2],photon->lambda,
			photon->w);
#endif

	//send the path in the scene:
	//set the flag that controls the use of MC branches to calc sources contibution to radiant intensity:
	int source_contrib=0;
	if (scene->algo==ALGO_DIRECT) source_contrib=0;
	if (scene->algo==ALGO_REVERSE) source_contrib=1;
	//*** launch photon in the scene:
	sx_photon_propagate(rng,scene,photon,source_contrib,scene->max_nb_scattering,\
			scene->max_nb_events,FLT_MAX,1,1,((scene->store_trajectories)||\
				(scene->store_error_trajectories)));


#ifdef PRINT_ALL_STEPS
//	if(photon->w!=0)
//	if(isnan(photon->w))
//	if(photon->w>1000000)
	{
		printf("*** \n\n\n");
		struct sx_path_source* ps=scene->path_sources->data[scene->sx_path_source_index];
		printf("*** Probe %d: pixel=(%d,%d) \n",scene->sx_path_source_index,ps->x,ps->y);
		printf("***realisation after propagate*** Photon weight = %g\n",photon->w);
		sx_photon_print(photon);
		sx_photon_print_history(photon);
	}
#endif
	if(isnan(photon->w))
	{
		photon->interaction_type=ERROR_WEIGHT_NAN;
		printf("***realisation after propagate*** Photon weight = %g\n",photon->w);
		printf("Error NaN weight for probe %ld\n",scene->sx_path_source_index);
		printf("Photon history\n");
		sx_photon_print_history(photon);
	}
	f3_set(path_source->position_end,photon->x);
	path_source->index_end_surface=photon->index_end_surface;
	path_source->index_end_volume=photon->index_end_volume;
	if(scene->algo!=ALGO_DIRECT)
	{
		//reverse path: 
/*
		char flag[100];
		sx_photon_get_flag(flag,photon->interaction_type);
		struct sx_path_source* ps=scene->path_sources->data[scene->sx_path_source_index];
		if (photon->w>10) 
{
printf("Probe %d: pixel=(%d,%d) interaction=%s  w=%g  \n",scene->sx_path_source_index,ps->x,ps->y,flag,photon->w);
sx_photon_print_history(photon);
}
*/
		switch (photon->interaction_type)
		{
			case SURFACE_ABSORBED:
				photon->w=0;
				break;
		}
		//increment the Monte-Carlo weight: 
		s[(unsigned int)ithread*path_source->weight_dim+0] += photon->w;
		s2[(unsigned int)ithread*path_source->weight_dim+0] += pow(photon->w,2);
	}
	else
	{
		//direct path : set weight to radiance for the surface where the photon is absorbed
		int pixel_index=0;
		if(photon->index_end_surface!=-1)
		{
			int lambda_index_in_spectrum=sampled_data_index_of(scene->spectrum,photon->lambda);
			struct sx_surface* surf;
			struct sx_sensor* sensor;
			int index_in_weight;//index of the weight dimension
			int index_in_sums;//index in the sum array used for parallelisation
			switch (photon->interaction_type)
			{
				case RAY_ENTERED_A_CAMERA:
					//the photon has been detected by a camera
					//CAMERA_DIRECT:
					//determine which pixel is hitted by the ray:
					pixel_index=0;
					struct sx_surface* aperture=scene->surfaces->data[photon->index_end_surface];
					struct sx_camera* camera=(struct sx_camera*)aperture->camera;
					float hit_global[3];
					float hit_local[2];
					sx_camera_calc_pixel_hit(camera,&pixel_index,hit_global,hit_local,photon);
					//store the new position in the path:
					f3_set(photon->x,hit_global);
					if (scene->store_trajectories) sx_photon_store(photon);

					if (pixel_index!=-1)
					{
						//calculate the index of the weight that has to be incremented:
						//TODO: correct for one camera, not more
						index_in_weight=photon->index_end_surface*2*
							scene->spectrum->nb_vals+scene->spectrum->nb_vals*photon->hit_front+lambda_index_in_spectrum+pixel_index;
						index_in_sums=ithread*path_source->weight_dim+index_in_weight;
						//deliver the radiance:
						s[index_in_sums]+=photon->w;
						s2[index_in_sums]+=pow(photon->w,2);
					}
					break;
				case RAY_ENTERED_A_SENSOR:
					//the path has ended in a SENSOR
					//we want to get the power (W) seen by the sensor

					//get the sensor's surface:
					surf=scene->surfaces->data[photon->index_end_surface];
					//get the sensor:
					sensor=surf->sensor;

					//cos(theta):
					float cos_theta=f3_dot(photon->normal,photon->dir);
					if (cos_theta>cos(sensor->angle/2*PI/180.0)) 
					{
						//the ray is inside the sensor acceptance cone
						//calc the index of the dimension of the weight corresponding to 
						// this sensor:
						index_in_weight=photon->index_end_surface*2*
							scene->spectrum->nb_vals+scene->spectrum->nb_vals*photon->hit_front+lambda_index_in_spectrum;
						index_in_sums=ithread*path_source->weight_dim+index_in_weight;
						s[index_in_sums]+=photon->w;
						s2[index_in_sums]+=pow(photon->w,2);
#ifdef PRINT_ALL_STEPS
				printf("sx_monte_carlo_realisation RAY_ENTERED_A_SENSOR front=%d lambda=%f (index=%d) cos theta=%f index_end_surface=%d  index_in_w= %d index in sums=%d w=%f\n",photon->hit_front,photon->lambda,lambda_index_in_spectrum,cos_theta, photon->index_end_surface,index_in_weight,index_in_sums,photon->w);
#endif
					}

					break;
				case SURFACE_ABSORBED:
					//the weight is multidimensional array , a concatenation of the
					// surfaces' spectra:  surface0_spectrum,surface1_spectrum,....
					index_in_weight=photon->index_end_surface*2*
						scene->spectrum->nb_vals+scene->spectrum->nb_vals*photon->hit_front+lambda_index_in_spectrum;
					index_in_sums=ithread*path_source->weight_dim+index_in_weight;
					//printf("***realisation direct*** end surface:%d  index of lambda=%f is %d, index of weight is %d\n",photon->index_end_surface,photon->lambda,lambda_index_in_spectrum,index_in_weight);
					//printf("   index in sums=%d    weight=%g\n",index_in_sums,photon->w);
					//deliver the radiance:
					s[index_in_sums]+=photon->w;
					s2[index_in_sums]+=pow(photon->w,2);
#ifdef PRINT_ALL_STEPS
					float cos_theta1=f3_dot(photon->normal,photon->dir);
					printf("sx_monte_carlo_realisation SURFACE_ABSORBED cos theta=%f index_end_surface=%d  index in sums=%d    weight=%g\n",cos_theta1,photon->index_end_surface,index_in_sums,photon->w);
#endif
					break;
				default:
					break;
			}
		}
		//<SYMBOLIC 
		//symbolic calculation case, store the nb of scattering, absorptions and null collisions:
		if (scene->symbolic)
		{
			//printf("Symbolic, nb of scattering events=%d\n",photon->nb_scattering);
			//printf("Symbolic, nb of absorption events=%d\n",photon->nb_absorption);
			//printf("Symbolic, nb of null collision events=%d\n",photon->nb_null_collision);
			float* element=calloc(1,scene->path_symbolic_events->dim*sizeof(float));
			element[0]=(float)photon->lambda;
			element[1]=(float)photon->index_end_surface;
			element[2]=(float)photon->nb_scattering;
			element[3]=(float)photon->nb_absorption;
			element[4]=(float)photon->nb_null_collision;
			float_list_symbolic_add(scene->path_symbolic_events,element);
			//float_list_symbolic_print(scene->path_symbolic_events);
			//float_list_symbolic_print_element2(scene->path_symbolic_events,element);
		}
		//SYMBOLIC>

	}

	//BLURRING_ACCELERATION
	//get the impact lists of the surfaces, if any:
	//and store the photon weight:
	for (int i=0;i<scene->surfaces->dim;i++)
	{
		struct sx_surface* surface=scene->surfaces->data[i];
		if (surface->store_impacts==1)
				sx_surface_add_weight(surface,photon);
	}

	//if asked, store the photon's history 
	//check if the dynamic list of stored photons is created:
	if (scene->stored_photons==NULL)
	{
			//the list has not been created yet, do it:
			d_list_create(&scene->stored_photons);
	}
	int can_release=1;
	if (scene->store_trajectories)
	{
			if (!sx_photon_is_error(photon))
			{
					d_list_add(scene->stored_photons,photon);
					can_release=0;
			}
	}
	if (scene->store_error_trajectories)
	{
			if (sx_photon_is_error(photon))
			{
					d_list_add(scene->stored_photons,photon);
					can_release=0;
			}
	}

	//check what to return before releasing the photon:
	int res;
	if (sx_photon_is_error(photon))
	{
		res=RES_BAD_ARG;
		//sx_photon_print_history(photon);
	}
	else
	{
		res=RES_OK;
	}


	//release the photon if its history is not stored:
	if(can_release)	
	{
			sx_photon_release(photon);
	}

	return res;

}



