/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SX_MONTE_CARLO_H_
#define SX_MONTE_CARLO_H_


#include <star/ssp.h>
#include "sx_configuration.h"
#include "sx_source.h"


struct sx_scene;
struct sx_volume;
struct sx_mat_surf;
struct filter_context;
struct sx_photon;




int sx_montecarlo_compute(
		struct sx_scene* scene
		);



int mc_loop(
		struct sx_scene* scene,
		struct sx_path_source* ps,
		struct ssp_rng** rngs,//array of random nb generator, one for each thread
		size_t nt,   //nb of threads
  long* nb_errors  //nb of realisations with errors
		);


int sx_monte_carlo_realisation (
		struct sx_scene* scene,   //the scene
		struct sx_path_source* path_source,//the path source of this MC calculation
		double *s, //array oh the weights sums
		double *s2, //array oh the weights squares sums
		struct ssp_rng* rng, //random number generator
		int ithread //num of thread
		);

#endif /* SX_MONTE_CARLO_H_ */
