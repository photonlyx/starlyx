/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <star/s3d.h>
#include <star/ssp.h>

#include "sx_path_source.h"
#include "sampled_data.h"
#include "sx_scene.h"
#include "sx_camera.h"
#include "sx_source.h"
#include "sx_photon.h"
#include "sx_xml.h"
#include "sx_sensor.h"
#include "sx_configuration.h"
#include "sx_geometry.h"





void sx_path_source_create(struct sx_path_source** p_p_sx_path_source)
{
	struct sx_path_source* ps; 
	ps=(struct sx_path_source*)calloc(1,sizeof(struct sx_path_source));
    ps->type=-1;
	strcpy(ps->name,"");
	ps->spectrum=NULL;
	ps->wavelength_sorter=NULL;
	ps->weight_dim=0;
	ps->weight=NULL;
	ps->sigma=NULL;
	ps->position_end[0]=0;
	ps->position_end[1]=0;
	ps->position_end[2]=0;
	ps->index_end_surface=-1;
	ps->index_end_volume=-1;
	ps->x=0;
	ps->y=0;
	ps->starting_volume=NULL;
	ps->camera=NULL;
	ps->source=NULL;
	(*p_p_sx_path_source)=ps;
}




void sx_path_source_print(struct sx_path_source* ps)
{
	printf("Probe\n");
	printf("name=%s\n",ps->name);
	switch (ps->type) 
	{
		case PATH_SOURCE_TYPE_CAMERA:
			printf("type PATH_SOURCE_TYPE_CAMERA\n");
			printf("   camera:%s\n",ps->camera->name);
			break;
		case PATH_SOURCE_TYPE_SENSOR:
			printf("type PATH_SOURCE_TYPE_SENSOR\n");
			printf("   sensor:%s\n",ps->sensor->name);
			break;
		case PATH_SOURCE_TYPE_SOURCE: 
			printf("type PATH_SOURCE_TYPE_SOURCE\n");
			printf("   source:%s\n",ps->source->name);
			break;
	}
	//sampled_data_print(ps->spectrum,stdout);
}

void sx_path_source_release(struct sx_path_source* sx_path_source)
{
	//	free(sx_path_source->weight);
	//	free(sx_path_source->sigma);
	//	free(sx_path_source);
}


/**
 * find the spectra of each sx_path_source channel. 
 * For numerical values, create monochromatic spectra.
 */
int sx_path_source_find_or_create_spectrum(struct sampled_data*** filters,
		int* nb_filters,char* filters_list,struct sx_scene* scene)
{
	char** list;
	int nb_words=0;
	//replace the end of line by spaces:
	parsing_replace(filters_list,'\n',' ');//allow \n in xml definition
	parsing_parse_words(filters_list,&list,&nb_words);
	if (nb_words>=1)
	{
		(*nb_filters)=nb_words;
		(*filters)=calloc(1,sizeof (struct sampled_data*)*nb_words);
		for (int i=0;i<nb_words;i++)
		{
			struct sampled_data* spectrum;
			//if it is a number, create a spectrum with one point 
			//with wavelength this number (weight 1):
			int is_numerical=parsing_is_numerical_value(list[i]);
			if (is_numerical)
			{
				float value;
				sscanf(list[i],"%f",&value);
				//create a spectrum with a unique value
				//sampled_data_create2( &spectrum,1,"monochromatic");
				sampled_data_create2( &spectrum,1,list[i]);
				spectrum->x[0]=value;
				spectrum->y[0]=1;
			}
			else
			{
				//find the spectrum_name:
				spectrum=sx_scene_find_spectrum(scene,list[i]);
				if (spectrum==NULL)
				{
					printf("Can't find filter spectrum of name %s\n" ,list[i]);
					return 0;
				}
			}
			(*filters)[i]=spectrum;
		}
	}
	else //no filter
	{
		(*nb_filters)=0;
		printf("Empty list of filters \n");
		return 0;
	}
	free(list);

	return 1;
}

/**
start the path
*/
void sx_path_source_sort_photon(
struct sx_path_source* sx_path_source,
struct ssp_rng* rng,
struct sx_photon** photon_
)
{
#ifdef PRINT_ALL_STEPS
	printf("***sx_path_source. Sort photon\n");
#endif
	switch (sx_path_source->type) 
	{
		case PATH_SOURCE_TYPE_CAMERA:
			//this path source is associated with a camera
			//create a reverse photon
			sx_camera_create_reverse_photon(sx_path_source->camera,photon_,
					sx_path_source->x,sx_path_source->y,rng);
			break;
		case PATH_SOURCE_TYPE_SENSOR:
			//paths starts from a sensor:
			//create a reverse photon
			sx_sensor_create_reverse_photon(sx_path_source->sensor,photon_,rng);
			break;
		case PATH_SOURCE_TYPE_SOURCE: 
			//path source associated with a source spot or a source with a geometry defined by a surface
			//create a direct photon
			sx_source_create_photon(sx_path_source->source,photon_,rng);
			break;
	}
	//sort a wavelength index:
	unsigned int i=ssp_ranst_discrete_get(rng,sx_path_source->wavelength_sorter);
    //set the photon's wavelength
	(*photon_)->lambda=sx_path_source->spectrum->x[i];
	//set the photon volume:
	(*photon_)->volume=sx_path_source->starting_volume;


#ifdef PRINT_ALL_STEPS
	printf("***sx_path_source. Sort photon pos=(%g,%g,%g),  dir=(%g,%g,%g)\n",
                (*photon_)->x[0],(*photon_)->x[1],(*photon_)->x[2],
                (*photon_)->dir[0],(*photon_)->dir[1],(*photon_)->dir[2]);
	printf("*** sx_path_source.  Sort lambda in the spectrum:\n");
	sampled_data_print(sx_path_source->spectrum,stdout);
	printf("*** sx_path_source. lambda sorted=%f\n",(*photon_)->lambda);
	printf("*** sx_path_source. radiance sorted=%f\n",(*photon_)->w);
#endif

}

//build the wavelength sorter:
int sx_path_source_create_wavelength_sorter(
		struct ssp_ranst_discrete** wavelength_sorter,
		struct sampled_data* spectrum)
{
	double w[spectrum->nb_vals];//weights
	for (unsigned int i=0;i<spectrum->nb_vals;i++)
	{
		w[i]=spectrum->y[i];
	}
	//build the random generator fo source index:
	ssp_ranst_discrete_create(NULL,wavelength_sorter);
	ssp_ranst_discrete_setup((*wavelength_sorter),w,spectrum->nb_vals);
	return 1;
}



void test_sort_lambda(void)
{
	struct ssp_rng* rng;
	ssp_rng_create(NULL, SSP_RNG_THREEFRY, &rng);
	struct sampled_data * s;
	sampled_data_create_from_string(&s,"450 1 530 1 650 1","spectrum");
	unsigned int n=s->nb_vals;
	double w[n];//weights
	for (unsigned int i=0;i<n;i++)
	{
		w[i]=s->y[i];
	}
	//build the random generator fo source index:
	struct ssp_ranst_discrete* wavelength_sorter;//lambda sorter
	ssp_ranst_discrete_create(NULL,&(wavelength_sorter));
	ssp_ranst_discrete_setup(wavelength_sorter,w,n);
	for (int i=0;i<10;i++)
	{
		int j=ssp_ranst_discrete_get(rng,wavelength_sorter);
		float lambda=s->x[j];
		printf("lambda=%f\n",lambda);
	}
}
