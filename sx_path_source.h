/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SX_PROBE_H_
#define SX_PROBE_H_

#include <star/ssp.h>

#include "sx_photon.h"
#include "sx_xml.h"

struct sx_scene;

enum {
PATH_SOURCE_TYPE_SOURCE, //the paths starts from a source
PATH_SOURCE_TYPE_CAMERA, //the paths starts from a camera
PATH_SOURCE_TYPE_SENSOR //the paths starts from a radiance/fluence sensor
};


/**
define the geometry of photon/path generator
position , direction , aperture
*/
struct sx_path_source
	{
	int type;//PATH_SOURCE_TYPE_...
	char name[MAX_NAME_LENGTH];
	struct sampled_data* spectrum;//list of the spectra 
	struct ssp_ranst_discrete* wavelength_sorter;//lambda sorter
 //weight calculated by Monte-Carlo, dimension 1 for reverse path,
	// dimension = nb of sensors (surface) *2 (sides) for direct path
	unsigned int weight_dim;
	double* weight;
	double* sigma;//sigma calculated by Monte-Carlo
	float position_end[3];//position at end of trajectory
	int index_end_surface;
	int index_end_volume;
	int x,y;//x and y of the pixel (case of sx_path_source is a camera pixel)
	struct sx_volume* starting_volume;//starting volume
	//PATH_SOURCE_TYPE_CAMERA:
	struct sx_camera* camera;
	//PATH_SOURCE_TYPE_SOURCE:
	struct sx_source* source;
	//PATH_SOURCE_TYPE_SENSOR: 
	struct sx_sensor* sensor;
	};

void sx_path_source_create(struct sx_path_source** p_p_sx_path_source);
int sx_path_source_create_from_xml(struct sx_path_source** p_p_sx_path_source,
									mxml_node_t *node);
int sx_path_source_find_or_create_spectrum(struct sampled_data*** filters,
					int* nb_filters,char* filters_list,struct sx_scene* scene);
void sx_path_source_release(struct sx_path_source* sx_path_source);

void sx_path_source_print(struct sx_path_source* sx_path_source);

void sx_path_source_sort_photon(struct sx_path_source* sx_path_source,
								struct ssp_rng* rng,struct sx_photon** photon_);
int sx_path_source_create_wavelength_sorter(struct ssp_ranst_discrete** wavelength_sorter,
												struct sampled_data* spectrum);
int sx_path_source_sort_a_wavelength(struct sx_path_source* sx_path_source,
										struct ssp_rng* rng,float* lambda);

void test_sort_lambda(void);
#endif /* SX_PROBE_H_ */
