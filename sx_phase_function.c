
/*
   Copyright (C) 2023 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */



#include "sx_scene.h"
#include "sx_mat_vol.h"
#include "sampled_data.h"
#include "sx_phase_function.h"
#include "sx_image.h" //for test only

int sx_phase_function_create(struct sx_phase_function** pf_,char* name,int lambda)
{
	//create an empty phase function:
	(*pf_)=calloc(1,sizeof(struct sx_phase_function));
	struct sx_phase_function* pf=(*pf_);
	strcpy(pf->name,name);
	pf->lambda=lambda;
	return 0;
}

/**
  configure the phase function from sampled data having the same name got from the scene.
  The data of the scene is interpolated linearly
 */
int sx_phase_function_configure_from_scene(struct sx_phase_function* pf,struct sx_scene* scene)
{
	//get the data in the scene and configure the phase function
	//printf("configure sx_phase_function of name %s and wavelength %d nm\n",pf->name,pf->lambda);
	//find the phase function:
	int is_numerical=0;
	struct sampled_data* sd=sx_scene_find_spectrum2(scene,pf->name,&is_numerical);
	if (sd==NULL)
	{
		printf("sx_phase_function_configure : can't find spectrum of name %s\n",pf->name);
		return 0;
	}
	//interpolate:
	struct sampled_data* sd2;
	unsigned int n=181;//nb of samples to dicretize the phase function 
	sampled_data_create2(&sd2,n,"");
	//store the pointer to the complete phase function in the volume material:
	//pf->phase_function=sd2;
	sd2->x_regular=1;
	//fill the complete phase function (angle from 0 to PI radians):
	float angle_deg=0,angle_rad=0;
	for (unsigned int i=0;i<n;i++) 
	{	
		angle_deg=(float)i;
		angle_rad=angle_deg*(float)PI/180.0f;
		sd2->x[i]=angle_rad;
		sd2->y[i]=sampled_data_interpolate_linear(sd,angle_deg);
	}

	//sampled_data_save(sd,"sd_avant_interpolation.txt");
	//sampled_data_save(sd2,"sd_apres_interpolation.txt");
	//normalise and create the inverse pdf :
	sx_phase_function_configure_from_sampled_data(pf,sd2);

	return 0;
}


/**
  configure the phase function from sampled data with x: 181 angles in rad from 0 to PI y:probability
 */
int sx_phase_function_configure_from_sampled_data(struct sx_phase_function* pf,struct sampled_data* sd)
{
	pf->phase_function=sd;
	strcpy(pf->phase_function->name,pf->name);
	strcat(pf->phase_function->name," phase function");
	//sprintf(pf->phase_function->name,"%s phase function",pf->name);
	//printf("pf->phase_function before normalisation:\n");
	//sampled_data_print(sd,stdout);
	// calc the integral of the phase function over the full solid angle:
	// solid angle of a sphere cap: 2PI(1-cos(theta))
	// derivative: 2PI sin(theta)
	// integral for isotrope scattering (pdf=1/4PI):  2PI int_0_PI{ 1/4PI sin(theta) dtheta } = 2PI/4PI*(-cos(PI)+cos(0))=1/2*(1+1)=1 OK
	// integral for the general case: 2PI int_0_PI{ pf(theta) sin(theta) dtheta }
	//calc the irradiance (W/m²) :
	float f=0;
	float irradiance=0;
	float theta=0;//angle of scattering in rad
	unsigned int n=sd->nb_vals;
	float dtheta=(float)PI/(float)(n-1);
	for (unsigned int i=0;i<n;i++) 
	{
		theta=sd->x[i];
		f=sinf(theta)*sd->y[i]*dtheta;
		//printf("f=%f\n",f);
		irradiance+=f;
	}
	//printf("phase function irradiance: %f\n",sum);
	//normalise the irradiance:
	sampled_data_mul(sd,1/irradiance);
	//printf("pf->phase_function:\n");
	//sampled_data_print(pf->phase_function,stdout);

	//calculate g=<cos>
	float m0=0;
	float m1=0;
	float v=0;
	for (unsigned int i=0;i<n;i++) 
	{
		theta=sd->x[i];
		v=sinf(theta)*sd->y[i];
		m0+=v;
		m1+=cosf(theta)*v;
	}
	//float g=m1/m0;
	//printf("sx_phase_function_configure_from_sampled_data, pf of name %s : g=%f\n",pf->name,g);
	//create the curve used to sort the scattering angles:
	struct sampled_data* inv1;
	sx_phase_function_create_sorter(&inv1,sd);
	//printf("inv1:\n");
	//sampled_data_print(inv1,stdout);
	//regularize the curve:
	struct sampled_data* inv2;
	unsigned int n2=181;//nb of point of final inv of integral pdf sampling
	sampled_data_create2(&inv2,n2,"");
	inv2->x_regular=1;
	//sprintf(inv2->name,"%s integral of pdf, inverse",pf->name);
	strcpy(inv2->name,"#");
	strcat(inv2->name,pf->name);
	strcat(inv2->name," integral of pdf, inverse");
	float x=0,sx=1.0f/(float)(n2-1);
	for (unsigned int i=0;i<n2;i++) 
	{	
		x=(float)i*sx;
		inv2->x[i]=x;
		inv2->y[i]=sampled_data_interpolate_linear(inv1,x);
	}

	pf->phase_function_integral_inverse_pdf=inv2;
	pf->phase_function_integral_inverse_pdf->x_regular=1;
	//printf("inv2:\n");
	//sampled_data_print(inv2,stdout);
	//sampled_data_save(sd,"sd.txt");
	//sampled_data_save(inv1,"inv1.txt");
	//sampled_data_release(inv1);
	//sampled_data_save(inv2,"pf_integral_inverse.txt");

//	sx_phase_function_test0(pf);
//	sx_phase_function_test(pf);

	return 0;

}
/**
Create the sorter: curve with random entry (r) from 0 to 1 and result angle (theta) from 0 to PI
*/
void sx_phase_function_create_sorter(struct sampled_data ** sorter,struct sampled_data * pf)
{
	float theta;
	unsigned int n=pf->nb_vals;
	sampled_data_create3(sorter,n);
	strcpy((*sorter)->name,"#sorter of ");
	strcat((*sorter)->name,pf->name);
	//calc integral and invert:
	for (unsigned int i=1;i<n;i++)
	{
		theta=pf->x[i];//scattering angle in rad
		(*sorter)->y[i]=theta;
		(*sorter)->x[i]=(float)(*sorter)->x[i-1]+pf->y[i]*sinf(theta);
	}
	//normalise such that last value is 1:
	for (unsigned int i=0;i<n;i++) (*sorter)->x[i]/=(*sorter)->x[n-1];
}

/**
  sort a new direction
dir: direction of the photon (normalized)
newdir: direction of scattering (normalized)
pdf: pointer to a float where is put the calculated pdf (if not NULL)
 */
void sx_phase_function_sort(struct sx_phase_function* pf,struct ssp_rng* rng,float* new_dir,float* dir,float* pdf)
{
	float r,theta,cos_theta,sin_theta,phi;
	float sample_local[3];
	//sort theta
	r=ssp_rng_canonical_float(rng);//sort a float between 0 and 1
	//printf("r=%f \n",r);
	theta=sampled_data_interpolate_linear(pf->phase_function_integral_inverse_pdf,r);
	//printf("r=%f ---->   theta=%f\n",r,theta);
	sin_theta=sinf(theta);
	cos_theta=cosf(theta);
	//phi=ssp_rng_uniform_float(rng,0,2*(float)PI);
	phi=ssp_rng_canonical_float(rng)*2*(float)PI;
	sample_local[0]=cosf(phi)*sin_theta;
	sample_local[1]=sinf(phi)*sin_theta;
	sample_local[2]=cos_theta;

	float basis[9];
	f33_mulf3(new_dir,f33_basis(basis,dir),sample_local);
	//printf("New dir :(%g,%g,%g)\n",new_dir[0],new_dir[1],new_dir[2]);

	if (pdf!=NULL) (*pdf)=r;
}


/**
  get the probability of scattering
dir: direction of the photon (normalized)
newdir: direction of scattering (normalized)
 */
float sx_phase_function_pdf(
		struct sx_phase_function* pf,
		float* newdir,
		float* dir
		)
{
	float cos_theta=f3_dot(dir,newdir);//cosinus of scattering angle
	if (cos_theta<-1) cos_theta=-1;//protect from numerical error
	if (cos_theta>1) cos_theta=1;//protect from numerical error
	float theta=acosf(cos_theta);
	if (theta<0) theta=0;
	if (theta>PI) theta=PI;
	float pdf=sampled_data_interpolate_linear(pf->phase_function,theta);
	return pdf;
}


void sx_phase_function_test0(struct sx_phase_function *pf)
{
	//test:
	struct ssp_rng* rng;
	ssp_rng_create(NULL,SSP_RNG_THREEFRY,&rng);
	unsigned int dim=200;
	int n=1000000;
	struct sampled_data* pdf_;
	sampled_data_create3(&pdf_,dim);
	//sprintf(pdf_->name,"%s phase function",pf->name);
	strcpy(pdf_->name,pf->name);
	strcat(pdf_->name," phase function");
	for (unsigned int i=0;i<dim;i++) pdf_->x[i]=(float)i*(float)PI/(float)(dim-1);
	for (int k=0;k<n;k++)
	{
		float r=ssp_rng_canonical_float(rng);//sort a float between 0 and 1
		float theta=sampled_data_interpolate_linear(pf->phase_function_integral_inverse_pdf,r);
		int cell=(int)(theta*(float)dim/(float)PI);
		pdf_->y[cell]+=1;
	}
	sampled_data_mul(pdf_,30.0f/(float)n);
	char fname[200];
	//sprintf(fname,"%s.txt",pf->name);
	strcpy(fname,pf->name);
	strcat(fname,".txt");
	sampled_data_save(pdf_,fname);
}




void sx_phase_function_test(struct sx_phase_function *pf)
{
	//test:
	struct ssp_rng* rng;
	ssp_rng_create(NULL,SSP_RNG_THREEFRY,&rng);
	float newdir[3];
	float dir[3];
	float pdf;
	struct sx_image* image;
	float x,y;
	int i,j;
	int index=0;
	int w=200;
	int h=200;
	float d=50;
	int n=1000000;
	sx_image_create(&image,w,h,1);
	unsigned int dim=200;
	struct sampled_data* pdf_;
	sampled_data_create3(&pdf_,dim);
	for (unsigned int ii=0;ii<dim;ii++) pdf_->x[ii]=(float)ii*(float)PI/(float)(dim-1);
	for (int k=0;k<n;k++)
	{
		dir[0]=1;
		dir[1]=0;
		dir[2]=0;
		sx_phase_function_sort(pf,rng,newdir,dir,&pdf);
		//ssp_ran_sphere_hg_float(rng,dir,0.813,newdir,&pdf);
		/*
		   float r1=(2*ssp_rng_canonical_float(rng)-1)*PI;
		   float alpha=acos(r1);
		   float teta=ssp_rng_canonical_float(rng)*PI/4;
		   y=cos(alpha);
		   z=sin(alpha);
		   x=cos(teta);
		   newdir[0]=x;

		   f3_normalise(
		 */
		//printf("dir =[%f,%f,%f] newdir=[%f,%f,%f] \n",dir[0],dir[1],dir[2],newdir[0],newdir[1],newdir[2]);
		x=newdir[1]*d;
		y=newdir[2]*d;
		i=(int)(x+(float)(w/2));
		j=(int)(y+(float)(h/2));
		if ((i<0)||(i>=w)) continue;
		if ((j<0)||(j>=h)) continue;
		index=i+w*j;
		//printf("x=%f y=%f i=%d j=%d index=%d\n",x,y,i,j,index);
		//if ((index>=0)&&(index<(w*h))) image->data[index]+=1;
		image->data[index]+=1;

		float costheta=f3_dot(newdir,dir);
		float theta=acosf(costheta);
		int cell=(int)(theta*(float)dim/PI);
		pdf_->y[cell]+=1;
	}
	//remove bright central pixel:
	//image->data[w/2+w*h/2]=0;
	sampled_data_mul(pdf_,(float)(30.0/(int)n));
	sx_image_save_png(image,1,-1,"","test_pf");
	sampled_data_save(pdf_,"pdf2.txt");
}
