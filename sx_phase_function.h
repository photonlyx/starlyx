
/*
Copyright (C) 2023 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SX_PHASE_FUNCTION__
#define SX_PHASE_FUNCTION_


#include "sx_xml.h"
#include "sx_mat_vol.h"


/**
	* Henyey-Greenstein scattering
	*/
struct sx_phase_function
{
	char name[MAX_NAME_LENGTH];//the name of the phase function
	int lambda;
	struct sampled_data* phase_function;//phase fonction, x is the angle in degrees 180 values from 0 to 180  y is the probability
	struct sampled_data* phase_function_integral_inverse_pdf;//to sort an angle, 360 values, x from 0 to 1, y from 0 to 360 
};

int sx_phase_function_create(
		struct sx_phase_function** pf_,
		char* name,
		int lambda
		);

int sx_phase_function_configure_from_scene(
		struct sx_phase_function* pf,
		struct sx_scene* scene
		);

int sx_phase_function_configure_from_sampled_data(
		struct sx_phase_function* pf,
		struct sampled_data* sd
		);

void sx_phase_function_create_sorter(
		struct sampled_data ** sorter,
		struct sampled_data * pf
		);

void sx_phase_function_sort(
		struct sx_phase_function* pf,
		struct ssp_rng* rng,
		float* new_dir,
		float* dir,
		float* pdf
		);

float sx_phase_function_pdf(
		struct sx_phase_function* pf,
		float* newdir,
		float* dir
		);

void sx_phase_function_test0(
		struct sx_phase_function *pf
		);

void sx_phase_function_test(
		struct sx_phase_function *pf
		);

#endif /* SX_PHASE_FUNCTION_H_ */
