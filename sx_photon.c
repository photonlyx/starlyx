
/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <float.h>
#include <stdlib.h>
#include <stdio.h>

#include <star/s3d.h>

#include "sx_photon.h"
#include "sx_volume.h"
#include "sx_mat_vol.h"
#include "sx_surface.h"
#include "sx_mat_surf.h"
#include "sx_source.h"
#include "sx_scene.h"
#include "sx_symbolic.h"
#include "lists.h"
#include "sx_configuration.h"

//Filter used by s3d ray tracing to deal with photon very close to a surface
// in this case, due to numerical errors it is not clear at which side of the surface 
// is the origin of the ray which intersections with surfaces is tested
// the intersection with the close surface is ignored thanks to the filter
// return 1 when the ray hits the surface or triangle indicated in the filter
// manage also the case when the ray is just between 2 triangles
// The filter looks also at the triangle to avoid ignoring the intersection with the
// same surface but at the other side of it.
	extern int hit_filter
(
 const struct s3d_hit* hit,
 const float ray_org[3],
 const float ray_dir[3],
 const float range[2],
 void* filter_context,
 void* dummy)
{
	struct filter_context* filter_ctx = filter_context;
	//case of same surface and same triangle (possible in case of numerical error)
	if (
			(filter_ctx->active == 1) &&
			(hit->prim.geom_id == filter_ctx->surface_id) &&//check if same surface
			(hit->prim.prim_id == filter_ctx->triangle_id)  //check if same triangle
	   )
		return 1;
	//case of next hit in different triangle but very close to the current position (contiguous triangle):
	if (
			(filter_ctx->active == 1) &&
			(hit->prim.geom_id == filter_ctx->surface_id) &&//check if same surface
			(hit->prim.prim_id != filter_ctx->triangle_id) && //check if  triangle if different
			(hit->distance<0.00001) //check if next hit is very close
	   )
		return 1;
	else return 0;
}



const struct filter_context FILTER_CONTEXT_NULL =
{
	0,//  unsigned active;
	0,//  unsigned surface_id;
	0//  unsigned triangle_id;
};


void sx_photon_create(struct sx_photon** photon)
{
	struct sx_photon* ph1=calloc(1, sizeof(struct sx_photon));
	ph1->is_propagating=1;
	ph1->x[0]=0.f;
	ph1->x[1]=0.f;
	ph1->x[2]=0.f;
	ph1->dir[0]=0.f;
	ph1->dir[1]=0.f;
	ph1->dir[2]=1.f;
	ph1->ray_range[0]=0.f;
	ph1->ray_range[1]=FLT_MAX;
	ph1->normal[0]=0.f;
	ph1->normal[1]=0.f;
	ph1->normal[2]=0.f;
	ph1->w=0;
	ph1->lambda=650.f;
	ph1->index_end_volume=-1;
	ph1->index_end_surface=-1;
	ph1->volume=NULL;
	ph1->nb_events=0;
	ph1->nb_scats=0;
	strcpy(ph1->name,"photon1");
	//SYMBOLIC
	//used for symbolic calculation:
	ph1->nb_scattering=0;
	ph1->nb_absorption=0;
	ph1->nb_null_collision=0;
	d_list_create(&ph1->history);
	ph1->distance=0;
 ph1->triangle_id=0;
	ph1->interaction_type=PHOTON_CREATION;
	(*photon)=ph1;
}

void sx_photon_release(struct sx_photon* photon)
{
	struct sx_interaction* in;
	if (photon->history)
	{
		for (unsigned int i=0;i<photon->history->dim;i++)
		{
			in=(struct sx_interaction*)photon->history->data[i];
			free(in);
		}
		d_list_release(photon->history);
	}
	free(photon);
}

void sx_photon_print(struct sx_photon* photon)
{
	printf("Photon:\n");
	printf("   name=%s\n",photon->name);
	printf("   is_propagating=%d\n",photon->is_propagating);
	printf("   pos=(%g,%g,%g) dir=(%g,%g,%g)\n", photon->x[0], photon->x[1],
			photon->x[2],photon->dir[0], photon->dir[1], photon->dir[2]);
	printf("   ray range=(%g,%g)\n",photon->ray_range[0],photon->ray_range[1]);
	printf("   normal=(%g,%g,%g)\n", photon->normal[0], photon->normal[1],
			photon->normal[2]);
	printf("   weight=%f\n",photon->w);
	printf("   lambda=%f\n",photon->lambda);
	printf("   index_end_volume=%d\n",photon->index_end_volume);
	printf("   index_end_surface=%d\n",photon->index_end_surface);
	if (photon->volume!=NULL) printf("   volume=%s\n",photon->volume->name);
		else printf("   volume= NULL\n");
	if (photon->surface!=NULL) 
		{
		printf("   surface=%s\n",photon->surface->name);
		printf("   triangle ID=%d\n",photon->triangle_id);
		}	
		else printf("   surface= NULL\n");
	printf("   nb_events=%d\n",photon->nb_events);
	printf("   nb_scats=%d\n",photon->nb_scats);
	printf("   distance=%g\n",photon->distance);
	char flag[100];
	sx_photon_get_flag(flag,photon->interaction_type);
	printf("   interaction=%s\n",flag);
}


/**
 * create a copy of a photon (without the history)
 */
void sx_photon_copy(struct sx_photon** photon2_,struct sx_photon* photon)
{
	struct sx_photon* photon2;
	sx_photon_create(&photon2);
	strcpy(photon2->name,photon->name);
	photon2->is_propagating=photon->is_propagating;
	f3_set(photon2->x,photon->x);
	f3_set(photon2->dir,photon->dir);
	photon2->ray_range[0]=photon->ray_range[0];
	photon2->ray_range[1]=photon->ray_range[1];
	f3_set(photon2->normal,photon->normal);
	photon2->w=photon->w;
	photon2->lambda=photon->lambda;
	photon2->index_end_volume=photon->index_end_volume;
	photon2->index_end_surface=photon->index_end_surface;
	photon2->volume=photon->volume;
	photon2->surface=photon->surface;
	photon2->triangle_id=photon->triangle_id;
	photon2->nb_events=photon->nb_events;
	photon2->nb_scats=photon->nb_scats;
	photon2->nb_scattering=photon->nb_scattering;
	photon2->nb_absorption=photon->nb_absorption;
	photon2->nb_null_collision=photon->nb_null_collision;
	photon2->distance=photon->distance;
	photon2->interaction_type=photon->interaction_type;
	(*photon2_)=photon2;
}

int  sx_photon_is_error(struct sx_photon* photon)
{
	return (
			(photon->interaction_type==ERROR_RAY_TRACING)||
			(photon->interaction_type==ERROR_PHOTON_LEAK)||
			(photon->interaction_type==CALC_ATTENUATION_ERROR_PHOTON_VOLUME)||
			(photon->interaction_type==CALC_ATTENUATION_ERROR_RAY_TRACING)||
			(photon->interaction_type==CALC_ATTENUATION_MAX_PATH_LENGTH_REACHED)||
			(photon->interaction_type==CALC_ATTENUATION_MAX_NB_EVENTS_REACHED)
		   );
}



void sx_photon_store(struct sx_photon* photon)
{
	struct sx_interaction* in=calloc(1,sizeof(struct sx_interaction));
	f3_set(in->pos,photon->x);
	f3_set(in->dir,photon->dir);
	in->s=photon->surface;
	in->v=photon->volume;
	in->type=photon->interaction_type;
	in->w=(float)photon->w;
	in->distance=photon->distance;
	d_list_add(photon->history,in);
}

/**
 * get the string version of the photon's flag
 */
void sx_photon_get_flag(char* type,int flag)
{
	switch(flag)
	{	
		case	-1: 
			strcpy(type,"UNDEFINED_INTERACTION_TYPE");
			break;
		case	ERROR_RAY_TRACING:
			strcpy(type,"ERROR_RAY_TRACING");
			break;
		case	ERROR_PHOTON_LEAK:
			strcpy(type,"ERROR_PHOTON_LEAK");
			break;
		case	PHOTON_CREATION: 
			strcpy(type,"PHOTON_CREATION");
			break;
		case	SURFACE_REFLEXION:
			strcpy(type,"SURFACE_REFLEXION");
			break;
		case	SURFACE_REFRACTION:
			strcpy(type,"SURFACE_REFRACTION");
			break;
		case	SURFACE_NO_INTERACTION:
			strcpy(type,"SURFACE_NO_INTERACTION");
			break;
		case	SURFACE_ABSORBED:
			strcpy(type,"SURFACE_ABSORBED");
			break;
		case	SURFACE_EMISSION:
			strcpy(type,"SURFACE_EMISSION");
			break;
		case	SURFACE_EMISSION_NADA:
			strcpy(type,"SURFACE_EMISSION_NADA");
			break;
		case	SURFACE_GRATING_OUT_OF_RANGE:
			strcpy(type,"SURFACE_GRATING_OUT_OF_RANGE");
			break;
		case	SURFACE_OPAQUE_AND_NO_SCATTERING:
			strcpy(type,"SURFACE_OPAQUE_AND_NO_SCATTERING");
			break;
		case	VOLUME_ABSORPTION:
			strcpy(type,"VOLUME_ABSORPTION");
			break;
		case	VOLUME_SCATTERING:
			strcpy(type,"VOLUME_SCATTERING");
			break;
		case	RAY_ENTERED_A_CAMERA:
			strcpy(type,"RAY_ENTERED_A_CAMERA");
			break;
		case	RAY_ENTERED_A_SENSOR:
			strcpy(type,"RAY_ENTERED_A_SENSOR");
			break;
		case	PATH_GOES_TO_INFINITE:
			strcpy(type,"PATH_GOES_TO_INFINITE");
			break;
		case	MAX_PATH_LENGTH_REACHED:
			strcpy(type,"MAX_PATH_LENGTH_REACHED");
			break;
		case	MAX_NB_SCATS_REACHED:
			strcpy(type,"MAX_NB_SCATS_REACHED");
			break;
		case	MAX_NB_EVENTS_REACHED:
			strcpy(type,"MAX_NB_SCATS_REACHED");
			break;
		case	MAX_DISTANCE_REACHED:
			strcpy(type,"MAX_DISTANCE_REACHED");
			break;
		case	CALC_ATTENUATION_OK:
			strcpy(type,"CALC_ATTENUATION_OK");
			break;
		case	CALC_ATTENUATION_ERROR_RAY_TRACING:
			strcpy(type,"CALC_ATTENUATION_ERROR_RAY_TRACING");
			break;
		case	CALC_ATTENUATION_ERROR_PHOTON_VOLUME:
			strcpy(type,"CALC_ATTENUATION_ERROR_PHOTON_VOLUME");
			break;
		case	CALC_ATTENUATION_MAX_PATH_LENGTH_REACHED:
			strcpy(type,"CALC_ATTENUATION_MAX_PATH_LENGTH_REACHED");
			break;
		case	CALC_DIRECT_WEIGHT_OK:
			strcpy(type,"CALC_DIRECT_WEIGHT_OK");
			break;
		case	ERROR_WEIGHT_NAN:
			strcpy(type,"ERROR_WEIGHT_NAN");
			break;
	}
}


void sx_photon_print_interaction(struct sx_interaction* in,unsigned int index)
{
	char type[100];
	sx_photon_get_flag(type,in->type);
	printf("%d pos=(%f,%f,%f) dir=(%f,%f,%f) d=%f %s",index,
			in->pos[0],in->pos[1],in->pos[2],
			in->dir[0],in->dir[1],in->dir[2],in->distance,type);
	if (in->s!=NULL) printf(" surf=%s ",in->s->name);
	if (in->v!=NULL) printf(" vol=%s ",in->v->name);
	printf(" w=%f ",in->w);
	printf("\n");
}

void sx_photon_print_history(struct sx_photon* photon)
{
	struct sx_interaction* in;
	printf("Photon name=%s \n",photon->name);
	for (unsigned int i=0;i<photon->history->dim;i++)
	{
		in=(struct sx_interaction*)photon->history->data[i];
		sx_photon_print_interaction(in,i);
	}
}




/**
 * Simulate photon evolution in the scene.
 */
int sx_photon_propagate(
		struct ssp_rng* rng, //random number generator
		struct sx_scene* scene,//the scene with volumes, surfaces, sources and sensors
		struct sx_photon* photon,//the photons
		int source_contribution,//1: add direct source radiance a each scatterring 0:don't
		int max_nb_scats,//nb of max photon scattering 
		int max_nb_events,//nb of max photon events
		float max_distance,//max distance that the photon can recover
		int surface_scattering,//if 0 don't apply surface scattering
		int volume_scattering,//if 0 don't apply volume scattering
		int store//if 1 store the photon's history
)
{
	struct s3d_hit hit;
	float tmp[3];
	//filter used in ray tracing when photon is close to a surface
	struct filter_context filter_;
	struct filter_context* filter=&filter_;
	//check if the photon is on a surface:
	if (photon->surface!=NULL)
	{
		//yes it is on a surface, activate the ray tracing filter:
		//set filter active for this triangle to avoid rehit it on next ray-tracing
		filter->active = 1;
		filter->surface_id = photon->surface->id;
		filter->triangle_id = photon->triangle_id;
#ifdef PRINT_ALL_STEPS
		printf("<---propagate---> %s is on the surface %s \n",photon->name,
				photon->surface->name);
#endif
	}
	else
	{
#ifdef PRINT_ALL_STEPS
		printf("<---propagate---> %s is in a volume %s \n",photon->name,
				photon->volume->name);
#endif
		//unactivate ray-tracing filter:
		filter->active = 0;
	}

#ifdef PRINT_ALL_STEPS
	char flag[100];
	sx_photon_get_flag(flag,photon->interaction_type);
	printf("<---propagate---> %s  filctx->active =%d, filctx->surface_id =%d,\
filctx->triangle_id =%d \n",
				photon->name,
				filter->active , filter->surface_id,filter->triangle_id);
	printf("<---propagate---> %s  max_nb_scats=%d  max_nb_events=%d max_distance=%g \
surface_scattering=%d volume_scattering=%d\n",
				photon->name,
				max_nb_scats,max_nb_events,max_distance,
				surface_scattering,volume_scattering);
	printf("<---propagate---> %s propagates in volume: %s\
 (nb_events=%d,dist=%g,interaction=%s)\n",photon->name,photon->volume->name,photon->nb_events,photon->distance,flag);
#endif
	if (store) sx_photon_store(photon);
	//Loop until going to infinite, absorption by volume, by surface, 
	//  ray tracing error, max interactions or scattering reached.
	for(;;)
	{
#ifdef 	PRINT_ALL_STEPS
		printf("<---propagate---> %s loop start in %s,  lambda=%g  nb_scats=%d\n",
				photon->name,photon->volume->name,photon->lambda,photon->nb_scats);
#endif
		photon->nb_events++;
		float d;//distance of next scattering or absorption
		int isAbsorbed=0;//0 if next event is a scattering, 1 if is an absorption
		struct sx_mat_vol* mv_sorted=NULL;//volume material sorted
		if (volume_scattering) 
			sx_volume_sort_next_distance(&d,&isAbsorbed,&mv_sorted,photon->volume,photon->lambda,rng);
			//sx_volume_sort_next_distance2(&d,&isAbsorbed,&mv_sorted,photon->volume,(int)photon->lambda,rng);
		else d=FLT_MAX;

		//<SYMBOLIC
		int symbolic_sort=0;
		if ((scene->symbolic==1)&&(mv_sorted!=NULL))
		{
			//symbolic path, the path is driven by k_hat and events by 
			//constant probabilities:
			d=(float)ssp_ran_exp(rng,scene->k_hat);
			//sort the type of event
			//sort a float between 0 and 1
			float r=ssp_rng_canonical_float(rng);
			//printf("Symbolic, sorted %g\n",r);
			if (r<scene->p_scat) 
			{
				//case scattering
				photon->nb_scattering++;
				symbolic_sort=SYMBOLIC_SCATTERING;
				//printf("Symbolic: scattering\n");
			}
			else if (r<scene->p_scat+scene->p_abs) 
			{
				//case absorption
				photon->nb_absorption++;
				symbolic_sort=SYMBOLIC_ABSORPTION;
				isAbsorbed=1;
				//printf("Symbolic: absorption\n");
			}
			else  
			{
				//case null collision 
				photon->nb_null_collision++;
				symbolic_sort=SYMBOLIC_NULL_COLLISION;
				//printf("Symbolic: null collision\n");
			}
		}
		//SYMBOLIC>


#ifdef PRINT_ALL_STEPS
		if (mv_sorted!=NULL) printf("<---propagate---> %s next distance %g \
sorted=%s isAbsorbed=%d  ks=%g ka=%g\n",photon->name,d,mv_sorted->name,\
				isAbsorbed,mv_sorted->ks(mv_sorted,photon->lambda),\
				mv_sorted->ka(mv_sorted,photon->lambda));
		if (mv_sorted==NULL) printf("<---propagate---> %s mv_sorted NULL  next\
distance %g  isAbsorbed=%d  \n",photon->name,d,isAbsorbed);
#endif

		// the distance of next event is set as the max distance for embree 
		//next intersection search:
		photon->ray_range[1] = photon->ray_range[0] + d;

#ifdef PRINT_ALL_STEPS
		printf("<---propagate---> %s :  pos=(%g,%g,%g) dir=(%g,%g,%g) w=%f\n",
				photon->name,
				photon->x[0], photon->x[1], photon->x[2],photon->dir[0],\
				photon->dir[1], photon->dir[2],
				photon->w);
		printf("<---propagate---> %s ray_range %g -> %g, filctx->active =%d,\
filctx->surface_id =%d, filctx->triangle_id =%d\n",\
				photon->name,
				photon->ray_range[0],photon->ray_range[1],filter->active,\
				filter->surface_id,filter->triangle_id);
#endif
		//look for next intersection with a surface:
		int res = s3d_scene_view_trace_ray(scene->view, photon->x,photon->dir,\
				photon->ray_range, filter, &hit);
		if(res != RES_OK)
		{
			//if (scene->verbose) fprintf(stderr, "Error : s3d_scene_view_trace_ray\n");
			photon->is_propagating=0;
			photon->interaction_type=ERROR_RAY_TRACING;
		}
#ifdef PRINT_ALL_STEPS
		printf("<---propagate---> %s hitted something=%d  surface id=%d  \
triangle id=%d  hit.distance= %g\n",photon->name,!S3D_HIT_NONE(&hit),
				hit.prim.geom_id,hit.prim.prim_id,hit.distance);
		if (mv_sorted==NULL) printf("<---propagate---> %s mv_sorted==NULL hitted \
something=%d  surface id=%d   triangle id=%d  hit.distance= %g\n",photon->name,
				!S3D_HIT_NONE(&hit),hit.prim.geom_id,hit.prim.prim_id,hit.distance);
#endif
		// check if ray tracing hitted a surface:
		if (!S3D_HIT_NONE(&hit)) //hitted a surface
		{
			//store surface and triangle in photon:
			int indexHittedSurface=hit.prim.geom_id;
			struct sx_surface* surf=scene->surfaces->data[indexHittedSurface];
			photon->surface=surf;
			photon->triangle_id=hit.prim.prim_id;
			//set filter active for this triangle to avoid rehit it on next ray-tracing
			filter->active = 1;
			filter->surface_id = hit.prim.geom_id;
			filter->triangle_id = hit.prim.prim_id;
			int indexOtherVolume=sx_surface_get_volume_id_other_than(surf,\
					photon->volume->id);
#ifdef PRINT_ALL_STEPS
			printf("<---propagate---> %s hitted surface id=%d(%s), triangle id=%d \n",\
					photon->name,
					indexHittedSurface,scene->surfaces->data[indexHittedSurface]->name,\
					indexHittedSurface,scene->surfaces->data[indexHittedSurface]->name,\
					hit.prim.prim_id);
			printf("<---propagate---> %s other volume id=%d(%s) \n",
					photon->name,indexOtherVolume,
					scene->volumes->data[indexOtherVolume]->name);
#endif
			// volume at the other side of the surface
			struct sx_volume* volume1;
			//set the pointer to the object at the other side of the surface:
			if (indexOtherVolume==-1) volume1=NULL;
			else volume1=scene->volumes->data[indexOtherVolume];

			//advance path to the surface:
			f3_add(photon->x,photon->x,f3_mulf(tmp, photon->dir,hit.distance));
			//increment all distance recovered by photon:
			photon->distance+=hit.distance;
#ifdef PRINT_ALL_STEPS
			printf("<---propagate---> %s advanced to pos=(%g,%g,%g) \n",
					photon->name,photon->x[0],photon->x[1],photon->x[2]);
#endif


			//BLURRING_ACCELERATION
			//add the impact to the list of impacts:
			if (surf->store_impacts==1)
				sx_surface_add_impact(surf,photon);

			if (surf->camera!=NULL)
			{
				// CAMERA_DIRECT
				// check if the surface is a camera aperture
				photon->index_end_surface=surf->id;
				photon->is_propagating = 0;
				photon->interaction_type=RAY_ENTERED_A_CAMERA;
#ifdef PRINT_ALL_STEPS
				printf("<---propagate--->  %s camera lens touched  \
						mat_surf %s surface %s at pos: (%g,%g,%g) \n",
						photon->name,ms->name,surf->name,
						photon->x[0],photon->x[1],photon->x[2]);
#endif
			}	
/*
			else if (surf->sensor!=NULL)
			{
				//the surface touched belongs to a sensor:
				f3_normalize(photon->normal,hit.normal);
				photon->index_end_surface=surf->id;
				photon->is_propagating = 0;
				photon->interaction_type=RAY_ENTERED_A_SENSOR;
#ifdef PRINT_ALL_STEPS
				printf("<---propagate--->  %s sensor touched  \
						mat_surf %s surface %s at pos: (%g,%g,%g) \n",
						photon->name,ms->name,surf->name,
						photon->x[0],photon->x[1],photon->x[2]);
#endif
			}	
*/
			else
			{
				// surface interaction:
				// normalize normal (normal to surface at hit position is not 
				// returned normalized by s3d)!!:
				f3_normalize(photon->normal,hit.normal);
				//if the photon has the same dir as the normal it hits the front of the surface
				// else it hits the back
				// the normal side is the one defined by blender
				struct sx_mat_surf* ms;
				photon->hit_front=(f3_dot(photon->normal,photon->dir)>0);
				if (photon->hit_front) ms=surf->mat[0];
				else ms=surf->mat[1];

#ifdef PRINT_ALL_STEPS
				printf("<---propagate hit surface---> photon dir= (%g,%g,%g) normal:(%g,%g,%g), hit front=%d \n",
						photon->dir[0],photon->dir[1],photon->dir[2],
						photon->normal[0],photon->normal[1],photon->normal[2], photon->hit_front);
				printf("<---propagate hit surface--->  %s surface material: %s,  surface: %s at pos: (%g,%g,%g) \n",
						photon->name,ms->name,surf->name,
						photon->x[0],photon->x[1],photon->x[2]);
#endif
				if (source_contribution)
					//only lambertian surface gives direct source intensity:
					if (ms->type==SURFACE_TYPE_LAMBERT)
					{
						//calc the intensity seen directly from the sources:
						float w1=0;
						struct sx_photon* photon2;
						int res=sx_photon_calc_direct_weight(rng,scene,photon,&photon2,&w1);
						if (res!=CALC_DIRECT_WEIGHT_OK) 
						{
							photon->is_propagating=0;
							photon->interaction_type=res;
						}
						else
						{
							//get the probality density of scattering from
							//source back to photon direction:
							//get pdf of surface scattering (BRDF) 
							float pdf=sx_mat_surf_scatter_pdf(ms,photon,photon2->dir);
							photon->w+=w1*pdf;
#ifdef PRINT_ALL_STEPS
							printf("<---propagate--->  %s normal: (%g,%g,%g) \n",
									photon->name,
									photon->normal[0],photon->normal[1],photon->normal[2]);
							printf(" <---propagate---> %s light dir=(%g,%g,%g)\n",
									photon->name,
									photon2->dir[0],photon2->dir[1],photon2->dir[2]);
							printf(" <---propagate---> %s pdf=%f direct weight= %g (total:%g)\n",
									photon->name,pdf,w1,photon->w);
#endif
							sx_photon_release(photon2);
						}
					}
				//apply the deviation at interface:
	//printf("Surface scattering=%d apply deviation\n",surface_scattering);
				if (surface_scattering) ms->apply(ms,volume1,photon,rng);
				else	surface_no_surface(surf,volume1,photon,rng);
				//if no scattering and surface is opaque, stop the propagation:
				int isOpaque=sx_surface_is_opaque(surf);
				if ((!surface_scattering)&&(!volume_scattering)&&(sx_surface_is_opaque(surf)))
				{
					//no way to continue
					photon->is_propagating=0;
					photon->interaction_type=SURFACE_OPAQUE_AND_NO_SCATTERING;
#ifdef PRINT_ALL_STEPS
					printf("<---propagate---> %s no surface nor volume scattering\
and surface opaque : STOP \n",photon->name);
#endif
				}
				//only lambertian surfaces scatter the photon:
				if (ms->type==SURFACE_TYPE_LAMBERT)
					photon->nb_scats++;
			}




		}
		else //the photon don't hit any surface
		{
			//update surface info on photon
			photon->surface=NULL;
			photon->triangle_id=-1;
			//unactivate ray tracing filter:
			filter->active = 0;
			if (photon->volume->id==0) //the photon is in the external world
			{
#ifdef PRINT_ALL_STEPS
				printf("<---propagate---> I'm in external world and I escape \
to infinite\n");
#endif
				//check if the path falls in a source at infinite (suns)
				for (int i=0;i<scene->nb_sources;i++)
				{		
						struct sx_source* source=scene->sources[i];
						if (source->type==SOURCE_TYPE_SUN)
							{
								if ((-f3_dot(source->dir,photon->dir))>cos(source->angle/2*PI/180))
									photon->w+=source->radiance;	
							}
				}
				//advance path by 10
				f3_add(photon->x,photon->x,f3_mulf(tmp, photon->dir,10));
				//increment all distance recovered by photon:
				photon->distance+=10;
				photon->is_propagating = 0;
				photon->interaction_type=PATH_GOES_TO_INFINITE;
				photon->index_end_volume=0;
			}
			else if (isAbsorbed) 
			{
				//the photon don't hit any surface and	the photon will be absorbed
#ifdef PRINT_ALL_STEPS
				printf("<---propagate---> %s %g %g %g ABSORBED in %s\n",
				photon->name,
				photon->x[0],photon->x[1],photon->x[2],photon->volume->name);
#endif
				//advance path to the absorption point:
				f3_add(photon->x,photon->x,f3_mulf(tmp, photon->dir,d));
				//increment all distance recovered by photon:
				photon->distance+=d;
				photon->is_propagating = 0;
				photon->interaction_type=VOLUME_ABSORPTION;
				photon->index_end_volume=photon->volume->id;
			}
			else if ((photon->volume->id!=0)&&(mv_sorted==NULL))
			{
				//the photon is in a volume that is not the "World" 
				//this volume has no material
				//the photon don't hit any surface 
				//this must be a non detected intersection with the surface
				//enclosing the volume due to digital error
				photon->is_propagating = 0;
				photon->interaction_type=ERROR_PHOTON_LEAK;
				photon->index_end_volume=photon->volume->id;
				//printf("ERROR_PHOTON_LEAK\n");
			}
			else  
			{
				//the photon don't hit any surface and will scatter inside the  volume
				//we propagate the photon by the distance path->ray_range[1] 
				//which is free_path ( unless if the photon is just entering 
				//		in the volume):
				f3_add(photon->x,photon->x,f3_mulf(tmp, photon->dir,d));
#ifdef PRINT_ALL_STEPS
				printf("<---propagate---> Advance photon %s by the distance %g \
to pos=(%g,%g,%g) \n",
						photon->name,d,photon->x[0],photon->x[1],photon->x[2]);
				printf("<---propagate---> %s VOLUME SCATTERING in %s pos=(%g,%g,%g) \n",
						photon->name,photon->volume->name,
						photon->x[0],photon->x[1],photon->x[2]);
#endif
				photon->distance+=d; 
				photon->interaction_type=VOLUME_SCATTERING;
				photon->nb_scats++;
				if (source_contribution==1)
				{
					//(case reverse path): integrate the sources contributions
					//to the weight
					//get the weight got directly from sources:
					float w1=0;
					struct sx_photon* photon2;
					int res=sx_photon_calc_direct_weight(rng,scene,photon,&photon2,&w1);
					if (res!=CALC_DIRECT_WEIGHT_OK) 
					{
						photon->is_propagating=0;
						photon->interaction_type=res;
					}
					else
					{
						//get the probality density of scattering from source 
						//back to photon direction:
						float dir_to_source[3];
						f3_mulf(dir_to_source,photon2->dir,-1);
						sx_photon_release(photon2);
						float mv_pdf=sx_mat_vol_scatter_pdf(mv_sorted,photon,dir_to_source);
						photon->w = photon->w + w1*mv_pdf;
#ifdef PRINT_ALL_STEPS
						printf(" <---propagate---> pdf=%f direct radiance= %g (total:%g)\n",mv_pdf,w1,photon->w);
#endif
					}
				}
				//<SYMBOLIC
				if (!((scene->symbolic)&&(symbolic_sort==SYMBOLIC_NULL_COLLISION))) 
				{
					//it is not a symbolic calculation or it is not a null collison 
					//sort now a new scattered direction using the phase 
					//function of the sorted volume material:
					//TODO is it necessary to normalize here ??
					f3_normalize(photon->dir,photon->dir);
					//float norm=f3_len(photon->dir);
					if (mv_sorted!=NULL) sx_mat_vol_scatter(mv_sorted,rng,photon,NULL);
				}
				// if it is a symbolic calculation and it is a null collision
				//just go straight forward:
				//else printf("Symbolic, null collision: just go forward\n");
				//SYMBOLIC>

			}
		}
#ifdef PRINT_ALL_STEPS
		printf("<---propagate---> %s: nb_scats=%d\n",photon->name,photon->nb_scats);
#endif
		if (scene->max_path_length!=-1)
			if (photon->nb_events>scene->max_path_length)
			{
				//if the path is too long stop and store the trajectory in a file
				photon->is_propagating = 0;//stop the path propagation:
				photon->interaction_type=MAX_PATH_LENGTH_REACHED;
			}
		if (max_nb_scats!=-1)
		{
			if (photon->nb_scats>=max_nb_scats)
			{
				photon->is_propagating = 0;//stop the path propagation:
				photon->interaction_type=MAX_NB_SCATS_REACHED;
#ifdef PRINT_ALL_STEPS
				printf("<---propagate---> %s: MAX_NB_SCATS_REACHED\n",photon->name);
#endif
			}
		}
		if (max_nb_events!=-1)
		{
		//the max nb of events is reached
			if (photon->nb_events>=max_nb_events)
			{
				photon->is_propagating = 0;//stop the path propagation:
				photon->interaction_type=MAX_NB_EVENTS_REACHED;
#ifdef PRINT_ALL_STEPS
				printf("<---propagate---> %s: MAX_NB_EVENTS_REACHED\n",photon->name);
#endif
			}
		}
		if (photon->distance>=max_distance)
		{
			photon->is_propagating = 0;//stop the path propagation:
			photon->interaction_type=MAX_DISTANCE_REACHED;
#ifdef PRINT_ALL_STEPS
			printf("<---propagate---> %s: MAX_DISTANCE_REACHED\n",photon->name);
#endif
		}
#ifdef PRINT_ALL_STEPS
		char type[100];
		sx_photon_get_flag(type,photon->interaction_type);
		printf("<---propagate---> %s interaction_type: %s (%d) \n",photon->name,
				type,photon->interaction_type);
#endif
		if (store)	sx_photon_store(photon);
		if (photon->is_propagating==0) break;
	}
	return	photon->interaction_type;
}



/**
 * calc the MC weight (intensity) directly from the sources
 * used for ALGO_REVERSE  
 */
int sx_photon_calc_direct_weight(
		struct ssp_rng* rng,//the random number generator
		struct sx_scene* scene,// the scene 
		struct sx_photon* photon,// the photon or path
		struct sx_photon** photon2_,//the photon used to calc direct radiance 
		float *weight1 // weight (direct radiance)  given by sources
		)
{
	float attenuation_scene=1;
	struct sx_photon* photon2=NULL;//used for direct path
	struct sx_photon* photon3=NULL;//used for not direct path
	struct sx_source* source=NULL;
	float photon_photon2[3];
	float photon_photon3[3];
	float dir_to_source[3];
	float dist_to_source=0;
	struct sx_mat_vol* mv_sorted=photon->volume->mv_sorted;//last sorted material
	(*weight1)=0;
	int res=CALC_DIRECT_WEIGHT_OK;

	//sort a source:
	sx_scene_sort_a_source(scene,rng,&source);
#ifdef PRINT_ALL_STEPS
	printf("<---direct-weight---> source %s sorted \n",source->name);
#endif
	//get a direct photon coming from the source
	sx_source_sort_photon_passing_by_a_point(&photon2,rng,source,photon);

	//if (source->type==SOURCE_TYPE_SUN)
	if (photon2->w!=0)
	{
		//there is a direct way from the source to the point
#ifdef PRINT_ALL_STEPS
		printf("<---direct-weight---> there is a direct way from source %s to the point \n",source->name);
#endif
		//sx_source_sort_photon_passing_by_a_point(&photon2,rng,source,photon);
		//compensation of just one source sorted:
		photon2->w*=scene->nb_sources;
		//calculate the attenuation when crossing the scene:
		//calc the distance from photon1 to the source
		if (source->type==SOURCE_TYPE_SUN) dist_to_source=FLT_MAX;
		else 
		{
			f3_sub(photon_photon2,photon2->x,photon->x);
			dist_to_source=f3_len(photon_photon2);
		}
		//calc the direction from photon1 to the source
		f3_mulf(dir_to_source,photon2->dir,-1);
		struct sx_photon* photon3;
		int res2=sx_photon_calc_attenuation(rng,scene,photon,
				dir_to_source,dist_to_source,&attenuation_scene,
				source,&photon3);
		if(isnan(attenuation_scene)) res= 0;
		if(isnan(attenuation_scene))
		{
			sx_photon_print(photon2);
			printf("dist to source=%g\n",dist_to_source);
			printf("attenuation_scene=%g   photon2->w=%g\n",\
					attenuation_scene,photon2->w);
			sx_photon_print_history(photon3);
		}
		sx_photon_release(photon3);
		if (res2==CALC_ATTENUATION_OK) 
		{
			*weight1=attenuation_scene*photon2->w;
		}
		else
		{
			res=res2;
		}
#ifdef PRINT_ALL_STEPS
		printf(" <---direct_weight---> scattering point=\
				(%g,%g,%g) \n", photon->x[0],photon->x[1],photon->x[2]);
		printf(" <---direct_weight---> : sorted point on source\
				=(%g,%g,%g) \n", photon2->x[0],photon2->x[1],photon2->x[2]);
		printf(" <---direct_weight---> dir to source =\
				(%g,%g,%g) \n", dir_to_source[0],dir_to_source[1],dir_to_source[2]);
		printf(" <---direct_weight---> : attenuation_scene= %g\
				\n",attenuation_scene);
		printf(" <---direct_weight--->  source radiance:%g  after attenuation:%g \n",photon2->w,(*weight1));
#endif

	}
/*

	else 
	{
		//there is no direct line from the source to the point
		//will try getting a undirect way by propagating the photon from the source
		//the source is not SUN since there is no direct way
#ifdef PRINT_ALL_STEPS
	printf("<---direct-weight---> there is NO direct way from source %s to the point \n",
			source->name);
#endif
	printf("<---direct-weight---> there is NO direct way from source %s to the point \n",
			source->name);
		// (source->type!=SOURCE_TYPE_SUN)
#ifdef PRINT_ALL_STEPS
		printf("<---direct-weight---> source %s sorted \n",source->name);
#endif
		//algorithm reverse with source contribution after one scattering
		// (2 steps from source)
#ifdef PRINT_ALL_STEPS
		printf("  <---direct_weight--->  \n");
#endif
		//start from the source and propagate until first scattering 
		sx_source_create_photon(source,&photon3,rng);
		strcpy(photon3->name,"PHOTON_FROM_SOURCE");
#ifdef PRINT_ALL_STEPS
		printf(" <---direct_weight--->    photon pos=(%g,%g,%g)\n", 
				photon->x[0], photon->x[1],photon->x[2]);
		printf(" <---direct_weight--->  source ray pos=\
(%g,%g,%g), dir=(%g,%g,%g)\n",photon3->x[0], photon3->x[1],\
				photon3->x[2],photon3->dir[0], photon3->dir[1],\
				photon3->dir[2]);
#endif
		//send the path in the volume until the first scattering:
		int res1=sx_photon_propagate(rng,scene,photon3,0,1,-1,MAX_DISTANCE,1,1,0);
		//sx_photon_print_history(photon3);
#ifdef PRINT_ALL_STEPS
		printf(" <---direct_weight--->   photon3 after one \
scattering: pos=(%g,%g,%g) dir=(%g,%g,%g)\n",
				photon3->x[0], photon3->x[1], photon3->x[2],\
				photon3->dir[0], photon3->dir[1],photon3->dir[2]);
#endif
		//the secondary source is valid only if it is a scattering point.
		// so check if the path from source has ended at a scattering point:
		if (res1!=MAX_NB_SCATS_REACHED)
		{
#ifdef PRINT_ALL_STEPS
			printf(" <---direct_weight---> secondary source is\
 not a scattering point (event=%d) \n",res1);
			printf(" <---direct_weight---> attenuation=0 \n");
#endif
		}
		else
		{
			// then calc the source contribution from photon to 
			// secondary source (photon3)
			f3_sub(photon_photon3,photon3->x,photon->x);
			//calc direction to secondary source:
			f3_normalize(dir_to_source,photon_photon3);
			//calc the distance from source to point:
			float dist=f3_len(photon_photon3);
			//calculate the square of the distance
			double dist2=pow(dist,2);
			//calc the attenuation by crossing the scene:
			int res2=sx_photon_calc_attenuation(rng,scene,photon,dir_to_source,
					dist,&attenuation_scene,source,NULL);
			if (res2==CALC_ATTENUATION_OK) 
			{
				float g=0;
				if (mv_sorted!=NULL) g=mv_sorted->g(mv_sorted,photon->lambda);
#ifdef PRINT_ALL_STEPS
				if (mv_sorted!=NULL) 
					printf("<---direct_weight---> ALGO_REVERSE_2 ,volume mat.\
sorted=%s  g=%g k=%g\n",
					mv_sorted->name,g,mv_sorted->ks(mv_sorted,photon->lambda));
#endif
				// now get the phase function to calc the secondary source power:
				f3_mulf(dir_to_source,photon3->dir,-1);
//TODO if the scattering is on a surface ?????
				float mv_pdf=ssp_ran_sphere_hg_float_pdf( photon->dir,
															g,dir_to_source);
				//set the photon intensity:
				//the source power is in w/sr, 
				//the surface power at the surface point is in w/m²
				(*weight1)=	source->power*
					source->spectr(source->spectrum,photon->lambda)
					/(dist2*1e-6)
					*attenuation_scene
					*mv_pdf;
				if(isnan(*weight1)) res= 0;
#ifdef PRINT_ALL_STEPS
				printf("<---direct-weight---> ALGO_REVERSE_2 source power=%f \
\n",source->power);
				printf("<---direct-weight---> ALGO_REVERSE_2 source spectral\
power for lambda=%f: %f \n",
				photon->lambda,source->spectr(source->spectrum,photon->lambda));
				printf("<---direct-weight---> ALGO_REVERSE_2  attenuation_scene=\
%f mv_pdf=%f -> weight1=%g \n",attenuation_scene,mv_pdf,*weight1);
#endif
			}
			else
			{
				res=res2;
			}
		}
	}
*/
	(*photon2_)=photon2;
	return res;
}


/**
calc the attenuation between a point and a source
*/
int sx_photon_calc_attenuation(
		struct ssp_rng* rng,
		struct sx_scene* scene,
		struct sx_photon* photon1,
		float* dir_to_source,
		float dist_to_source,
		float *attenuation,
		struct sx_source* source, //source up to which the attenuation is calculated
		struct sx_photon** photon3_ //to get the photon from pt to source,can be NULL
		)
{
	int res=CALC_ATTENUATION_OK;
	//create a new photon starting at photon1's place but in direction to the source
	struct sx_photon* photon3;
	//sx_photon_copy(&photon3,photon1);
	sx_photon_create(&photon3);
	strcpy(photon3->name,"PHOTON_FOR_ATTENUATION");
	f3_set(photon3->x,photon1->x);
	photon3->lambda=photon1->lambda;
	photon3->volume=photon1->volume;
	photon3->surface=photon1->surface;
	photon3->triangle_id=photon1->triangle_id;
	f3_set(photon3->dir,dir_to_source);
	photon3->distance=0;
	//send photon3 in the scene with no scattering at all up to the source:
	//the photon 3 stops when the distance to source is reached
	int res1=sx_photon_propagate(rng,scene,photon3,0,-1,1000,dist_to_source,0,0,1);

/*	if (photon3->nb_scats>0)
	{
		printf("\n    photon3 nb scats=%d\n",photon3->nb_scats); 
		sx_photon_print_history(photon3);
	}
*/

	if (res1==ERROR_RAY_TRACING)
	{
		res=CALC_ATTENUATION_ERROR_RAY_TRACING;
	}
	else if (res1==MAX_PATH_LENGTH_REACHED)
	{
		printf("\n  sx_photon_calc_attenuation MAX_PATH_LENGTH_REACHED  photon3 path length=%g\n",photon3->distance); 
		//sx_photon_print_history(photon3);
		res=CALC_ATTENUATION_MAX_PATH_LENGTH_REACHED;
	}
	else if (res1==MAX_NB_EVENTS_REACHED)
	{
		printf("\n sx_photon_calc_attenuation MAX_NB_EVENTS_REACHED   photon3 nb events=%d\n",photon3->nb_events); 
	//	sx_photon_print_history(photon3);
		res=CALC_ATTENUATION_MAX_NB_EVENTS_REACHED;
	}
	else if(photon3->volume->id!=source->starting_volume->id)
	{
		//printf("\n    photon3 CALC_ATTENUATION_ERROR_PHOTON_VOLUME\n"); 
		//sx_photon_print_history(photon3);
		//the final volume of photon3 is not th source's one
		res=CALC_ATTENUATION_ERROR_PHOTON_VOLUME;
	}
	else
	{
#ifdef PRINT_ALL_STEPS
		printf("<    calc attenuation    > History of photon %s \n",photon3->name);
		sx_photon_print_history(photon3);
#endif
		//calculate the attenuation between photon1 and source
		float tmp[3];
		struct sx_interaction* in1;
		struct sx_interaction* in2;
		*attenuation=1;
		float d;
		float kext;//extinction coef of the volume
		for (unsigned int i=0;i<photon3->history->dim-1;i++)
		{
			//get the interaction entering the volume:
			in1=(struct sx_interaction*)photon3->history->data[i];
			//get the interaction exiting the volume:
			in2=(struct sx_interaction*)photon3->history->data[i+1];
			//check opacity of the surface
			//if opaque set attenuation to zero end stop:
			if (in2->s!=NULL) 
				if (in2->s->source==NULL) //don't check opacity of the source surface
					if (sx_surface_is_opaque(in2->s))
					{
//int opaque=sx_surface_is_opaque(in2->s);
//printf("ooo  %d  %s opacity=%d\n",i,in2->s->name,opaque); 
						*attenuation=0;
						break;
					}
			//get the distance recovered in the volume:
			d=f3_len(f3_sub(tmp,in2->pos,in1->pos));
			//get the extinction coefficient of the current volume:
			kext=sx_volume_k_ext(in1->v,photon3->lambda);
			//calculate the attenuation:
			*attenuation*=expf(-kext*d);
#ifdef PRINT_ALL_STEPS
			printf("Entering the volume:\n");
			sx_photon_print_interaction(in1,0);
			printf("Exiting the volume:\n");
			sx_photon_print_interaction(in2,0);
			printf("Distance in the volume: %f. Attenuation= %f\n",d,expf(-kext*d));
#endif
		}
	}
	if (photon3_!=NULL) (*photon3_)=photon3;
	else sx_photon_release(photon3);
	return res;
}




