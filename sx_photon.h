/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
   */


#ifndef SX_PHOTON_H_
#define SX_PHOTON_H_

#include "sx_scene.h"
#include "lists.h"




//different flag used for path propagation:
enum {
	ERROR_RAY_TRACING,//propagation stopped by error in ray tracing
	ERROR_PHOTON_LEAK,//propagation stopped by intersection not detected
	PHOTON_CREATION,
	SURFACE_REFLEXION,
	SURFACE_REFRACTION,
	SURFACE_NO_INTERACTION,
	SURFACE_ABSORBED,
	SURFACE_EMISSION,
	SURFACE_EMISSION_NADA,
	SURFACE_GRATING_OUT_OF_RANGE,
	SURFACE_OPAQUE_AND_NO_SCATTERING,
	VOLUME_ABSORPTION,
	VOLUME_SCATTERING,
	RAY_ENTERED_A_CAMERA,
	RAY_ENTERED_A_SENSOR,
	PATH_GOES_TO_INFINITE,
	MAX_PATH_LENGTH_REACHED,
	MAX_NB_SCATS_REACHED,
	MAX_NB_EVENTS_REACHED,
	MAX_DISTANCE_REACHED,
	CALC_ATTENUATION_OK,
	CALC_ATTENUATION_ERROR_RAY_TRACING,
    CALC_ATTENUATION_ERROR_PHOTON_VOLUME,
	CALC_ATTENUATION_MAX_PATH_LENGTH_REACHED,
	CALC_ATTENUATION_MAX_NB_EVENTS_REACHED,
	CALC_DIRECT_WEIGHT_OK,
	ERROR_WEIGHT_NAN
};

//max distance that a photon can propagate in one go
#define MAX_DISTANCE 100000 

/**
 * A photon (path)
 */
struct sx_photon 
{
	char name[100];//name of the photon 
	unsigned is_propagating;//0: stop the photon 1: continue
	float x[3];//photon position
	float dir[3];//photon direction
	float ray_range[2] ;//used by ray tracing: where to look for intersections
	float normal[3];//surface normal when photon hits a surface.
	double w;//Monte-Carlo weight ( radiance W sr-1 m-2 )
	float lambda;//wavelength
		     //index of the volume when the photon ends (-1 if end on a surface)
	int index_end_volume;
	//index of the surface when the photon ends (-1 if end on a volume)
	int index_end_surface;
	//volume where is the path. if NULL the photon is on a surface
	struct sx_volume* volume;
	//surface where is the photon. if NULL the photon is in a volume
	struct sx_surface* surface;
	//triangle id of the surface where is the photon. -1 if the photon is in a volume:
	unsigned int triangle_id;
	//nb of intercations of the photon (with volumes and surfaces)
	int nb_events;
	//nb of single scattering in the volume
	int nb_scats;
	//symbolic:
	//counter of numbers of scattering events in the path (for symbolic):
	int nb_scattering;
	//counter of numbers of absorption events in the path (for symbolic)
	int nb_absorption;
	//counter of numbers of null collision events in the path (for symbolic)
	int nb_null_collision;
	//list of interactions:
	struct d_list* history;
	//distance recovered:
	float distance;
	//type of interaction:(MAX_DISTANCE_REACHED,MAX_DISTANCE_REACHED,...) 
	int interaction_type;
	//when the phton is on a surface, tells if it hitted the front or the back
	int hit_front;
};


struct filter_context
{
	unsigned active;
	unsigned surface_id;
	unsigned triangle_id;
};


void sx_photon_create(struct sx_photon** photon);

void sx_photon_release(struct sx_photon* photon);

void sx_photon_print(struct sx_photon* photon);

void sx_photon_copy(struct sx_photon** photon2,struct sx_photon* photon);

int sx_photon_is_error(struct sx_photon* photon);


int sx_photon_calc_direct_weight(
		struct ssp_rng* rng,
		struct sx_scene* scene,
		struct sx_photon* photon,
		struct sx_photon** photon2,
		float *weight1);

int sx_photon_calc_attenuation(
		struct ssp_rng* rng,
		struct sx_scene* scene,
		struct sx_photon* photon,
		float* dir_to_source,
		float dist_to_source,
		float *attenuation,
		struct sx_source* source,
		struct sx_photon** photon2_
		);

int sx_photon_propagate(struct ssp_rng* rng,
		struct sx_scene* scene,
		struct sx_photon* photon,
		int source_contribution,
		int max_nb_scats,
		int max_nb_events,//nb of max photon events
		float max_distance,
		int surface_scattering,
		int volume_scattering,
		int store);

//photon interaction (the nodes of a path)
struct sx_interaction 
{
	int type;//MAX_DISTANCE_REACHED,MAX_DISTANCE_REACHED,... 
	float pos[3];//photon position
	float dir[3];//photon direction
	float distance;//distance recovered by the photon
	struct sx_surface* s;
	struct sx_volume* v;
	float w;//weight
};

void sx_photon_store(struct sx_photon* photon);
void sx_photon_get_flag(char* type,int flag);
void sx_photon_print_interaction(struct sx_interaction* in,unsigned int index);
void sx_photon_print_history(struct sx_photon* photon);
void sx_photon_release_history(struct sx_photon* photon);


#endif /* SX_PHOTON_H_ */
