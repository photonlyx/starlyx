/*
   Copyright (C) 2024 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */


#define M_PI 3.14159265358979323846
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>
#include "sx_polydispersity.h"
#include "sx_mat_vol.h"


int sx_mat_vol_collection_polydispersity_calc(
		const double rMean,//mean radius
		const double sigma,// sigma of the normal pdf or of normal(ln(r)) if log normal
		const double phi_tot,//volume fraction of all the particles
		const int log_normal,//if 1 normal distrib, if 0 log normal
		double** r_,//radii choosen for the size distrib sampling
		double** phis_,//volume fractions for each radius
		int* n_ //nb of radii
		)
{
int n=16;
(*n_)=n;
double x[n];
double w[n];
(*r_)=(double*)calloc(1,n*sizeof(double));
(*phis_)=(double*)calloc(1,n*sizeof(double));
double* r=(*r_);
double* phis=(*phis_);

/*
###########################################################
#                  NOEUDS DE QUADRATURE                   #
#                        GAUSS                            #
###########################################################
*/
//#                        n = 16                           #
//$nG[2] = 16;
x[0]  = -0.989400934991649932596;
w[0]  =  0.02715245941175409485;
x[1]  = -0.944575023073232576078;
w[1]  =  0.06225352393864789286;
x[2]  = -0.865631202387831743880;
w[2]  =  0.09515851168249278481;
x[3]  = -0.755404408355003033895;
w[3]  =  0.12462897125553387205;
x[4]  = -0.617876244402643748447;
w[4]  =  0.14959598881657673208;
x[5]  = -0.458016777657227386342;
w[5]  =  0.16915651939500253818;
x[6]  = -0.281603550779258913230;
w[6]  =  0.18260341504492358886;
x[7]  = -0.095012509837637440185;
w[7]  =  0.18945061045506849628;
x[8]  = -x[7];
w[8]  =  w[7];
x[9]  = -x[6];
w[9]  =  w[6];
x[10] = -x[5];
w[10] =  w[5];
x[11] = -x[4];
w[11] =  w[4];
x[12] = -x[3];
w[12] =  w[3];
x[13] = -x[2];
w[13] =  w[2];
x[14] = -x[1];
w[14] =  w[1];
x[15] = -x[0];
w[15] =  w[0];

/*
###########################################################
#CALCUL DES BORNES D'INTEGRATION DE LA DISTRIB DES TAILLES#
###########################################################
*/
double rMin;
double rMax;
double epsilon=0.01;

if (log_normal)
  printf("r=%g sigma in log scale=%g phi_tot=%g\n",rMean,sigma,phi_tot);
else 
  printf("r=%g sigma=%g phi_tot=%g\n",rMean,sigma,phi_tot);

if (log_normal)
{
  rMax = gsl_cdf_lognormal_Pinv(1-epsilon/2,log(rMean),sigma);
  rMin = gsl_cdf_lognormal_Pinv(epsilon/2,log(rMean),sigma);
}
else
{
  double delta = -gsl_cdf_gaussian_Pinv(epsilon/2.0,sigma);
  //printf("delta=%g\n",delta);
  rMin = rMean - delta;
  rMax = rMean + delta;
}
if (rMin < 0) 
{
  printf("Polydispersity: size range goes to negative sizes. Put a lower SIGMA_D_UM\n");
  return 0;
}
printf("rMin=%g\n",rMin);
printf("rMax=%g\n",rMax);


/*
{
	char full_file_name[2*MAX_NAME_LENGTH];
	char file_name[2*MAX_NAME_LENGTH];
	strcpy(full_file_name,"");
	strcpy(file_name,"logn.txt");
	strcat(full_file_name,file_name);
	FILE* file1 = fopen(full_file_name, "w");
	fprintf(file1,"#x\ty\n");
int nn=100;
double xmin=rMin;
double xmax=rMax;
double sigma1=sigma;
double r1=rMean;
double lxmin=log(xmin);
double lxmax=log(xmax);
	for (int i=0;i<nn;i++)
	{
		double lx=lxmin+(lxmax-lxmin)/(nn-1)*i;
		double x=exp(lx);
		//double y=gsl_ran_normal_pdf(lx,r1,sigma1);
		//double y=sx_polydispersity_pdfNorm(r1,sigma1,lx,epsilon);
		double y=gsl_ran_lognormal_pdf(x,log(r1),sigma1);
		fprintf(file1,"%f\t%g\n",x,y);
	}
fclose(file1);

}




{
	char full_file_name[2*MAX_NAME_LENGTH];
	char file_name[2*MAX_NAME_LENGTH];
	strcpy(full_file_name,"");
	strcpy(file_name,"inv.txt");
	strcat(full_file_name,file_name);
	FILE* file1 = fopen(full_file_name, "w");
	fprintf(file1,"#x\ty\n");
	int nn=100;
	double xmin=0.0001;
	double xmax=0.9999;
	double sigma1=sigma;
	double r1=rMean;
	for (int i=0;i<nn;i++)
	{
		double x=xmin+(xmax-xmin)/(nn-1)*i;
		//double y=gsl_cdf_gaussian_Pinv(x,sigma1);
		double y=gsl_cdf_lognormal_Pinv(x,log(r1),sigma1);
		fprintf(file1,"%f\t%g\n",x,y);
	}
	fclose(file1);
}
*/

/*
###########################################################
#              CALCULATE MEAN OF R^3                       #
###########################################################
# my r3_mean = 3*rMean*sigma*2+rMean**3;
*/
double r3_mean=0;
double d[n];
double lrmin=log(rMin);
double lrmax=log(rMax);
for (int i=0;i<n;i++)
{
/*
	if (lognormal)	
	{
		r[i] = exp(lrmin+(lrmax-lrmin)/2.*(log(x[i])+1));
		d[i]=sx_polydispersity_pdfLogNorm(rMean,sigma,r[i],epsilon);
		r3_mean += pow(r[i],3) *(rMax-rMin)/2.*w[i]*d[i];
	}
	else	
*/
	{
		r[i] = rMin+(rMax-rMin)/2.*(x[i]+1);
		d[i]=sx_polydispersity_pdfNorm(rMean,sigma,r[i],epsilon);
		r3_mean += pow(r[i],3) *(rMax-rMin)/2.*w[i]*d[i];
	}
}
printf("r3_mean=%g\n",r3_mean);
printf("r_mean=%g\n",pow(r3_mean,0.3333333));
/*
###########################################################
#             CALCULATE VOLUME FRACTIONS                  #
###########################################################
	*/
for (int i = 0; i < n; i++)
{
	phis[i]=phi_tot*pow(r[i],3)/r3_mean*(rMax-rMin)/2.*w[i]*d[i];
}
/*
###########################################################
#                      VALIDATION                         #
###########################################################
	*/

double sum = 0;
for (int i = 0; i < n; i++)
{
	sum += phis[i];
}
printf("Validation: sum of phi_i= %g \t phi_tot= %g\n",sum,phi_tot);


return 1;
}



double sx_polydispersity_pdfNorm(
		double rM,
		double s,
		double r,
		double epsilon
		) 
{
	return exp(-pow(r-rM,2)/(2*s*s)) / (sqrt(2.*M_PI)*s) /(1-epsilon);
}

double sx_polydispersity_pdfLogNorm (
		double rM,
		double s,
		double r,
		double epsilon
		) 
{
return exp(  -pow(log(r)-log(rM),2.)  /(2.*  pow( (log(s)),2.)   ))/(sqrt(2.*PI)*r*log(s))/(1-epsilon);
}



int sx_mat_vol_collection_create(
    struct sx_mat_vol_collection** col,
    int n
)
{
	(*col)=(struct sx_mat_vol_collection*)calloc(1,sizeof(struct sx_mat_vol_collection));
	(*col)->array=(struct sx_mat_vol**)calloc(1,n*sizeof(struct sx_mat_vol*));
 (*col)->n=n;
}




int sx_mat_vol_collection_print(
    struct sx_mat_vol_collection* col,
    FILE* stream
    )
{
  fprintf(stream,"Collection of %d materials of volume:\n",col->n);
  fprintf(stream,"\tname=%s\n",col->name);
  for (int i=0;i<col->n;i++)
  {
    fprintf(stream,"\t%d ",i);
    sx_mat_vol_print(col->array[i],stream,"\t");
  }
  return 0;
}




/**
create an array of material volume of type HG_MIE or MIE to simulate polydispersity
*/
int sx_mat_vol_collection_create_mie_mv_collection_from_xml(
		struct sx_mat_vol_collection ** col,
		struct sx_scene* scene,
		mxml_node_t *node,
		int mv_type //TYPE_HG_MIE or TYPE_MIE
)
{
	char word[MAX_NAME_LENGTH];
	char name[MAX_NAME_LENGTH];
	float d,sigma,phi_tot;
 int res;
	//read the attribute NAME:
	if (!read_string_attribute2(node,"NAME",name))return 0;
	//get the attribute D_UM (central diameter):
	res=read_string_attribute23(node,"D_UM",word,1,1,MAX_NAME_LENGTH);
	if (res==0) return 0;
	else	sscanf(word,"%f",&d);
	//get the attribute PHI:
	res=read_string_attribute23(node,"PHI",word,1,1,MAX_NAME_LENGTH);
	if (res==0) return 0;
	else	sscanf(word,"%f",&phi_tot);
	//get the attribute SIGMA_D_UM:
	res=read_string_attribute23(node,"SIGMA_D_UM",word,1,1,MAX_NAME_LENGTH);
	if (res==0) return 0;
	else	sscanf(word,"%f",&sigma);
	//get the attribute DISTRIB_SHAPE:
	res=read_string_attribute23(node,"DISTRIB_SHAPE",word,1,1,MAX_NAME_LENGTH);
	if (res==0) return 0;
	int log=0;
	if (strcmp(word,"log normal")==0) log=1;

	//get the size and volume fraction distribs:
	double* r;
	double* phis;
	int n;
	res=sx_mat_vol_collection_polydispersity_calc((double)d/2.0,
			(double)sigma/2.0,phi_tot,log,&r,&phis,&n);
	if (res==0) return 0;
	
 sx_mat_vol_collection_create(col,n);
 strcpy((*col)->name,name);
	for (int i=0;i<n;i++)
	{
		struct sx_mat_vol* mv;
		sx_mat_vol_create(&mv,"");
		switch(mv_type)
		{
			case TYPE_HG_MIE:
				mv->type=TYPE_HG_MIE;
				sx_mat_vol_read_hg_mie_from_xml(mv,node);
				break;
			case TYPE_MIE:
				mv->type=TYPE_MIE;
				sx_mat_vol_read_Mie_from_xml(mv,node);
				break;
		}
		//put the name of the collection added with the index:
  char name1[MAX_NAME_LENGTH];
  sprintf(name1,"%s_%d",name,i);
		strcpy(mv->name,name1);
  //put the diameter:
  char diam_string[MAX_NAME_LENGTH];
  //float d1=d+(-n/2+i)*sigma;
  //sprintf(diam_string,"%f",d1);
  sprintf(diam_string,"%f",r[i]*2);
  strcpy(mv->spectrum_C_name,diam_string); 
  //put phi:
  char phi_string[MAX_NAME_LENGTH];
  //sprintf(phi_string,"%f",phi/3.0);
  sprintf(phi_string,"%0.10f",phis[i]);
  strcpy(mv->spectrum_A_name,phi_string); 
		(*col)->array[i]=mv;
	}

	//save the polydispersity curve:
	char full_file_name[2*MAX_NAME_LENGTH];
	char file_name[2*MAX_NAME_LENGTH];
	strcpy(full_file_name,scene->file_path);
	strcpy(file_name,(*col)->name);
	strcat(file_name,"_polydispersity.txt");
	strcat(full_file_name,file_name);
	FILE* file1 = fopen(full_file_name, "w");
	switch(mv_type)
	{
		case TYPE_HG_MIE:
			fprintf(file1,"#type HG_MIE\n");
			break;
		case TYPE_MIE:
			fprintf(file1,"#type MIE\n");
			break;
	}
	fprintf(file1,"#diameter(um)\tvolume_fraction\n");
	for (int i=0;i<n;i++)
	{
		fprintf(file1,"%f\t%0.10f\n",r[i]*2,phis[i]);
	}
printf("Size distribution saved at: %s\n",file_name);
	return 1;
}




/**
	* find the spectra used by the material volume collection
	in the scene and connect them
	*/
int sx_mat_vol_collection_connect_spectra(
		struct sx_mat_vol_collection* mvc,
		struct sx_scene* scene
		)
{
	for (int i=0;i<mvc->n;i++)
	{
		struct sx_mat_vol* mv=mvc->array[i];
		sx_mat_vol_connect_spectra(mv,scene);
	}
	return 1;
}



