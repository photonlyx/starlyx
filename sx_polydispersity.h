
/*
   Copyright (C) 2024 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SX_POLYDISPERSITY_H_
#define SX_POLYDISPESITY_H_

#include "sx_mat_vol.h"
/*
			collection of materials for volume
	*/
struct sx_mat_vol_collection
{
	char name[MAX_NAME_LENGTH];
	//array of sx_mat_vol
	struct sx_mat_vol** array;
	//nb of materials
	int n;
};


int sx_mat_vol_collection_polydispersity_calc(
		double rMoyen,
		double sigma,
		double Phi_tot,
		const int log_normal,//if 1 normal distrib, if 0 log normal
		double** r,
		double** phi,
		int* n
		);


double sx_polydispersity_pdfNorm(
		double rM,
		double s,
		double r,
		double epsilon
		); 

double sx_polydispersity_pdfLogNorm (
		double rM,
		double s,
		double r,
		double epsilon
		);



int sx_mat_vol_collection_create(
		struct sx_mat_vol_collection** col,
		int n
		);

int sx_mat_vol_collection_create_mie_mv_collection_from_xml(
		struct sx_mat_vol_collection** col,
		struct sx_scene* scene,
		mxml_node_t *node,
		int mv_type 
		);


int sx_mat_vol_collection_connect_spectra(
		struct sx_mat_vol_collection* mvc,
		struct sx_scene* scene
		);

#endif /* SX_POLYDISPERSITY_H_ */
