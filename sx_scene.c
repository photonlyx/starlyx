/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SX_SCENE_C_
#define SX_SCENE_C_

#include <star/s3daw.h>
#include <star/s3d.h>
#include <star/s3dstl.h>
#include <star/ssp.h>
#include <rsys/clock_time.h>

#include "sx_scene.h"
#include "sx_mat_surf.h"
#include "sx_volume.h"
#include "sx_path_source.h"
#include "sx_sensor.h"
#include "sampled_data.h"
#include "sx_camera.h"
#include "sx_image.h"
#include "sx_surface.h"
#include "sx_source.h"
#include "sx_configuration.h"
#include "sx_variation.h"
#include "sx_xml.h"
#include "sx_mat_vol.h"
#include "lists.h"
#include "float_list_symbolic.h"
#include "sx_cad.h"
#include "sx_polydispersity.h"

int hit_filter
(const struct s3d_hit* hit,
 const float ray_org[3],
 const float ray_dir[3],
 const float range[2],
 void* filter_context,
 void* dummy);

/**
 * find the sampled_data of name name
 */
struct sampled_data*  sx_scene_find_spectrum(const  struct sx_scene* scene,
		const char* name)
{
	struct sampled_data* sd=NULL;
	int nb=0;
	for (int i=0;i<scene->nb_spectra;i++)
	{
		//printf("Look for *%s* . Spectrum name=*%s*\n",name,scene->spectra[i]->name);
		if (strcmp(scene->spectra[i]->name,name)==0)
		{
			sd=scene->spectra[i];
			nb++;
		}
	}
	if (nb>1) 
	{
		printf("At least 2 spectra have the same name: %s\n",name);
		sd=NULL;
	}
	return sd;
}

/**
 * find the sampled_data of name name.
 * if name is a number create a  1 point sampled_data with x=0 and y value this number.
 */
struct sampled_data* sx_scene_find_spectrum2(const  struct sx_scene* scene,
		const char* name,int *is_numerical)
{
	struct sampled_data* sp;
	*is_numerical=parsing_is_numerical_value(name);
	//it is a numerical value create a uniform spectrum with this value for all lambdas
	if (*is_numerical)
	{
		float value;
		sscanf(name,"%f",&value);
		//create a spectrum with a unique value
		sampled_data_create2( &sp,1,"uniform");
		sp->x[0]=0;
		sp->y[0]=value;
	}
	else //find the spectrum with that name
	{
		sp=sx_scene_find_spectrum(scene,name);
		if (sp==NULL)
		{
			printf("Can't find spectrum of name *%s*\n", name);
			return NULL;
		}
	}
	return sp;
}


/**
 * find the material surface of name name
 */
struct sx_mat_surf* sx_scene_find_matsurf(const  struct sx_scene* scene,const char* name)
{
	struct sx_mat_surf* ms=NULL;
	int nb=0;
	for (int i=0;i<scene->nb_material_surface;i++)
	{
		//printf("Look for *%s* . matsurf name=*%s*\n",name,scene->material_surface[i]->name);
		if (strcmp((scene->material_surface[i]->name),name)==0)
		{
			ms=scene->material_surface[i];
			nb++;
		}
	}
	if (nb>1) 
	{
		printf("At least 2 surface materials have the same name: %s\n",name);
		ms=NULL;
	}
	return ms;
}

/**
 * find the sx_surface of name name
 */
struct sx_surface* sx_scene_find_surface(const  struct sx_scene* scene,
		const char* name)
{
	struct sx_surface* surf=NULL;
	int nb=0;
	for (int i=0;i<scene->surfaces->dim;i++)
	{
		struct sx_surface* s1=scene->surfaces->data[i];
		if (strcmp(s1->name,name)==0)
		{
			surf=s1;
			nb++;
		}
	}
	if (nb>1) 
	{
		printf("At least 2 surfaces have the same name: %s\n",name);
		surf=NULL;
	}
	return surf;
}


/**
 * find the sx_surface of name name
 */
struct sx_volume* sx_scene_find_volume(const  struct sx_scene* scene,
		const char* name)
{
	struct sx_volume* v=NULL;
	int nb=0;
	for (int i=0;i<scene->volumes->dim;i++)
	{
		struct sx_volume* v1=scene->volumes->data[i];
		if (strcmp((v1->name),name)==0)
		{
			v=v1;
			nb++;
		}
	}
	if (nb>1) 
	{
		printf("At least 2 volumes have the same name: %s\n",name);
		v=NULL;
	}
	return v;
}


/**
 * find the sx_mat_vol of name name
 */
struct sx_mat_vol* sx_scene_find_mv(
								const  struct sx_scene* scene,
								const char* name
								)
{
	struct sx_mat_vol* mv=NULL;
	int nb=0;
	for (int i=0;i<scene->nb_mv;i++)
	{
		if (strcmp((scene->mv[i]->name),name)==0)
		{
			mv=scene->mv[i];
			nb++;
		}
	}
	if (nb>1) 
	{
		printf("At least 2 volume materials  have the same name: %s\n",name);
		mv=NULL;
	}
	return mv;
}


/**
 * find the sx_mat_vol_collection of name name
 */
struct sx_mat_vol_collection* sx_scene_find_mv_collection(
								const  struct sx_scene* scene,
								const char* name
								)
{
	struct sx_mat_vol_collection* mvc=NULL;
	int nb=0;
	for (int i=0;i<scene->nb_mv_collections;i++)
	{
		if (strcmp((scene->mv_collections[i]->name),name)==0)
		{
			mvc=scene->mv_collections[i];
			nb++;
		}
	}
	if (nb>1) 
	{
		printf("At least 2 volume materials collections  have the same name: %s\n",name);
		mvc=NULL;
	}
	return mvc;
}



/**
 * find the sx_source of name name
 */
struct sx_source* sx_scene_find_source(const  struct sx_scene* scene,
		const char* name)
{
	struct sx_source* source=NULL;
	int nb=0;
	for (int i=0;i<scene->nb_sources;i++)
	{
		if (strcmp((scene->sources[i]->name),name)==0)
		{
			source=scene->sources[i];
			nb++;
		}
	}
	if (nb>1) 
	{
		printf("At least 2 sources have the same name: %s\n",name);
		source=NULL;
	}
	return source;
}


int  sx_scene_create(
		struct sx_scene** scene_
		)
{
	struct sx_scene* scene=calloc(1,sizeof(struct sx_scene));
	scene->store_trajectories=0;
	scene->store_error_trajectories=0;
	scene->algo=0;
	scene->verbose=0;
	scene->nb_photons=0;
	scene->nb_sources=0;
	scene->nb_spectra=0;
	d_list_create(&scene->volumes);
	d_list_create(&scene->surfaces);
	scene->nb_material_surface=0;
	scene->nb_cameras=0;
	scene->nb_threads=1;
	scene->nb_sx_sensors=0;
	scene->nb_mv=0;
	scene->nb_mv_collections=0;
	d_list_create(&scene->path_sources);
	scene->stored_photons=NULL;
	scene->max_path_length=-1;
	scene->max_nb_scattering=-1;
	scene->max_nb_events=-1;
	(*scene_)=scene;
}



int  sx_scene_read_from_xml(
		struct sx_scene* scene, 
		mxml_node_t *tree
		)
{
	//create the "World" object with id=0:
	struct sx_volume* world;
	if (!sx_volume_create_world(&world)) return 0;
	d_list_add(scene->volumes,world);//add the world 

	if (tree)
	{
		mxml_node_t *node1,*top;
		node1=tree;
		top=tree;
		int found_a_scene=0;
		int res;
		for (;;)
		{
			node1 = mxmlWalkNext(top, tree,MXML_DESCEND);
			top=NULL;
			if (node1)
			{
#ifdef PRINT_ALL_STEPS
				printf("sx_scene_create_from_xml, read node of type: <%s>\n",node1->value.element.name);
#endif
				//Scene :
				if (strcmp(node1->value.element.name,"Scene")==0)//found a scene
				{
					found_a_scene=1;
					char buffer[MAX_NAME_LENGTH];
					//this attribute is deprecated:
					res=read_string_attribute23(node1,"DIRECT_PATH",buffer,0,0,MAX_NAME_LENGTH);
					if (!res)
					{
						//no DIRECT_PATH attribute found, look for new algo flag:
						res=read_string_attribute23(node1,"ALGORITHM",buffer,1,1,MAX_NAME_LENGTH);
						if (strcmp(buffer,"direct")==0) scene->algo=ALGO_DIRECT;
						else if (strcmp(buffer,"reverse")==0) scene->algo=ALGO_REVERSE;
						else if (strcmp(buffer,"reverse_1")==0) scene->algo=ALGO_REVERSE;
						else if (strcmp(buffer,"reverse_2")==0) scene->algo=ALGO_REVERSE;
						else 
						{
							printf("Scene: attribute ALGORITHM %s is unknown.\n",buffer);
							printf("   Use options: direct or reverse\n");
							return 0;
						}
					}
					else
					{
						int direct;
						sscanf(buffer,"%d",&direct);
						printf("Scene: use deprecated attribute DIRECT_PATH.\n");
						if (direct) 
						{
							scene->algo=ALGO_DIRECT;
							printf("Scene: use algorithm: direct.\n");
						}
						else 
						{
							scene->algo=ALGO_REVERSE;
							printf("Scene: use algorithm: reverse_1.\n");
						}
					}
					//read VERBOSE attribute:
					res=read_string_attribute2(node1,"VERBOSE",buffer);
					if (!res)
					{
						//put the default which is verbose : 1
						scene->verbose=1;
					}
					else
					{
						sscanf(buffer,"%d",&scene->verbose);
					}
					//read MAX_NB_EVENTS attribute:
					res=read_string_attribute2(node1,"MAX_NB_EVENTS",buffer);
					if (!res)
					{
						//put the default which is 1000000 (no limit)
						scene->max_nb_events=1000000;
						printf("Set to default (%d)\n",scene->max_nb_events);
					}
					else
					{
						sscanf(buffer,"%d",&scene->max_nb_events);
					}
					scene->max_nb_scattering=scene->max_nb_events;
					//read NB_PHOTONS attribute:
					res=read_string_attribute2(node1,"NB_PHOTONS",buffer);
					if (!res)
					{
						exit(0);
					}
					else
					{
						sscanf(buffer,"%lu",&scene->nb_photons);
					}
					//read STORE_TRAJECTORIES attribute:
					res=read_string_attribute2(node1,"STORE_TRAJECTORIES",buffer);
					if (!res)
					{
						//put the default which is store_trajectories=0
						scene->store_trajectories=0;
					}
					else
					{
						sscanf(buffer,"%d",&scene->store_trajectories);
					}
					//read STORE_ERROR_TRAJECTORIES attribute:
					res=read_string_attribute2(node1,"STORE_ERROR_TRAJECTORIES",buffer);
					if (!res)
					{
						//put the default which is store_error_trajectories=0
						scene->store_error_trajectories=0;
					}
					else
					{
						sscanf(buffer,"%d",&scene->store_error_trajectories);
					}
					//read NB_THREADS attribute:
					res=read_string_attribute2(node1,"NB_THREADS",buffer);
					if (!res)
					{
						//put the default which is store_error_trajectories=0
						scene->nb_threads=1;
						printf("NB_THREADS set to 1 by default\n");
					}
					else
					{
						sscanf(buffer,"%d",&scene->nb_threads);
					}
					//<SYMBOLIC
					res=read_string_attribute22(node1,"K_HAT",buffer,0);
					if (res) sscanf(buffer,"%f",&scene->k_hat);
					if ((res)&&(scene->k_hat!=-1))
					{
						if(scene->algo==ALGO_DIRECT)
						{
							printf("Symbolic calculation only available for direct path algorithm\n");
							return 0;
						}
						else
						{
							scene->symbolic=1;
							res=read_string_attribute22(node1,"P_SCAT",buffer,1);//read p_scat
							if (!res) scene->p_scat=0.4f;//put the default which is 0.4
							else sscanf(buffer,"%f",&scene->p_scat);
							res=read_string_attribute22(node1,"P_ABS",buffer,1);//read p_abs
							if (!res) scene->p_abs=0.3f;//put the default which is 0.3
							else sscanf(buffer,"%f",&scene->p_abs);
							//create the list of types of events
							float_list_symbolic_create(&scene->path_symbolic_events,
									5,(int)scene->nb_photons);
							float p_null=1-scene->p_scat-scene->p_abs;
							if (scene->verbose) 
								printf("Use symbolic calculation with k_hat=%g, pscat=%g, p_abs=%g, p_null=%g \n",
										scene->k_hat,scene->p_scat,scene->p_abs,p_null);
						}
					}
					else
					{
						//if (scene->verbose) printf("Standard Monte-Carlo\n");
						scene->k_hat=0;
						scene->symbolic=0;
					}
					//SYMBOLIC>
				}
				//spectra:
				if ((strcmp(node1->value.element.name,"spectrum")==0)||
						(strcmp(node1->value.element.name,"sampled_data")==0))
				{
					//found a spectrum or a sampled data
					res =sampled_data_create_from_xml( &scene->spectra[scene->nb_spectra],node1);
#ifdef PRINT_ALL_STEPS
					sampled_data_print(scene->spectra[scene->nb_spectra],stdout);
#endif
					if (res==0) return 0;
					//sort the wavelength in increasing order
					sampled_data_sort_x(scene->spectra[scene->nb_spectra]);
					scene->nb_spectra++;
					if (scene->nb_spectra==MAX_NB_SPECTRA) 
						{
						printf("Max nb of spectra reached (%d)\n",MAX_NB_SPECTRA);
						return 0;
						}
				}
				//camera:
				if (strcmp(node1->value.element.name,"camera")==0)//found a camera
				{
					struct sx_camera*  camera;
					res =sx_camera_create_from_xml(&camera,node1);
					if (res==0) return 0;
#ifdef PRINT_ALL_STEPS
					sx_camera_print(camera);
#endif
					scene->camera[scene->nb_cameras]=camera;
					scene->nb_cameras++;
					if (scene->nb_cameras==MAX_NB_CAMERAS) 
						{
						printf("Max nb of cameras reached (%d)\n",MAX_NB_CAMERAS);
						return 0;
						}
				}
				//sx_sensor:
				if ((strcmp(node1->value.element.name,"probe")==0)//found a sx_sensor
						||(strcmp(node1->value.element.name,"sensor")==0))
				{
					struct sx_sensor*  sx_sensor;
					res =sx_sensor_create_from_xml(&sx_sensor,node1,scene);
					if (res==0) return 0;
#ifdef PRINT_ALL_STEPS
					sx_sensor_print(sx_sensor,stdout);
#endif
					scene->sx_sensors[scene->nb_sx_sensors]=sx_sensor;
					scene->nb_sx_sensors++;
					if (scene->nb_sx_sensors==MAX_NB_PROBES) 
						{
						printf("Max nb of sensors reached (%d)\n",MAX_NB_PROBES);
						return 0;
						}
				}
				//surface:
				if (strcmp(node1->value.element.name,"surface")==0)//found a surface
				{
					struct sx_surface* surf;
					res=sx_surface_create_from_xml(&surf,node1,scene->file_path);
					d_list_add(scene->surfaces,surf);
					if (res==0) return 0;
					//set the ID of the surface:
					surf->id=scene->surfaces->dim-1;
#ifdef PRINT_ALL_STEPS
					sx_surface_print(surf,stdout));
#endif
				}
				//materials of volume (scatterers):
				if ((strcmp(node1->value.element.name,"Henyey-Greenstein")==0)
						||(strcmp(node1->value.element.name,"Scattering")==0))
				{
					//first look for a polydisperse material volume:
					char word[MAX_NAME_LENGTH];
					res=read_string_attribute22(node1,"SIGMA_D_UM",word,0);
					int polydisp=0;//flag of presence of polydispersity
					float sigma;//sigma of size distribution
					if (res==0)
					{
						polydisp=0;
					}
					else
					{
						if (parsing_is_numerical_value(word))	sscanf(word,"%f",&sigma);
						if (sigma<0) 
						{
							fprintf(stderr,"%s : SIGMA_D_UM<0!\n",node1->value.element.name);
							return 0;
						}
						if (sigma!=0) polydisp=1;
					}
					if(polydisp)
					{
						//polydisperse case, create a collection of mv:	
						struct sx_mat_vol_collection * col;
						res=sx_mat_vol_collection_create_mie_mv_collection_from_xml(&col,scene,node1,TYPE_HG_MIE);
						//sx_mat_vol_collection_print(col,stdout);
						//sx_scene_print(scene,stdout);
						if(res==0) return 0;
						scene->mv_collections[scene->nb_mv_collections++]=col;
						if (scene->nb_mv_collections==MAX_NB_MATERIAL) 
						{
							printf("Max nb of volume materials reached (%d)\n",MAX_NB_MATERIAL);
							return 0;
						}
					}
					else
					{
						//monodisperse case,create a scatterer
						struct sx_mat_vol* mv;
						res =sx_mat_vol_create_from_xml(&mv,node1);
						if (res==0) return 0;
						scene->mv[scene->nb_mv++]=mv;
						if (scene->nb_mv==MAX_NB_MATERIAL) 
						{
							printf("Max nb of volume materials reached (%d)\n",MAX_NB_MATERIAL);
							return 0;
						}
					}
				}

				if (strcmp(node1->value.element.name,"Mie")==0)
				{
					//first look for a polydisperse material volume:
					char word[MAX_NAME_LENGTH];
					res=read_string_attribute22(node1,"SIGMA_D_UM",word,0);
					int polydisp=0;//flag of presence of polydispersity
					float sigma;//sigma of size distribution

					if (res==0)
					{
						polydisp=0;
					}
					else
					{
						if (parsing_is_numerical_value(word))	sscanf(word,"%f",&sigma);
						if (sigma<0) 
						{
							fprintf(stderr,"%s : SIGMA_D_UM<0!\n",node1->value.element.name);
							return 0;
						}
						if (sigma!=0) polydisp=1;
					}


/*
					if (res!=0)
						if (parsing_is_numerical_value(word))	sscanf(word,"%f",&sigma);
					if (sigma<0) 
					{
						fprintf(stderr,"%s : SIGMA_D_UM<0!\n",node1->value.element.name);
						return 0;
					}
					if (sigma!=0) polydisp=1;
*/
					if(polydisp)
					{
						//polydisperse case, create a collection of mv:	
						struct sx_mat_vol_collection * col;
						res=sx_mat_vol_collection_create_mie_mv_collection_from_xml(&col,scene,node1,TYPE_MIE);
						//sx_mat_vol_collection_print(col,stdout);
						//sx_scene_print(scene,stdout);
						if(res==0) return 0;
						scene->mv_collections[scene->nb_mv_collections++]=col;
					}
					else
					{
						//monodisperse case, found a scatterer of type Mie
						//create the mat_vol:
						struct sx_mat_vol* mv;
						sx_mat_vol_create(&mv,"");
						//read the attributes:
						res =sx_mat_vol_read_Mie_from_xml(mv,node1);
						if (res==0) return 0;
						scene->mv[scene->nb_mv++]=mv;
					}
				}

				if (
						(strcmp(node1->value.element.name,"material_surface")==0)||
						(strcmp(node1->value.element.name,"lambert")==0)||
						(strcmp(node1->value.element.name,"dielectric")==0)||
						(strcmp(node1->value.element.name,"mirror")==0)||
						(strcmp(node1->value.element.name,"emission")==0)||
						(strcmp(node1->value.element.name,"emission_nada")==0)||
						(strcmp(node1->value.element.name,"grating")==0)

				   )
				{
					//found a material for surface
					res =sx_mat_surf_create_from_xml(&scene->material_surface[scene->nb_material_surface],node1);
					if (res==0) return 0;
					scene->nb_material_surface++;
						if (scene->nb_material_surface==MAX_NB_MATERIAL) 
						{
							printf("Max nb of surface materials reached (%d)\n",MAX_NB_MATERIAL);
							return 0;
						}
				}
				if (strcmp(node1->value.element.name,"source")==0)
				{
					//found a source
					if (!sx_source_create_source_from_xml(&scene->sources[scene->nb_sources],node1,scene)) return 0;
					scene->nb_sources++;
					if (scene->nb_sources==MAX_NB_SOURCES) 
						{
						printf("Max nb of sources reached (%d)\n",MAX_NB_SOURCES);
						return 0;
						}
#ifdef PRINT_ALL_STEPS
					sx_source_print(stdout,scene->sources[scene->nb_sources-1]);
#endif
				}
				if (strcmp(node1->value.element.name ,"volume")==0)
				{
					//found a geometrical volume
					struct sx_volume* vol; 
					if (!sx_volume_create_from_xml( &vol,node1)) return 0;
					d_list_add(scene->volumes,vol);
				}
				if (strcmp(node1->value.element.name ,"openscad")==0)
				{
					//found a stl file generator
					if (!sx_cad_launch_openscad(scene,node1)) return 0;
				}
/*
				if (strcmp(node1->value.element.name ,"cylinder")==0)
				{
					//found a stl file generator
					if (!sx_cad_create_cyl_from_xml(scene,node1)) return 0;
				}
				if (strcmp(node1->value.element.name ,"disk")==0)
				{
					//found a stl file generator
					if (!sx_cad_create_disk_from_xml(scene,node1)) return 0;
				}
				if (strcmp(node1->value.element.name ,"box")==0)
					//found a stl file generator
				{
					if (!sx_cad_create_box_from_xml(scene,node1)) return 0;
				}
*/
			}
			else
			{
				break;
			}
			if (!top) top=node1;
		}
		if (found_a_scene==0)
		{
			printf("Can't find a Scene\n");
			return 0;
		}
	}// end of scanning xml tree
	else
	{
		printf("Can't find xml tree\n");
		return 0;
	}


	//check double names:
	for (int i=0;i<scene->nb_sources;i++)
		if (!sx_scene_find_source(scene,scene->sources[i]->name)) return 0;
	for (int i=0;i<scene->volumes->dim;i++)
		if (sx_scene_find_volume(scene,((struct sx_volume*)scene->volumes->data[i])->name)==NULL) return 0;
	for (int i=0;i<scene->nb_spectra;i++)
		if (!sx_scene_find_spectrum(scene,scene->spectra[i]->name)) return 0;
	for (int i=0;i<scene->surfaces->dim;i++)
		if (sx_scene_find_surface(scene,((struct sx_surface*)scene->surfaces->data[i])->name)==NULL) return 0;
	for (int i=0;i<scene->nb_material_surface;i++)
		if (!sx_scene_find_matsurf(scene,scene->material_surface[i]->name)) return 0;
	for (int i=0;i<scene->nb_mv;i++)
		if (!sx_scene_find_mv(scene,scene->mv[i]->name)) return 0;

	//fill the indices of the volumes:
	for (int i=0;i<scene->volumes->dim;i++)
	{
		struct sx_volume* vol=scene->volumes->data[i];
	 vol->id=i;
	}
	//set the objects as sons of the world volume:
	struct sx_volume* vol0=scene->volumes->data[0];
	vol0->nbSons=scene->volumes->dim-1;
	for (int i=1;i<scene->volumes->dim;i++)//don't process the world object
	{
		vol0->indexSons[i]=i;
	}
	//fill the surface indices of the volumes:
	for (int i=0;i<scene->volumes->dim;i++)
	{
		struct sx_volume* volume=scene->volumes->data[i];
		volume->nb_surfaces=0;
		char** list;
		int nb_words=0;
		parsing_parse_words(volume->surface_names,&list,&nb_words);
		for (int j=0;j<nb_words;j++)
		{
			struct sx_surface* s=sx_scene_find_surface(scene,list[j]);
			if (s!=NULL)
			{
				volume->surfaces_id[volume->nb_surfaces]=s->id;
				volume->nb_surfaces++;
				if (volume->nb_surfaces>MAX_NB_SURFACES_PER_VOLUME)
				{
					printf("Volume %s: max nb of surfaces reached (%d)\n",volume->name,MAX_NB_SURFACES_PER_VOLUME);
					return 0;
				}
			}
			else //can't find the surface
			{
				printf("Volume %s : can't find surface of name %s\n",
						volume->name,list[j]);
				return 0;
			}
		}
		//free the list of words:
		for (int ii=0;ii>nb_words;ii++) free(list[ii]);
		free(list);
	}
	//fill the object indices of the surfaces:
	for (int i=0;i<scene->volumes->dim;i++)
	{
		struct sx_volume* volume=scene->volumes->data[i];
		for (int j=0;j<volume->nb_surfaces;j++)
		{
			int surf_id=volume->surfaces_id[j];
			struct sx_surface* s=scene->surfaces->data[surf_id];
			sx_surface_add_volume(s,volume->id);
		}
	}

	//do the volumes connections
	for (int i=0;i<scene->volumes->dim;i++)
	{
		struct sx_volume* v=scene->volumes->data[i];
		//connect the spectra of the volumes:
		if (!sx_volume_connect_spectra(v,scene))return 0;
		//connect the material-volumes of the volumes:
		if (!sx_volume_connect_mv(v,scene))return 0;
	}


	//connect the spectra of the volume materials(sx_mat_vol):
	for (int i=0;i<scene->nb_mv;i++)
	{
		struct sx_mat_vol *mv=scene->mv[i];
		if (!sx_mat_vol_connect_spectra(mv,scene))return 0;
	}

	//connect the spectra of the volume materials collections(sx_mat_vol):
	for (int i=0;i<scene->nb_mv_collections;i++)
	{
		struct sx_mat_vol_collection *mvc=scene->mv_collections[i];
		if (!sx_mat_vol_collection_connect_spectra(mvc,scene))return 0;
	}


	//connect the spectra of the camera filters:
	for (int i=0;i<scene->nb_cameras;i++)
	{
		struct sx_camera *cam=scene->camera[i];
		if (!sx_camera_connect_spectra(cam,scene))return 0;
	}

	//connect the spectra of the sx_sensors filters:
	for (int i=0;i<scene->nb_sx_sensors;i++)
	{
		struct sx_sensor *sx_sensor=scene->sx_sensors[i];
		if (!sx_sensor_connect_spectra(sx_sensor,scene))return 0;
	}

	//connect the spectra and the starting volumes of the sources:
	for (int i=0;i<scene->nb_sources;i++)
	{

		//connect the spectrum
		struct sx_source* source=scene->sources[i];
		if (!sx_source_connect_spectra(source,scene))return 0;
		//look for the source starting volume:
		struct sx_volume* starting_volume=
			sx_scene_find_volume(scene,source->volume_name);
		if (starting_volume==NULL)
		{
			printf("Error looking for volume %s in source %s\n",
					source->volume_name,source->name);
			return 0;
		}
		else source->starting_volume=starting_volume;
	}


	//connect the starting volumes of the sx_sensors:
	for (int i=0;i<scene->nb_sx_sensors;i++)
	{
		struct sx_sensor* sensor=scene->sx_sensors[i];
		//look for the source starting volume:
		struct sx_volume* starting_volume=
			sx_scene_find_volume(scene,sensor->volume_name);
		if (starting_volume==NULL)
		{
			printf("Error looking for volume %s in sensor %s\n",
					sensor->volume_name,sensor->name);
			return 0;
		}
		else sensor->starting_volume=starting_volume;
	}
	//create the source sorter:
	sx_scene_create_source_sorter(scene);

	//connect the material of each surface
	for (int i=0;i<scene->surfaces->dim;i++)
	{
		struct sx_surface* s=scene->surfaces->data[i];
		if(!sx_surface_connect_materials(s,scene)) return 0;
	}
	//connect the spectra of the material surfaces:
	for (int i=0;i<scene->nb_material_surface;i++)
	{
		struct sx_mat_surf* ms=scene->material_surface[i];
		if(!sx_mat_surf_connect_spectra(ms,scene))return 0;
	}
	mxmlDelete(tree);
	return 1;
}

/**
 * check if a surface stores its
 */
int check_storing_impacts(struct sx_scene * scene)
{
	int b=0;
	for (int i=0;i<scene->surfaces->dim;i++)
	{
		struct sx_surface* s=scene->surfaces->data[i];
		if(s->store_impacts) b=1;
	}
	return b;
}

/**
print a scene description into a stream
*/
void sx_scene_print(
		struct sx_scene * scene,
		FILE* stream
		)
{
	//print the sources:
	fprintf(stream,"nbSources :%d\n", scene->nb_sources);
	for (int i=0;i<scene->nb_sources;i++)
	{
		fprintf(stream,"source %d:\n",i);
		struct sx_source* source=scene->sources[i];
		sx_source_print(source,stream);
	}
	//print the camera:
	if (scene->nb_cameras==1) sx_camera_print(scene->camera[0],stream);

	//print the sx_sensors:
	for (int i=0;i<scene->nb_sx_sensors;i++)
	{
		sx_sensor_print(scene->sx_sensors[i],stream);
	}

	//print the spectra:
	for (int i=0;i<scene->nb_spectra;i++)
	{
		fprintf(stream,"Spectrum %d :\n",i);
		sampled_data_print(scene->spectra[i],stream);
	}
	//print the surfaces:
	for (int i=0;i<scene->surfaces->dim;i++)
	{
		struct sx_surface* s=scene->surfaces->data[i];
		sx_surface_print(s,stream);
	}
	//print the materials of surfaces:
	for (int i=0;i<scene->nb_material_surface;i++)
	{
		sx_mat_surf_print(scene->material_surface[i],stream);
	}
	//print the scatterers:
	for (int i=0;i<scene->nb_mv;i++)
	{
		sx_mat_vol_print(scene->mv[i],stream,"");
	}
	//print the collections of materials of volume:
	for (int i=0;i<scene->nb_mv_collections;i++)
	{
		sx_mat_vol_collection_print(scene->mv_collections[i],stream);
	}
	//print the volumes:
	for (int i=0;i<scene->volumes->dim;i++)
	{
		sx_volume_print(scene->volumes->data[i],stream);
	}
}

/*
void sx_scene_print(
		FILE* stream,
		struct sx_scene * scene
)
{
	fprintf(stream,"Scene\n");
}
*/


int sx_scene_create_scene_spectrum(struct sx_scene* scene)
{
	if(scene->algo==ALGO_DIRECT)
	{
		//merge the spectra of all the sources to create the simulation spectrum:
		sampled_data_create3(&scene->spectrum,0);
		for (int i=0;i<scene->nb_sources;i++)
		{
			sampled_data_add_merge(scene->spectrum,scene->sources[i]->spectrum);
		}
	}
	return 1;
}



/**
check the wavelength of the path_sources spectra used in the scene: 
must be integer (in nm) >0 and <1000
*/
int sx_scene_check_spectra(struct sx_scene* scene)
{
	for (unsigned int k=0;k<scene->path_sources->dim;k++) 
	{
		struct sx_path_source* ps=scene->path_sources->data[k];
		struct sampled_data* sp=ps->spectrum;
		float lambda_min=sp->x[0];
		float lambda_max=sp->x[sp->nb_vals-1];
		if (lambda_min<0) 
		{
			fprintf(stderr,"%s : ",ps->name);
			fprintf(stderr,"the source spectra must have their wavelength > 0 nm\n");
			return 0;
		}
		if (lambda_max>=1000) 
		{
			fprintf(stderr,"%s : ",ps->name);
			fprintf(stderr,"the source spectra must have their wavelength < 1000 nm\n");
			return 0;
		}
		for (int i=0;i<sp->nb_vals;i++)
		{
			if (((int)sp->x[i])!=sp->x[i])
			{
				fprintf(stderr,"%s : ",ps->name);
				fprintf(stderr,"the wavelength values must be integers (in nm): %f\n",sp->x[i]);
				return 0;
			}
		}
	}
}



int sx_scene_create_sx_path_sources(struct sx_scene* scene)
{

	if(scene->algo!=ALGO_DIRECT)//reverse path
	{
		if ((scene->nb_cameras==0)&&(scene->nb_sx_sensors==0))
		{
			printf("At least one camera or one sx_sensor is needed for reverse path mode\n");
			return 0;
		}

		//create sx_path_sources from the camera:
		for (int i=0;i<scene->nb_cameras;i++)
		{
			struct sx_camera* camera=scene->camera[i];
			int res = sx_camera_create_sx_path_sources(camera,scene);
			if(!res)
			{
				if(scene->view) S3D(scene_view_ref_put(scene->view));
				return 0;
			}
		}
		//create sx_path_sources from the sx_sensors
		for (int i=0;i<scene->nb_sx_sensors;i++)
		{
			struct sx_sensor* sensor=scene->sx_sensors[i];
			sx_sensor_create_sx_path_sources(sensor,scene);
		}

	}
	else //direct path
	{
		//collect spot sources:
		struct sx_source *sources[MAX_NB_SOURCES];
		int nb_sources_spot=0;
		for (int i=0;i<scene->nb_sources;i++)
		{
			if (scene->sources[i]->type!=SOURCE_TYPE_SUN) 
				sources[nb_sources_spot++]=scene->sources[i];
		}
		//create one sx_path_source for each spot and for each wavelength of the simulation:
		// allocate memory for the sx_path_sources:
		//set the sx_path_sources of type spot:
		for (int i=0;i<nb_sources_spot;i++)
		{
			struct sx_path_source* ps;
			sx_source_create_sx_path_source(sources[i],&ps,scene);
			d_list_add(scene->path_sources,ps);
		}
		if (nb_sources_spot==0)
		{
			printf("At least one spot is needed for direct path mode\n");
			return 0;
		}
	}
	return 1;
}






/**
 * read the scene described in a xml file
 * load the surfaces in star-engine/embree
 */
int sx_scene_import_data (struct sx_scene* scene,char* filename)
{
	int res = 1;
	mxml_node_t *tree;
	res=sx_xml_read_xml(&tree,filename);
	if (!res) return 0;
	res=sx_scene_read_from_xml(scene,tree);
	if (!res) return 0;

#ifdef PRINT_ALL_STEPS
	sx_scene_print(scene);
#endif

	//CAMERA_DIRECT:
	// if the algo is direct and there are cameras, 
	//create and store to disk the surfaces (disks) corresponding to the lenses:
	if(scene->algo==ALGO_DIRECT)
	{
		for (int i=0;i<scene->nb_cameras;i++)
		{
			//create the aperture surface object:
			struct sx_surface* as;
			sx_camera_create_aperture_surface(scene->camera[i],scene,&as);
			//add the surface to the list of surfaces of the scene:
			d_list_add(scene->surfaces,as);
			as->id=scene->surfaces->dim-1;
		}
	}

	const int verbosity = 0; /* Control the verbosity of Star-3D */
	res = s3d_device_create(NULL,NULL,verbosity,&scene->s3d_device);
	if(res != RES_OK) goto error;
	res = s3d_scene_create(scene->s3d_device,&scene->s3d_scene);
	if(res != RES_OK) goto error;
	//import OBJ and/or STL files of the sx_surfaces:
	int res1=0;
	for (int i=0;i<scene->surfaces->dim;i++)
	{
		struct sx_surface* surf=scene->surfaces->data[i];
		int length = strlen(surf->file);
		char extension[10];
		parsing_substring(extension,surf->file,length-4,length);
		//printf("%s\n",extension);
		if (parsing_compare_strings(extension,".stl")==0)
		{
			//printf("%s is an STL file\n",surf->file);
			res1=sx_scene_import_stl(surf->file,scene->s3d_scene,scene->s3d_device);
		}
		if (parsing_compare_strings(extension,".obj")==0)
		{
			//printf("%s is an OBJ file\n",surf->file);
			res1=sx_scene_import_obj(surf->file,scene->s3d_scene,scene->s3d_device);
		}
	}


	if(!res1)
	{
		fprintf(stderr, "Couldn't import objects\n");
		goto error;
	}
	res = s3d_scene_view_create(scene->s3d_scene, S3D_TRACE, &scene->view);
	if(res != RES_OK)
	{
		goto error;
	}
	//exit:
	if(scene->s3d_device) S3D(device_ref_put(scene->s3d_device));
	return 1;
error:
	if(scene->view) S3D(scene_view_ref_put(scene->view));
	if(scene->s3d_device) S3D(device_ref_put(scene->s3d_device));
	return 0;
}


/**
 * import shapes from an OBJ format file:
 */
int sx_scene_import_obj(const char* filename,
		struct s3d_scene* out_scene,
		struct s3d_device* s3d_device)
{
	struct s3daw* s3daw = NULL;
	const int S3D_VERBOSE = 0;
	size_t ishape, nshapes;
	int res = 1;
	ASSERT(out_scene);

	// objfile ----  s3daw ------ s3d_device ----- s3d_scene -------- shape
	//                                                       |
	//                                                       -------  shape

	//create the aw s3daw of type s3daw associated with s3d
	s3daw_create(NULL, NULL, NULL, NULL, s3d_device, S3D_VERBOSE, &s3daw);
	//load the geometry from the file (type *.obj)
	s3daw_load(s3daw, filename);
	//get the nb of shapes:
	s3daw_get_shapes_count(s3daw, &nshapes);
	if (nshapes>1)
	{
		printf("%s has more than one shape\n",filename);
		if(s3daw) S3DAW(ref_put(s3daw));/* Release memory */
		return 0;
	}
	FOR_EACH(ishape, 0, nshapes)
	{
		struct s3d_shape* shape;
		s3daw_get_shape(s3daw, ishape, &shape);
		s3d_mesh_set_hit_filter_function(shape, hit_filter, NULL);
		s3d_scene_attach_shape(out_scene, shape);
	}

	////test of clones:
	////create the final scene :
	//struct s3d_scene* finalscene = NULL;
	//CALL(s3d_scene_create(s3d, &finalscene));
	//for (int i=-2;i<=2;i++)for (int j=-2;j<=2;j++)
	//	{
	////		// clone of the shape:
	//		struct s3d_shape* shape_clone=NULL;
	//		//create a scene for the clone:
	//		CALL(s3d_scene_instantiate (scene, &shape_clone));
	////		//prepare the translation vector:
	//		float  trans[3];
	//		trans[0]=(i*12);
	//		trans[1]=(j*12);
	//		trans[2]=0;
	////		//translate shape_clone:
	//		CALL(s3d_instance_translate(shape_clone, S3D_WORLD_TRANSFORM,trans));
	//		//attach the clone to the final scene:
	//		CALL(s3d_scene_attach_shape(finalscene, shape_clone));
	//	}

	/* Release memory */
	if(s3daw) S3DAW(ref_put(s3daw));
	return res;
}


/**
 * import a shape from an STL format file:
 */
int sx_scene_import_stl(const char* filename,
		struct s3d_scene* out_scene,
		struct s3d_device* s3d)
{
	struct s3dstl* s3dstl = NULL;
	struct s3d_shape* shape = NULL;
	const int S3D_VERBOSE = 1;
	int res = 1;

	/* Create a Star-3D shape from and STL geometry */
	res = s3dstl_create(NULL, NULL, NULL, s3d, S3D_VERBOSE, &s3dstl);
	if(res != RES_OK) goto error;
	res = s3dstl_load(s3dstl, filename);
	if(res != RES_OK) goto error;
	res = s3dstl_get_shape(s3dstl, &shape);
	if(res != RES_OK) goto error;
	//add the filter:
	s3d_mesh_set_hit_filter_function(shape, hit_filter, NULL);

	/* Create a Star-3D scene containing the STL geometry */
	res = s3d_scene_attach_shape(out_scene, shape);
	if(res != RES_OK) goto error;

exit:
	/* Clean-up temporary data */
	if(s3dstl) S3DSTL(ref_put(s3dstl));
	return 1;

error:
	res=0;
	goto exit;
}






void sx_scene_release(struct sx_scene* scene)
{
	//if (scene->verbose) printf("Release sx_scene...\n");
	if (scene->s3d_scene != NULL) S3D(scene_ref_put(scene->s3d_scene));
	if (scene->view != NULL) S3D(scene_view_ref_put(scene->view));
	//release sx_path_sources:
	for (long unsigned int i=0;i<scene->path_sources->dim;i++)
	{
		sx_path_source_release(scene->path_sources->data[i]);
	}
	d_list_release(scene->path_sources);
	//release cameras:
	for (int i=0;i<scene->nb_cameras;i++)
	{
		sx_camera_release(scene->camera[i]);
	}
	//release sources:
	for (int i=0;i<scene->nb_sources;i++)
	{
		sx_source_release(scene->sources[i]);
	}
	//release the volumes:
	for (int i=0;i<scene->volumes->dim;i++)
	{
		sx_volume_release(scene->volumes->data[i]);
	}
	//release the volume materials:
	for (int i=0;i<scene->nb_mv;i++)
	{
		sx_mat_vol_release(scene->mv[i]);
	}
	//release the surfaces:
	for (int i=0;i<scene->surfaces->dim;i++)
	{
		sx_surface_release(scene->surfaces->data[i]);
	}
	//release the spectra:
	for (int i=0;i<scene->nb_spectra;i++)
	{
		sampled_data_release(scene->spectra[i]);
	}
	//release the material surfaces:
	for (int i=0;i<scene->nb_material_surface;i++)
	{
		sx_mat_surf_release(scene->material_surface[i]);
	}
	//release the eventual stored photons:
	if (scene->stored_photons!=NULL)
	{
		for (int i=0;i<scene->stored_photons->dim;i++)
		{
			struct sx_photon* p=scene->stored_photons->data[i];
			sx_photon_release(p);
		}
		free(scene->stored_photons);
	}
	//release the source sorter:
	if (scene->source_sorter!=NULL)
	{
		free(scene->source_sorter);
	}

}



/**
 * save the images png and optionally in raw format
 */
void sx_scene_save_images(struct sx_scene* scene,int save_raw)
{
	//CAMERA_DIRECT:
	int index_in_weight=0;//used for direct path
	if(scene->algo==ALGO_DIRECT)
		index_in_weight=scene->surfaces->dim*scene->spectrum->nb_vals;
	for (int i=0;i<scene->nb_cameras;i++)
	{
		struct sx_camera* camera=scene->camera[i];
		struct sx_image* image;
		struct sx_image* image_sigma;
		//calc image of weights
		if(scene->algo==ALGO_DIRECT)
		{
			//calc the index of the weights corresponding to this camera:
			for (int j=0;j<camera->nbPixelsX*camera->nbPixelsY;j++)
			{
				index_in_weight+=scene->spectrum->nb_vals;
			}
			//TODO only one path source ???
			struct sx_path_source* path_source=scene->path_sources->data[0];
			sx_camera_calc_image_algo_direct(&image,path_source,
					index_in_weight,camera,0);
		}
		else 
		{
			sx_camera_calc_image_algo_reverse(&image,scene->path_sources,camera,0);
		}
		//printf("Multispectral image: (%dx%d) %d channels\n",sx_image->w,sx_image->h,sx_image->nbc);
		if (save_raw)
		{
			//save image of weights:
			char raw_filename[3*MAX_NAME_LENGTH];
			sprintf(raw_filename,"%s%s.raw",scene->file_path,camera->name);
			sx_image_save(image,raw_filename);
			//calc image of sigmas:
			if(scene->algo==ALGO_DIRECT)
			{
				//calc the index of the weights corresponding to this camera:
				for (int j=0;j<camera->nbPixelsX*camera->nbPixelsY;j++)
				{
					index_in_weight+=scene->spectrum->nb_vals;
				}
				//TODO only one path source ???
				struct sx_path_source* path_source=scene->path_sources->data[0];
				sx_camera_calc_image_algo_direct(&image_sigma,path_source,
						index_in_weight,camera,1);
			}
			else 
			{
				sx_camera_calc_image_algo_reverse(&image_sigma,
						scene->path_sources,camera,1);
			}
			//save image of sigmas:
			char raw_sigma_filename[3*MAX_NAME_LENGTH];
			sprintf(raw_sigma_filename,"%s%s_sigma.raw",
					scene->file_path,camera->name);
			sx_image_save(image_sigma,raw_sigma_filename);
			sx_image_release(image_sigma);
		}
		//save image in png format
		sx_image_save_png(image,camera->gamma,camera->max,scene->file_path,camera->name);
		sx_image_release(image);
		//if (scene->verbose) printf("Image of %s saved\n",cam->name);
	}
}

/**
 * export all data (images, paths, paths ends)
 */
void sx_scene_export_data(struct sx_scene* scene)
{
	//delete the output  files:
	char full_file_name[2*MAX_NAME_LENGTH];
	{
		strcpy(full_file_name,scene->file_path);
		strcat(full_file_name,PATHS_OBJ_FILE);
		remove(full_file_name);
	}
	{
		strcpy(full_file_name,scene->file_path);
		strcat(full_file_name,PATHS_ERROR_OBJ_FILE);
		remove(full_file_name);
	}
	{
		strcpy(full_file_name,scene->file_path);
		strcat(full_file_name,PATHS_FILE);
		remove(full_file_name);
	}
	{
		strcpy(full_file_name,scene->file_path);
		strcat(full_file_name,SENSORS_RESULT_FILE);
		//printf("remove %s\n",full_file_name);
		remove(full_file_name);
	}
	if (scene->store_trajectories)
	{
		sx_scene_store_photons_in_obj_file(scene,
				scene->file_path,"paths.obj",0);
		sx_scene_store_paths(scene,
				scene->file_path,"paths.csv");
	}
	if (scene->store_error_trajectories)
	{
		sx_scene_store_photons_in_obj_file(scene,
				scene->file_path,"paths_error.obj",1);
	}
	//when reverse path save the camera's sx_path_source weights and
	// sigmas in an HDR image and a png image:
	if(scene->algo!=ALGO_DIRECT)
	{
		//reverse algorithm
		//when more than 1 camera save weights as image of radiances:
		sx_scene_save_images(scene,1);
		if (scene->nb_sx_sensors>0)
		{
			//save weights and sigmas of the sx_path_sources not belonging to a camera in a table
			strcpy(full_file_name,scene->file_path);
			strcat(full_file_name,SENSORS_RESULT_FILE);
			if (scene->verbose) printf("Save %s\n",full_file_name);
			FILE* file1 = fopen(full_file_name, "w");
			//print info line
			fprintf(file1,"reverse_path\n");
			for (int i=0;i<scene->nb_sx_sensors;i++)
			{
				struct sx_sensor* sensor=scene->sx_sensors[i];
				int index=sensor->index_in_path_sources;
				//get the sensor aperture angle
				float alpha=sensor->angle/2.0f*(float)PI/180.0f;
				//line with the wavelengths :
				fprintf(file1,"%s_channels:\t",sensor->name);
				for (int k=0;k<sensor->nb_filters;k++) 
					fprintf(file1,"%s\t",sensor->filters[k]->name);
				fprintf(file1,"\n");
				//check if the sensor has null area or null aperture:
				int useRadiance=(sensor->area==0)||(alpha==0);
				//line with the power received by the sensor :
				if (useRadiance) fprintf(file1,"%s_weight_radiance\t",sensor->name);
				else fprintf(file1,"%s_weight_power\t",sensor->name);
				for (int k=0;k<sensor->nb_filters;k++) 
				{
					struct sx_path_source* ps=scene->path_sources->data[index+i+k];
					float radiance=(float)ps->weight[0];
					float power=radiance*sensor->area*(float)PI*powf(sinf(alpha),2.0f);
					if (useRadiance) fprintf(file1,"%g\t",radiance);
					else fprintf(file1,"%g\t",power);
				}
				fprintf(file1,"\n");
				//line with the sigmas:
				if (useRadiance) fprintf(file1,"%s_sigma_radiance\t",sensor->name);
				else fprintf(file1,"%s_sigma\t",sensor->name);
				for (int k=0;k<sensor->nb_filters;k++) 
				{
					struct sx_path_source* ps=scene->path_sources->data[index+i+k];
					float radiance_sigma=(float)ps->sigma[0];
					float power_sigma=radiance_sigma*sensor->area*(float)PI*powf(sinf(alpha),2.0f);
					if (useRadiance) fprintf(file1,"%g\t",radiance_sigma);
					else fprintf(file1,"%g\t",power_sigma);
				}
				fprintf(file1,"\n");
			}
			fclose(file1);
		}
	}

	if(scene->algo==ALGO_DIRECT)
	{
//DEBUG print the weights with there surfaces, sides and lambda
/*
		printf("%d lambdas \n",scene->spectrum->nb_vals);
		printf("%d surfaces \n",scene->surfaces->dim);
		for (int k=0;k<scene->path_sources->dim;k++) 
		{
			struct sx_path_source* ps=scene->path_sources->data[k];
		printf("Weight dim = %d \n",ps->weight_dim);
				printf("path source: %s\n",ps->name);
			for (int i=0;i<ps->weight_dim;i++)
			{
				int i_side=i/scene->spectrum->nb_vals;
				int i_surf=i/scene->spectrum->nb_vals/2;
				int i_lambda=i%scene->spectrum->nb_vals;
				printf("w[%d]=%f  i_side=%d i_surf=%d i_lambda=%d \n",i,ps->weight[i],i_side,i_surf,i_lambda);
			}
		}
*/
		strcpy(full_file_name,scene->file_path);
		strcat(full_file_name,SENSORS_RESULT_FILE);
		if (scene->verbose) printf("Save %s\n",full_file_name);
		FILE* file1 = fopen(full_file_name, "w");
		fprintf(file1,"lambda(nm):\t");
		for (int i=0;i<scene->spectrum->nb_vals;i++) 
			fprintf(file1,"%g\t",scene->spectrum->x[i]);
		fprintf(file1,"\n");
		//calculate the weights
		for (int i=0;i<scene->surfaces->dim;i++)
		{
			struct sx_surface* s1=scene->surfaces->data[i];
	//			for (int ii=1;ii>=0;ii--)//scan the 2 sides of the surface, 1: front 0 : back
			int ii=1;//put only the front sensor signal
			//put only the sensor's surfaces:
			if (s1->sensor!=NULL)
				{
					//put weight (power)
					if (ii) fprintf(file1,"%s_front_weight\t",s1->name);
					else fprintf(file1,"%s_back_weight\t",s1->name);
					for ( int j=0;j<scene->spectrum->nb_vals;j++)
					{
						unsigned int index_in_weight=i*scene->spectrum->nb_vals*2+ii*scene->spectrum->nb_vals+j;
						double w=0;
						for (int k=0;k<scene->path_sources->dim;k++) 
						{
							struct sx_path_source* ps=scene->path_sources->data[k];
							w+=ps->weight[index_in_weight];
						}
						fprintf(file1,"%g\t",w);
					}
					fprintf(file1,"\n");
					//put sigma
					if (ii) fprintf(file1,"%s_front_sigma\t",s1->name);
					else fprintf(file1,"%s_back_sigma\t",s1->name);
					for ( int j=0;j<scene->spectrum->nb_vals;j++)
					{
						unsigned int index_in_weight=i*scene->spectrum->nb_vals*2+ii*scene->spectrum->nb_vals+j;
						double sig2=0;
						for (unsigned int k=0;k<scene->path_sources->dim;k++) 
						{
							struct sx_path_source* ps=scene->path_sources->data[k];
							sig2+=pow(ps->sigma[index_in_weight],2);
						}
						fprintf(file1,"%g\t",sqrt(sig2));
					}
				fprintf(file1,"\n");
			}
		}
		fclose(file1);
		//when more than 1 camera save weights as images:
		sx_scene_save_images(scene,1);

	}



	//BLURRING_ACCELERATION
	//export the lists of impacts of the paths for each surface asked to do:
	for (int i=0;i<scene->surfaces->dim;i++)
	{
		struct sx_surface* surface=scene->surfaces->data[i];
		if (surface->store_impacts==1)
		{
			//sx_surface_export_impacts(surface,scene->file_path);
			sx_surface_export_impacts2(surface,scene);
		}
	}

}



//build the source sorter:
//TODO to be redesigned when will use a more complex set of sources (with any surface)
int sx_scene_create_source_sorter(struct sx_scene* scene)
{
	double w[scene->nb_sources];//weights
	for (int i=0;i<scene->nb_sources;i++)
	{
		//sort all the sources with equal proba to avoid noise,
		// it is compensated by source power in the MC algorithm
		w[i]=1;//scene->sources[i]->power;
	}
	//build the random generator fo source index:
	ssp_ranst_discrete_create(NULL,&scene->source_sorter);
	ssp_ranst_discrete_setup(scene->source_sorter,w,(unsigned int)scene->nb_sources);
	return 1;
}


/**
 * sort a source from the ensemble of sources 
 */
int sx_scene_sort_a_source(struct sx_scene* scene,
		struct ssp_rng* rng,struct sx_source** source)
{
	int i=(int)ssp_ranst_discrete_get(rng,scene->source_sorter);
	*source=scene->sources[i];
	return 1;
}


/**
 * store the trajectory of the stored photons in an obj file
 * see https://en.wikipedia.org/wiki/Wavefront_.obj_file
 */
void sx_scene_store_photons_in_obj_file(
		struct sx_scene* scene,
		char *file_path,
		char *filename,
		int only_error_photons //if 1 save only error paths
		)
{
	char full_file_name[MAX_NAME_LENGTH];
	strcpy(full_file_name,file_path);
	strcat(full_file_name,filename);
	FILE* file=fopen(full_file_name,"w");
	int nb_paths=0;
	//put the vertices:
	for (unsigned int i=0;i<scene->stored_photons->dim;i++)
	{
		struct sx_photon* photon=scene->stored_photons->data[i];
		if (only_error_photons) 
			if (!sx_photon_is_error(photon))
				continue;
		nb_paths++;
		struct d_list* h=photon->history;
		for (unsigned int j=0;j<h->dim;j++)
		{
			struct sx_interaction* in=h->data[j]; 
			float* pos=in->pos;
			fprintf(file,"v %f %f %f\n",pos[0],pos[1],pos[2]);
		}
	}
	printf("Store %d paths in file %s\n",nb_paths,full_file_name);

	//put the lines made with the vertices index
	int c=1;
	for (unsigned int i=0;i<scene->stored_photons->dim;i++)
	{
		struct sx_photon* photon=scene->stored_photons->data[i];
		if (only_error_photons) 
			if (!sx_photon_is_error(photon))
				continue;
		struct d_list* h=photon->history;
		if (h->dim>0) fprintf(file,"l ");
		for (unsigned int j=0;j<h->dim;j++)
		{
			fprintf(file,"%d ",c);
			c++;
		}
		fprintf(file,"\n");
	}
	fclose(file);

	//printf("The paths contains %d vertices (%d bytes)\n",nbVertices,nbVertices*sizeof(float)*3);
}


/**
 * store the trajectory of the stored photons in an obj file
 * see https://en.wikipedia.org/wiki/Wavefront_.obj_file
 */
void sx_scene_store_paths(
		struct sx_scene* scene,
		char *file_path,
		char *filename
		)
{
	char full_file_name[MAX_NAME_LENGTH];
	strcpy(full_file_name,file_path);
	strcat(full_file_name,filename);
	FILE* file=fopen(full_file_name,"w");
	int nb_path=0;
	//store the full path history
	fprintf(file,"path_nb\tlambda_nm\tinteraction_type\tpos_x\tpos_y\tpos_z\tdir_x\tdir_y\tdir_z\tdistance\tsurface_name\tvolume_name\tweight\n");
	//for the case of no surface or volume at interaction:
	char s_null[5];
	strcpy(s_null,"null");
	//intercation type
	char it[200];
	for (unsigned int i=0;i<scene->stored_photons->dim;i++)
	{
		struct sx_photon* photon=scene->stored_photons->data[i];
		struct d_list* h=photon->history;
		for (unsigned int j=0;j<h->dim;j++)
		{
			struct sx_interaction* in=h->data[j]; 
			char* sn;//surface name
			if (in->s!=NULL) sn=in->s->name;
			else sn=s_null;
			char* vn;//volume name
			if (in->v!=NULL) vn=in->v->name;
			else vn=s_null;
			sx_photon_get_flag(it,in->type);
			fprintf(file,"%d\t%f\t%s\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%s\t%s\t%f\n",
					nb_path,photon->lambda,it,
					in->pos[0],in->pos[1],in->pos[2],
					in->dir[0],in->dir[1],in->dir[2],
					in->distance,
					sn,vn,in->w
				   );
		}
		nb_path++;
	}
	printf("Store %d paths in file %s\n",nb_path,full_file_name);

	fclose(file);

	//printf("The paths contains %d vertices (%d bytes)\n",nbVertices,nbVertices*sizeof(float)*3);
}





#endif /* SX_SCENE_C_ */
