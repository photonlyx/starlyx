/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SX_SCENE_H_
#define SX_SCENE_H_

#include <stddef.h>

#include <mxml.h>
#include <star/ssp.h>

#include "sx_xml.h"

#define MAX_NB_SOURCES 1000
#define MAX_NB_SPECTRA 1000
#define MAX_NB_MATERIAL 1000
#define MAX_NB_CAMERAS 100
#define MAX_NB_PROBES 5000000
#define MAX_NB_PROBE_SETS 1000

#define SENSORS_RESULT_FILE "sensors.csv"
#define PATHS_FILE "paths.obj"
#define PATHS_ERROR_OBJ_FILE "paths_error.obj"
#define PATHS_OBJ_FILE "paths.csv"

/*
ALGO_DIRECT: radiant intensity I is calculated starting from the sources 
reverse algorithms calculate the radiant intensity starting from probe up to sources
with different options:
ALGO_REVERSE_0: no source contribution is added to I (the weight of MC) at each scattering 
ALGO_REVERSE: the source radiance are added at each scattering after one step from source
 */
enum {ALGO_DIRECT,ALGO_REVERSE};

struct s3d_scene;
struct s3d_device;

struct sx_sampled_data;
struct sx_camera;
struct sx_surface;
struct sx_source_sun;
struct sx_source_laser;
struct sx_mat_vol;


struct sx_scene
{
	char systemFileName[MAX_NAME_LENGTH];//file name of xml scene description
	char file_path[MAX_NAME_LENGTH];//path to xml scene description
	int store_trajectories;
	int store_error_trajectories;
	int verbose;//if 1 printout things during execution
	int max_path_length;//paths are stopped when reaching this value to avoid infinite loops
	int max_nb_scattering;//paths are stopped when reaching this value 
	int max_nb_events;//paths are stopped when reaching this value 
	long unsigned int nb_photons;

	int nb_threads;

	//algorithm used to calculate radiant intensity:
	int algo;//ALGO_DIRECT,ALGO_REVERSE

	//sx_path_sources:
	//the current path source used in the MC calculation
	unsigned long sx_path_source_index;
	struct d_list* path_sources;

	//cameras:
	struct sx_camera *camera[MAX_NB_CAMERAS];
	int nb_cameras;

	//sx_sensors:
	struct sx_sensor* sx_sensors[MAX_NB_PROBE_SETS];
	int nb_sx_sensors;

	char objFileName1[MAX_NAME_LENGTH];
	char objFileName2[MAX_NAME_LENGTH];

	//sources or light:
	struct sx_source *sources[MAX_NB_SOURCES];
	int nb_sources;

	//source sorter:
	struct ssp_ranst_discrete* source_sorter;

	//volumes:
	struct d_list* volumes;

	//surfaces:
	struct d_list* surfaces;

	//spectra:
	struct sampled_data* spectra[MAX_NB_SPECTRA];
	int nb_spectra;

	//materials of volume: scatterer henyey greenstein or Mie:
	struct sx_mat_vol* mv[MAX_NB_MATERIAL];
	int nb_mv;
	//material of volume collections: 
	struct sx_mat_vol_collection* mv_collections[MAX_NB_MATERIAL];
	int nb_mv_collections;

	//materials of surfaces:
	struct sx_mat_surf* material_surface[MAX_NB_MATERIAL];
	int nb_material_surface;

	//list of paths (for debug/viewing)
	struct sx_paths* paths;
	//list of error paths (for debug)
	struct sx_paths* paths_error;

	//s3d objects:
	struct s3d_scene* s3d_scene;
	struct s3d_scene_view* view;
	struct s3d_device* s3d_device ;

	//scene spectrum. Used for direct path. 
	//Concatenation of all the sources spectra.
	struct sampled_data* spectrum;

	//list of photons launched into the scene (if storage asked):
	struct d_list* stored_photons;

	//<SYMBOLIC
	//used for symbolic calculation:
	int symbolic;
	float k_hat;
	float p_scat;
	float p_abs;
	struct float_list_symbolic* path_symbolic_events;
	//SYMBOLIC>

};

int  sx_scene_create(
		struct sx_scene** scene_
		);

struct sx_source* sx_scene_find_source(
		const  struct sx_scene* scene,
		const char* name
		);

int sx_scene_read_from_xml(
		struct sx_scene* scene,
		mxml_node_t *tree
		);

int check_storing_impacts(
		struct sx_scene * scene
		);

void sx_scene_print(
		struct sx_scene * sx_scene,
		FILE* stream
		);

void sx_scene_release(
		struct sx_scene* sx_scene
		);

int sx_scene_import_data(
		struct sx_scene* scene,
		char* filename
		);

int sx_scene_import_obj(
		const char* filename, 
		struct s3d_scene* out_scene,
		struct s3d_device* s3d_device);

int sx_scene_import_stl(
		const char* filename,
		struct s3d_scene* out_scene,
		struct s3d_device* s3d);

struct sx_volume* sx_scene_find_volume(
		const struct sx_scene* scene,
		const char* name);

struct sx_mat_vol* sx_scene_find_mv(
		const struct sx_scene* scene,
		const char* name);

struct sx_mat_vol_collection* sx_scene_find_mv_collection(
		const  struct sx_scene* scene,
		const char* name
		);

struct sx_surface* sx_scene_find_surface(
		const struct sx_scene* scene,
		const char* name);

struct sx_mat_surf* sx_scene_find_matsurf(
		const  struct sx_scene* scene,
		const char* name);

struct sx_mat_vol* sx_scene_find_mat_vol(
		const struct sx_scene* scene,
		const char* name);

struct sampled_data*  sx_scene_find_spectrum(
		const struct sx_scene* sx_scene,
		const char* name);

struct sampled_data*  sx_scene_find_spectrum2(
		const struct sx_scene* sx_scene,
		const char* name,
		int *is_numerical);

void sx_scene_save_images(
		struct sx_scene* sx_scene,
		int save_raw);

void sx_scene_export_data(
		struct sx_scene* sx_scene
		);

int sx_scene_create_source_sorter(
		struct sx_scene* scene
		);

int sx_scene_sort_a_source(
		struct sx_scene* scene,
		struct ssp_rng* rng,
		struct sx_source** source);

int sx_scene_create_scene_spectrum(
		struct sx_scene* scene
		);

int sx_scene_check_spectra(
		struct sx_scene* scene
		);

int sx_scene_create_sx_path_sources(
		struct sx_scene* scene
		);

void sx_scene_store_photons_in_obj_file(
		struct sx_scene* scene,
		char *file_path,
		char *filename,
		int only_error_photons
		);

void sx_scene_store_paths(
		struct sx_scene* scene,
		char *file_path,
		char *filename
		);
#endif /* SX_SCENE_H_ */
