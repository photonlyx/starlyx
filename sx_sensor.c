/*
   Copyright (C) 2022 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>

#include <star/ssp.h>
#include <star/s3daw.h>
#include <star/s3d.h>
#include <star/s3dstl.h>

#include "sx_xml.h"
#include "sx_path_source.h"
#include "sx_sensor.h"
#include "sx_configuration.h"
#include "sx_surface.h"
#include "sx_mat_surf.h"
#include "sx_cad.h"
#include "sampled_data.h"
#include "sx_ssp.h"

void sx_sensor_print(
		struct sx_sensor* sensor,
		FILE* stream
		)
{
	fprintf(stream,"Sensor\n");
	switch(sensor->type)
	{
		case SENSOR_TYPE_DISK:
			fprintf(stream,"Type DISK \n");
			break;
		case SENSOR_TYPE_SURFACE:
			fprintf(stream,"Type SURFACE \n");
			break;
		default:
			break;
	}
	fprintf(stream,"  name=%s\n",sensor->name);
	switch(sensor->type)
	{
		case SENSOR_TYPE_DISK:
			fprintf(stream,"  diameter=%f mm\n",sensor->diameter);
			fprintf(stream,"  area=%g m2\n",sensor->area);
			fprintf(stream,"  position=(%f,%f,%f)\n",sensor->pos[0],sensor->pos[1],sensor->pos[2]);
			fprintf(stream,"  direction=(%f,%f,%f)\n",sensor->dir[0],sensor->dir[1],sensor->dir[2]);
			break;
		case SENSOR_TYPE_SURFACE:
			fprintf(stream,"  area=%g m2\n",sensor->area);
			break;
	}	
	fprintf(stream,"  angle=%f deg\n",sensor->angle);
	fprintf(stream,"  starting volume=%s\n",sensor->volume_name);
	//sampled_data_print(sx_path_source->spectrum,stdout);
}


void sx_sensor_create(
		struct sx_sensor** sensor_
		)
{
	(*sensor_)=malloc (sizeof(struct sx_sensor));
	(*sensor_)->type=SENSOR_TYPE_DISK;
	(*sensor_)->diameter=0;
	(*sensor_)->area=0;
	(*sensor_)->angle=0;
	(*sensor_)->nb_filters=0;
	(*sensor_)->index_in_path_sources=0;
	(*sensor_)->surface=NULL;
	(*sensor_)->s3d_scene=NULL;
	(*sensor_)->scnview=NULL;
	strcpy((*sensor_)->surface_filename,"");
}





int sx_sensor_create_from_xml(
		struct sx_sensor** sensor_,
		mxml_node_t *node,
		struct sx_scene* scene
		)
{
	sx_sensor_create(sensor_);
	struct sx_sensor* sensor=(*sensor_);
	char buffer[MAX_NAME_LENGTH];
	//get the sensor name
	if (!read_string_attribute2(node,"NAME",buffer))return 0;
	strcpy(sensor->name,buffer);
	//get the volume where is the sensor ("WORLD" by default)
	if (read_string_attribute2(node,"VOLUME",buffer))
	{
		strcpy(sensor->volume_name,buffer);
	}
	else
	{
		strcpy(sensor->volume_name,"World");
		printf("Set to default (%s)\n",sensor->volume_name);
	}

	//get the filters list:
	if (!read_string_attribute2(node,"FILTERS",buffer))return 0;
	strcpy(sensor->filters_list,buffer);

	if (!read_string_attribute2(node,"ANGLE",buffer))return 0;
	sscanf(buffer,"%f",&sensor->angle);

	//float solid_angle=2*PI*(1-cos(sensor->angle/2*PI/180));//steradian
	//printf("Sensor %s solid angle: %f sr\n",sensor->name,solid_angle);

	// look for FILE attribute. If exists the sensor surface is a
	//  mesh file OBJ or STL which filename is the FILE attribute value:
	int i1=read_string_attribute22(node,"FILE",buffer,0);
	if (i1!=0)
	{
		sensor->type=SENSOR_TYPE_SURFACE;
		strcpy(sensor->surface_filename,buffer);
		//add the surface to the scene:
		sx_sensor_add_surface_with_black_lambert_material_to_scene(sensor,scene,
					sensor->surface_filename);
		//create the source s3d scene to sort the reverse photons:
		sx_sensor_create_s3d_scene(sensor);
		//compute the area of the sensor's surface:
		float area_mm2;
		s3d_scene_view_compute_area(sensor->scnview,&area_mm2);
		sensor->area=area_mm2*1e-6f;
		printf("Sensor with surface geometry: %s\n",sensor->surface_filename);
	}
	else
	{
		sensor->type=SENSOR_TYPE_DISK;
		//get the diameter of the disk:
		if (!read_string_attribute2(node,"DIAMETER",buffer))return 0;
		sscanf(buffer,"%f",&sensor->diameter);
		//compute the sensor area in m2
		sensor->area=(float)PI*powf(sensor->diameter*1e-3f,2.0f)/4.0f;
		//find position
		mxml_node_t* pos=mxmlFindElement(node,node,"pos",NULL,NULL,MXML_DESCEND);
		if (pos)
		{
			if (!read_string_attribute2(pos,"X",buffer))return 0;
			sscanf(buffer,"%f",&sensor->pos[0]);
			if (!read_string_attribute2(pos,"Y",buffer))return 0;
			sscanf(buffer,"%f",&sensor->pos[1]);
			if (!read_string_attribute2(pos,"Z",buffer))return 0;
			sscanf(buffer,"%f",&sensor->pos[2]);
		}
		else
		{
			printf("sx_path_source %s has no son of type pos\n",sensor->name);
			return 0;
		}
		//find direction
		if (sensor->angle!=360)
		{
			//directional sensor
			//find viewPoint
			mxml_node_t* node_vp=mxmlFindElement(node,node,"viewPoint",NULL,NULL,MXML_DESCEND);
			float vp[3];//view point
			if (node_vp)
			{
				if (!read_string_attribute2(node_vp,"X",buffer))return 0;
				sscanf(buffer,"%f",&vp[0]);
				if (!read_string_attribute2(node_vp,"Y",buffer))return 0;
				sscanf(buffer,"%f",&vp[1]);
				if (!read_string_attribute2(node_vp,"Z",buffer))return 0;
				sscanf(buffer,"%f",&vp[2]);
				//calc the direction of the sx_path_source:
				f3_sub(sensor->dir,vp,sensor->pos); //put X local frame parallel to (lens,view point)
				if (f3_len(sensor->dir)==0)
				{
					printf("sensor %s: viewPoint and pos are the same location\n",sensor->name);
					return 0;
				}
				f3_normalize(sensor->dir,sensor->dir);
#ifdef PRINT_ALL_STEPS
				printf("sx_sensor dir: %g %g %g \n",sensor->dir[0],sensor->dir[1],sensor->dir[2]);
#endif
			}
			else
			{
				printf("sx_path_source %s has no son of type viewPoint\n",sensor->name);
				return 0;
			}
		}
		else
		{
			//undirectional sensor
		}
		//the sensor is a disk, create the surface (disk)
		if (sensor->diameter>0)
		{
			//name of the surface:
			char name[MAX_NAME_LENGTH];
			char surface_filename[MAX_NAME_LENGTH];
			char file[MAX_NAME_LENGTH];
			strcpy(name,"surface_of_sensor_");
			strcat(name,sensor->name);
			//name of the obj file:
			strcpy(file,name);
			strcat(file,".obj");
			//full name of the obj file:
			strcpy(surface_filename,scene->file_path);
			strcat(surface_filename,file);
			//create a disk with normal in direction of sensor->dir:
			sx_cad_save_disk_obj(sensor->diameter,50,sensor->pos,sensor->dir,
					scene->file_path,file);
			//add the surface to the scene:
			sx_sensor_add_surface_with_black_lambert_material_to_scene(sensor,scene,
					file);
		}
	}



	return 1;
}






int sx_sensor_connect_spectra(
		struct sx_sensor* sx_sensor,
		struct sx_scene* scene
		)
{
	struct sampled_data** filters;
	int nb_filters;
	int res=sx_path_source_find_or_create_spectrum(&filters,&nb_filters,
			sx_sensor->filters_list,scene);
	if (res)
	{
		sx_sensor->nb_filters=nb_filters;
		sx_sensor->filters=calloc(1,sizeof(struct sampled_data*)*nb_filters);
		for (int i=0;i<nb_filters;i++)
		{
			sx_sensor->filters[i]=filters[i];
		}
		return 1; 
	}
	else
	{
		printf("Error connecting spectra of sx_path_source %s\n",sx_sensor->name); 
		return 0;
	}
}



int sx_sensor_create_sx_path_sources(
		struct sx_sensor* sensor,
		struct sx_scene* scene
		)
{
	sensor->index_in_path_sources=scene->sx_path_source_index;
	//set the starting volume of the sx_path_sources not belonging to the camera:
	for (int k=0;k<sensor->nb_filters;k++)
	{
		struct sx_path_source* ps;
		sx_path_source_create(&ps);
  ps->weight_dim=1;
		ps->weight=calloc(1,ps->weight_dim*sizeof(double));//one dimensional weight 
		ps->sigma=calloc(1,ps->weight_dim*sizeof(double));//same as weight
		//set the starting volume:
		ps->starting_volume=sensor->starting_volume;
		ps->spectrum=sensor->filters[k];
		//wavelength sorter:
		struct ssp_ranst_discrete* wavelength_sorter;
		sx_path_source_create_wavelength_sorter(&wavelength_sorter,sensor->filters[k]);
		ps->wavelength_sorter=wavelength_sorter;
		//add the path source to the scene:
		d_list_add(scene->path_sources,ps);
		//set the type of path source:
		ps->type=PATH_SOURCE_TYPE_SENSOR;
		ps->sensor=sensor;
		strcpy(ps->name,"path source of ");
		strcat(ps->name,sensor->name);
	}
	return 1;
}


/**
  start a new path
 */
void sx_sensor_create_reverse_photon(
		struct sx_sensor* sensor,
		struct sx_photon** photon_,
		struct ssp_rng* rng
		)
{
	float v[3];
	float n[3];//normal
	float r1,r2,r3;
	struct s3d_attrib attr0;
	struct s3d_primitive prim;
	float uv[2];
	sx_photon_create(photon_);
	struct sx_photon* photon=(*photon_);
	float alpha=sensor->angle/2.0f*(float)PI/180.0f;

	switch (sensor->type)
	{
		case SENSOR_TYPE_DISK:
			//set the position:
			if (sensor->diameter!=0)
			{
				if (sensor->angle!=360)
				{
					//sort a point in a disk with uniform probability: 
					ssp_ran_uniform_disk_float(rng,sensor->diameter/2,sensor->dir,v,NULL);
				}
				else
				{
					//sort a point in a sphere of diameter diameter
					ssp_ran_sphere_uniform_float(rng,v,NULL);
					f3_mulf(v,v,sensor->diameter/2);
				}
				f3_add(photon->x,sensor->pos,v);
			}
			else
			{
				f3_set(photon->x,sensor->pos);
			}

			//set the direction:
			if (sensor->angle==360)
			{
				//sx_path_source direction has norm zero: isotrope emission
				//sort vector uniformly in 4PI sr:
				ssp_ran_sphere_uniform_float(rng,v,NULL);
				f3_set(photon->dir,v);
			}
			else
			{
				//sort a direction around photon->dir in a cone of half angle:
				//printf("sensor angle = %f alpha=%f \n",sensor->angle,alpha);
				//ssp_ran_sphere_cap_uniform_float(rng,sensor->dir,cos(alpha),photon->dir,NULL);
				//ssp_ran_hemisphere_cos_float(rng,sensor->dir,photon->dir,NULL);
				sort_sphere_cap_cos_float(rng,sensor->dir,alpha,photon->dir,NULL);
			}
			//printf("sensor disk: sort photon dir=(%f,%f,%f)\n",sensor->name,photon->dir[0],photon->dir[1],photon->dir[2]);
			break;
		case SENSOR_TYPE_SURFACE:
			//sort uniformely a point on the source surface:
			r1=ssp_rng_canonical_float(rng);
			r2=ssp_rng_canonical_float(rng);
			r3=ssp_rng_canonical_float(rng);
			s3d_scene_view_sample(sensor->scnview,r1,r2,r3,&prim,uv);
			s3d_primitive_get_attrib(&prim,S3D_POSITION,uv,&attr0);
			//printf("attrib pos : %g %g %g %g \n",attr0.value[0],attr0.value[1],attr0.value[2],attr0.value[3]);
			for (int i=0;i<3;i++) photon->x[i]=attr0.value[i];
			//get the normal of the surface at the sorted point:
			s3d_primitive_get_attrib(&prim,S3D_GEOMETRY_NORMAL,uv,&attr0);
			//printf("attrib normal : %g %g %g %g \n",attr0.value[0],attr0.value[1],attr0.value[2],attr0.value[3]);
			for (int i=0;i<3;i++) n[i]=attr0.value[i];
			f3_normalize(n,n);
			//to have normal in the same direction as the one given by blender:
			f3_mulf(n,n,-1);
			//printf("sensor surface %s: sort photon dir=(%f,%f,%f)\n",sensor->surface->name,n[0],n[1],n[2]);
			//printf("sensor angle = %f alpha=%f \n",sensor->angle,alpha);
			//sort a directon of the photon in the reception cone with cos(theta) probability:
			//ssp_ran_sphere_cap_uniform_float(rng,n,cos(alpha),photon->dir,NULL);
			//ssp_ran_hemisphere_cos_float(rng,n,photon->dir,NULL);
			sort_sphere_cap_cos_float(rng,n,alpha,photon->dir,NULL);
			photon->surface=sensor->surface;
			photon->triangle_id=prim.prim_id;
#ifdef PRINT_ALL_STEPS
			printf(" sx_sensor_create_reverse_photon on surface %s on triangle %d \n",
					photon->surface->name,photon->triangle_id);
#endif
			break;
	}
	//it is a reverse photon set the radiance to 0:
	photon->w=0;
	//set the surface where is the photon to avoid self-intersection:
	photon->surface=sensor->surface;
}

/**
  create a surface from a STL or OBJ file and add it to the scene 
 */
int sx_sensor_add_surface_with_black_lambert_material_to_scene (
		struct sx_sensor *sensor,
		struct sx_scene* scene,
		char* surface_filename //name of the mesh file (OBJ or STL)
)
{
	//remove .obj or .stl to the file name to get the surface name:
	char surface_name[MAX_NAME_LENGTH];
	int n=strlen(surface_filename)-4;//length of surface name
	memcpy(surface_name,surface_filename,n);
	surface_name[n]='\0';
	//create the surface object:
	//add this surface to the scene:
	struct sx_surface* s1;
	sx_surface_create(&s1);
	strcpy(s1->name,surface_name);
	//set the file name of the surface:
	strcpy(s1->file,scene->file_path);
	strcat(s1->file,surface_filename);
	strcpy(s1->mat_names,"");

	//create a lambert material surface with albedo=0 
	struct sx_mat_surf* ms;
	char name1[MAX_NAME_LENGTH];
	strcpy(name1,sensor->name);
	strcat(name1,"_ms");
	sx_mat_surf_create_lambert(&ms,name1,0);
	//add the material to the list of material surfaces of the scene:
	scene->material_surface[scene->nb_material_surface++]=ms;


	//add the front and back materials of surface to the sensor's list:
	strcpy(s1->mat_names,ms->name);

	s1->mat=calloc(1,sizeof(struct sx_mat_surf*)*2);
	s1->mat[0]=ms;
	s1->mat[1]=ms;
	//add the surface to the list of surfaces of the scene:
	d_list_add(scene->surfaces,s1);
	s1->id=scene->surfaces->dim-1;
	//store in sensor a pointer to the surface:
	sensor->surface=s1;
	//set the pointer of the created surface to this sensor:
	sensor->surface->sensor=sensor;
	strcpy(sensor->surface_filename,surface_filename);
#ifdef PRINT_ALL_STEPS
	printf("sx_sensor. Create disk surface %s \n",
			sensor->surface->name);
#endif
	return 1;
}



/**
  Create the s3d scene, internal of the sensor, used to sort the emitted photons
 */
int sx_sensor_create_s3d_scene(struct sx_sensor* sensor)
{
	struct s3d_device* s3d_device = NULL;
	const int verbosity=0; /* Control the verbosity of Star-3D */
	int res = s3d_device_create(NULL,NULL,verbosity,&s3d_device);
	if(res != RES_OK) 
	{
		printf("sx_source_create_source_from_xml error with s3d_device_create \n");
		return 0;
	}
	res = s3d_scene_create(s3d_device,&sensor->s3d_scene);
	if(res != RES_OK) 
	{
		printf("sx_source_create_source_from_xml error with s3d_scene_create \n");
		return 0;
	}
	if (strstr(sensor->surface_filename,".stl"))
	{
		//printf("OOOOOOOOOOOO sx_source_create_s3d_scene import STL: %s\n",sensor->surface_filename);
		sx_scene_import_stl(sensor->surface_filename,
				sensor->s3d_scene,s3d_device);
	}

	if (strstr(sensor->surface_filename,".obj"))
	{
		//printf("OOOOOOOOOOOO sx_source_create_s3d_scene import OBJ: %s\n",sensor->surface_filename);
		sx_scene_import_obj(sensor->surface_filename,
				sensor->s3d_scene,s3d_device);
	}
	res = s3d_scene_view_create(sensor->s3d_scene,S3D_SAMPLE,&sensor->scnview);
	return res;
}
