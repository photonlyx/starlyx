/*
   Copyright (C) 2022 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
   */

#ifndef SX_SENSOR_SET_H_
#define SX_SENSOR_SET_H_


#include "sx_xml.h"
#include "sx_scene.h"


enum {SENSOR_TYPE_DISK,SENSOR_TYPE_SURFACE};

struct mxml_node_t;
struct sx_path_source;


/**
Sensor for radiosity or fluence(if dir=(0,0,0) ).
For reverse algorithms
*/
struct sx_sensor
{
	char name[MAX_NAME_LENGTH];
	char volume_name[MAX_NAME_LENGTH];//starting volume name
	struct sx_volume* starting_volume;//starting volume
	int type;
	float pos[3];
	float dir[3];
	float diameter;//diameter in mm of the sensor (case SENSOR_TYPE_DISK) 
	float area;//area of the sensor in m²
	float angle;//angle of acceptance of th sx_path_source in degrees
	char filters_list[MAX_NAME_LENGTH];
	int nb_filters;
	struct sampled_data **filters;
	unsigned long index_in_path_sources;
	//name of the OBJ file describing the sensor geometry
	char surface_filename[MAX_NAME_LENGTH];
	//pointer to the surface object materialising the sensor 
	struct sx_surface* surface;
	//s3d objects:
	struct s3d_scene* s3d_scene;//the sub-scene  
	struct s3d_scene_view* scnview;//the sub-scene scene view
};

void sx_sensor_create(struct sx_sensor** sensor_);

int sx_sensor_create_from_xml(
		struct sx_sensor** sx_sensor,
		mxml_node_t *node,
		struct sx_scene* scene
);

void sx_sensor_print(
		struct sx_sensor* sx_sensor,
		FILE* stream
);

void sx_sensor_release(struct sx_sensor* sx_sensor);

int sx_sensor_create_sx_path_sources(
struct sx_sensor* sensor,
struct sx_scene* scene
);


int sx_sensor_connect_spectra(
struct sx_sensor* sx_sensor,
struct sx_scene* scene
);

void sx_sensor_create_reverse_photon(
  struct sx_sensor* sx_sensor,
  struct sx_photon** photon,
  struct ssp_rng* rng);


int sx_sensor_add_surface_with_black_lambert_material_to_scene (
		struct sx_sensor *sensor,
		struct sx_scene* scene,
		char* surface_filename //name of the mesh file
);


int sx_sensor_create_s3d_scene(struct sx_sensor* sensor);

#endif /* SX_SENSOR_SET_H_ */
