/*

   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>

#include "star/ssp.h"
#include <star/s3daw.h>
#include <star/s3d.h>
#include <star/s3dstl.h>

#include "sx_configuration.h"
#include "sx_source.h"
#include "sx_photon.h"
#include "sx_scene.h"
#include "sx_geometry.h"
#include "sx_cie.h"
#include "sx_path_source.h"
#include "sx_camera.h"
#include "sx_surface.h"
#include "sx_cad.h"
#include "sx_mat_surf.h"
#include "sx_volume.h"
#include "sx_ssp.h"

#include "sampled_data.h"





void sx_source_print(
		struct sx_source * source,
		FILE* stream
		)
{
	fprintf(stream,"Source %s :\n",source->name);
	switch(source->type)
	{
		case SOURCE_TYPE_SUN:
			fprintf(stream,"Type SUN \n");
			break;
		case SOURCE_TYPE_SPOT:
			fprintf(stream,"Type SPOT \n");
			break;
		case SOURCE_TYPE_SURFACE:
			fprintf(stream,"Type SURFACE \n");
			break;
		default:
			break;
	}
	fprintf(stream,"\tpower=%g W\n",source->power);
	fprintf(stream,"\tintensity=%g W sr-1\n",source->intensity);
	fprintf(stream,"\tradiance=%g W m-2 sr-1\n",source->radiance);
	switch (source->type)
	{
		case SOURCE_TYPE_SPOT:
			fprintf(stream,"\tdiameter=%f mm\n",source->diameter);
			fprintf(stream,"\tarea=%g m2\n",source->area);
			break;
		case SOURCE_TYPE_SURFACE:
			fprintf(stream,"\tarea=%g m2\n",source->area);
			break;
	}
	fprintf(stream,"\tangle=%f deg\n",source->angle);
	fprintf(stream,"\tstarting volume %s \n",source->volume_name);
	if ((source->type==SOURCE_TYPE_SURFACE)||(source->type==SOURCE_TYPE_SPOT))
	{
		fprintf(stream,"\tsurface: %s \n",source->surface->name);
		fprintf(stream,"\tsurface file: %s \n",source->surface_filename);
	}
	if (source->type==SOURCE_TYPE_SPOT)
	{
		fprintf(stream,"\tpos: ");
		for (int i=0;i<3;i++) fprintf(stream,"%f ",source->pos[i]);
		fprintf(stream,"\n");
		fprintf(stream,"\tdir: ");
		for (int i=0;i<3;i++) fprintf(stream,"%f ",source->dir[i]);
		fprintf(stream,"\n");
	}
	fprintf(stream,"\tSpectrum name %s \n",source->spectrum_name);
}


int sx_source_create(
		struct sx_source **source_
		)
{
	//create a new source
	struct sx_source* source=calloc(1,sizeof(struct sx_source));
	source->type=0;
	strcpy(source->name,"");
	source->power=0;
	strcpy(source->spectrum_name,"");
	source->spectrum=NULL;
	source->pos[0]=0;
	source->pos[1]=0;
	source->pos[2]=0;
	source->dir[0]=0;
	source->dir[1]=0;
	source->dir[2]=0;
	source->diameter=0;
	source->angle=0;
	source->attenuation=0;
	strcpy(source->volume_name,"");
	source->starting_volume=NULL;
	strcpy(source->surface_filename,"");
	source->s3d_scene=NULL;
	source->scnview=NULL;
	(*source_)=source;
	return 0;
}




/**
 * create source object from an xml description
 */

int sx_source_create_source_from_xml(
		struct sx_source ** p_p_s,
		mxml_node_t *node1,
		struct sx_scene* scene
		)
{
	char str[MAX_NAME_LENGTH];
	//create a new source:
	struct sx_source* source=calloc(1,sizeof(struct sx_source));
	char source_type[MAX_NAME_LENGTH];
	if (!read_string_attribute2(node1,"TYPE",source_type))
	{
		source->type=SOURCE_TYPE_SPOT;
		//return 0;
	}
	else
	{
		if (strcmp(source_type,"laser")==0)
		{
			//for backward compatibility
			source->type=SOURCE_TYPE_SPOT;
		}
		else if (strcmp(source_type,"point")==0)
		{
			//for backward compatibility
			source->type=SOURCE_TYPE_SPOT;
		}
		else if (strcmp(source_type,"spot")==0)
		{
			source->type=SOURCE_TYPE_SPOT;
		}
		else if (strcmp(source_type,"sun")==0)
		{
			source->type=SOURCE_TYPE_SUN;
		}
		else if (strcmp(source_type,"surface")==0)
		{
			source->type=SOURCE_TYPE_SURFACE;
		}
		else source->type=SOURCE_TYPE_SPOT;
	}
	if (!read_string_attribute2(node1,"NAME",source->name)) return 0;




	if (!read_string_attribute2(node1,"SPECTRUM",source->spectrum_name)) return 0;

	//get the volume where is the sx_source ("WORLD" by default)
	if (read_string_attribute23(node1,"VOLUME",str,0,1,MAX_NAME_LENGTH))
	{
		strcpy(source->volume_name,str);
	}
	else
	{
		strcpy(source->volume_name,"World");
		printf("VOLUME of %s set to default (%s)\n",source->name,
				source->volume_name);
	}

	if (source->type==SOURCE_TYPE_SPOT)
	{
		//find source spot position:
		mxml_node_t* pos=mxmlFindElement(node1,node1,"pos",NULL,NULL,MXML_DESCEND);
		if (pos)
		{
			//printf("found dir <%s>:%d \n", pos->value.element.name, pos->value.element.num_attrs);
			if (!read_string_attribute2(pos,"X",str)) return 0;
			sscanf(str,"%f",&source->pos[0]);
			if (!read_string_attribute2(pos,"Y",str)) return 0;
			sscanf(str,"%f",&source->pos[1]);
			if (!read_string_attribute2(pos,"Z",str)) return 0;
			sscanf(str,"%f",&source->pos[2]);
		}
		else
		{
			printf("Can't' find position pos of source: %s\n",source->name);
			return 0;
		}
		//get the source spot diameter:
		if (read_string_attribute23(node1,"DIAMETER",str,0,1,MAX_NAME_LENGTH))
		{
			sscanf(str,"%f",&source->diameter);
		}
		else
		{
			source->diameter=0;
			printf("DIAMETER of %s set to default (%f)\n",source->name,
					source->diameter);
		}
	}

	if ((source->type==SOURCE_TYPE_SUN)||(source->type==SOURCE_TYPE_SPOT))
	{
	//find source direction for spot and sun:
		mxml_node_t* dir=mxmlFindElement(node1,node1,"dir",NULL,NULL,MXML_DESCEND);
		if (dir)
		{
			//printf("found dir <%s>:%d \n", dir->value.element.name, dir->value.element.num_attrs);
			if (!read_string_attribute2(dir,"X",str)) return 0;
			//sscanf(mxmlElementGetAttr(dir,"X"),"%f",&sun->sun_dir[0]);
			sscanf(str,"%f",&source->dir[0]);
			if (!read_string_attribute2(dir,"Y",str)) return 0;
			sscanf(str,"%f",&source->dir[1]);
			if (!read_string_attribute2(dir,"Z",str)) return 0;
			sscanf(str,"%f",&source->dir[2]);
			f3_normalize(source->dir,source->dir);
		}
		else
		{
			printf("Can't' find direction dir of source of type sun: %s\n",
					source->name);
			return 0;
		}
	}




	//get the source divergence (ANGLE):
	if (!read_string_attribute23(node1,"ANGLE",str,0,1,MAX_NAME_LENGTH))
	{
		if (source->type==SOURCE_TYPE_SUN) 
			source->angle=0.26f;
		else 
			source->angle=90.0f;
		printf("ANGLE of %s set to default (%f deg)\n",
				source->name,source->angle);
	}
	else
	{
		sscanf(str,"%f",&source->angle);
	}





//SOURCE_SPOT create the disk surface and put it in the scene:
	if	(source->type==SOURCE_TYPE_SPOT)
	{
		//create the surface (disk)
		if (source->diameter>0)
		{
			//if source spot with non null diameter, create the disk surface,
			//save a mesh file of the source disk:
			//name of the surface:
			char name[MAX_NAME_LENGTH];
			char file[MAX_NAME_LENGTH];
			strcpy(name,"surface_of_");
			strcat(name,source->name);
			//name of the obj file:
			strcpy(file,name);
			strcat(file,".obj");
			sx_cad_save_disk_obj(source->diameter,50,source->pos,source->dir,
					scene->file_path,file);
			//add this new surface to the scene of starlyx:
			sx_source_add_surface_with_emission_material_to_scene(source,scene,file);
			//create the source s3d scene to sort the photons:
			sx_source_create_s3d_scene(source);
		}
		//compute the sensor area in m2:
		source->area=(float)PI*powf(source->diameter*1e-3f,2.0f)/4.0f;
	}

	//SOURCE_SURFACE get the geometry from a mesh file and put it in the scene:
	if (source->type==SOURCE_TYPE_SURFACE)
		{
			//get the surface file name:
			if (read_string_attribute2(node1,"FILE",source->surface_filename)) 
				printf("Source with surface geometry: %s\n",source->surface_filename);
			//add this new surface to the scene of starlyx:
			sx_source_add_surface_with_emission_material_to_scene(source,scene,
					source->surface_filename);
			//create the source s3d scene to sort the photons:
			sx_source_create_s3d_scene(source);
			//compute the area of the sensor's surface:
			float area_mm2;
			s3d_scene_view_compute_area(source->scnview,&area_mm2);
			source->area=area_mm2*1e-6f;
			if (source->area==0) 
			{
				printf("Surface: %s, area is 0 !.\n",source->surface_filename);	
				return 0;
			}
		}





	if (source->type==SOURCE_TYPE_SUN)
	{
		//collect the energy parameters for the case of sun source:
		if (!read_string_attribute23(node1,"RADIANCE",str,0,1,MAX_NAME_LENGTH))
		{
			source->radiance=1.5e7;
			printf("RADIANCE of %s set to default (%f W/m²/sr)\n",
					source->name,source->radiance);
		}
		else
		{
			sscanf(str,"%f",&source->radiance);
		}
	}

	if (source->type==SOURCE_TYPE_SPOT)
	{
		//collect the energy parameters for the case of spot source:
		if ((scene->algo==ALGO_REVERSE)&&((source->diameter==0)||(source->angle==0)))
		//if ((source->diameter==0)||(source->angle==0))
		{
			//if diameter or angle is 0, the user must indicate the radiance (luminance in fr)
			if (!read_string_attribute23(node1,"RADIANCE",str,0,1,MAX_NAME_LENGTH))
			{
				source->radiance=1;
				printf("RADIANCE of %s set to default (%f W/m²/sr)\n",
						source->name,source->radiance);
			}
			else
			{
				sscanf(str,"%f",&source->radiance);
			}
		}
		else
		{
			if (!read_string_attribute23(node1,"POWER",str,0,1,MAX_NAME_LENGTH))
			{
				source->power=1;
				printf("POWER of %s set to default (%f W)\n",
						source->name,source->power);
			}
			else
			{
				sscanf(str,"%f",&source->power);
			}
			//radiant intensity in W/m²:
			float alpha=source->angle/2.0f*(float)PI/180.0f;
			source->intensity=source->power/(float)PI/powf(sinf(alpha),2);
			//radiance in W/m²/sr:
			source->radiance=source->intensity/source->area;
		}
	}

	if (source->type==SOURCE_TYPE_SURFACE)
	{
		//collect the energy parameters for the case of surface source
		if ((scene->algo==ALGO_REVERSE)&&(source->angle==0))
		{
			//if angle is 0, the user must indicate the radiance (luminance in fr)
			if (!read_string_attribute23(node1,"RADIANCE",str,0,1,MAX_NAME_LENGTH))
			{
				source->radiance=1;
				printf("RADIANCE of %s set to default (%f W/m²/sr)\n",
						source->name,source->radiance);
			}
			else
			{
				sscanf(str,"%f",&source->radiance);
			}
		}
		else
		{
			if (!read_string_attribute23(node1,"POWER",str,0,1,MAX_NAME_LENGTH))
			{
				source->power=1;
				printf("POWER of %s set to default (%f W)\n",
						source->name,source->power);
			}
			else
			{
				sscanf(str,"%f",&source->power);
			}
				//radiant intensity in W/m²:
				float alpha=source->angle/2.0f*(float)PI/180.0f;
				source->intensity=source->power/(float)PI/powf(sinf(alpha),2);
				//radiance in W/m²/sr:
				source->radiance=source->intensity/source->area;
		}
	}


	//printf("Source %s intensity: %g W sr-1\n",source->name,source->intensity);
	//printf("Source %s radiance: %g W sr-1 m-2\n",source->name,source->radiance);
	(*p_p_s)=source;
	return 1;
}






/**
  Create the s3d scene, internal of the source, used to sort the emitted photons
 */
int sx_source_create_s3d_scene(
		struct sx_source* source
		)
{
	struct s3d_device* s3d_device = NULL;
	const int verbosity=0; /* Control the verbosity of Star-3D */
	int res = s3d_device_create(NULL,NULL,verbosity,&s3d_device);
	if(res != RES_OK) 
	{
		printf("sx_source_create_source_from_xml error with s3d_device_create \n");
		return 0;
	}
	res = s3d_scene_create(s3d_device,&source->s3d_scene);
	if(res != RES_OK) 
	{
		printf("sx_source_create_source_from_xml error with s3d_scene_create \n");
		return 0;
	}
	if (strstr(source->surface_filename,".stl"))
	{
		//printf("OOOOOOOOOOOO sx_source_create_s3d_scene import STL: %s\n",source->surface_filename);
		sx_scene_import_stl(source->surface_filename,
				source->s3d_scene,s3d_device);
	}

	if (strstr(source->surface_filename,".obj"))
	{
		//printf("OOOOOOOOOOOO sx_source_create_s3d_scene import OBJ: %s\n",source->surface_filename);
		sx_scene_import_obj(source->surface_filename,
				source->s3d_scene,s3d_device);
	}
	res = s3d_scene_view_create(source->s3d_scene,S3D_SAMPLE,&source->scnview);
	return res;
}

/**
  create a surface from a STL or OBJ file and add it to the scene 
 */
int sx_source_add_surface_with_emission_material_to_scene (
		struct sx_source *source,
		struct sx_scene* scene/*,struct sx_surface** s1_*/,
		char* surface_filename //name of the mesh file
)
{
	//remove .obj or .stl to the file name to get the surface name:
	char surface_name[MAX_NAME_LENGTH];
	int n=strlen(surface_filename)-4;//length of surface name
	memcpy(surface_name,surface_filename,n);
	surface_name[n]='\0';
	//create the surface object:
	//add this surface to the scene:
	struct sx_surface* s1;
	sx_surface_create(&s1);
	strcpy(s1->name,surface_name);
	//set the file name of the surface:
	strcpy(s1->file,scene->file_path);
	strcat(s1->file,surface_filename);
	strcpy(s1->mat_names,"");
	s1->source=source;
	//create an emission material surface for the front side of the surface:
	struct sx_mat_surf* ms_emission;
	sx_mat_surf_create(&ms_emission);
	char name1[MAX_NAME_LENGTH];
	strcpy(name1,source->name);
	strcat(name1,"_mat_surf_front");
	strcpy(ms_emission->name,name1);
	ms_emission->type=SURFACE_TYPE_EMISSION;
	ms_emission->apply=&surface_emission;
	ms_emission->angle=source->angle;
	ms_emission->surface=s1;
	//set the emission spectrum:
	strcpy(ms_emission->spectrum_name,source->spectrum_name);
	//add the emission material to the list of material surfaces of the scene:
	scene->material_surface[scene->nb_material_surface]=ms_emission;
	scene->nb_material_surface++;
	//associate the pointer to function spectrum(lambda) to
	//a discrete function:
	ms_emission->spectrum=&sampled_data_discrete;
	//create a black lambert material surface for the back side of the surface:
	struct sx_mat_surf* ms_black;
	sx_mat_surf_create(&ms_black);
	char name2[MAX_NAME_LENGTH];
	strcpy(name2,source->name);
	strcat(name2,"_mat_surf_back");
	strcpy(ms_black->name,name2);
	ms_black->type=SURFACE_TYPE_LAMBERT;
	ms_black->apply=&surface_lambert;
	ms_black->surface=s1;
	//set the albedo spectrum:
	strcpy(ms_black->spectrum_name,"0");
	//add the black material to the list of material surfaces of the scene:
	scene->material_surface[scene->nb_material_surface]=ms_black;
	scene->nb_material_surface++;
	//set the materials to the surface
	s1->mat=calloc(1,sizeof(struct sx_mat_surf*)*2);
	s1->mat[0]=ms_emission;//front
	s1->mat[1]=ms_black;//back
	//put the names of the surface materials in the list of the surface materials:
	strcpy(s1->mat_names,ms_emission->name);
	strcat(s1->mat_names," ");
	strcat(s1->mat_names,ms_black->name);
	//add the surface to the list of surfaces of the scene:
	d_list_add(scene->surfaces,s1);
	s1->id=scene->surfaces->dim-1;
	source->surface=s1;
	strcpy(source->surface_filename,surface_filename);
#ifdef PRINT_ALL_STEPS
	printf("YYYYYYY sx_source. SPOT. Create disk surface %s \n",
			source->surface->name);
#endif
	return 1;
}


/**
 * find the spectra . For numerical values, create monochromatic spectra. 
 */
int sx_source_connect_spectra(
		struct sx_source* source,
		struct sx_scene* scene
		)
{
	struct sampled_data* spectrum;
	if (strcmp(source->spectrum_name,"d65")==0)
	{
		//get the standart D65 illumination of 
		//Commission Internationale de l'Eclairage (CIE).
		struct sampled_data* d65;
		sx_cie_create_d65(&d65);
		//affect this to the source spectrum. 
		//Note: keep all the values of d65 (every 5nm)
		spectrum=d65;
	}
	else
	{
		spectrum=sx_scene_find_spectrum(scene,source->spectrum_name);
		if (spectrum==NULL)
		{
			printf("source: %s : can't find spectrum of name %s\n", 
					source->name ,source->spectrum_name);
			return 0;
		}
	}
	//normalise the spectrum
	double integral=sampled_data_integral(spectrum);
	sampled_data_mul(spectrum,(float)(1/integral));
	//affect the spectrum:
	source->spectrum=spectrum;
	//affect the function power(lambda):
	source->spectr=&sampled_data_discrete;
	return 1;
}

void sx_source_release(
		struct sx_source * s
		)
{
	//sampled_data_release(s->spectrum);
	free(s);
}




/**
 * create (sort) a photon starting at the source in the world volume
 */
void sx_source_create_photon(
		struct sx_source * source,
		struct sx_photon** photon,
		struct ssp_rng* rng
		)
{
	struct sx_photon* photon1;
	float alpha=(float)PI/180.0f*source->angle/2.0f;
	sx_photon_create(&photon1);
	switch (source->type)
	{	
		case SOURCE_TYPE_SUN:
			if (source->angle>0)
			{
				//sort a point in a sphere cap of half angle: source->angle/2
				ssp_ran_sphere_cap_uniform_float(rng,source->dir,
						cosf(alpha),photon1->dir,NULL);
#ifdef PRINT_ALL_STEPS
				printf(" sx_source_create_photon... source dir =(%g,%g,%g) \n", 
						source->dir[0], source->dir[1],source->dir[2]);
				printf(" sx_source_create_photon... angle=%f   photon1 dir =(%g,%g,%g) \n",
						source->angle,photon1->dir[0],
						photon1->dir[1],photon1->dir[2]);
#endif
			}
			else f3_set(photon1->dir,source->dir);
			break;
		case SOURCE_TYPE_SPOT:
			//set the photon position
			if (source->diameter==0)
			{
				f3_set(photon1->x,source->pos);
				//set the photon direction
				if (source->angle>0)
				{
					//sort a point in a sphere cap of half angle: source->angle/2
					//ssp_ran_sphere_cap_uniform_float(rng,source->dir,cos(alpha),photon1->dir,NULL);
					//ssp_ran_hemisphere_cos_float(rng,source->dir,photon1->dir,NULL);
					sort_sphere_cap_cos_float(rng,source->dir,alpha,photon1->dir,NULL);
				}
				else f3_set(photon1->dir,source->dir);
				//set the surface where is this photon from:
				photon1->surface=source->surface;
			}
			else sx_source_sort_photon_on_surface(source,photon1,alpha,rng);
			break;
		case SOURCE_TYPE_SURFACE:
			sx_source_sort_photon_on_surface(source,photon1,alpha,rng);
			break;
	}
	photon1->volume=source->starting_volume;
	photon1->lambda=0.f;
	//set the weight:
	photon1->w=source->power;
	//sx_photon_print(photon1);
	(*photon)=photon1;
}

void sx_source_sort_photon_on_surface(
		struct sx_source * source,
		struct sx_photon* photon1,
		float alpha,
		struct ssp_rng* rng
		)
{
	float r1,r2,r3;
	struct s3d_attrib attr0;
	struct s3d_primitive prim;
	float uv[2];
	float n[3];//normal
	//sort uniformely a point on the source surface:
	r1=ssp_rng_canonical_float(rng);
	r2=ssp_rng_canonical_float(rng);
	r3=ssp_rng_canonical_float(rng);
	s3d_scene_view_sample(source->scnview,r1,r2,r3,&prim,uv);
	s3d_primitive_get_attrib(&prim,S3D_POSITION,uv,&attr0);
	//printf("attrib pos : %g %g %g %g \n",attr0.value[0],attr0.value[1],attr0.value[2],attr0.value[3]);
	for (int i=0;i<3;i++) photon1->x[i]=attr0.value[i];
	s3d_primitive_get_attrib(&prim,S3D_GEOMETRY_NORMAL,uv,&attr0);
	//printf("attrib normal : %g %g %g %g \n",attr0.value[0],attr0.value[1],attr0.value[2],attr0.value[3]);
	for (int i=0;i<3;i++) n[i]=attr0.value[i];
	f3_normalize(n,n);
	////to have normal in the same direction as the one given by blender and jstarlyx:
	f3_mulf(n,n,-1);
	//printf("source surface %s: sort photon n=(%f,%f,%f)\n",source->surface->name,n[0],n[1],n[2]);
	//printf("source angle = %f alpha=%f \n",source->angle,alpha);
	//sort a direction of the photon in the emission cone with cos(theta) probability:
	//ssp_ran_sphere_cap_uniform_float(rng,n,cos(alpha),photon1->dir,NULL);
	//ssp_ran_hemisphere_cos_float(rng,n,photon1->dir,NULL);
	sort_sphere_cap_cos_float(rng,n,alpha,photon1->dir,NULL);
	//printf("source surface sort photon dir=(%f,%f,%f)\n",photon1->dir[0],photon1->dir[1],photon1->dir[2]);
	photon1->surface=source->surface;
	photon1->triangle_id=prim.prim_id;
#ifdef PRINT_ALL_STEPS
			printf(" sx_source_create_photon on surface %s on triangle %d \n",
					photon1->surface->name,photon1->triangle_id);
#endif
}




/**
  create the path source from this source
 */
int sx_source_create_sx_path_source(
		struct sx_source* source,
		struct sx_path_source** ps_,
		struct sx_scene* scene)
{
	struct sx_path_source* ps;
	sx_path_source_create(&ps);
	//set the type of path source:
	ps->type=PATH_SOURCE_TYPE_SOURCE;
	// calc the dimension of the Monte-Carlo weight, 
	// one dimension for each surface SIDE and each wavelength:
	// the weight is multidimensional array , 
	// a concatenation of the surfaces' spectra: 
	// surface0_front_spectrum,surface0_back_spectrum,surface1_front_spectrum,surface1_back_spectrum,....
	ps->weight_dim=scene->surfaces->dim*2*scene->spectrum->nb_vals;
	//CAMERA_DIRECT
	//also add to the dimension the nb of cameras' pixels 
	//(multiply by the nb of wavelengths)
	for (int i=0;i<scene->nb_cameras;i++) 
	  ps->weight_dim+=scene->camera[i]->nbPixelsX*
			  scene->camera[i]->nbPixelsY*scene->spectrum->nb_vals;
	switch (source->type)
	{
		case SOURCE_TYPE_SPOT:
		case SOURCE_TYPE_SURFACE:
			//dimension=nb of surfaces*nb of wavelength of simulation spectrum:
			ps->weight=calloc(1,ps->weight_dim*sizeof(double));
			//same as weight:
			ps->sigma=calloc(1,ps->weight_dim*sizeof(double));
			ps->spectrum=source->spectrum;
			sx_path_source_create_wavelength_sorter(&(ps->wavelength_sorter),
					ps->spectrum);
			ps->starting_volume=source->starting_volume;
			ps->source=source;
			strcpy(ps->name,"path source of ");
			strcat(ps->name,source->name);
			(*ps_)=ps;
			break;
		default:
			printf("Only source of type spot can be used in direct path\n");
			break;
	}
	return 1;
}

/**
  Used for reverse path algorithms
  get a photon coming from a source and placed at photon->x
  photon2->w is set with the irrandiance (W/m²) for the wavelength photon->wavelength
  photon2->x is set with  position of the emission point on the source (if source not a sun)
  photon2->dir is the direction from source emission point to photon position
inputs:
source: the source
photon: the photon which position to go to from the source
outputs:
photon2: photon created at source sorted point in direction of photon->x
 */
int sx_source_sort_photon_passing_by_a_point(
		struct sx_photon** photon2_,
		struct ssp_rng* rng,
		struct sx_source* source,
		struct sx_photon* photon
		)
{
	struct sx_photon* photon2;//new photon starting at the source
	float photon2_point[3];
	float r1=0,r2=0,r3=0;
	//create a new photon:
	sx_photon_create(&photon2);
	strcpy(photon2->name,"photon from ");
	strcat(photon2->name,source->name);

	switch(source->type)
	{
		case SOURCE_TYPE_SUN:
			//printf("lambda=%f spectreval=%f power=%f\n",path->lambda,
			//	source->spectr(source->spectrum,path->lambda),source->power_);
			//sort a point in a sphere cap of half angle: source->angle/2
			ssp_ran_sphere_cap_uniform_float(rng,source->dir,
					cosf((float)PI/180.0f*source->angle/2.0f),photon2->dir,NULL);
			//set the photon radiance:
			photon2->w=source->radiance*source->spectr(source->spectrum,
					photon->lambda);//W/m²
			break;

		case SOURCE_TYPE_SPOT:
			//sort a point in the source disk:
			if (source->diameter>0) 
			{
				//sort the photon position in source disk in local coordinates:
				ssp_ran_uniform_disk_float(rng,source->diameter/2,source->dir,
						photon2->x,NULL);
				//set the coordinates in global frame:
				f3_add(photon2->x,photon2->x,source->pos);
			}
			else 
			{
				//set the photon position at the source position:
				f3_set(photon2->x,source->pos);
			}
			//calc the photon direction:
			f3_sub(photon2_point,photon->x,photon2->x);
			f3_normalize(photon2->dir,photon2_point);
			//check if the photon2 direction is inside the emission cone
			if (f3_dot(photon2->dir,source->dir)>cos(source->angle/2*PI/180.0))
			{
				//the photon2 direction is inside the emission cone
				photon2->w=source->radiance*source->spectr(source->spectrum,
						photon->lambda);
			}
			else photon2->w=0;//not in emission cone
#ifdef PRINT_ALL_STEPS
			printf("\t sort point in source spot:  (%g ,%g ,%g) \n",
					photon2->x[0],photon2->x[1],photon2->x[2]);
#endif
			break;

		case SOURCE_TYPE_SURFACE:
			//sort a point on the source surface
			r1=ssp_rng_canonical_float(rng);
			r2=ssp_rng_canonical_float(rng);
			r3=ssp_rng_canonical_float(rng);
			struct s3d_primitive prim;
			float uv[2];
			s3d_scene_view_sample(source->scnview,r1,r2,r3,&prim,uv);
			struct s3d_attrib attr0;
			s3d_primitive_get_attrib(&prim,S3D_POSITION,uv,&attr0);
			//printf("attrib pos : %f %f %f %f \n",attr0.value[0],attr0.value[1],attr0.value[2],attr0.value[3]);
			//set the photon position:
			for (int i=0;i<3;i++) photon2->x[i]=attr0.value[i];
			//get the normal of the surface at the sorted point:
			s3d_primitive_get_attrib(&prim,S3D_GEOMETRY_NORMAL,uv,&attr0);
			//printf("attrib normal : %f %f %f %f \n",attr0.value[0],attr0.value[1],attr0.value[2],attr0.value[3]);
			float n[3];//normal
			for (int i=0;i<3;i++) n[i]=attr0.value[i];
			f3_normalize(n,n);
			//to have normal in the same direction as the one given by blender
			f3_mulf(n,n,-1);
			//calc the photon2 direction:
			f3_sub(photon2_point,photon->x,photon2->x);
			f3_normalize(photon2->dir,photon2_point);
			//check if the ray is in the source emission cone:
			float cos1=f3_dot(photon2->dir,n);
			if (cos1>cos(source->angle/2*PI/180.0))
			{
				//the photon2 direction is inside the emission cone
				//printf("angle : %f\n",acos(cos1)*180/PI);
				photon2->w=source->radiance*
					source->spectr(source->spectrum,photon->lambda);
			}
			else
			{
				//the photon2 direction is outside the emission cone
				//set the photon radiance:
				photon2->w=0;// unit W/m²
			}
#ifdef PRINT_ALL_STEPS
	printf("sx_source_sort_photon_passing_by_a_point photon2 sorted\
 pos=(%g,%g,%g) dir=(%g,%g,%g)\n",photon2->x[0],photon2->x[0],photon2->x[0],
 photon2->dir[0],photon2->dir[1],photon2->dir[2]);
	printf("sx_source_sort_photon_passing_by_a_point normal=(%g,%g,%g) cos=%g\n", 
	 n[0],n[1],n[2],cos1);
#endif
			break;

		default:
			printf("sx_source_sort_photon_passing_by_a_point Error of source type (%d)\n",
					source->type);
			break;
	}

#ifdef PRINT_ALL_STEPS
	printf(" <...source sort photon...>photon2 pos=(%g,%g,%g) dir=(%g,%g,%g) w=%g \n", 
			photon2->x[0],photon2->x[1],photon2->x[2],
			photon2->dir[0],photon2->dir[1],photon2->dir[2],photon2->w);
#endif




	(*photon2_)=photon2;
	return 1;
}



