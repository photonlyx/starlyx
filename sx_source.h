/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SX_SOURCE_H_
#define SX_SOURCE_H_

#define MAX_NB_SOURCES 100


#include <mxml.h>

#include "sx_xml.h"
#include "sx_scene.h"
#include "sx_photon.h"
#include "sx_path_source.h"


enum {SOURCE_TYPE_SUN, SOURCE_TYPE_SPOT,SOURCE_TYPE_SURFACE};

struct sampled_data;

struct sx_source
{
	// can be: SOURCE_TYPE_SUN, SOURCE_TYPE_SPOT,SOURCE_TYPE_SURFACE
	int type;
	char name[MAX_NAME_LENGTH];
	//power of the source W
	float power;
	//radiant intensity (Intensité énergétique) W sr-1
	float intensity;
	//radiance in W m-2 sr-1 (for sun)
	float radiance;
	char spectrum_name[MAX_NAME_LENGTH];
	struct sampled_data* spectrum;
	//function that gives spectrum(lambda), lambda in nm:
	float (*spectr)(struct sampled_data* spectrum,float lambda); 
	float pos[3];//position of the source
	float dir[3];//direction of light
				 //diameter of the source (for laser) 
	float diameter;
	float area;//area of the sensor in m²
			   //angle of divergence (0 for laser, 360 for point)
	float angle;
	//used later when calc attenuation from one point to the source
	float attenuation;
	//starting volume name
	char volume_name[MAX_NAME_LENGTH];
	//starting volume
	struct sx_volume* starting_volume;
	//if the source type is SOURCE_SURFACE:
	//name of the OBJ file describing the source geometry
	char surface_filename[MAX_NAME_LENGTH];
	//pointer to the surface object 
	struct sx_surface* surface;
	//s3d objects:
	struct s3d_scene* s3d_scene;//the sub-scene  
	struct s3d_scene_view* scnview;//the sub-scene scene view
};

void sx_source_print(
		struct sx_source* source,
		FILE* stream
		);

int sx_source_create(
		struct sx_source **source_
		);

int sx_source_create_source_from_xml(
		struct sx_source ** p_p_s,
		mxml_node_t *node1,
		struct sx_scene* scene
		);

int sx_source_connect_spectra(
		struct sx_source* source,
		struct sx_scene* scene
		);

void sx_source_release(
		struct sx_source* s
		);

void sx_source_create_photon(
		struct sx_source * source,
		struct sx_photon** photon,
		struct ssp_rng* rng
		);

void sx_source_sort_photon_on_surface(
		struct sx_source * source,
		struct sx_photon* photon,
		float alpha,
		struct ssp_rng* rng
		);

int sx_source_create_sx_path_source(
		struct sx_source* source,
		struct sx_path_source** sx_path_source,
		struct sx_scene* scene);

int sx_source_sort_photon_passing_by_a_point(
		struct sx_photon** photon2_,
		struct ssp_rng* rng,
		struct sx_source* source,
		struct sx_photon* photon
		);

int sx_source_create_s3d_scene(
		struct sx_source* source
		);

int sx_source_add_surface_with_emission_material_to_scene(
		struct sx_source *source,
		struct sx_scene* scene,
		char* surface_name
		);


#endif /* SX_SOURCE_H_ */
