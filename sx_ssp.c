/*

   Copyright (C) 2024 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <math.h>
#include "star/ssp.h"
#include "sx_ssp.h"


float* sort_sphere_cap_cos_float_local
  (struct ssp_rng* rng, //random number generator
   float alpha, //emission cone half angle
   float sample[3],//sorted photon direction
   float* pdf) //pdf. Can be NULL => pdf not computed 
{
  float phi, cos_theta, sin_theta, v,sin_alpha;
  phi = ssp_rng_uniform_float(rng, 0, 2 * (float)PI);
  v = ssp_rng_canonical_float(rng);
  sin_alpha=sinf(alpha);
  sin_theta = sqrtf(v)*sin_alpha;
  cos_theta = sqrtf(1 - sin_theta*sin_theta);
  sample[0] = cosf(phi) * sin_theta;
  sample[1] = sinf(phi) * sin_theta;
  sample[2] = cos_theta;
  if (pdf) *pdf = 2*cos_theta*sin_theta / powf(sin_alpha,2);
  //printf(" alpha=%f   sample dir =(%g,%g,%g) \n",alpha,sample[0],sample[1],sample[2]);
  return sample;
}

float* sort_sphere_cap_cos_float
  (struct ssp_rng* rng, //random number generator
   const float up[3],//normalised axis of the emission cone
   float alpha, //emission cone half angle
   float sample[3],//sorted photon direction
   float* pdf) /* Can be NULL => pdf not computed */
{
  float basis[9];
  float sample_local[3];
  sort_sphere_cap_cos_float_local(rng,alpha,sample_local,pdf);
  return f33_mulf3(sample, f33_basis(basis, up), sample_local);
}



