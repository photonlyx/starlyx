/*
   Copyright (C) 2024 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SX_SSP_H_
#define SX_SSP_H_


float* sort_sphere_cap_cos_float_local
  (struct ssp_rng* rng, //random number generator
   float alpha, //emission cone half angle
   float sample[3],//sorted photon direction
   float* pdf) //pdf. Can be NULL => pdf not computed 
;
float* sort_sphere_cap_cos_float
  (struct ssp_rng* rng, //random number generator
   const float up[3],//normalised axis of the emission cone
   float alpha, //emission cone half angle
   float sample[3],//sorted photon direction
   float* pdf) /* Can be NULL => pdf not computed */
;




#endif /* SX_SSP_H_ */

