/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sx_surface.h"
#include "sx_scene.h"
#include "parsing.h"
#include "sx_mat_surf.h"
#include "lists.h"
#include "sx_image.h"
#include "sx_photon.h"


/**
 * create a  new surface 
 */
int sx_surface_create(struct sx_surface ** p_p_s)
{
	struct sx_surface* surface=calloc(1,sizeof(struct sx_surface));//create a new sx_surface
	strcpy(surface->name,"");
	strcpy(surface->file,"");
	surface->id=0;
	surface->volume1_id=0;
	surface->volume2_id=0;
	strcpy(surface->mat_names,"");
	surface->mat=NULL;
	surface->sensor=NULL;
	surface->source=NULL;
	surface->camera=NULL;
	surface->store_impacts=0;
	surface->impacts_list=0;
	surface->impact_list_index=0;
	(*p_p_s)=surface;
	return 1;
}


/**
 * create a surface from xml description
 */
int sx_surface_create_from_xml(struct sx_surface ** p_p_s,mxml_node_t *node1,char* project_path)
{
	//struct sx_surface* surface=calloc(1, sizeof(struct sx_surface));//create a new sx_surface
	struct sx_surface* surface;
	sx_surface_create(&surface);
	char name[MAX_NAME_LENGTH];
	char file[MAX_NAME_LENGTH];
	char file_with_path[MAX_NAME_LENGTH];
	if (!read_string_attribute2(node1,"NAME",name)) return 0;
	if (!read_string_attribute2(node1,"FILE",file)) return 0;
	strcpy(file_with_path,project_path);
	strcat(file_with_path,file);
	//check if file exist:
	//printf("sx_surface_create_from_xml   Open OBJ file: %s\n",file_with_path);
	FILE* obj_file=fopen(file_with_path,"r");
	if (!obj_file)
	{
		printf("surface %s: can't find file %s\n",name,file_with_path);
		return 0;
	}
	else fclose(obj_file);
	strcpy(surface->name,name);
	strcpy(surface->file,file_with_path);

	//find material_surface names:
	if (!read_string_attribute23(node1,"MATERIALS",surface->mat_names,1,0,
				MAX_NAME_LENGTH))return 0;

	//check the user wants to store the impacts of the paths on the surface:
	char buffer[MAX_NAME_LENGTH];
	if (read_string_attribute22(node1,"STORE_IMPACTS",buffer,0)) sscanf(buffer,"%d",&surface->store_impacts);
	//else  printf("STORE_IMPACTS set to default (%d)\n",surface->store_impacts);
	//if storing impacts asked, create the list of float_list :
	if (surface->store_impacts) 
	{
		printf("Storing the impacts on the surface %s \n",surface->name);
		d_list_create(&surface->impacts_list);
		sx_surface_create_new_impacts_list(surface);
	}
	surface->volume1_id=0;
	surface->volume2_id=0;

	(*p_p_s)=surface;
	return 1;
}


/**
 * print surface parameters
 */
void sx_surface_print(
		struct sx_surface* s,
		FILE* stream
		)
{
	fprintf(stream,"Surface: %s\n",s->name);
	fprintf(stream,"\t geometry file:%s\n",s->file);
	fprintf(stream,"\t id:%d\n",s->id);
	fprintf(stream,"\t volume 1 id:%d\n",s->volume1_id);
	fprintf(stream,"\t volume 2 id:%d\n",s->volume2_id);
	fprintf(stream,"\t materials:%s\n",s->mat_names);
	fprintf(stream,"\t\t front surface name:%s\n",s->mat[0]->name);
	fprintf(stream,"\t\t back surface name:%s\n",s->mat[1]->name);
}

int sx_surface_create_new_impacts_list(struct sx_surface* surface)
{
	struct float_list* impacts;
	float_list_create(&impacts);
	d_list_add(surface->impacts_list,impacts);
	return 1;
}






/**
 * find the materials used by the surface in the scene and connect them
 */
int sx_surface_connect_materials(struct sx_surface* surface,struct sx_scene* scene)
{
	char** list;
	int nb_words;
	parsing_parse_words(surface->mat_names,&list,&nb_words);
	if (nb_words>2)
	{
		printf("Surface %s : you can't specify more than 2 materials (front and back) !\n",surface->name);
		return 0;
	}
//create the list of surface materials (front and back)
		surface->mat=calloc(1,sizeof(struct sx_mat_surf*)*2);
	if (nb_words==0)
	{
		//create a material surface that don't modify the path:
		struct sx_mat_surf * no_surf;
		sx_mat_surf_create(&no_surf);
		strcpy(no_surf->name,"no_surf");
		no_surf->surface=surface;
		no_surf->type=SURFACE_TYPE_NO_SURFACE;
		no_surf->apply=surface_no_surface;
		surface->mat[0]=no_surf;
		surface->mat[1]=no_surf;
		scene->material_surface[scene->nb_material_surface++]=no_surf;
		printf("Add matsurf %s\n",no_surf->name);
	}
	else if (nb_words==1)
	{
			//one matetrial only is specified by the user
			//put it at both sides
			struct sx_mat_surf* ms=sx_scene_find_matsurf(scene,list[0]);
			if (ms==NULL)
			{
				printf("Surface %s : can't find material surface of name %s\n",surface->name,list[0]);
				return 0;
			}
			ms->surface=surface;
			surface->mat[0]=ms;
			surface->mat[1]=ms;
	}
	else
	{
		for (int i=0;i<2;i++)
		{
			struct sx_mat_surf* ms=sx_scene_find_matsurf(scene,list[i]);
			if (ms==NULL)
			{
				printf("Surface %s : can't find material surface of name %s\n",surface->name,list[i]);
				return 0;
			}
			ms->surface=surface;
			surface->mat[i]=ms;
		}
	}
	free(list);
	return 1;
}

/**
 * get the the volume ID other than the volume of ID volume_id
 */
int sx_surface_get_volume_id_other_than(struct sx_surface* s,int object_id)
{
	int r=-1;
	if (object_id!=s->volume1_id) r=s->volume1_id;
	else r=s->volume2_id;
	return r;
}

/**
 * add a volume associated to this surface
 */
int sx_surface_add_volume(struct sx_surface* s,int volume_id)
{
	if (s->volume1_id==0) s->volume1_id=volume_id; //no object attached yet
	else if (s->volume2_id==0) s->volume2_id=volume_id;//attach the second object
	return 1;
}

void sx_surface_release(struct sx_surface* s)
{
	free(s);
}

//BLURRING_ACCELERATION
/**
add the photon coordinates to the list of impacts
*/
void sx_surface_add_impact(struct sx_surface* surface,struct sx_photon* photon)
{
  //the pattern surface is hitted, store the impact:
  //printf("nb of impacts %ld\n",photon->impacts->dim/3);
  struct float_list* fl1=(struct float_list*)d_list_get_last(surface->impacts_list);
  if (fl1->is_closed)//first time that an impact is stored for this path
  {
    sx_surface_create_new_impacts_list(surface);
    fl1=d_list_get_last(surface->impacts_list);//replace the current impacts list by the new one
  }
  //add the path coords to the impact list:
  float_list_add(fl1,photon->x[0]);
  float_list_add(fl1,photon->x[1]);
  float_list_add(fl1,photon->x[2]);
}

//BLURRING_ACCELERATION
/**
add the photon weight at the end of the impacts list
*/
void sx_surface_add_weight(struct sx_surface* surface,struct sx_photon* photon)
		{
			//get the current list of impacts:
			struct float_list* fl1=(struct float_list*)d_list_get_last(surface->impacts_list);
			if (fl1->is_closed)//first time that an impact is stored for this path
			{
				sx_surface_create_new_impacts_list(surface);
				fl1=d_list_get_last(surface->impacts_list);//replace the current impacts list by the new one
			}
			//add the path weight at the end of the list:
			float_list_add(fl1,(float)photon->w);
			fl1->is_closed=1;
		}


//BLURRING_ACCELERATION
/**
save the impacts in a file of name impacts_[surface name].txt
*/
int sx_surface_export_impacts(
	struct sx_surface* surface,
	char* path
	)
{
	char full_file_name[2*MAX_NAME_LENGTH];
	strcpy(full_file_name,path);
	strcat(full_file_name,"impacts_");
	strcat(full_file_name,surface->name);
	strcat(full_file_name,".txt");
	printf("Save %s\n",full_file_name);
	FILE* file1 = fopen(full_file_name, "w");
	for (unsigned int i=0;i<surface->impacts_list->dim;i++)
	{
		struct float_list* impact=d_list_get(surface->impacts_list,i);
		for (unsigned int k=0;k<impact->dim;k++)
			fprintf(file1,"%f ",impact->data[k]);
		fprintf(file1,"\n");
	}
	fclose(file1);
	return 1;
}

void sx_surface_export_impacts2(
		struct sx_surface* surface,
		struct sx_scene* scene
		)
{
/*
	char full_file_name[MAX_NAME_LENGTH];
	strcpy(full_file_name,file_path);
	strcat(full_file_name,filename);
*/
	char full_file_name[2*MAX_NAME_LENGTH];
	strcpy(full_file_name,scene->file_path);
	strcat(full_file_name,"impacts_");
	strcat(full_file_name,surface->name);
	strcat(full_file_name,".txt");
	printf("Save %s\n",full_file_name);
	FILE* file=fopen(full_file_name,"w");
	int nb_path=0;
	//store the full path history
	//fprintf(file,"path_nb\tinteraction_type\tpos_x\tpos_y\tpos_z\tdir_x\tdir_y\tdir_z\tdistance\tsurface_name\tvolume_name\tweight\n");
	//for the case of no surface or volume at interaction:
	char s_null[5];
	strcpy(s_null,"null");
	//intercation type
	//char it[200];
	for (unsigned int i=0;i<scene->stored_photons->dim;i++)
	{
		struct sx_photon* photon=scene->stored_photons->data[i];
		struct d_list* h=photon->history;
		for (unsigned int j=0;j<h->dim;j++)
		{
			struct sx_interaction* in=h->data[j]; 
			char* sn;//surface name
			if (in->s!=NULL) sn=in->s->name;
			else sn=s_null;
/*
			char* vn;//volume name
			if (in->v!=NULL) vn=in->v->name;
			else vn=s_null;
			sx_photon_get_flag(it,in->type);
			fprintf(file,"%d\t%s\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%s\t%s\t%f\n",
					nb_path,it,
					in->pos[0],in->pos[1],in->pos[2],
					in->dir[0],in->dir[1],in->dir[2],
					in->distance,
					sn,vn,in->w
				   );
*/
			if (strcmp(sn,surface->name)==0)
			{
				fprintf(file,"%f %f %f ",in->pos[0],in->pos[1],in->pos[2]);
			}
		}
		//store the weight at the end of the path:
		struct sx_interaction* in_last=h->data[h->dim-1]; 
		fprintf(file,"%f \n",in_last->w);
		nb_path++;
	}
	printf("Store %d paths in file %s\n",nb_path,full_file_name);

	fclose(file);

	//printf("The paths contains %d vertices (%d bytes)\n",nbVertices,nbVertices*sizeof(float)*3);
}



/**
  return 1 if surface is opaque, 0 if not
 */
int sx_surface_is_opaque(
		struct sx_surface* s
		)
{
	return (
			sx_mat_surf_is_opaque(s->mat[0])
			||
			sx_mat_surf_is_opaque(s->mat[1])
			);
}

