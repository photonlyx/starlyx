/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SX_SURFACE_H_
#define SX_SURFACE_H_

#include <mxml.h>

#include "sx_xml.h"
#include "sx_scene.h"
#include "sx_photon.h"

struct sx_surface
{
	char name[MAX_NAME_LENGTH];
	char file[MAX_NAME_LENGTH];
	int id;//id of the surface
	int volume1_id;//id of the volume that this surface is part of
				   //id of the volume that this surface is part of, 
				   //-1 if this surface belongs to only one volume;
	int volume2_id;

	char mat_names[MAX_NAME_LENGTH];//list of the surface material names
	struct sx_mat_surf** mat;//list of surface materials: front(0) and back(1)
	void* sensor;//pointer to sensor to which this surface belongs
	void* source;//pointer to source to which this surface belongs
	void* camera;//pointer to camera to which this surface belongs
	int store_impacts;
	//BLURRING_ACCELERATION
	struct d_list* impacts_list;//list (as many as paths) of list of impacts on the surface
	long impact_list_index;
};

void sx_surface_print(
		struct sx_surface* s,
		FILE* stream
		);

int sx_surface_create(
		struct sx_surface ** p_p_s
		);

int sx_surface_create_from_xml(
		struct sx_surface ** p_p_s,
		mxml_node_t *node1,
		char* project_path
		);

int sx_surface_create_new_impacts_list(
		struct sx_surface* surface
		);

int sx_surface_connect_materials(
		struct sx_surface* surface,
		struct sx_scene* scene
		);

int sx_surface_get_volume_id_other_than(
		struct sx_surface* s,
		int volume_id
		);

int sx_surface_add_volume(
		struct sx_surface* s,
		int volume_id
		);

void sx_surface_release(
		struct sx_surface* s
		);

void sx_surface_add_impact(
		struct sx_surface* surface,
		struct sx_photon* photon
		);

void sx_surface_add_weight(
		struct sx_surface* surface,
		struct sx_photon* photon
		);

int sx_surface_export_impacts(
		struct sx_surface* surface,
		char* path
		);

void sx_surface_export_impacts2(
		struct sx_surface* surface,
		struct sx_scene* scene
		);

int sx_surface_is_opaque(
		struct sx_surface* s
		);

#endif /* SX_SURFACE_H_ */
