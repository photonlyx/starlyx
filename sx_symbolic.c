
/*
   Copyright (C) 2022 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "float_list_symbolic.h"
#include "sx_symbolic.h"
#include "sx_scene.h"
#include "sampled_data.h"
#include "sx_surface.h"
#include "sx_mat_vol.h"


/**
 * return the index of the  corresponding order, -1 if not found
 */
int  sx_symbolic_get(struct float_list_symbolic* orders,float scat_order,float abs_order,float null_order)
{
	for (int i=0;i<orders->index;i++)
	{
		float* l=orders->list[i];
		if ((scat_order==l[1])&&(abs_order==l[2])&&(null_order==l[3]))
		{
			return i;
		}
	}
	return -1;
}


/**
calulate the polynomial function from the paths
*/
void sx_symbolic_calc_polynom(struct sx_scene* scene,struct float_list_symbolic*** polynoms_,int* nb_polynoms)
{
	struct float_list_symbolic* list=scene->path_symbolic_events;
	/*
	//print the list of paths, with the wavelength, the index of the surface hitted at end, the nb of scattering events, the nb of absortion events and the number of null collisions
	printf("(lambda,#surface,nb_scat,nb_abs,nb_null)\n");
	//float_list_symbolic_print(list);

	for (int k=0;k<list->size;k++) 
	{
	float* element=float_list_symbolic_get(list,k);
	if (element[1]!=-1)//the path has not finished at infinite
	{
	float_list_symbolic_print_element(list,k);
	}
	}
	 */
	int print=0;	
	for (int j=0;j<scene->spectrum->nb_vals;j++)
		for (int i_surf=0;i_surf<scene->surfaces->dim;i_surf++)
		{
			float lambda=scene->spectrum->x[j];
			int index=j*scene->spectrum->nb_vals+i_surf;
			long nb_paths_hitted=0;
			double order_scat_mean=0;
			double order_abs_mean=0;
			double order_null_mean=0;
			//int n=scene->nb_photons;
			for (int k=0;k<list->size;k++) 
			{
				float* element=float_list_symbolic_get(list,k);
				if ((element[0]==lambda)&&(element[1]==i_surf))
				{
					order_scat_mean+=element[2];
					order_abs_mean+=element[3];
					order_null_mean+=element[4];
					nb_paths_hitted++;
				}
			}
			if (print) if (nb_paths_hitted!=0)
			{
				double wnull=(double)nb_paths_hitted/(double)scene->nb_photons;
				if(print) 
				{
					struct sx_surface* surf=scene->surfaces->data[i_surf];
					printf("surface of index %d  lambda=%g surface=%s   Weight just using the null collision algorithm: %g\n",index,lambda,surf->name,wnull);
				}
				order_scat_mean/=nb_paths_hitted;
				order_abs_mean/=nb_paths_hitted;
				order_null_mean/=nb_paths_hitted;
				if (print) printf("scat mean polynom order: %g\n",order_scat_mean);
				if (print) printf("abs mean polynom order: %g\n",order_abs_mean);
				if (print) printf("null mean polynom order: %g\n",order_null_mean);

			}
		}




	// create the polynom for each wavelength and surface
	// gather the repeated power combinations
	int nb=scene->spectrum->nb_vals*scene->surfaces->dim;
	*nb_polynoms=nb;
	struct float_list_symbolic** polynoms=(struct float_list_symbolic**)calloc(1,nb*sizeof(struct float_list*));
	(*polynoms_)=polynoms;
	for (int j=0;j<scene->spectrum->nb_vals;j++)
	{
		float lambda=scene->spectrum->x[j];
		for (int i_surf=0;i_surf<scene->surfaces->dim;i_surf++)
		{
			//create a list of orders possibilities of the polynom
			//The order of the parameters is: (occurence,order_scat,order_abs,order_null)
			struct float_list_symbolic* polynom;
			char *label=(char*)calloc(1,500*sizeof(char));
			struct sx_surface* surf=scene->surfaces->data[i_surf];
			sprintf(label,"lambda=%gnm_surface:%s",lambda,surf->name);
			float_list_symbolic_create2(&polynom,4,1000,label);
			for (int k=0;k<list->size;k++) 
			{
				float* element=float_list_symbolic_get(list,k);
				if ((element[0]==lambda)&&(element[1]==i_surf))
				{
					float order_scat=element[2];
					float order_abs=element[3];
					float order_null=element[4];
					int index=sx_symbolic_get(polynom,order_scat,order_abs,order_null);
					if (index!=-1)
					{
						polynom->list[index][0]++;
					}
					else
					{
						float* order=(float*)calloc(1,sizeof(float)*polynom->dim);
						order[0]=1;//occurence
						order[1]=order_scat;//order for scattering
						order[2]=order_abs;//order for absorption
						order[3]=order_null;//order for null collision
						float_list_symbolic_add(polynom,order);
					}
				}
			}	
			polynoms[j*scene->spectrum->nb_vals+i_surf]=polynom;
		}
	}
	if (print) 
	{
		//print the polynoms:
		for (int j=0;j<scene->spectrum->nb_vals;j++)
			for (int i_surf=0;i_surf<scene->surfaces->dim;i_surf++)
			{
				float lambda=scene->spectrum->x[j];
				int index=j*scene->spectrum->nb_vals+i_surf;
				struct float_list_symbolic* polynom=polynoms[index];
				struct sx_surface* surf=scene->surfaces->data[i_surf];
				printf("lambda %f surface: %s\n",lambda,surf->name);
				printf("Non null symbolic polynom components:\n");
				printf("(Occurence,power_scat,power_abs,power_null)\n");
				float_list_symbolic_print(polynom);
				//calc the weight just using the null collision algorithm :
				long nb_paths_hitted=0;
				for (int k=0;k<polynom->index;k++) 
				{
					float *el=polynom->list[k];
					int occurrence=(int)el[0];
					nb_paths_hitted+=occurrence;
				}
			}
	}

	if (print) 
	{
		//check with a variation of k for 1rst wavelength and surface 0:
		//int index=5;//index of the polynom
		float lambda=scene->spectrum->x[0];
		struct sx_mat_vol* mv=scene->mv[0];//take the first radiative properties 
		printf("hg: %s\n",mv->name);
		printf("k_hat=%g\n",scene->k_hat);
		float k_abs=mv->ka(mv,lambda);
		printf("k_abs=%g\n",k_abs);
		float ksmin=scene->k_hat/10;
		float ksmax=scene->k_hat*0.95;
		int n=50;
		float sxlog=(log(ksmax)-log(ksmin))/n; 
		printf("k_scat\t");
		for (int j=0;j<nb;j++) printf("%s\t",polynoms[j]->label);	
		printf("\n");
		for (int i=0;i<n;i++)
		{
			//printf("polynom: %s\n",polynoms[i]->label);
			//take this k:
			//float k_scat=scene->k_hat/pow(1.1,i);
			float k_scat=exp(log(ksmin)+sxlog*i);
			float p_null=1-scene->p_scat-scene->p_abs;
			printf("%g\t",k_scat);
			for (int j=0;j<nb;j++) 
			{
				float w=sx_symbolic_polynom_value(polynoms[j],scene->nb_photons,scene->k_hat,scene->p_scat,scene->p_abs,p_null,k_scat,k_abs);
				printf("%g\t",w);
			}
			printf("\n");
		}
	}
}


/**
 *
 * polynom: the polynom, a list of (occurence,power_scat,power_abs,power_null)
 * n the total number of paths
 * k_hat the scattering coef used for null collision method
 * p_scat the constant probability for scattering event used in null collision method
 * p_abs the constant probability for absorption event used in null collision method
 * p_null the constant probability for null collision  event used in null collision method (p_null=1-p_scat-p_abs)
 * k_scat the scattering coef for which we want the polynom value
 * k_abs the absorption coefficient for which we want the polynom value
 * 
 * return: the value of the polynom for k_scat and k_abs
 */
float sx_symbolic_polynom_value(struct float_list_symbolic* polynom,int n,float k_hat,float p_scat,float p_abs,float p_null,float k_scat,float k_abs)
{
	double w=0;
	double r=1;
	float k_null=k_hat-k_scat-k_abs;
	for (int i=0;i<polynom->index;i++)
	{
		float* element=polynom->list[i];
		int occurrence=(int)element[0];
		double power_scat=(double)element[1];
		double power_abs=(double)element[2];
		double power_null=(double)element[3];
		r=1;
		if (power_scat!=0) r*=pow((double)k_scat/(double)k_hat/(double)p_scat,power_scat);
		if (power_abs!=0)  r*=pow((double)k_abs/(double)k_hat/(double)p_abs,power_abs);
		if (power_null!=0) r*=pow((double)k_null/(double)k_hat/(double)p_null,power_null);
		w+=r*occurrence;
		//n+=occurrence;
	}
	if (n!=0) w/=n;
	return w;
}

/**
 * the average weight directly calculated from the paths events
 */
float sx_symbolic_value(struct float_list_symbolic* path_symbolic_event,int n,float k_hat,float p_scat,float p_abs,float p_null,float k_scat,float k_abs)
{
	double w=0;
	double r=1;
	float k_null=k_hat-k_scat-k_abs;
	for (int i=0;i<path_symbolic_event->index;i++)
	{
		float* element=path_symbolic_event->list[i];
		double power_scat=(double)element[2];
		double power_abs=(double)element[3];
		double power_null=(double)element[4];
		r=1;
		if (power_scat!=0) r*=pow((double)k_scat/(double)k_hat/(double)p_scat,power_scat);
		if (power_abs!=0)  r*=pow((double)k_abs/(double)k_hat/(double)p_abs,power_abs);
		if (power_null!=0) r*=pow((double)k_null/(double)k_hat/(double)p_null,power_null);
	}
	if (n!=0) w/=n;
	return w;
}

