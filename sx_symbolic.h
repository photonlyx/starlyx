
/*
Copyright (C) 2022 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SYMBOLIC_H_
#define SYMBOLIC_H_


#include "sx_scene.h"



enum{SYMBOLIC_SCATTERING,SYMBOLIC_ABSORPTION,SYMBOLIC_NULL_COLLISION};


int sx_symbolic_has(struct float_list_symbolic* orders,float* element);
void sx_symbolic_calc_polynom(struct sx_scene* scene,struct float_list_symbolic*** polynoms,int* nb_polynoms);
float sx_symbolic_polynom_value(struct float_list_symbolic* polynom,int n,float k_hat,float p_scat,float p_abs,float p_null,float k_scat,float k_abs);





#endif /* SYMBOLIC_H_ */
