/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>

#include "sx_variation.h"

#include "sx_monte_carlo.h"
#include "sampled_data.h"
#include "sx_scene.h"
#include "sx_path_source.h"
#include "sx_surface.h"
#include "sx_mat_vol.h"
#include "sx_volume.h"



/**
 * create a new sx_variation using xml
 */
int sx_variation_create(struct sx_variation** variation, mxml_node_t *tree)
{
	int found_a_variation=0;//1 if xml contains a Variation object
	*variation=calloc(1,sizeof(struct sx_variation));
	if (tree)
	{
		mxml_node_t *node1,*top;
		node1=tree;
		top=tree;
		for (;;)
		{
			node1 = mxmlWalkNext(top, tree,MXML_DESCEND);
			top=NULL;
			if (node1)
			{
				//printf("::::::: node %s\n",node1->value.element.name);//keep it for debug
				if (strcmp(node1->value.element.name,"Variation")==0)//found a variation
				{
					//<Variation NAME="variation1" OBJECT="mat_vol2" ATTRIBUTE="K" MIN="0.1" MAX="10" LOG="1"></Variation>
					//get the name
					char string[MAX_NAME_LENGTH];
					if (!read_string_attribute2(node1,"NAME",string))return 0;
					strcpy((*variation)->name,string);
					//get the object:
					if (!read_string_attribute2(node1,"OBJECT",string))return 0;
					strcpy((*variation)->object,string);
					//get the attribute to vary:
					if (!read_string_attribute2(node1,"ATTRIBUTE",string))return 0;
					strcpy((*variation)->attribute,string);
					//get the min value:
					if (!read_string_attribute2(node1,"MIN",string))return 0;
					sscanf(string,"%f",&(*variation)->min);
					//get the max value:
					if (!read_string_attribute2(node1,"MAX",string))return 0;
					sscanf(string,"%f",&(*variation)->max);
					//get the log boolean:
					if (!read_string_attribute2(node1,"LOG",string))return 0;
					sscanf(string,"%d",&(*variation)->log);
					//get the dim:
					if (!read_string_attribute2(node1,"DIM",string))return 0;
					sscanf(string,"%d",&(*variation)->dim);
					//get the wavelength:
					if (!read_string_attribute2(node1,"LAMBDA",string))return 0;
					sscanf(string,"%f",&(*variation)->lambda);
					found_a_variation=1;
				}
			}
			else
			{
				break;
			}
			if (!top) top=node1;
		}
	}
	if (found_a_variation) return 1;else return 0;
}



void sx_variation_release(struct sx_variation* variation)
{
free(variation);
}

/**
 * print the sx_variation object
 */
void sx_variation_print(const struct sx_variation* variation)
{
	printf("variation: %s\n",variation->name);
	printf("\t object: %s\n",variation->object);
	printf("\t attribute: %s\n",variation->attribute);
	printf("\t min: %f\n",variation->min);
	printf("\t max: %f\n",variation->max);
	printf("\t log: %d\n",variation->log);
	printf("\t dim: %d\n",variation->dim);
	printf("\t lambda: %f\n",variation->lambda);
}



/**
 * vary the target attribute and run an MC for each value
 */
int sx_variation_study(const struct sx_variation* variation,struct sx_scene* scene)
{
	int res;
	float x,dx;
	int n=variation->dim;
	struct sampled_data* param_spectrum=NULL;  //spectrum of the parameter to vary

	//find in the scene the spectrum belonging to "object" and having  name "attribute"
	struct sx_mat_vol* mv;
	struct sx_volume* v;
	mv=sx_scene_find_mv(scene,variation->object);
	if (mv!=NULL)
		{
		param_spectrum=sx_mat_vol_get_spectrum_of_attribute(mv,variation->attribute,1);
		if (param_spectrum==NULL) return 0;
		}
	else
		{
		v=sx_scene_find_volume(scene,variation->object);
		if (v!=NULL) param_spectrum=sx_volume_get_spectrum_of_attribute(v,variation->attribute,0);
		else
			{
			fprintf(stderr, "Can't find attribute %s in object %s\n",variation->attribute,variation->object);
			return 0;
			}
		}
	if (variation->log)
	{
		dx=(log10f(variation->max)-log10f(variation->min))/((float)n-1.0f);
	}
	else
	{
		dx=(variation->max-variation->min)/(float)(n-1);
	}

	char filename_w[2*MAX_NAME_LENGTH];
	sprintf(filename_w,"%svariation_weight.txt",scene->file_path);
	FILE* file_weight = fopen(filename_w, "w");
	fprintf(file_weight,"%s_%s\t",variation->object,variation->attribute);
	for (int i=0;i<scene->surfaces->dim;i++) 
		{
			struct sx_surface* surf=scene->surfaces->data[i];
			fprintf(file_weight,"%s\t",surf->name);
		}
	fprintf(file_weight,"\n");

	char filename_s[2*MAX_NAME_LENGTH];
	sprintf(filename_s,"%svariation_sigma.txt",scene->file_path);
	FILE* file_sigma = fopen(filename_s, "w");
	fprintf(file_sigma,"%s_%s\t",variation->object,variation->attribute);
	for (int i=0;i<scene->surfaces->dim;i++) 
		{
			struct sx_surface* surf=scene->surfaces->data[i];
			fprintf(file_sigma,"%s\t",surf->name);
		}
	fprintf(file_sigma,"\n");

	for (int i=0;i<n;i++)
	{
		if (variation->log)
		{
			float log10x=log10f(variation->min)+(float)i*dx;
			x=powf(10,log10x);
		}
		else
		{
			x=variation->min+(float)i*dx;
		}
		if (scene->verbose) printf("sx_variation %d/%d %s=%f\n",i+1,n,variation->attribute,x);
		//get the index of the wavelength:
		int index=-1;
		if (param_spectrum->nb_vals==1) index=0;//spectrum with just one wavelength
		else index=sampled_data_index_of(param_spectrum,variation->lambda);
		if (index==-1)
		{
			printf("Variation. Can't find wavelength %f in %s\n",variation->lambda,param_spectrum->name);
			return(0);
		}
		//set the varying parameter:
		float save=param_spectrum->y[index];
		param_spectrum->y[index]=x;
		//compute Monte-Carlo:
		res = sx_montecarlo_compute(scene);
		//export the results:
		//struct sx_path_source* sx_path_source=scene->sx_path_sources[0];
		struct sx_path_source* sx_path_source=scene->path_sources->data[0];
		//fill the weight file:
		fprintf(file_weight,"%f\t",x);
		//for (int ii=0;ii<scene->nb_surfaces;ii++) fprintf(file_weight,"%f\t",sx_path_source->weight[ii]);
		for (int ii=0;ii<scene->surfaces->dim;ii++) fprintf(file_weight,"%f\t",sx_path_source->weight[ii]);
		fprintf(file_weight,"\n");
		//fill the sigma file:
		fprintf(file_sigma,"%f\t",x);
		//for (int ii=0;ii<scene->nb_surfaces;ii++) fprintf(file_sigma,"%f\t",sx_path_source->sigma[ii]);
		for (int ii=0;ii<scene->surfaces->dim;ii++) fprintf(file_sigma,"%f\t",sx_path_source->sigma[ii]);
		fprintf(file_sigma,"\n");

		if (res==0) 
			{	
			printf("Error in Monte-Carlo\n");
			return 0;
			}
		param_spectrum->y[index]=save;
	}
	if (scene->verbose) printf("Save %s\n",filename_w);
	if (scene->verbose) printf("Save %s\n",filename_s);

	fclose(file_weight);
	fclose(file_sigma);
	return 1;
}





