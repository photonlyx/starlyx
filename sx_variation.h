/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SX_VARIATION_H_
#define SX_VARIATION_H_

#include "sx_xml.h"
#include "sx_scene.h"

struct sampled_data;

//variation of an object attribute of the scene:
struct sx_variation
{
	char name[MAX_NAME_LENGTH];
	char object[MAX_NAME_LENGTH];//name of the object having the attribute to vary
	char attribute[MAX_NAME_LENGTH];//name of the object attribute to vary
	float min;//min value of the attribute
	float max;//max value of the attribute
	int log;//if 0 vary attribute linearly, if 1 vary in log
	int dim;//nb of MC calculations
	float lambda;//wavelength
};


int sx_variation_create(struct sx_variation** variation, mxml_node_t *tree);

void sx_variation_release(struct sx_variation* variation);

void sx_variation_print(const struct sx_variation* variation);

int sx_variation_study(const struct sx_variation* variation,struct sx_scene* scene);







#endif /* SX_VARIATION_H_ */
