/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <star/ssp.h>

#include "sx_volume.h"
#include "sx_xml.h"
#include "sx_scene.h"
#include "sampled_data.h"
#include "sx_mat_vol.h"
#include "sx_polydispersity.h"

int sx_volume_create( struct sx_volume ** p_p_vol)
{
	struct sx_volume*  vol=calloc(1,sizeof(struct sx_volume));
	vol->n=&sampled_data_discrete;//associate the pointer to function n(lambda) to a discrete function
	(* p_p_vol)=vol;
	return 1;
}

int sx_volume_create_from_xml( struct sx_volume ** p_p_vol,mxml_node_t *node)
{
	struct sx_volume* vol;
	sx_volume_create(&vol);
	//get the name
	//char name[MAX_NAME_LENGTH] = {'\0'};
	if (!read_string_attribute2(node,"NAME",vol->name))return 0;
	//read surfaces names:
	if (!read_string_attribute23(node,"SURFACES",vol->surface_names,1,1,MAX_SURFACES_NAME_LENGTH)) return 0;
	//find refractive index name or value
	if (!read_string_attribute2(node,"N",vol->spectrum_n_name))return 0;
	//find material volume names:
	if (!read_string_attribute23(node,"MATERIALS",vol->scats_names,1,0,
				MAX_MAT_VOL_NAMES_LENGTH)) return 0;
	(* p_p_vol)=vol;
	return 1;
}

void sx_volume_print(
		struct sx_volume* v,
		FILE* stream
		)
{
	fprintf(stream,"volume: %s (ID=%d)\n",v->name,v->id);
	fprintf(stream,"\t sons IDs: ");
	for (int i=0;i<v->nbSons;i++) fprintf(stream,"%d ",v->indexSons[i]); 
	fprintf(stream,"\n");
	fprintf(stream,"\t father index: %d\n",v->indexFather);
	fprintf(stream,"\t surfaces names:%s\n",v->surface_names);
	fprintf(stream,"\t surfaces IDs: ");
	for (int i=0;i<v->nb_surfaces;i++) fprintf(stream,"%d ",v->surfaces_id[i]); 
	fprintf(stream,"\n");
	fprintf(stream,"\t refraction index:%s\n",v->spectrum_n_name);
	fprintf(stream,"\t materials names:%s\n",v->scats_names);
	fprintf(stream,"\t materials name and proba "); 
	for (int i=0;i<v->nb_mv;i++) fprintf(stream,"%s ",v->mv[i]->name);
	fprintf(stream,"\n");
}

/**
 * find the spectra (of refractive index) used by the volume in the scene and connect them
 */
int sx_volume_connect_spectra(struct sx_volume* v,struct sx_scene* scene)
{
	struct sampled_data* sp;
	int is_numerical=0;
	//find or create spectrum of n:
	sp=sx_scene_find_spectrum2(scene,v->spectrum_n_name,&is_numerical);
	if (sp==NULL)return 0;
	if (is_numerical) v->n=&sampled_data_constant;else v->n=&sampled_data_discrete;
	v->spectrum_n=sp;
	return 1;
}

/**
 * find the  the material volume in the scene and connect them
 */
int sx_volume_connect_mv(struct sx_volume* vol,struct sx_scene* scene)
{
	char** list;
	int nb_words=0;
	parsing_parse_words(vol->scats_names,&list,&nb_words);
	//create a dynamic list of materials of volume:
	struct d_list* mvlist;
	d_list_create(&mvlist);
	//count the number of materials of volume
	for (int i=0;i<nb_words;i++)
	{
		//try to find this name as a mv collection:
		struct sx_mat_vol_collection* mv_col=sx_scene_find_mv_collection(scene,list[i]);
		if (mv_col!=NULL)
		{
			//add the materials of volume of this collection to the list:
			for (int j=0;j<mv_col->n;j++)
			{
				d_list_add(mvlist,mv_col->array[j]);
			}
		}
		else
		{
			//try to find this name as a simple mv:
			struct sx_mat_vol* mv=sx_scene_find_mv(scene,list[i]);
			if (mv!=NULL)
			{
				//add the material of volume to the list:
				d_list_add(mvlist,mv);
			}
			else
			{
				printf("Volume %s. Can`t find material volume (or collection) of name %s\n",vol->name,list[i]);
				free(list);
				return 0;
			}
		}
	}
	//transfer the list to the scene's list:
	vol->nb_mv=mvlist->dim;
	vol->mv=calloc(1,sizeof(struct sx_mat_vol*)*(vol->nb_mv));
	for (int i=0;i<mvlist->dim;i++)
	{
		vol->mv[i]=mvlist->data[i];
		vol->mv[i]->volume=vol;
	}
	//free the list of mv:
	d_list_release(mvlist);
	//free the list of words:
	for (int i=0;i>nb_words;i++) free(list[i]);
	free(list);
	return 1;
}


/**
 * create the object containing all the sx_scene
 */
int sx_volume_create_world(struct sx_volume** pw)
{
	*pw=calloc(1, sizeof(struct sx_volume));//create a new object
	struct sx_volume* w=*pw;
	w->id=0;
	w->indexFather=-1;
	strcpy(w->name,"World");
	strcpy(w->surface_names,"");

	//create a constant spectrum refractive index:
	strcpy(w->spectrum_n_name,"1");
	struct sampled_data*spn;
	sampled_data_create2( &spn,1,"uniform n");
	spn->x[0]=0;
	spn->y[0]=1;
	w->n=&sampled_data_constant;
	w->spectrum_n=spn;

	//create a constant spectra scatterer sx_mat_vol
	struct sx_mat_vol* mv;
	sx_mat_vol_create_with_constant_spectra(&mv,(float)0.0,(float)0.0,(float)0.0);
	w->nb_mv=1;
	w->mv=calloc(1,sizeof (struct sx_mat_vol*)*1);
	w->mv[0]=mv;

	return 1;
}

void sx_volume_release(struct sx_volume* v)
{
	for (int i=0;i<v->nb_mv;i++) sx_mat_vol_release(v->mv[i]);
	free(v->mv);
	free(v);
}



/**
 * get the spectrum of attribute "attribute"
 * if verbose is 1, print error message if not found
 */
struct sampled_data* sx_volume_get_spectrum_of_attribute(struct sx_volume * v,
                                          const char* name,const int verbose)
{
	struct sampled_data* sd=NULL;
	if (strcmp(name,"N") == 0) sd=v->spectrum_n;
	if (verbose) 
    if (sd==NULL) 
      fprintf(stderr, "Object %s has no attribute of name %s\n",v->name,name);
	return sd;
}

/**
 * get the  extinction coefficient (sum of scattering and absorption coefs)
 */
float sx_volume_k_ext(struct sx_volume * v,float lambda)
{
	float kext=0;
	for (int i=0;i<v->nb_mv;i++)
		{
		struct sx_mat_vol* mv=v->mv[i];
		kext+=mv->ks(mv,lambda)+mv->ka(mv,lambda);
		}
	return kext;
}



/**
  sort the distance of the next (scattering or absorption) event
  sort absoption and scattering next event distance for each material
  and choose the smaller one.
output:
  d: distance sorted
  isAbsorbed: 0 if next event is a scattering, 1 if is an absorption
  mv_sorted: material sorted
input:
  v volume: where is the photon
  lambda : wavelength
  rng : random generator
 */
int sx_volume_sort_next_distance(float* d,int *isAbsorbed,
              struct sx_mat_vol** mv_sorted,struct sx_volume * v,
              float lambda,struct ssp_rng* rng)
{
	struct sx_mat_vol *mv;
	float dmin=1e10,ks,ka;
	(*mv_sorted)=NULL;
	//printf("Volume %s has %d mat vol (lambda=%f) \n",v->name,v->nb_mv,lambda);
	for (int i=0;i<v->nb_mv;i++)
	{
		mv=v->mv[i];
		//sort the distances to the next scattering event 
    //and the next absorption event:
		ks=mv->ks(mv,lambda);
		if (ks!=0)
		{
			mv->free_path=(float)ssp_ran_exp(rng,ks);
			if (mv->free_path<dmin)
			{
				dmin=mv->free_path;
				*isAbsorbed=0;
				(*mv_sorted)=mv;
			}
		}
		ka=mv->ka(mv,lambda);
		//if (ka!=0) printf("sx_volume_sort_next_distance %s ka=%f\n",v->name,ka);
		if (ka!=0)
		{
			mv->abs_path=(float)ssp_ran_exp(rng,ka);
			if (mv->abs_path<dmin)
			{
				dmin=mv->abs_path;
				*isAbsorbed=1;
				(*mv_sorted)=mv;
			}
		}
		//if (dmin<1000) printf("sx_volume_sort_next_distance dmin=%f isAbsorbed=%d \n",dmin,*isAbsorbed);
	}
	*d=dmin;
  v->mv_sorted=(*mv_sorted);
		//printf("volume: %s dmin=%f\n",v->name,dmin);
		//if (v->mv_sorted!=NULL) printf("%s dmin=%f ks=%f \n",v->mv_sorted->spectrum_C_name,dmin,v->mv_sorted->ks(v->mv_sorted,lambda));
		//if (v->mv_sorted!=NULL) sx_mat_vol_print(v->mv_sorted,stdout,"");
	return 1;
}

/**
calculate the next distance from k=ks0+ka0+ks1+ka1+ks2+ka2+...
and sort the material of volume using the weights ks and ka of each one
tell if the event is scaterring or absorption
*/
int sx_volume_sort_next_distance2(
		float* d,//next distance counting all the materials of volume
		int *isAbsorbed,//0 if scattering 1 if absorption
		struct sx_mat_vol** mv_sorted, //pointer to the sorted material
		struct sx_volume * v, //the volume
		int lambda, //the wavelength in nm
		struct ssp_rng* rng
		)
{
	if (v->mv_sorters[lambda]==NULL)
	{
		//printf("Creation sorter for volume %s and lambda=%d (%d mat vol)\n",v->name,lambda,v->nb_mv);
		//create the k sorter
		int nb_k=2*v->nb_mv;//nb of ks and ka of all the mats vol
		double w[nb_k];//the weights of the sorter are the k	
		for (int i=0;i<v->nb_mv;i++)
		{
			struct sx_mat_vol* mv=v->mv[i];
			w[2*i+0]=mv->ks(mv,(float)lambda);
			w[2*i+1]=mv->ka(mv,(float)lambda);
		}
		//normalisation:
		double sum=0;
		for (int k=0;k<nb_k;k++){sum+=w[k];}
		for (int k=0;k<nb_k;k++){w[k]/=sum;}
		//for (int k=0;k<nb_k;k++){printf("poids_%d=%g",k,w[k]);printf("\n");}
		v->k[lambda]=sum;
		if (ssp_ranst_discrete_create(NULL,&v->mv_sorters[lambda])!=RES_OK)
		{		
			printf("Error creation sorter\n");
			return 0;
		}
		ssp_ranst_discrete_setup(v->mv_sorters[lambda],w,(unsigned int)nb_k);
	}

	unsigned int k=ssp_ranst_discrete_get(rng,v->mv_sorters[lambda]);
	unsigned int index_mv=k/2;
	//printf("k=%d index_mv=%d\n",k,index_mv);

	(*d)=(float)ssp_ran_exp(rng,v->k[lambda]);
	(*isAbsorbed)=k%2;
	(*mv_sorted)=v->mv[index_mv];
	v->mv_sorted=(*mv_sorted);
	//if (v->mv_sorted!=NULL) printf("volume %s k=%f sorted:%s(k=%f) abs=%d dist=%f\n",v->name,v->k[lambda],(*mv_sorted)->name,(*mv_sorted)->ks(*mv_sorted,(float)lambda),(*isAbsorbed),*d);
	return 1;
}
