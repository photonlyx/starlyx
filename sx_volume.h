/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SX_VOLUME_H_
#define SX_VOLUME_H_

#define MAX_NB_SONS_PER_VOLUME 10000
#define MAX_NB_SURFACES_PER_VOLUME 10000
#define MAX_SURFACES_NAME_LENGTH 30000
#define MAX_MAT_VOL_NAMES_LENGTH 300

#include <stdint.h>
#include <star/ssp.h>

#include "mxml.h"
#include "sx_xml.h"
#include "sx_scene.h"

struct sx_mat_vol;

struct sx_volume
{
	int id;
	int indexFather;//index of volume containing this volume. -1 it is world
	int indexSons[MAX_NB_SONS_PER_VOLUME];
	int nbSons;
	char name[MAX_NAME_LENGTH];
	char surface_names[MAX_SURFACES_NAME_LENGTH];
	int surfaces_id[MAX_NB_SURFACES_PER_VOLUME];
	int nb_surfaces;
	//refractive index:
	char spectrum_n_name[MAX_NAME_LENGTH];
	struct sampled_data* spectrum_n;//refraction index
    //function that gives n(lambda), lambda in nm:
	float (*n)(struct sampled_data* spectrum_n,float lambda);
	char scats_names[MAX_MAT_VOL_NAMES_LENGTH];//list of volume material names
	int nb_mv;//nb of volume materials
	struct sx_mat_vol** mv;//list of pointers to volume materials
	struct sx_mat_vol* mv_sorted;//last sorted material
	struct ssp_ranst_discrete* mv_sorters[1000];//mat_vol sorters for all wavelengths
	float k[1000];//sum of all ks and ka
};

int sx_volume_create( struct sx_volume ** p_p_vol);

int sx_volume_create_from_xml( struct sx_volume ** p_p_o,mxml_node_t *node);

void sx_volume_release(struct sx_volume* v);

void sx_volume_print(
		struct sx_volume* p_object,
		FILE* stream
);

int sx_volume_connect_spectra(struct sx_volume* v,struct sx_scene* scene);

int sx_volume_connect_mv(struct sx_volume* vol,struct sx_scene* scene);

int sx_volume_create_world(struct sx_volume** pw);

struct sampled_data* sx_volume_get_spectrum_of_attribute(
		struct sx_volume * v,
		const char* name,
		const int verbose
		);

float sx_volume_k_ext(struct sx_volume * v,float lambda);

int sx_volume_sort_next_distance(
		float* d,
		int *isAbsorbed,
		struct sx_mat_vol** mv_sorted,
		struct sx_volume * v,
		float lambda,
		struct ssp_rng* rng
		);

int sx_volume_sort_next_distance2(
		float* d,
		int *isAbsorbed,
		struct sx_mat_vol** mv_sorted,
		struct sx_volume * v,
		int lambda,
		struct ssp_rng* rng
		);

#endif /* SX_VOLUME_H_ */
