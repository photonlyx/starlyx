/*
   Copyright (C) 2021 Laurent Brunel
   This file is part of starlyx.
   starlyx is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   starlyx is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sx_xml.h"
#include "sx_scene.h"


/**
 * read the xml description of the scene
 */
int  sx_xml_read_xml(
  mxml_node_t **tree,
  char* filename)
{
 FILE *file;
 file = fopen(filename, "r");
 if(!file)
 {
  printf("Can't find the file %s\n",filename);
  return 0;
 }
 *tree = mxmlLoadFile(NULL,file,MXML_OPAQUE_CALLBACK);

 return 1;
}


const char* read_string_attribute( 
  mxml_node_t* node,
  const char* attribute_name,
  const char** p_p_attribute)
{
 (*p_p_attribute)=mxmlElementGetAttr(node,attribute_name);
 if ((*p_p_attribute)==NULL)
 {
  printf("xml node %s has no attribute of name %s\n",
    node->value.element.name,attribute_name);
  return NULL;
 }
 //printf("Read attribute %s in xml node %s: value:%s\n",attribute_name,node->value.element.name,*p_p_attribute);
 return (*p_p_attribute);
}

/**
 * read an attribute value in the xml node
 * node: pointer to the xml node
 * attribute_name: name of the attribute to look for
 * attribute: pointer to char where to put the attribute value
 */
int read_string_attribute2( 
  mxml_node_t* node,
  const char* attribute_name,
  char* attribute)
{
 return read_string_attribute22(node,attribute_name,attribute,1);
}


/**
 * read an attribute value in the xml node
 * node: pointer to the xml node
 * attribute_name: name of the attribute to look for
 * attribute: pointer to char where to put the attribute value
 * verbose: if 1 print message if not found
 * return 1 if ok, 0 if no attribute of that name found, -1 if attribute is empty
 */
int read_string_attribute22
( 
 mxml_node_t* node,
 const char* attribute_name,
 char* attribute,int verbose
 )
{
	return read_string_attribute23(node,attribute_name,attribute,verbose,1,-1);
}

/**
 * read an attribute value in the xml node
 * node: pointer to the xml node
 * attribute_name: name of the attribute to look for
 * attribute: pointer to char where to put the attribute value
 * verbose: if 1 print message if not found
 * check_empty: if 1 check if the field is empty
 * max _length: if >0 check if the field is longer than this, if <0 don't check
 * return 1 if ok, 0 if no attribute of that name found, -1 if attribute is empty
 */
int read_string_attribute23
(
 mxml_node_t* node,
 const char* attribute_name,
 char* attribute,
 int verbose,
 int check_empty, 
 int max_length
 )
{
	const char* s=mxmlElementGetAttr(node,attribute_name);
	//printf("node %s  attr name:%s  attr. val:%s (verbose=%d)\n",node->value.element.name,attribute_name,s,verbose);
	if (s==NULL)
	{
		if (verbose)
		{
			//eventually get the value of the attribute "NAME":
			//			int res=read_string_attribute2(node,"NAME",name);
			//			if (!res) printf("xml node %s has no attribute of name %s\n",node->value.element.name,attribute_name);
			//			else printf("xml node %s has no attribute of name %s\n",name,attribute_name);
			printf("xml node %s has no attribute of name %s\n",
					node->value.element.name,attribute_name);
		}
		return 0;
	}
	if (max_length>0) 
		if (strlen(s)>max_length) 
		{
			printf("xml node %s: the value of the attribute %s is too long (max length is %d)\n",
					node->value.element.name,attribute_name,max_length);
			printf("Value= %s\n",s);
			return 0; 
		}
	if (check_empty) if (strlen(s)==0)
	{
		if (verbose)
		{
			/*
			   char name[MAX_NAME_LENGTH];
			   int res=read_string_attribute2(node,"NAME",name);
			   if (!res) printf("xml node %s, attribute %s is empty\n",name,attribute_name);
			   else printf("xml node %s, attribute %s is empty\n",node->value.element.name,attribute_name);
			 */
			printf("xml node %s, attribute %s is empty\n",
					node->value.element.name,attribute_name);
		}
		return 0;
	}
	strcpy(attribute,s);
	//printf("Read attribute %s in xml node %s: value:%s\n",attribute_name,node->value.element.name,*p_p_attribute);
	return 1;
}


/**
 * read an attribute value in the xml node
 * node: pointer to the xml node
 * attribute_name: name of the attribute to look for
 * node_name: pointer to name of the node (used in case of error message)
 * attribute: pointer to char where to put the attribute value
 */
int read_string_attribute21( 
  mxml_node_t* node,
  const char* attribute_name,
  char* node_name,
  char attribute[MAX_NAME_LENGTH])
{
 const char* s=mxmlElementGetAttr(node,attribute_name);
 if (s==NULL)
 {
  printf("xml node %s %s has no attribute of name %s\n",
    node->value.element.name,node_name,attribute_name);
  return 0;
 }
 strcpy(attribute,s);
 //printf("Read attribute %s in xml node %s: value:%s\n",attribute_name,node->value.element.name,*p_p_attribute);
 return 1;
}

/**
 * read an attribute value in the xml node
 * node: pointer to the xml node
 * attribute_name: name of the attribute to look for
 * attribute: pointer to pointer to char where to put the attribute value
 */
int read_string_attribute3(
  mxml_node_t* node,
  char* attribute_name,
  const char** attribute)
{
 const char* s=mxmlElementGetAttr(node,attribute_name);
 if (s==NULL)
 {
  printf("xml node %s has no attribute of name %s\n",
    node->value.element.name,attribute_name);
  return 0;
 }
 (*attribute)=s;
 //printf("Read attribute %s in xml node %s: value:%s\n",attribute_name,node->value.element.name,*p_p_attribute);
 return 1;
}
