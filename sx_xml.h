/*
Copyright (C) 2021 Laurent Brunel
This file is part of starlyx.
starlyx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
starlyx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with starlyx.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SX_XML_H_
#define SX_XML_H_

#include<stddef.h>
#include <mxml.h>

#define MAX_NAME_LENGTH 200


int  sx_xml_read_xml
(  
 mxml_node_t **tree,
 char* filename
 );

int read_string_attribute2
(  
 mxml_node_t* node,
 const char* attribute_name,
 char* attribute
 );

int read_string_attribute22
( 
 mxml_node_t* node,
 const char* attribute_name,
 char* attribute,
 int verbose
 );

int read_string_attribute23
( 
 mxml_node_t* node,
 const char* attribute_name,
 char* attribute,
 int verbose,
 int check_empty,
 int max_length
 );

int read_string_attribute21
(  
 mxml_node_t* node,
 const char* attribute_name,
 char* node_name,
 char attribute[MAX_NAME_LENGTH]);


int read_string_attribute3
( 
 mxml_node_t* node,
 char* attribute_name,
 const char** attribute
 );




#endif /* SX_XML_H_ */
